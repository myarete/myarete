<?php
use kartik\widgets\AlertBlock;
use yii\helpers\Html;
use admin\assets\AppAsset;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;

AppAsset::register($this);
?>


<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title><?= $this->title ?></title>

        <?php $this->head() ?>
    </head>

    <body class="page-<?=Yii::$app->controller->module->id.'-'.Yii::$app->controller->id.'-'.Yii::$app->controller->action->id?>">

    <div id="wrapper">
        <?php $this->beginBody() ?>

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element" style="padding-top: 48px;">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold"><?=Yii::$app->user->model->email?></strong>
                                </span>
                                <span class="text-muted text-xs block"><?= Yii::$app->user->model->role ?> <b class="caret"></b></span>
                            </span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><?= Html::a('Выйти', ['/user/default/logout']) ?></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            IN+
                        </div>
                    </li>
                    <?= \admin\widgets\menu\Menu::widget()?>
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                        <form role="search" class="navbar-form-custom" method="post" action="#">
                            <div class="form-group">
                                <!--<input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">-->
                            </div>
                        </form>
                    </div>
                    <?= Menu::widget([
                        'options' => ['class' => 'nav navbar-top-links navbar-right'],
                        'encodeLabels' => false,
                        'items' => [
                            [
                                'url' => Yii::$app->params['scheme'].'://'.Yii::$app->params['host'],
                                'label' => '<i class="fa fa-globe"></i> Сайт',
                            ],
                            [
                                'url' => Yii::$app->params['scheme'].'://'.Yii::$app->params['host'].'/logout',
                                'label' => '<i class="fa fa-sign-out"></i> Выйти',
                            ],
                        ],
                    ]) ?>
                </nav>
            </div>

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?= $this->title ?></h2>
                    <?php if (isset($this->params['breadcrumbs']) && count($this->params['breadcrumbs'])): ?>
                        <?= Breadcrumbs::widget([
                            'tag' => 'ol',
                            'homeLink' => [
                                'label' => 'Home',
                                'url' => ['/main/admin/index'],
                            ],
                            'options' => ['class' => 'breadcrumb'],
                            'activeItemTemplate' => '<li class="active"><strong>{link}</strong></li>',
                            'links' => $this->params['breadcrumbs'],
                        ]) ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="wrapper wrapper-content">
                <?= $content ?>
            </div>

        </div>
    </div>

    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>
