<?php
use yii\helpers\Html;
?>

<div id="view-formula" class="form-group"></div>
<div class="form-group">
    <?= Html::label('Формула', ['for' => 'formula-text', 'class' => 'form-label']) ?>
    <?= Html::textarea('tex', '', ['rows' => 1, 'id' => 'formula-text', 'class' => 'form-control']) ?>
</div>
<?= Html::button('Просмотр', ['id' => 'submit-formula-text', 'class' => 'btn btn-sm btn-primary']) ?>&nbsp;
<?= Html::button('Сохранить', ['id' => 'save-formula', 'class' => 'btn btn-sm btn-primary']) ?>&nbsp;
<?= Html::button('Очистить', ['id' => 'clean-formula', 'class' => 'btn btn-sm btn-primary']) ?>

<script>
    $(function() {
        $('#submit-formula-text').click(function() {
            $.post('/html/default/php-tex', {tex: $('[name=tex]').val()}, function(response) {
                $('#view-formula').html('');
                $('#view-formula').append(response);
            });
        });
        $('#clean-formula').click(function() {
            $('#view-formula').html('');
            $('[name=tex]').val('');
        });
        $('#save-formula').click(function() {
            if($('#view-formula img').is('img')) {
                $.post('/html/default/save-formula', {src: $('#view-formula img').attr('src')}, function(response) {
                    if(response == 1)
                        alert('Формула сохранена');
                });
            }
        });
    });
</script>
