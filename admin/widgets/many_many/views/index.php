<?php foreach ($allData as $id => $label): ?>
    <?= \yii\helpers\Html::a(
        $label,
        str_replace('{id}', $id, $route),
        [
            'class' => 'label label-' . (isset($existsData[$id]) ? 'success' : 'default-light'),
        ]
    ) ?>
<?php endforeach; ?>