<?php

namespace admin\widgets\many_many;

use yii\base\Widget;

class ManyManyWidget extends Widget
{
    /**
     * @var array
     */
    public $allData;

    /**
     * @var array
     */
    public $existsData;

    /**
     * @var string
     */
    public $route;


    /**
     * @return string
     */
    public function run()
    {
        return $this->render('index', [
            'allData' => $this->allData,
            'existsData' => $this->existsData,
            'route' => $this->route,
        ]);
    }
}