<?php

namespace admin\widgets\menu;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Menu extends \yii\widgets\Menu
{
    /**
     * @return string
     */
    public function init()
    {
        $this->options['tag'] = false;
        $this->encodeLabels = false;
        $this->activateParents = true;
        $this->items = $this->getItems();
        $this->submenuTemplate = '<ul class="nav nav-second-level">{items}</ul>';

        parent::init();
    }

    /**
     * @return array
     */
    private function getItems()
    {
        $items = [
            [
                'label' => '<i class="fa fa-fw fa-bar-chart-o"></i><span class="nav-label">Общая информация</span>',
                'url' => ['/main/default/index'],
            ],
            [
                'label' => '<i class="fa fa-fw fa-envelope-o"></i><span class="nav-label">Сообщения</span>',
                'url' => ['/messages/default/index'],
                'visible' => Yii::$app->user->can('admin'),
            ],
            [
                'label' => '<i class="fa fa-fw fa-eye"></i><span class="nav-label">Визуальное оформление</span><span class="fa arrow"></span>',
                'url' => '',
                'visible' => Yii::$app->user->can('admin'),
                'items' => [
                    [
                        'label' => '<i class="fa fa-fw fa-cubes"></i><span class="nav-label">Предметы на главной</span>',
                        'url' => ['/subjectonhome/default/index'],
                        'visible' => Yii::$app->user->can('admin'),
                        'activeRoutes' => [
                            'subjectonhome/default/create',
                            'subjectonhome/default/update',
                            'subjectonhome/default/view',
                        ],
                    ],
                    [
                        'label' => '<i class="fa fa-fw fa-calendar"></i><span class="nav-label">Расписание предметов</span>',
                        'url' => ['/scheduleofsubjects/default/index'],
                        'visible' => Yii::$app->user->can('admin')
                    ],
                    [
                        'label' => '<i class="fa fa-fw fa-dollar"></i><span class="nav-label">Цены на главной</span>',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('admin'),
                        'activeRoutes' => [
                            '/pricesonhome/prices-on-home/create',
                            '/pricesonhome/prices-on-home/update',
                            '/pricesonhome/prices-on-home/view',
                        ],
                        'items' => [
                            [
                                'label' => 'Цены и категории',
                                'url' => ['/pricesonhome/prices-vs-category/index'],
                                'activeRoutes' => [
                                    '/pricesonhome/prices-vs-category/create',
                                    '/pricesonhome/prices-vs-category/update',
                                    '/pricesonhome/prices-vs-category/view',
                                ],
                            ],
                            [
                                'label' => 'Цены на главной',
                                'url' => ['/pricesonhome/prices-on-home/index'],
                                'activeRoutes' => [
                                    '/pricesonhome/prices-on-home/create',
                                    '/pricesonhome/prices-on-home/update',
                                    '/pricesonhome/prices-on-home/view',
                                ],
                            ],
                            [
                                'label' => 'Ценовые категории',
                                'url' => ['/pricesonhome/prices-category/index'],
                                'activeRoutes' => [
                                    '/pricesonhome/prices-category/create',
                                    '/pricesonhome/prices-category/update',
                                    '/pricesonhome/prices-category/view',
                                ],
                            ],
                            [
                                'label' => 'Предметы в ценах',
                                'url' => ['/pricesonhome/prices-vs-subjects/index'],
                                'activeRoutes' => [
                                    '/pricesonhome/prices-vs-subjects/create',
                                    '/pricesonhome/prices-vs-subjects/update',
                                    '/pricesonhome/prices-vs-subjects/view',
                                ],
                            ],
                        ],
                    ],
                    [
                        'label' => '<i class="fa fa-fw fa-question"></i><span class="nav-label">Вопросы и ответы</span>',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('admin'),
                        'activeRoutes' => [
                            '/questionsandanswers/default/index',
                            '/questionsandanswers/default/create',
                            '/questionsandanswers/default/update',
                            '/questionsandanswers/default/view',
                            '/pages/pages/index',
                            '/pages/pages/create',
                            '/pages/pages/update',
                            '/pages/pages/view',
                        ],
                        'items' => [
                            [
                                'label' => 'Список вопросов и ответов',
                                'url' => ['/questionsandanswers/default/index'],
                                'activeRoutes' => [
                                    '/questionsandanswers/default/index',
                                    '/questionsandanswers/default/create',
                                    '/questionsandanswers/default/update',
                                    '/questionsandanswers/default/view',
                                    '/pages/pages/index',
                                    '/pages/pages/create',
                                    '/pages/pages/update',
                                    '/pages/pages/view',
                                ],
                            ],
                            [
                                'label' => 'Создать вопрос и ответ',
                                'url' => ['/questionsandanswers/default/create'],
                                'activeRoutes' => [
                                    '/questionsandanswers/default/index',
                                    '/questionsandanswers/default/create',
                                    '/questionsandanswers/default/update',
                                    '/questionsandanswers/default/view',
                                    '/pages/pages/index',
                                    '/pages/pages/create',
                                    '/pages/pages/update',
                                    '/pages/pages/view',
                                ],
                            ],
                            [
                                'label' => 'Список страниц',
                                'url' => ['/pages/pages/index'],
                                'activeRoutes' => [
                                    '/questionsandanswers/default/index',
                                    '/questionsandanswers/default/create',
                                    '/questionsandanswers/default/update',
                                    '/questionsandanswers/default/view',
                                    '/pages/pages/index',
                                    '/pages/pages/create',
                                    '/pages/pages/update',
                                    '/pages/pages/view',
                                ],
                            ],
                            [
                                'label' => 'Создать страницу',
                                'url' => ['/pages/pages/create'],
                                'activeRoutes' => [
                                    '/questionsandanswers/default/index',
                                    '/questionsandanswers/default/create',
                                    '/questionsandanswers/default/update',
                                    '/questionsandanswers/default/view',
                                    '/pages/pages/index',
                                    '/pages/pages/create',
                                    '/pages/pages/update',
                                    '/pages/pages/view',
                                ],
                            ],
                        ],
                    ],
                    [
                        'label' => '<i class="fa fa-fw fa-question"></i><span class="nav-label">Частые вопросы</span>',
                        'url' => ['/faq/default/index'],
                        'visible' => Yii::$app->user->can('admin'),
                        'activeRoutes' => [
                            'faq/default/create',
                            'faq/default/update',
                            'faq/default/view',
                        ],
                    ],
                    [
                        'label' => '<i class="fa fa-fw fa-users"></i><span class="nav-label">Учителя</span>',
                        'url' => ['/teacher/default/index'],
                        'visible' => Yii::$app->user->can('admin'),
                        'activeRoutes' => [
                            'teacher/default/create',
                            'teacher/default/update',
                            'teacher/default/view',
                        ],
                    ],
                    [
                        'label' => '<i class="fa fa-fw fa-comment"></i><span class="nav-label">Отзывы учеников</span>',
                        'url' => ['/feedback/default/index'],
                        'visible' => Yii::$app->user->can('admin'),
                        'activeRoutes' => [
                            'feedback/default/create',
                            'feedback/default/update',
                            'feedback/default/view',
                        ],
                    ],
                    /*[
                        'label' => '<i class="fa fa-fw fa-save"></i><span class="nav-label">Запись на курсы</span>',
                        'url'   => ['/course/default/index'],
                        'visible' => Yii::$app->user->can('admin'),
                        'activeRoutes' => [
                            'course/default/index',
                            'course/default/view',
                        ],
                    ],*/
                    [
                        'label' => '<i class="fa fa-fw fa-phone"></i><span class="nav-label">Заказать звонок</span>',
                        'url' => ['/call_request/default/index'],
                        'visible' => Yii::$app->user->can('admin'),
                    ],
                    [
                        'label' => '<i class="fa fa-fw fa-map-marker"></i><span class="nav-label">Филиалы</span>',
                        'url' => ['/affiliate/default/index'],
                        'visible' => Yii::$app->user->can('admin'),
                        'activeRoutes' => [
                            'affiliate/default/create',
                            'affiliate/default/update',
                            'affiliate/default/view',
                        ],
                    ],
                    /*[
                        'label' => '<i class="fa fa-fw fa-image"></i><span class="nav-label">Галерея</span>',
                        'url' => ['/gallery/default/index'],
                        'visible' => Yii::$app->user->can('admin'),
                    ],*/
                    [
                        'label' => '<i class="fa fa-fw fa-image"></i><span class="nav-label">Файловый менеджер</span>',
                        'url' => Yii::$app->params["scheme"] . '://' . Yii::$app->params["subdomain.filemanager"] . '.' . Yii::$app->params["host"],
                        'visible' => Yii::$app->user->can('admin'),
                    ],
                    [
                        'label' => '<i class="fa fa-fw fa-newspaper-o"></i><span class="nav-label">Новости</span>',
                        'url' => ['/news/default/index'],
                        'visible' => Yii::$app->user->can('admin'),
                        'activeRoutes' => [
                            'news/default/create',
                            'news/default/update',
                            'news/default/view',
                        ],
                    ],
                    [
                        'label' => '<i class="fa fa-fw fa-code"></i><span class="nav-label">HTML</span>',
                        'url' => ['/html/default/index'],
                        'visible' => Yii::$app->user->can('admin'),
                        'activeRoutes' => [
                            'html/default/create',
                            'html/default/update',
                            'html/default/view',
                        ],
                    ],
                    [
                        'label' => '<i class="fa fa-fw fa-code"></i><span class="nav-label">Помощь</span>',
                        'url' => ['/help/default/index'],
                        'visible' => Yii::$app->user->can('admin'),
                        'activeRoutes' => [
                            'help/default/create',
                            'help/default/update',
                            'help/default/view',
                        ],
                    ],
                    [
                        'label' => '<i class="fa fa-fw fa-code"></i><span class="nav-label">CSS</span>',
                        'url' => ['/css/default/index'],
                        'visible' => Yii::$app->user->can('admin'),
                        'activeRoutes' => [
                            'css/default/index',
                            'css/default/view',
                        ],
                    ],
                    [
                        'label' => '<i class="fa fa-fw fa-newspaper-o"></i><span class="nav-label">Лэндинг</span>',
                        'url' => ['/landing/default/index'],
                        'visible' => Yii::$app->user->can('admin'),
                        'activeRoutes' => [
                            'landing/default/create',
                            'landing/default/update',
                            'landing/default/view',
                        ],
                    ],
                ],
            ],
            [
                'label' => '<i class="fa fa-fw fa-info-circle"></i><span class="nav-label">Информация</span><span class="fa arrow"></span>',
                'url' => '#',
                'visible' => Yii::$app->user->can('admin'),
                'items' => [
                    [
                        'label' => 'Количество задач',
                        'url' => ['/information/tasks/index'],
                    ],
                    [
                        'label' => 'Реализация бонусов',
                        'url' => ['/bonus/info/index'],
                    ],
                ],
            ],
            [
                'label' => '<i class="fa fa-fw fa-list"></i><span class="nav-label">Ежедневные задания</span><span class="fa arrow"></span>',
                'url' => '#',
                'visible' => Yii::$app->user->can('admin'),
                'items' => [
                    [
                        'label' => 'Список заданий',
                        'url' => ['/dailytasks/daily-tasks/index'],
                        'activeRoutes' => [
                            'dailytasks/daily-tasks/view',
                            'dailytasks/daily-tasks/create',
                            'dailytasks/daily-tasks-conditions/view',
                            'dailytasks/daily-tasks-conditions/create',
                        ],
                    ],
                    [
                        'label' => 'Создать задание',
                        'url' => ['/dailytasks/daily-tasks/create'],
                    ],
                    [
                        'label' => 'Список условий',
                        'url' => ['/dailytasks/daily-tasks-conditions/index'],
                    ],
                    [
                        'label' => 'Создать условие',
                        'url' => ['/dailytasks/daily-tasks-conditions/create'],
                    ],
                ],
            ],
            [
                'label' => '<i class="fa fa-fw fa-list"></i><span class="nav-label">Задачи</span><span class="fa arrow"></span>',
                'url' => '#',
                'visible' => Yii::$app->user->can('admin'),
                'items' => [
                    [
                        'label' => '<i class="fa fa-fw fa-cubes"></i><span class="nav-label">Предметы</span>',
                        'url' => ['/subject/default/index'],
                        'visible' => Yii::$app->user->can('admin'),
                        'activeRoutes' => [
                            'subject/default/create',
                            'subject/default/update',
                            'subject/default/view',
                        ],
                    ],
                    [
                        'label' => 'Список задач',
                        'url' => ['/exercise/default/index'],
                        'activeRoutes' => [
                            'exercise/default/view',
                            'exercise/default/create',
                        ],
                    ],
                    [
                        'label' => 'Типы',
                        'url' => ['/exercise/exercise-type/index'],
                    ],
                ],
            ],
            [
                'label' => '<i class="fa fa-fw fa-list"></i><span class="nav-label">Тесты</span><span class="fa arrow"></span>',
                'url' => '#',
                'visible' => Yii::$app->user->can('admin'),
                'items' => [
                    [
                        'label' => 'Список тестов',
                        'url' => ['/testing/default/index'],
                        'activeRoutes' => [
                            'testing/default/view',
                            'testing/default/create',
                        ],
                    ],
                    [
                        'label' => 'Типы',
                        'url' => ['/testing/test-type/index'],
                    ],
                ],
            ],
            [
                'label' => '<i class="fa fa-fw fa-list"></i><span class="nav-label">Магазин тренировок</span><span class="fa arrow"></span>',
                'url' => '#',
                'visible' => Yii::$app->user->can('teacher'),
                'items' => [
                    [
                        'label' => 'Список тренировок',
                        'url' => ['/training/default/index'],
                        'activeRoutes' => [
                            'training/default/view',
                            'training/default/create',
                        ],
                    ],
                    [
                        'label' => 'Категории',
                        'url' => ['/training/category/index'],
                        'visible' => Yii::$app->user->can('admin'),
                    ],
                ],
            ],
            [
                'label' => '<i class="fa fa-fw fa-list"></i><span class="nav-label">Задачи</span>',
                'url' => ['/exercise/default/index'],
                'visible' => Yii::$app->user->can('teacher'),
                'activeRoutes' => [
                    'exercise/default/view',
                    'exercise/default/create',
                ],
            ],
            [
                'label' => '<i class="fa fa-fw fa-newspaper-o"></i><span class="nav-label">Уроки</span><span class="fa arrow"></span>',
                'url' => '#',
                'visible' => Yii::$app->user->can('teacher'),
                'items' => [
                    [
                        'label' => 'Список уроков',
                        'url' => ['/article/default/index'],
                        'activeRoutes' => [
                            'article/default/view',
                            'article/default/create',
                        ],
                    ],
                    [
                        'label' => 'Категории',
                        'url' => ['/article/category/index'],
                        'visible' => Yii::$app->user->can('admin'),
                    ],
                    [
                        'label' => 'Слайдеры',
                        'url' => ['/article/slider/index'],
                        'activeRoutes' => [
                            'article/slider/view',
                            'article/slider/create',
                            'article/slider/update',
                            'article/slider/index',
                        ],
                        'visible' => Yii::$app->user->can('teacher'),
                    ],
                    [
                        'label' => 'Комментарии',
                        'url' => ['/article/default/comments'],
                        'visible' => Yii::$app->user->can('admin'),
                    ]
                ],
            ],
            [
                'label' => '<i class="fa fa-fw fa-newspaper-o"></i><span class="nav-label">Журнал</span><span class="fa arrow"></span>',
                'url' => '#',
                'visible' => Yii::$app->user->can('admin'),
                'items' => [
                    [
                        'label' => 'Список постов',
                        'url' => ['/blog/post/index'],
                        'activeRoutes' => [
                            'article/default/view',
                            'article/default/create',
                        ],
                    ],
                    [
                        'label' => 'Список категорий',
                        'url' => ['/blog/category/index']
                    ],
                    [
                        'label' => 'Список тэгов',
                        'url' => ['/blog/tags/index']
                    ],
                    [
                        'label' => 'Комментарии',
                        'url' => ['/blog/comment/index'],
                    ]
                ],
            ],
            [
                'label' => '<i class="fa fa-fw fa-list"></i><span class="nav-label">Бонусы</span><span class="fa arrow"></span>',
                'url' => '#',
                'visible' => Yii::$app->user->can('admin'),
                'items' => [
                    [
                        'label' => 'Бонусы',
                        'url' => ['/bonus/default/index'],
                    ],
                    [
                        'label' => 'Опросы',
                        'url' => ['/bonus/poll/index']
                    ],
                ]
            ],
            [
                'label' => '<i class="fa fa-fw fa-users"></i><span class="nav-label">Пользователи</span><span class="fa arrow"></span>',
                'url' => '#',
                'visible' => Yii::$app->user->can('admin'),
                'items' => [
                    [
                        'label' => 'Пользователи',
                        'url' => ['/user/default/index']
                    ],
                    [
                        'label' => '<span class="nav-label">Списки пользователей</span><span class="fa arrow"></span>',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => '&nbsp;&nbsp;&nbsp;&nbsp;Согласные на рассылку',
                                'url' => ['/user/default/news-letters']
                            ],
                        ]
                    ]
                ]
            ],
            [
                'label' => '<i class="fa fa-fw fa-newspaper-o"></i><span class="nav-label">Премиумы</span><span class="fa arrow"></span>',
                'url' => '#',
                'visible' => Yii::$app->user->can('admin'),
                'items' => [
                    [
                        'label' => 'Премиум Учитель',
                        'url' => '/premium/default/index',
                    ]
                ]
            ],
            [
                'label' => '<i class="fa fa-fw fa-credit-card"></i><span class="nav-label">Промокоды</span><span class="fa arrow"></span>',
                'url' => ['/promocodes/default/promocodes'],
                'visible' => Yii::$app->user->can('admin'),
            ],
        ];

        return $items;
    }

    /**
     *
     */
    public function run()
    {
        if ($this->route === null && Yii::$app->controller !== null)
            $this->route = Yii::$app->controller->getRoute();

        if ($this->params === null)
            $this->params = Yii::$app->request->getQueryParams();

        $items = $this->normalizeItems($this->items, $hasActiveChild);

        if (!empty($items)) {
            $options = $this->options;
            $tag = ArrayHelper::remove($options, 'tag', 'ul');

            if ($tag !== false) {
                echo Html::tag($tag, $this->renderItems($items), $options);
            } else {
                echo $this->renderItems($items);
            }
        }
    }

    /**
     * @param array $item
     * @return bool
     */
    protected function isItemActive($item)
    {
        if (parent::isItemActive($item))
            return true;

        if (isset($item['activeRoutes'])) {
            foreach ($item['activeRoutes'] as $route) {
                if ($this->route == $route)
                    return true;
            }
        }

        return false;
    }
}
