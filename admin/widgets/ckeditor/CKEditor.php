<?php

namespace admin\widgets\ckeditor;

use Yii;

class CKEditor extends \dosamigos\ckeditor\CKEditor
{
    /**
     * @var bool
     */
    public $enableFinder = false;

    /**
     * @var bool
     */
    private static $_registered = false;

    /**
     *
     */
    protected function registerPlugin()
    {
        if ($this->enableFinder && !self::$_registered)
        {
            Yii::$app->view->registerJsFile('/ckfinder/ckfinder.js');
            Yii::$app->view->registerJs('$(function(){CKFinder.setupCKEditor()});');
        }

        parent::registerPlugin();
    }

}