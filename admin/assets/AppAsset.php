<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace admin\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];

    public $css = [
        'css/fa/font-awesome.min.css',
        'css/animate.css',
        'css/style.css',
        'css/custom.css?v=1',
        'css/plugins/sweetalert/sweetalert2.min.css',
    ];
    public $js = [
        //'js/bootstrap.min.js',
        'js/plugins/metisMenu/jquery.metisMenu.js',
        'js/plugins/slimscroll/jquery.slimscroll.min.js',
        'js/plugins/pace/pace.min.js',
        'js/plugins/chartJs/Chart.min.js',
        'js/main.js',
        'js/inspinia.js',
        'js/angular/angular.min.js',
        'js/angular/angular-sanitize.min.js',
        'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
        'js/plugins/sweetalert/sweetalert2.min.js',
        'js/yii.overrides.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\jui\JuiAsset'
    ];
}
