<?php
/**
 * User: Evgeniy Karpeev [karpeevea@gmail.com]
 * Date: 03.03.2016
 * Time: 20:08
 */

namespace admin\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FileManagerAsset extends AssetBundle
{
    public $sourcePath = '@vendor';
    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];

    public $css = [
        'mrjohnnova/responsivefilemanager/filemanager/css/filemanager.css',
        'mrjohnnova/responsivefilemanager/filemanager/js/jPlayer/skin/blue.monday/jplayer.blue.monday.css'
    ];
    public $js = [
        'mrjohnnova/responsivefilemanager/filemanager/js/plugins.js',
        'mrjohnnova/responsivefilemanager/filemanager/js/jPlayer/jquery.jplayer/jquery.jplayer.js',
        'mrjohnnova/responsivefilemanager/filemanager/js/modernizr.custom.js',
        'mrjohnnova/responsivefilemanager/filemanager/js/aviaryt.editor.v2.min.js'
    ];
    public $depends = [
        'admin\assets\AppAsset'
    ];
}
