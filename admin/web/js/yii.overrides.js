yii.confirm = function (message, okCallback, cancelCallback) {
    swal({
        title: message,
        type: 'warning',
        showCancelButton: true,
        allowOutsideClick: true
    }).then(okCallback);
}

alert = function(message) {
    swal(message);
}
