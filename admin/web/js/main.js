$(document).ready( function() {

    /**
     * $('#variants-grid').reload()
     */
    $.fn.reload = function(){
        this.each(function(){
            var element = this;

            $.get('').success(function(data){
                var newContent = $(data).find('#'+element.id);
                $(element).html(newContent.html());
            });

            return this;
        });
    };

});


/**
 *
 */
window.populateErrors = function(form, errors)
{
    form.find('[class^="error-attribute-"]').html('');
    $.each(errors, function(attribute, value){
        form.find('.error-attribute-'+attribute).html(value);
    });
}