$(function(){

    /**
     *
     */
    var form = $('#add-variant-form'),
        modal = $('#add-variant-modal'),
        themeIdInput = $('#theme-id-input'),
        variantsGrid = $('#variants-grid'),
        answerForm = $('#add-answer-form'),
        answerModal = $('#add-answer-modal'),
        answersGrid = $('#answers-grid');


    /**
     * Add variant
     */
    form.find('.submit-button').click(function(e){
        $.post(form.attr('action'), form.serialize()).success(function(response) {
            if (response === true)
            {
                modal.modal('hide');
                variantsGrid.reload();
            }
            else
            {
                window.populateErrors(form, response);
            }
        });

        return false;
    });


    /**
     * Add answer
     */
    answerForm.find('.submit-button').click(function(e){

        $.post(answerForm.attr('action'), answerForm.serialize()).success(function(response) {
            if (response === true)
            {
                answerModal.modal('hide');
                answersGrid.reload();
            }
            else
            {
                window.populateErrors(answerForm, response);
            }
        });

        return false;
    });


    /**
     * Update variant
     */
    $(document).on('click', '.js-update-variant', function(){
        modal.modal('show');

        var attributes = $(this).data('attributes');
        themeIdInput.treeview('checkNode', attributes.theme_id);
        form.find('[name$="[id]"]').val(attributes.id);
        form.find('[name$="[class]"]').val(attributes.class);
        form.find('[name$="[complexity]"]').val(attributes.complexity);

        return false;
    });


    /**
     * Delete variant
     */
    $(document).on('click', '.js-delete-variant', function(){
        if (!confirm('Вы уверены?'))
            return false;

        $.get(this.href).success(function(response) {
            if (response === true)
                variantsGrid.reload();
        });

        return false;
    });


    /**
     * Update answer
     */
    $(document).on('click', '.js-update-answer', function(){
        answerModal.modal('show');

        var attributes = $(this).data('attributes');
        answerForm.find('[name$="[id]"]').val(attributes.id);
        answerForm.find('[name$="[is_right]"]').prop('checked', attributes.is_right);
        answerForm.find('[name$="[answer]"]').val(attributes.answer);
        CKEDITOR.instances['testanswer-comment'].setData(attributes.comment)

        return false;
    });


    /**
     * Delete answer
     */
    $(document).on('click', '.js-delete-answer', function(){
        if (!confirm('Вы уверены?'))
            return false;

        $.get(this.href).success(function(response) {
            if (response === true)
                answersGrid.reload();
        });

        return false;
    });

    /**
     * Right answer
     */
    $(document).on('click', '.js-right-answer', function(){
        $.get(this.href).success(function(response) {
            if (response === true)
                answersGrid.reload();
        });
        return false;
    });


    /**
     * Close modal variant
     */
    modal.on('hidden.bs.modal', function (e) {
        form[0].reset();
        themeIdInput.data('treeinput').setInput();
        themeIdInput.val(null);
    });

    /**
     * Close modal answer
     */
    answerModal.on('hidden.bs.modal', function (e) {
        answerForm[0].reset();
        CKEDITOR.instances['testanswer-comment'].setData('');
        answerForm.find('[name$="[is_right]"]').val(0);
    });


    /**
     * Test
     */
    //form.modal();

});
