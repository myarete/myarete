$(function(){

    /**
     *
     */
    var form = $('#add-variant-form'),
        modal = $('#add-variant-modal'),
        themeIdInput = $('#theme-id-input'),
        variantsGrid = $('#variants-grid');


    /**
     * Add variant
     */
    form.find('.submit-button').click(function(e){

        $.post(form.attr('action'), form.serialize()).success(function(response) {
            if (response === true)
            {
                modal.modal('hide');
                variantsGrid.reload();
            }
            else
            {
                window.populateErrors(form, response);
            }
        });

        return false;
    });


    /**
     * Update variant
     */
    $(document).on('click', '.js-update-variant', function(){
        modal.modal('show');

        var attributes = $(this).data('attributes');
        themeIdInput.treeview('checkNode', attributes.theme_id);
        form.find('[name$="[id]"]').val(attributes.id);
        form.find('[name$="[class]"]').val(attributes.class);
        form.find('[name$="[complexity]"]').val(attributes.complexity);

        return false;
    });


    /**
     * Delete variant
     */
    $(document).on('click', '.js-delete-variant', function(){
        if (!confirm('Вы уверены?'))
            return false;

        $.get(this.href).success(function(response) {
            if (response === true)
                variantsGrid.reload();
        });

        return false;
    });


    /**
     * Close modal
     */
    modal.on('hidden.bs.modal', function (e) {
        form[0].reset();
        themeIdInput.data('treeinput').setInput();
        themeIdInput.val(null);
    });


    /**
     * Test
     */
    //form.modal();

});