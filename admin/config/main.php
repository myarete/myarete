<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'admin',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'admin\controllers',
    'defaultRoute' => 'main/default',
    'modules' => [
        'main' => [
            'class' => 'admin\modules\main\Main',
        ],
        'subject' => [
            'class' => 'admin\modules\subject\Subject',
        ],
        'subjectonhome' => [
            'class' => 'admin\modules\subjectonhome\SubjectOnHome',
        ],
        'teacher' => [
            'class' => 'admin\modules\teacher\Teacher',
        ],
        'treemanager' => [
            'class' => '\kartik\tree\Module',
        ],
        'faq' => [
            'class' => 'admin\modules\faq\Faq',
        ],
        'feedback' => [
            'class' => 'admin\modules\feedback\Feedback',
        ],
        'course' => [
            'class' => 'admin\modules\course\Course',
        ],
        'call_request' => [
            'class' => 'admin\modules\call_request\CallRequest',
        ],
        'affiliate' => [
            'class' => 'admin\modules\affiliate\Affiliate',
        ],
        'gallery' => [
            'class' => 'admin\modules\gallery\Gallery',
        ],
        'gridview' => [
            'class' => 'kartik\grid\Module',
        ],
        'news' => [
            'class' => 'admin\modules\news\News',
        ],
        'html' => [
            'class' => 'admin\modules\html\Html',
        ],
        'help' => [
            'class' => 'admin\modules\help\Help',
        ],
        'css' => [
            'class' => 'admin\modules\css\Css',
        ],
        'exercise' => [
            'class' => 'admin\modules\exercise\Exercise',
        ],
        'testing' => [
            'class' => 'admin\modules\testing\Testing',
        ],
        'article' => [
            'class' => 'admin\modules\article\Article',
        ],
        'user' => [
            'class' => 'admin\modules\user\User',
        ],
        'landing' => [
            'class' => 'admin\modules\landing\Landing',
        ],
        'messages' => [
            'class' => 'admin\modules\messages\Messages',
        ],
        'premium' => [
            'class' => 'admin\modules\premium\Premium',
        ],
        'blog' => [
            'class' => 'admin\modules\blog\Blog',
        ],
        'training' => [
            'class' => 'admin\modules\training\Training',
        ],
        'promocodes' => [
            'class' => 'admin\modules\promocodes\Promocodes',
        ],
        'scheduleofsubjects' => [
            'class' => 'admin\modules\scheduleofsubjects\ScheduleOfSubjects',
        ],
        'pricesonhome' => [
            'class' => 'admin\modules\pricesonhome\PricesOnHome',
        ],
        'information' => [
            'class' => 'admin\modules\information\Information',
        ],
        'pages' => [
            'class' => 'admin\modules\pages\Pages',
        ],
        'questionsandanswers' => [
            'class' => 'admin\modules\questionsandanswers\QuestionsAndAnswers',
        ],
        'dailytasks' => [
            'class' => 'admin\modules\dailytasks\DailyTasks',
        ],
        'bonus' => [
            'class' => 'admin\modules\bonus\Bonus',
        ],
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => $params['cookieValidationKey'],
            'enableCsrfValidation' => false,
        ],
        'errorHandler' => [
            'errorAction' => 'main/default/error',
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => require(__DIR__ . '/routes.php'),
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity', 'httpOnly' => true, 'domain' => $params['cookieDomain']],
        ],
        'session' => [
            'name' => 'advanced',
            'cookieParams' => [
                'httpOnly' => true,
                'domain' => $params['cookieDomain'],
            ]
        ],
    ],
    'params' => $params,
];
