<?php

namespace admin\modules\user;

class User extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\user\controllers';

    public $label = 'Пользователи';

    public function init()
    {
        parent::init();
    }
}
