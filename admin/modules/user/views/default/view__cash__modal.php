<?php

use yii\helpers\Html;

?>

<div class="modal inmodal" id="add-cash-modal">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Пополнить кошелек</h4>
            </div>
            
            <?= Html::beginForm('/user/default/add-cash') ?>
            <?= Html::hiddenInput('user_id', $user_id) ?>
            <div class="modal-body">
                <?= Html::textInput('cash', '', [
                    'class' => 'form-control',
                    'placeholder' => '0.00'
                ]) ?>
            </div>
            <div class="modal-footer">
                <?= Html::submitButton('Пополнить кошелек', ['class' => 'btn btn-primary col-xs-12']) ?>
            </div>
            
            <?= Html::endForm() ?>
        
        </div>
    </div>
</div>