<?php

use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::$app->getModule('user')->label;
?>

<div class="row">
    <div class="col-lg-12">
        <?= Html::beginForm() ?>
        <div class="input-group">
            <?= Html::textInput('UserSearch[search]', '', ['class' => 'form-control']) ?>
            <span class="input-group-btn">
                <?= Html::submitButton('Найти', ['class' => 'btn btn-primary', 'style' => 'width:100px']) ?>
            </span>
        </div>
        <?= Html::endForm() ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            
            <div class="ibox-title">
                <h5><?= $this->title ?></h5>
            </div>
            
            <div class="ibox-content">
                
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'contentOptions' => ['style' => 'width: 1px;'],
                        ],
                        [
                            'format' => 'raw',
                            'value' => function (User $user) {
                                return Html::a($user->profile->getImageTag([
                                    'defaultSrc' => '/img/default_user.png',
                                    'style' => 'max-width: 100px;',
                                ]), ['view', 'id' => $user->id]);
                            },
                        ],
                        'email:email',
                        'profile.cash',
                        [
                            'attribute' => 'role',
                            'value' => function ($user) {
                                return User::$roleLabels[$user->role];
                            },
                        ],
                        'profile.state.name:text:Статус',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['class' => 'action-column'],
                            'template' => '{view} {delete}',
                        ],
                    ],
                ]); ?>
            
            </div>
        </div>
    </div>
</div>
