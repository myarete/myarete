<?php

use common\models\Exercise;
use common\models\ExerciseComplexity;
use common\models\Theme;
use kartik\tree\TreeViewInput;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\Html;

?>

<div class="modal inmodal" id="send-promocode-modal">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Отправить промокод</h4>
            </div>
            
            <?= Html::beginForm('/promocodes/default/send-promocode') ?>
            <?= Html::hiddenInput('user_id', $user_id) ?>
            <div class="modal-body">
                <?= Html::textInput('value', '', [
                    'class' => 'form-control',
                    'placeholder' => '0.00'
                ]) ?>
                <?= Html::textInput('promocode', $promocode, ['class' => 'form-control']) ?>
            </div>
            <div class="modal-footer">
                <?= Html::submitButton('Отправить промокод', ['class' => 'btn btn-primary col-xs-12']) ?>
            </div>
            
            <?= Html::endForm() ?>
        
        </div>
    </div>
</div>