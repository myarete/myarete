<?php

use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\Html;

?>


<?php
$this->title = Yii::$app->getModule('user')->label . ' | Просмотр';
?>


<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-table"></i>
        <?= $this->title ?>
    </div>
    
    <div class="ibox-content">
        
        <?= Html::beginForm('save') ?>
        <p>Баланс пользователя: <?= $model->profile->cash ?> &nbsp <a href="#" data-toggle="modal"
                                                                      data-target="#add-cash-modal">
                <i class="fa fa-plus"></i> Пополнить кошелек</a> &nbsp
            <a href="#" data-toggle="modal" data-target="#send-promocode-modal">
                <i class="fa fa-plus"></i> Отправить промокод</a>
        </p>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'profile.image',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->profile->getImageTag([
                            'defaultSrc' => '/img/default_user.png',
                            'style' => 'max-width: 100px;',
                        ]);
                    }
                ],
                'email',
                'profile.first_name',
                'profile.last_name',
                'profile.birthdate',
                'profile.state.name:text:Статус',
                [
                    'attribute' => 'role',
                    'format' => 'raw',
                    'value' => $this->render('view__role', ['model' => $model]),
                ],
                [
                    'label' => 'История посещений',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '<a href="#history" data-toggle="collapse">Показать</a>';
                    }
                ],
                [
                    'label' => 'Подтвержден',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::dropDownList('is_confirmed', $model->is_confirmed, [1 => 'да', 0 => 'нет']);
                    }
                ]
            ],
        ]); ?>
        <p>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </p>
        <?= Html::hiddenInput('id', $model->id) ?>
        
        <div id="history" class="collapse">
            <?= GridView::widget([
                'dataProvider' => $dataHistory,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'user:text:Пользователь',
                    'page:text:Страница',
                    'link:text:Ссылка',
                    [
                        'attribute' => 'dt',
                        'label' => 'Дата/время',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return date('d.m.Y H:i:s', strtotime($model->dt));
                        }
                    ],
                ],
            ]); ?>
        </div>
        <div>
            <?php if (count($promocodes)): ?>
                <p>Промокоды</p>
                <?php foreach ($promocodes as $promocode): ?>
                    <ul>
                        <?php if ($promocode->status == 0): ?>
                            <li><?= $promocode->promocode ?> - <?= $promocode->value ?> руб. - Еще не
                                активирован <?= Html::a('Удалить', ['/promocodes/default/delete-promocode', 'promocode_id' => $promocode->id]) ?></li>
                        <?php else: ?>
                            <li><?= $promocode->promocode ?> - <?= $promocode->value ?> руб. - Уже
                                активирован <?= Html::a('Удалить', ['/promocodes/default/delete-promocode', 'promocode_id' => $promocode->id]) ?></li>
                        <?php endif; ?>
                    </ul>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<?= Html::endForm() ?>

<?= $this->render('view__promocode__modal', [
    'user_id' => $model->id,
    'promocode' => $newPromocode,
]) ?>

<?= $this->render('view__cash__modal', [
    'user_id' => $model->id,
]) ?>
