<?php
use common\models\User;
use yii\helpers\Html;
?>

<?= User::$roleLabels[$model->role] ?>

<?php if (in_array($model->role, [
            User::ROLE_CLIENT,
            User::ROLE_TEACHER,
            User::ROLE_DESIGNER,
            User::ROLE_FREE,
            User::ROLE_CLIENT,
            //User::ROLE_PREMIUMTUTOR,
            //User::ROLE_LEARNER,
            //User::ROLE_TEACHERLEARNER
        ]
    )): ?>
    <div>
        <?= Html::a('Назначить админом', ['change-role', 'id' => $model->id, 'role' => User::ROLE_ADMIN]) ?>
    </div>
<?php endif; ?>

<?php if (in_array($model->role, [
            User::ROLE_ADMIN,
            User::ROLE_TEACHER,
            User::ROLE_DESIGNER,
            User::ROLE_FREE,
            User::ROLE_CLIENT,
            //User::ROLE_PREMIUMTUTOR,
            //User::ROLE_LEARNER,
            //User::ROLE_TEACHERLEARNER
        ]
    )): ?>
    <div>
        <?= Html::a('Назначить клиентом', ['change-role', 'id' => $model->id, 'role' => User::ROLE_CLIENT]) ?>
    </div>
<?php endif; ?>

<?php if (in_array($model->role, [
            User::ROLE_ADMIN,
            User::ROLE_TEACHER,
            User::ROLE_DESIGNER,
            User::ROLE_FREE,
            User::ROLE_CLIENT,
            //User::ROLE_PREMIUMTUTOR,
            //User::ROLE_CLIENT,
            //User::ROLE_TEACHERLEARNER
        ]
    )): ?>
    <div>
        <?= Html::a('Назначить бесплатным пользователем', ['change-role', 'id' => $model->id, 'role' => User::ROLE_FREE]) ?>
    </div>
<?php endif; ?>

<?php if (in_array($model->role, [
            User::ROLE_ADMIN,
            User::ROLE_TEACHER,
            User::ROLE_DESIGNER,
            User::ROLE_FREE,
            User::ROLE_CLIENT,
            //User::ROLE_PREMIUMTUTOR,
            //User::ROLE_LEARNER,
            //User::ROLE_TEACHERLEARNER
        ]
    )): ?>
    <div>
        <?= Html::a('Назначить нашим учителем', ['change-role', 'id' => $model->id, 'role' => User::ROLE_TEACHER]) ?>
    </div>
<?php endif; ?>

<?php if (in_array($model->role, [
            User::ROLE_ADMIN,
            User::ROLE_TEACHER,
            User::ROLE_DESIGNER,
            User::ROLE_FREE,
            User::ROLE_CLIENT,
            //User::ROLE_PREMIUMTUTOR,
            //User::ROLE_LEARNER,
            //User::ROLE_TEACHERLEARNER
        ]
    )): ?>
    <div>
        <?= Html::a('Назначить дизайнером', ['change-role', 'id' => $model->id, 'role' => User::ROLE_DESIGNER]) ?>
    </div>
<?php endif; ?>

