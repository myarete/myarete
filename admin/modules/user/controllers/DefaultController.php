<?php

namespace admin\modules\user\controllers;

use common\components\yii\base\BaseController;
use common\models\Profile;
use common\models\Promocodes;
use common\models\User;
use common\models\UserSearch;
use common\models\VisitsHistory;
use common\models\Payment;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class DefaultController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->post());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Offer model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $dataHistory = new ActiveDataProvider([
            'query' => VisitsHistory::find()->where(['user' => $model->email])
        ]);

        $promocode = new Promocodes();
        $promocodes = Promocodes::findAll(['user_id' => $id, 'register' => 0]);
        $newPromocode = $promocode->random_string(10);

        return $this->render('view', [
            'model' => $model,
            'dataHistory' => $dataHistory,
            'promocode' => $promocode,
            'promocodes' => $promocodes,
            'newPromocode' => $newPromocode
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionAddCash()
    {
        $userId = Yii::$app->request->post('user_id');
        $addCash = Yii::$app->request->post('cash');
        $cash = Profile::find()->where(['user_id' => $userId])->one()->cash;
        $cash += $addCash;

        try {
            $profile = Profile::find()->where(['user_id' => $userId])->one();
            $old_cash = $profile->cash;
            $profile->cash = $cash;
            $profile->update();
            $payment = new Payment();
            $payment->user_id = $userId;
            $payment->type_id = 12;
            $payment->refill_sum = $cash - $old_cash;
            $payment->is_paid = 0;
            $payment->save();
        } catch (Exception $e) {
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @param $role
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionChangeRole($id, $role)
    {
        $model = $this->findModel($id);
        $model->role = $role;
        $model->update(false, ['role']);

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionSave()
    {
        $request = Yii::$app->request;
        if ($request->post() && ($model = $this->findModel($request->post('id')))) {
            $model->is_confirmed = $request->post('is_confirmed');
            $model->update();
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionNewsLetters()
    {
        $searchModel = new UserSearch();
        $params = Yii::$app->request->post();
        $params['UserSearch']['newsletter'] = 1;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if ($model = User::findOne($id)) {
            return $model;
        } else {
            return $this->pageNotFound();
        }
    }
}
