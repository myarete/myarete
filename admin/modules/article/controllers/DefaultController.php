<?php

namespace admin\modules\article\controllers;

use common\components\yii\base\BaseController;
use common\models\Article;
use common\models\ArticleSearch;
use common\models\ArticleComment;
use common\models\ArticleCommentSearch;
use common\models\Exercise;
use common\models\Test;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class DefaultController extends BaseController
{

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['admin']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'success', 'comments', 'set-visible'],
                        'roles' => ['teacher']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSetVisible($id, $visible)
    {
        if (($model = ArticleComment::findOne($id)) !== null) {
            $model->visible = $visible;
            $model->update(false, ['visible']);

        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create_update', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!$model->allowEdit()) {
            $this->forbidden();
        }

        $model->category_list = $model->categoriesTree();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create_update', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (!$model->allowEdit()) {
            $this->forbidden();
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @param $success
     * @return bool|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionSuccess($id, $success)
    {
        $model = $this->findModel($id);
        $model->category_list = $model->categoriesTree();
        $model->is_approved = $success;
        $model->update(false, ['is_approved']);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return string
     */
    public function actionGetExercise()
    {
        $exercises = ArrayHelper::map(Exercise::find()->select(['id', 'name'])->where(['is_approved' => 1])->asArray()->all(), 'id', 'name');

        return json_encode($exercises);
    }

    /**
     * @return string
     */
    public function actionGetTest()
    {
        $tests = ArrayHelper::map(Test::find()->select(['id', 'name'])->where(['is_approved' => 1])->asArray()->all(), 'id', 'name');

        return json_encode($tests);
    }

    public function actionComments()
    {
        $searchModel = new ArticleCommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view-comments', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDeleteComment($id)
    {
        ArticleComment::findOne($id)->delete();

        return $this->redirect(['comments']);
    }
}
