<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\Pagination;

$this->title = 'Комментарии';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
    </div>

    <hr>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'title',
                'header' => 'Название',
                'value' => function($model) {
                    return $model->article->title;
                }
            ],
            [
                'attribute' => 'email',
                'header' => 'Пользователь',
                'value' => function($model) {
                    return $model->user->email;
                }
            ],
            'text',
            'create_dt',
            [
                'attribute' => 'visible',
                'format' => 'raw',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'visible',
                    [0 => 'Нет', 1 => 'Да'],
                    ['class' => 'form-control', 'prompt' => '-']
                ),
                'value' => function($model) {
                    if ($model->visible == 1)
                    {
                        $html = Html::tag('span', 'Да', ['class' => 'label label-primary']);
                        $html .= ' | '.Html::a('Нет', ['set-visible', 'id' => $model->id, 'visible' => 0]);

                        return $html;
                    }
                    else if ($model->visible == 0)
                    {
                        $html = Html::tag('span', 'Het', ['class' => 'label label-danger']);
                        $html .= ' | '.Html::a('Да', ['set-visible', 'id' => $model->id, 'visible' => 1]);

                        return $html;
                    }
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        if (Yii::$app->user->can('admin'))
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete-comment', 'id' => $model->id], ['data' => [
                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                'method' => 'post'
                            ]]);
                    },
                ],
            ],
        ],
    ]) ?>

</div>
