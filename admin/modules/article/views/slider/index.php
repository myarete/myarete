<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\ArticleSlider;

$this->title = 'Слайдеры';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox float-e-margins">

    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
        <?php if(!Yii::$app->user->can('admin')): ?>
        <div class="ibox-tools">
            <?= Html::a('', ['create'], ['class' => 'fa fa-plus', 'title' => 'Создать']) ?>
        </div>
        <?php endif ?>
    </div>

    <div class="ibox-content">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'slider-grid',
            'filterModel' => $searchModel,
            'layout' => '{items}',
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => [
                        'style' => 'width: 50px;'
                    ],
                ],
                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function(ArticleSlider $slider) {
                        return Html::a($slider->name, ['view', 'id' => $slider->id]);
                    },
                ],
                [
                    'attribute' => 'email',
                    'value' => 'user.email',
                ],
                [
                    'attribute' => 'date_create',
                    'filter' => false,
                    'format' => ['date', 'php:d.m.Y H:i:s'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'width: 1px; white-space: nowrap;'],
                    'buttons' => [
                        'update' => function ($url, ArticleSlider $model, $key) {
                            if (!$model->allowEdit())
                                return '';
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                        },
                        'delete' => function ($url, $model, $key) {
                            if (!Yii::$app->user->can('admin'))
                                return '';
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['data' => [
                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                'method' => 'post'
                            ]]);
                        },
                    ],
                ],
            ],
        ]); ?>

    </div>
</div>
