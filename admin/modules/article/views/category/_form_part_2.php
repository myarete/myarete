<p>Разрешить покупку этой категории</p>
<?= $form->field($node, 'status')
    ->radioList([1 => 'Да', 0 => 'Нет'])
    ->label(false);
?>
<?= $form->field($node, 'hide_fr_clients')->checkbox()?>
<?= $form->field($node, 'price')->textInput() ?>
<?= $form->field($node, 'description_short')->textarea() ?>
<?php if(!$node->isNewRecord && !empty($node->image)):?>
<div class="form-group">
    <label class="control-label">Текущее изображение</label>
    <img width="200" src="<?= \Yii::getAlias('@web/upload/article/') . $node->image ?>" />
</div>
<?php endif; ?>
<?= $form->field($node, 'imageFile')->fileInput() ?>
<?= $form->field($node, 'removable_all')->checkbox() ?>
