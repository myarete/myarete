<?php
use yii\grid\GridView;
use yii\helpers\Html;
?>


<?php
$this->title = 'HTML';
?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5><?= $this->title ?></h5>
                <div class="ibox-tools">
                    <?= Html::a('<i class="fa fa-plus"></i> Создать', ['create']) ?>
                </div>
            </div>

            <div class="ibox-content">

                <?= GridView::widget([
                    'layout' => '{items}',
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'title',
                            'contentOptions' => [
                                'style' => 'white-space:nowrap;'
                            ],
                        ],

                        [
                            'attribute' => 'code',
                        ],

                        [
                            'attribute' => 'content',
                            'format' => 'raw',
                            'value' => function($data) {
                                $value = strip_tags($data->content);
                                $value = mb_substr($value, 0, 200, Yii::$app->charset);
                                return trim($value).'...';
                            },
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['class' => 'action-column'],
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>
