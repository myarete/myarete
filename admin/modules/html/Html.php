<?php

namespace admin\modules\html;

class Html extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\html\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
