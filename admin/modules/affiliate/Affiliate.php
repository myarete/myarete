<?php

namespace admin\modules\affiliate;

class Affiliate extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\affiliate\controllers';

    public $label = 'Филиалы';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
