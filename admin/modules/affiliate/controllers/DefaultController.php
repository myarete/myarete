<?php

namespace admin\modules\affiliate\controllers;

use common\models\AffiliateVsTeacher;
use common\components\yii\base\BaseController;
use Yii;
use common\models\Affiliate;
use common\models\AffiliateSearch;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for Affiliate model.
 */
class DefaultController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                ],
            ],
        ];
    }


    /**
     * Lists all Affiliate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AffiliateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Affiliate model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Affiliate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Affiliate();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create_update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Affiliate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create_update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Affiliate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $teacher_id
     * @param $affiliate_id
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionAddRemoveTeacher($teacher_id, $affiliate_id)
    {
        $attributes = ['teacher_id' => $teacher_id, 'affiliate_id' => $affiliate_id];
        $existModel = AffiliateVsTeacher::find()->where($attributes)->one();

        if ($existModel === null)
        {
            $model = new AffiliateVsTeacher;
            $model->attributes = $attributes;
            $model->save(false);
        }
        else
        {
            $existModel->delete();
        }

        $redirectUrl = Yii::$app->request->referrer;
        if ($redirectUrl === null)
            $redirectUrl = ['view', 'id' => $affiliate_id];
        return $this->redirect($redirectUrl);
    }

    /**
     * Finds the Affiliate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Affiliate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Affiliate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
