<?php
use yii\grid\GridView;
use yii\helpers\Html;

/**
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
?>


<?php
$this->title = 'Лэндинг';
?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5><?= $this->title ?></h5>
                <div class="ibox-tools">
                    <?= Html::a('<i class="fa fa-plus"></i> Создать', ['create']) ?>
                </div>
            </div>

            <div class="ibox-content">

                <?= GridView::widget([
                    'layout' => '{items}',
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'title',
                            'contentOptions' => [
                                'style' => 'white-space:nowrap;'
                            ],
                        ],

                        [
                            'attribute' => 'alias',
                        ],

                        [
                            'attribute' => 'content',
                            'format' => 'raw',
                            'value' => function($data) {
                                $value = strip_tags($data->content);
                                $value = mb_substr($value, 0, 200, Yii::$app->charset);
                                return trim($value).'...';
                            },
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['class' => 'action-column'],
                            'template' => '{link} {view} {update} {delete}',
                            'buttons' => [
                                'link' => function($url, $model, $key){
                                    return Html::a('<span class="glyphicon glyphicon-link"></span>', Yii::$app->params['scheme'].'://'.Yii::$app->params['subdomain.landing'].'.'.Yii::$app->params['host'].'/'.$model->alias, ['target' => '_blank']);
                                }
                            ]
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>
