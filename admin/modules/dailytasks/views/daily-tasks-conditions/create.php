<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DailyTasksConditions */

$this->title = 'Создать условие для ежедневных заданий';
$this->params['breadcrumbs'][] = ['label' => 'Условия для ежедневных заданий', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-tasks-conditions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
