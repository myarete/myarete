<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DailyTasksConditions */

$this->title = 'Редактировать условие для ежедневных заданий: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Условия для ежедневных заданий', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="daily-tasks-conditions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
