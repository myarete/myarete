<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DailyTasksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ежедневные задания';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-tasks-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать ежедневное задание', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'reward',
            [
                'label' => 'Условие выполнения',
                'value' => 'conditions.title',
            ],
            'date_from',
            'date_to',
            'repeat_task',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
