<?php

namespace admin\modules\training\controllers;

use common\components\yii\base\BaseController;
use common\models\ExerciseVariant;
use common\models\TrainingBuyVsExerciseVariant;
use common\models\TrainingBuy;
use common\models\TrainingBuySearch;
use common\models\TrainingSale;
use common\models\TrainingSaleSearch;
use common\models\Exercise;
use common\models\TrainingSaleVsExercise;
use common\models\Test;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use online\modules\training\forms\CreateTrainingBuyForm;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\Json;
use common\models\Theme;
use common\models\Training;
use yii\helpers\Url;

class DefaultController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['success', 'delete', 'comments', 'delete-comment'],
                        'roles' => ['admin']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'update-exercise', 'get-exercise', 'get-test', 'exercise-list', 'variant-list', 'add-exercise', 'delete-exercise'],
                        'roles' => ['teacher']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        //$trainings = TrainingBuy::find()->all();
        $searchModel = new TrainingBuySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            //'trainings' => $trainings,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $exercises = [];
        $trainingBuyVsExerciseVariant = TrainingBuyVsExerciseVariant::find()->joinWith('variant')->where(['training_id' => $id])->orderBy('order_by')->all();

        foreach ($trainingBuyVsExerciseVariant as $index => $variant) {
            //$exercises[$index]['id'] = $variant['variant']['id'];
            $exercises[$index] = Exercise::find()->select('name')->where(['id' => $variant['variant']['exercise_id']])->asArray()->one();
            $exercises[$index]['theme'] = Theme::FindOne(['id' => $variant['variant']['theme_id']])->getFullName();
            $exercises[$index]['id'] = $variant['variant']['id'];
            $exercises[$index]['exercise_id'] = $variant['variant']['exercise_id'];
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'exercises' => $exercises,
        ]);
    }

    /**
     * @param integer $id
     * @return TrainingBuy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TrainingBuy::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new TrainingBuy();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create_update', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!$model->allowEdit()) {
            $this->forbidden();
        }

        //$post = Yii::$app->request->post('TrainingBuy');
        //$post['order_by'];
        //$order_by = explode(', ', $post['order_by']);

        $model->category_list = $model->categoriesTree();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            /*TrainingBuyVsExercise::deleteAll(['training_id' => $model->id]);

            foreach ($order_by as $order)
            {
                $training = new TrainingBuyVsExercise;
                $training->training_id = $model->id;
                $training->exercise_id = $order;
                $training->save(false);
            }*/

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create_update', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (!$model->allowEdit()) {
            $this->forbidden();
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @param $success
     * @return bool|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionSuccess($id, $success)
    {
        $model = $this->findModel($id);
        $model->category_list = $model->categoriesTree();
        $model->is_approved = $success;
        $model->update(false, ['is_approved']);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return string
     */
    public function actionGetExercise()
    {
        $exercises = ArrayHelper::map(Exercise::find()->select(['id', 'name'])->where(['is_approved' => 1])->asArray()->all(), 'id', 'name');

        return json_encode($exercises);
    }

    /**
     * @return string
     */
    public function actionGetTest()
    {
        $tests = ArrayHelper::map(Test::find()->select(['id', 'name'])->where(['is_approved' => 1])->asArray()->all(), 'id', 'name');

        return json_encode($tests);
    }

    public function actionComments()
    {
        $searchModel = new ArticleCommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view-comments', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDeleteComment($id)
    {
        ArticleComment::findOne($id)->delete();

        return $this->redirect(['comments']);
    }



    public function actionAddExercise($training_id)
    {
        $model = new ExerciseVariant;

        $training = Training::find()->where(['id' => $training_id])->one();
        $trainingBuyVsExerciseVariant = new TrainingBuyVsExerciseVariant;
        Yii::trace('start calculating average revenue');
        if ($model->load(Yii::$app->request->post()))
        {
            $exerciseVariant = ExerciseVariant::findOne(['exercise_id' => $model->exercise_id, 'theme_id' => $model->theme_id]);
            //print_r($model->id);
            $trainingBuyVsExerciseVariant->training_id = $training_id;
            $trainingBuyVsExerciseVariant->exercise_variant_id = $exerciseVariant->id;
            $trainingBuyVsExerciseVariant->save();

            return $this->redirect('/training/default/view?id='.$training_id);
        }

        return $this->redirect('/training/default/view?id='.$training_id);
    }

    public function actionUpdateExercise($id = null, $trainingId = null, $exerciseId = null)
    {
        $trainingId = Yii::$app->request->get('training_id');
        $exerciseId = Yii::$app->request->get('exercise_id');
        $id = Yii::$app->request->get('id');

        $trainingBuyVsExerciseVariant = TrainingBuyVsExerciseVariant::findOne(['exercise_variant_id' => $exerciseId]);

        $exercise = Exercise::find()->select('name')->where(['id' => $id])->asArray()->one();

        $themes = ExerciseVariant::find()->joinWith('theme')->where(['exercise_id' => $id])->all();
        foreach ($themes as $index => $theme) {
            $result[$index]['id'] = $theme['theme']['id'];
            $result[$index]['exercise_id'] = $theme['id'];
            $result[$index]['name'] = strip_tags(Theme::FindOne(['id' => $theme['theme']['id']])->getFullName());
        }

        if ($trainingBuyVsExerciseVariant->load(Yii::$app->request->post()))
        {
            $trainingBuyVsExerciseVariant->update();
            return $this->redirect('/training/default/view?id='.$trainingId);
        }

        return $this->render('exercise-update', [
            'themes' => $result,
            'exercise' => $exercise,
            'trainingBuyVsExerciseVariant' => $trainingBuyVsExerciseVariant
        ]);
    }

    public function actionDeleteExercise($id, $training_id)
    {
        if(TrainingBuyVsExerciseVariant::deleteAll(['exercise_variant_id' => $id])){
            return $this->redirect('/training/default/view?id='.$training_id);
        }

        return $this->redirect('/training/default/view?id='.$training_id);
    }

    public function actionExerciseList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, name AS text')
                ->from('exercise')
                ->where(['like', 'name', $q])
                ->orWhere(['like', 'id', $q])
                ->limit(50);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Exercise::find($id)->name];
        }
        return $out;
    }

    public function actionVariantList()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $exercise_id = $parents[0];
                $themes = ExerciseVariant::find()->joinWith('theme')->where(['exercise_id' => $exercise_id])->all();

                foreach ($themes as $index => $theme) {
                    $out[$index]['id'] = $theme['theme']['id'];
                    $out[$index]['name'] = strip_tags(Theme::FindOne(['id' => $theme['theme']['id']])->getFullName());
                }

                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }
}