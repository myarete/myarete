<?php
use yii\helpers\Html;
use common\models\ArticleVsCategory;
use kartik\widgets\Select2;
?>


<?php
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Тренировки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="col-xs-9">
        <div class="ibox ">

            <div class="ibox-title">
                <?= $this->title ?>
                <div class="ibox-tools">
                    <?php if ($model->allowEdit()): ?>
                        <?= Html::a(
                            '<i class="fa fa-edit"></i> Редактировать</a>',
                            ['update', 'id' => $model->id]
                        ) ?>

                        <?= Html::a(
                            '<i class="fa fa-trash"></i> Удалить</a>',
                            ['delete', 'id' => $model->id],
                            ['onclick' => 'return confirm("Вы уверены?")']
                        ) ?>
                    <?php endif; ?>

                </div>
            </div>
            <div class="ibox-content" style="min-height: 329px;">

                <h2><?=$model->getAttributeLabel('name')?></h2>
                <?= $model->name ?>

                <hr>

                <h2><?=$model->getAttributeLabel('description')?></h2>
                <?= $model->description ?>

                <hr>

                <h2><?=$model->getAttributeLabel('price')?></h2>
                <?= $model->price.' ₽' ?>
            </div>
        </div>
    </div>



    <div class="col-xs-3">
        <div class="ibox">
            <div class="ibox-content">

                <?php if ($model->is_approved): ?>
                    <span class="label label-primary">Одобрено</span>
                <?php else: ?>
                    <span class="label label-danger">Не одобрено</span>
                <?php endif; ?>

                <hr>
            </div>
        </div>

    </div>
</div>

<?= $this->render('view__variants', ['model' => $model, 'training_id' => $model->id, 'exercises' => $exercises]) ?>

