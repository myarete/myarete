<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\Pagination;
use common\models\TrainingBuy;
use common\models\TrainingCategory;
use common\models\TrainingVsCategory;

$this->title = 'Тренировки';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox float-e-margins">

    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
        <?php if (!Yii::$app->user->can('admin')): ?>
            <div class="ibox-tools">
                <?= Html::a('', ['create'], ['class' => 'fa fa-plus', 'title' => 'Создать']) ?>
            </div>
        <?php endif ?>
    </div>

    <div class="ibox-content">

        <div class="row">
            <div class="col-xs-6">
                <label>Тема</label>
                (<a href="/training/default/index" id="reset-search-category">Сбросить</a>)
                <?= \kartik\tree\TreeViewInput::widget([
                    'id' => 'search-category',
                    'model' => $searchModel,
                    'attribute' => 'categoryId',
                    'query' => TrainingCategory::find()->addOrderBy('root, lft'),
                    'headingOptions' => ['label' => 'Категория'],
                    'rootOptions' => ['label' => '<i class="fa fa-tree text-success"></i>'],
                    'fontAwesome' => true,
                    'asDropdown' => true,
                    'multiple' => false,
                    'options' => [
                        //'disabled' => empty($searchModel->categoryId),
                        'class' => 'filter-selector'
                    ],
                    'dropdownConfig' => [
                        'input' => [
                            'placeholder' => 'Все',
                        ],
                    ],
                ]) ?>
            </div>
        </div>

        <hr>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'training-grid',
            'filterSelector' => '.filter-selector, #training-grid input, #training-grid select',
            'filterModel' => $searchModel,
            'layout' => '{items}',
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => [
                        'style' => 'width: 50px;'
                    ],
                ],
                [
                    'attribute' => 'title',
                    'format' => 'raw',
                    'value' => function (TrainingBuy $training) {
                        return Html::a($training->name, ['view', 'id' => $training->id]);
                    },
                ],
                [
                    'attribute' => 'description',
                    'format' => 'raw',
                    'value' => function (TrainingBuy $training) {
                        return mb_substr(strip_tags($training->description), 0, 50, 'UTF-8') . '...';
                    },
                ],
                [
                    'attribute' => 'is_approved',
                    'format' => 'raw',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'is_approved',
                        [0 => 'Скрыто', 1 => 'Преподавателям', 2 => 'Всем'],
                        ['class' => 'form-control', 'prompt' => '-']
                    ),
                    'value' => function (TrainingBuy $training) {
                        if ($training->is_approved == TrainingBuy::SUCCESS_ALL) {
                            $html = Html::tag('span', 'Всем', ['class' => 'label label-primary']);
                            if (Yii::$app->user->can('admin')) {
                                $html .= ' | ' . Html::a('Преподаватели', ['success', 'id' => $training->id, 'success' => TrainingBuy::SUCCESS_TEACHER]);
                                $html .= ' | ' . Html::a('Скрыть', ['success', 'id' => $training->id, 'success' => TrainingBuy::SUCCESS_HIDE]);
                            }
                            return $html;
                        } elseif ($training->is_approved == TrainingBuy::SUCCESS_TEACHER) {
                            $html = Html::tag('span', 'Преподаватели', ['class' => 'label label-warning']);
                            if (Yii::$app->user->can('admin')) {
                                $html .= ' | ' . Html::a('Всем', ['success', 'id' => $training->id, 'success' => TrainingBuy::SUCCESS_ALL]);
                                $html .= ' | ' . Html::a('Скрыть', ['success', 'id' => $training->id, 'success' => TrainingBuy::SUCCESS_HIDE]);
                            }
                            return $html;
                        } elseif ($training->is_approved == TrainingBuy::SUCCESS_HIDE) {
                            $html = Html::tag('span', 'Скрыто', ['class' => 'label label-danger']);
                            if (Yii::$app->user->can('admin')) {
                                $html .= ' | ' . Html::a('Всем', ['success', 'id' => $training->id, 'success' => TrainingBuy::SUCCESS_ALL]);
                                $html .= ' | ' . Html::a('Преподаватели', ['success', 'id' => $training->id, 'success' => TrainingBuy::SUCCESS_TEACHER]);
                            }
                            return $html;
                        }
                    }
                ],
                [
                    'attribute' => 'email',
                    'value' => 'user.email',
                ],
                [
                    'attribute' => 'price'
                ],
                [
                    'attribute' => 'date_created',
                    'filter' => false,
                    'format' => ['date', 'php:d.m.Y H:i:s'],
                ],
                [
                    'attribute' => 'date_updated',
                    'filter' => false,
                    'format' => ['date', 'php:d.m.Y H:i:s'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'width: 1px; white-space: nowrap;'],
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            if (!$model->allowEdit())
                                return '';
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                        },
                        'delete' => function ($url, $model, $key) {
                            if (!Yii::$app->user->can('admin'))
                                return '';
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['data' => [
                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                'method' => 'post'
                            ]]);
                        },
                    ],
                ],
            ],
        ]); ?>
        <?php echo \yii\widgets\LinkPager::widget([
            'pagination' => $dataProvider->pagination,
        ]);
        ?>
    </div>
</div>
