<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\Pagination;

$this->title = 'Комментарии';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
    </div>

    <hr>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'title',
                'header' => 'Название',
                'value' => function($model) {
                    return $model->article->title;
                }
            ],
            [
                'attribute' => 'email',
                'header' => 'Пользователь',
                'value' => function($model) {
                    return $model->user->email;
                }
            ],
            'text',
            'create_dt',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        if (Yii::$app->user->can('admin'))
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete-comment', 'id' => $model->id], ['data' => [
                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                'method' => 'post'
                            ]]);
                    },
                ],
            ],
        ],
    ]) ?>

</div>
