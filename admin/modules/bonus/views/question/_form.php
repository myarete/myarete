<?php

use admin\widgets\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BonusQuestion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bonus-question-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'poll_id')->hiddenInput(['value' => $poll_id])->label(false) ?>

    <?= $form->field($model, 'question')->widget(CKEditor::className(), [
        'enableFinder' => true,
        'preset' => 'full',
        'clientOptions' => [
            'toolbar' => [
                [
                    'name' => 'clipboard',
                    'items' => [
                        'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                    ],
                ],
                [
                    'name' => 'links',
                    'items' => [
                        'Link', 'Unlink', 'Anchor'
                    ],
                ],
                [
                    'name' => 'insert',
                    'items' => [
                        'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                    ],
                ],
                [
                    'name' => 'align',
                    'items' => [
                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                    ],
                ],
                [
                    'name' => 'document',
                    'items' => [
                        'Maximize', 'ShowBlocks', 'Source'
                    ],
                ],
                '/',
                [
                    'name' => 'basicstyles',
                    'items' => [
                        'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                    ],
                ],
                [
                    'name' => 'color',
                    'items' => [
                        'TextColor', 'BGColor'
                    ],
                ],
                [
                    'name' => 'paragraph',
                    'items' => [
                        'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                    ],
                ],
                [
                    'name' => 'styles',
                    'items' => [
                        'Styles', 'Format', 'Font', 'FontSize'
                    ],
                ],
                '/',
                [
                    'name' => 'insert',
                    'items' => [
                        'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                    ],
                ],
            ],
            'height' => 200,
            'allowedContent' => true,
            'extraPlugins' => 'mathjax',
            'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
        ],
    ]) ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'enableFinder' => true,
        'preset' => 'full',
        'clientOptions' => [
            'toolbar' => [
                [
                    'name' => 'clipboard',
                    'items' => [
                        'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                    ],
                ],
                [
                    'name' => 'links',
                    'items' => [
                        'Link', 'Unlink', 'Anchor'
                    ],
                ],
                [
                    'name' => 'insert',
                    'items' => [
                        'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                    ],
                ],
                [
                    'name' => 'align',
                    'items' => [
                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                    ],
                ],
                [
                    'name' => 'document',
                    'items' => [
                        'Maximize', 'ShowBlocks', 'Source'
                    ],
                ],
                '/',
                [
                    'name' => 'basicstyles',
                    'items' => [
                        'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                    ],
                ],
                [
                    'name' => 'color',
                    'items' => [
                        'TextColor', 'BGColor'
                    ],
                ],
                [
                    'name' => 'paragraph',
                    'items' => [
                        'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                    ],
                ],
                [
                    'name' => 'styles',
                    'items' => [
                        'Styles', 'Format', 'Font', 'FontSize'
                    ],
                ],
                '/',
                [
                    'name' => 'insert',
                    'items' => [
                        'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                    ],
                ],
            ],
            'height' => 200,
            'allowedContent' => true,
            'extraPlugins' => 'mathjax',
            'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
        ],
    ]) ?>

    <?= $form->field($model, 'is_required')->checkbox() ?>

    <?= $form->field($model, 'type')->dropDownList([
        '0' => 'Ответ текстом',
        '1' => 'Один верный ответ',
        '2' => 'Несколько верных ответов',
        '3' => 'Просмотр видео'
    ]) ?>

    <div id="questions-wrap" <?= $model->type != 1 && $model->type != 2 ? 'style="display:none"' : '' ?>>
        <div class="ibox-title">
            <h5>Ответы</h5>
            <?php if (!Yii::$app->user->can('admin')): ?>
                <div class="ibox-tools">
                    <?= Html::a('', ['answer/create', 'question_id' => $model->id], ['class' => 'fa fa-plus', 'title' => 'Создать']) ?>
                </div>
            <?php endif ?>
        </div>

        <div class="form-group">
            <?php
            if ($model->bonusAnswers) {
                echo '<ul class="list-group" id="sortable">';
                foreach ($model->bonusAnswers as $answer) {
                    echo '<li class="list-group-item" id="' . $answer->id . '">' . strip_tags($answer->answer)
                        . '<div class="pull-right">'
                        . Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['answer/update', 'id' => $answer->id]) . ' '
                        . Html::a('<span class="glyphicon glyphicon-trash"></span>', ['answer/delete', 'id' => $answer->id], [
                            'data' => [
                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                'method' => 'post'
                            ]
                        ]) . '</div></li>';
                }
                echo '</ul>';
                echo Html::hiddenInput('answers', 0, ['id' => 'answers']);
            }
            ?>
        </div>
    </div>

    <?php if (!$model->isNewRecord && $model->video): ?>
        <div class="form-group">
            <label class="control-label">Текущее видео</label>
            <?= \wbraganca\videojs\VideoJsWidget::widget([
                'options' => [
                    'class' => 'video-js',
                    'controls' => true,
                    'preload' => 'auto',
                ],
                'tags' => [
                    'source' => [
                        ['src' => '/upload/bonus/' . $model->video, 'type' => 'video/mp4']
                    ],
                ]
            ]); ?>
        </div>
    <?php endif; ?>

    <div id="video-wrap" <?= $model->type != 3 ? 'style="display:none"' : '' ?>>
        <?= $form->field($model, 'videoFile')->fileInput()->label('Загрузка видео') ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $(function () {
        $('#sortable').sortable({
            update: function (event, ui) {
                var productOrder = $('#sortable').sortable('toArray').toString();
                $('#answers').val(productOrder);
            },
            axis: 'y'
        });
        $('#sortable').disableSelection();

        $('[name="BonusQuestion[type]"]').on('change', function () {
            if ($(this).val() == 0) {
                $('#video-wrap').hide();
                $('#questions-wrap').hide();
            } else if ($(this).val() == 1 || $(this).val() == 2) {
                $('#video-wrap').hide();
                $('#questions-wrap').show();
            } else if ($(this).val() == 3) {
                $('#questions-wrap').hide();
                $('#video-wrap').show();
            }
        });
    });
</script>
<?php if ($model->isNewRecord): ?>
    <script>
        $(function () {
            $('#video-wrap').hide();
            $('#questions-wrap').hide();
        });
    </script>
<?php endif; ?>
