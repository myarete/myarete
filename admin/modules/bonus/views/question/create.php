<?php

use yii\helpers\Html;
use common\models\BonusPoll;

/* @var $this yii\web\View */
/* @var $model common\models\BonusQuestion */

$this->title = 'Создание вопроса';
?>
<div class="bonus-question-create">

	<?= $this->render('_form', [
		'model' => $model,
		'poll_id' => $poll_id
	]) ?>

</div>
