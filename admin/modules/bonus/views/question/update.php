<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BonusQuestion */

$this->title = 'Редактирование вопроса';
?>
<div class="bonus-question-update">

	<?= $this->render('_form', [
		'model' => $model,
		'poll_id' => $poll_id
	]) ?>

</div>
