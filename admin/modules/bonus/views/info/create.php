<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BonusStat */

$this->title = 'Create Bonus Stat';
$this->params['breadcrumbs'][] = ['label' => 'Bonus Stats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonus-stat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
