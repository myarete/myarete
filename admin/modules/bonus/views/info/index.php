<?php

use common\models\Bonus;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Бонусы';
?>
<div class="ibox-float-e-margins">

    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
        <?php if (!Yii::$app->user->can('admin')): ?>
            <div class="ibox-tools">
                <?= Html::a('', ['create'], ['class' => 'fa fa-plus', 'title' => 'Создать']) ?>
            </div>
        <?php endif ?>
    </div>
    <div class="ibox-content">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'name',
                    'content' => function ($model) {
                        return Html::a($model->name, ['view', 'id' => $model->id]);
                    }
                ],
                [
                    'attribute' => 'date_created',
                    'content' => function ($model) {
                        return date('Y-m-d', strtotime($model->date_created));
                    }
                ],
                [
                    'attribute' => 'date_disabled',
                    'content' => function ($model) {
                        return date('Y-m-d', strtotime($model->date_updated));
                    }
                ],
                'price',
                'numbers_of_uses',
                [
                    'label' => 'Sum',
                    'content' => function ($model) {
                        return $model->price * $model->numbers_of_uses;
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}'
                ],
            ],
        ]); ?>
    </div>
</div>
