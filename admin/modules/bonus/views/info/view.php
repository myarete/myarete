<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\BonusQuestion;
use common\models\BonusAnswer;

$this->title = 'Bonus Stats';
?>
<div class="ibox-float-e-margins">
    <div class="ibox-title">
        <h5><?= Html::encode($this->title) ?></h5>
    </div>

    <div class="ibox-content">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'fullUserName:ntext:Пользователь',
                'userEmail:ntext:Email',
                [
                    'label' => 'Дата прохождения',
                    'content' => function ($model) {
                        return $model->bonusStatValues[0]->created_dt;
                    }
                ],
				[
				        'class' => 'yii\grid\ActionColumn'
                ],
            ],
        ]); ?>
    </div>
</div>