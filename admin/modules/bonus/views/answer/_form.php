<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use admin\widgets\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\BonusAnswer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bonus-answer-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'question_id')->hiddenInput(['value' => $question_id])->label(false) ?>

	<?= $form->field($model, 'answer')->widget(CKEditor::className(), [
		'enableFinder' => true,
		'preset' => 'full',
		'clientOptions' => [
			'toolbar' => [
				[
					'name' => 'clipboard',
					'items' => [
						'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
					],
				],
				[
					'name' => 'links',
					'items' => [
						'Link', 'Unlink', 'Anchor'
					],
				],
				[
					'name' => 'insert',
					'items' => [
						'Image', 'Table', 'HorizontalRule', 'SpecialChar'
					],
				],
				[
					'name' => 'align',
					'items' => [
						'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
					],
				],
				[
					'name' => 'document',
					'items' => [
						'Maximize', 'ShowBlocks', 'Source'
					],
				],
				'/',
				[
					'name' => 'basicstyles',
					'items' => [
						'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
					],
				],
				[
					'name' => 'color',
					'items' => [
						'TextColor', 'BGColor'
					],
				],
				[
					'name' => 'paragraph',
					'items' => [
						'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
					],
				],
				[
					'name' => 'styles',
					'items' => [
						'Styles', 'Format', 'Font', 'FontSize'
					],
				],
				'/',
				[
					'name' => 'insert',
					'items' => [
						'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
					],
				],
			],
			'height' => 200,
			'allowedContent' => true,
			'extraPlugins' => 'mathjax',
			'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
		],
	]) ?>

	<?= $form->field($model, 'right_answer')->dropDownList(['0' => 'Неверный', '1' => 'Правильный']) ?>

	<div class="form-group">
		<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
