<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BonusAnswer */

$this->title = 'Редактирование ответа';
?>
<div class="bonus-answer-update">

    <?= $this->render('_form', [
        'model' => $model,
		'question_id' => $question_id
    ]) ?>

</div>
