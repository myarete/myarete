<?php

use yii\helpers\Html;
use common\models\BonusQuestion;

/* @var $this yii\web\View */
/* @var $model common\models\BonusAnswer */

$this->title = 'Создание ответа';
?>
<div class="bonus-answer-create">

	<?= $this->render('_form', [
		'model' => $model,
		'question_id' => $question_id
	]) ?>

</div>
