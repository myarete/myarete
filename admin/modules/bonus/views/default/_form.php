<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use admin\widgets\ckeditor\CKEditor;

?>

<div class="ibox float-e-margins">

	<div class="ibox-title">
		<h5><?= $this->title ?></h5>
	</div>

	<div class="ibox-content">

		<?php $form = ActiveForm::begin(['id' => 'bonus-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'description')->widget(CKEditor::className(), [
			'enableFinder' => true,
			'preset' => 'full',
			'clientOptions' => [
				'toolbar' => [
					[
						'name' => 'clipboard',
						'items' => [
							'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
						],
					],
					[
						'name' => 'links',
						'items' => [
							'Link', 'Unlink', 'Anchor'
						],
					],
					[
						'name' => 'insert',
						'items' => [
							'Image', 'Table', 'HorizontalRule', 'SpecialChar'
						],
					],
					[
						'name' => 'align',
						'items' => [
							'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
						],
					],
					[
						'name' => 'document',
						'items' => [
							'Maximize', 'ShowBlocks', 'Source'
						],
					],
					'/',
					[
						'name' => 'basicstyles',
						'items' => [
							'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
						],
					],
					[
						'name' => 'color',
						'items' => [
							'TextColor', 'BGColor'
						],
					],
					[
						'name' => 'paragraph',
						'items' => [
							'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
						],
					],
					[
						'name' => 'styles',
						'items' => [
							'Styles', 'Format', 'Font', 'FontSize'
						],
					],
					'/',
					[
						'name' => 'insert',
						'items' => [
							'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
						],
					],
				],
				'height' => 200,
				'allowedContent' => true,
				'extraPlugins' => 'mathjax',
				'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
			],
		]) ?>

		<?= $form->field($model, 'customer')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'access')->dropDownList(['0' => 'Скрыто', '1' => 'Преподаватели', '2' => 'Всем']) ?>

		<?php if (!$model->isNewRecord && !empty($model->image)): ?>
			<div class="form-group">
				<label class="control-label">Текущее изображение</label>
				<img width="200" src="<?= \Yii::getAlias('@web/upload/bonus/') . $model->image ?>"/>
			</div>
		<?php endif; ?>

		<?= $form->field($model, 'imageFile')->fileInput()->label(false)->label('Загрузка изображения') ?>

		<?php if (!$model->isNewRecord): ?>
			<div class="ibox-title">
				<h5>Опросы</h5>
				<?php if (!Yii::$app->user->can('admin')): ?>
					<div class="ibox-tools">
						<?= Html::a('', ['add', 'id' => $model->id], ['class' => 'fa fa-plus', 'title' => 'Добавить опрос']) ?>
					</div>
				<?php endif ?>
			</div>
			<div class="form-group">
				<?php
				if ($model->bonusVsPolls) {
					echo '<ul class="list-group" id="sortable">';
					foreach ($model->bonusVsPolls as $poll) {
						echo '<li class="list-group-item ui-state-default" id="' . $poll->id . '">' . $poll->poll->name
							. '<div class="pull-right">'
							. Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['poll/update', 'id' => $poll->poll->id]) . ' '
							. Html::a('<span class="glyphicon glyphicon-trash"></span>', ['default/remove', 'id' => $poll->id], [
								'data' => [
									'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
									'method' => 'post'
								]
							]) . '</div></li>';
					}
					echo '</ul>';
				}
//				echo Html::hiddenInput('polls', $poll->id, ['id' => 'polls']);
				?>
			</div>
		<?php endif; ?>

		<div class="form-group">
			<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
		</div>

		<?php ActiveForm::end(); ?>

	</div>
</div>
<script>
	$(function () {
		$('#sortable').sortable({
			update: function (event, ui) {
				var productOrder = $('#sortable').sortable('toArray').toString();
				$('#polls').val(productOrder);
			},
			axis: 'y'
		});
		$('#sortable').disableSelection();
	});
</script>