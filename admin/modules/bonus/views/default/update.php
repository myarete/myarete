<?php

$this->title = 'Редактирование бонуса';
?>
<div class="bonus-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
