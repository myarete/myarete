<?php

use common\models\BonusVsPoll;
use common\models\BonusPoll;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$polls = [];
$models = BonusPoll::find()->where(['not', ['id' => BonusVsPoll::find()->select('poll_id')->where(['bonus_id' => $id])]])->all();
foreach ($models as $item) {
	$polls[] = ['id' => $item->id, 'name' => $item->name];
}

Modal::begin([
	'header' => '<h2>Добавить опрос</h2>',
	'clientOptions' => ['show' => true],
	'footer' => '<div class="form-group">'
		. Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'contact-button', 'form' => 'poll-form'])
		. Html::a('Отмена', ['update', 'id' => $id], ['class' => 'btn btn-primary'])
		. '</div>'
]);
?>

<?php $form = ActiveForm::begin(['id' => 'poll-form']); ?>

<?= $form->field($model, 'bonus_id')->hiddenInput(['value' => $id])->label(false) ?>
<?= $form->field($model, 'poll_id')->dropDownList(ArrayHelper::map($polls, 'id', 'name'))->label('Опросы') ?>

<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>

