<?php

use common\models\Bonus;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Бонусы';
?>
<div class="ibox-float-e-margins">

    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
        <?php if (!Yii::$app->user->can('admin')): ?>
            <div class="ibox-tools">
                <?= Html::a('', ['create'], ['class' => 'fa fa-plus', 'title' => 'Создать']) ?>
            </div>
        <?php endif ?>
    </div>
    <div class="ibox-content">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                [
                    'attribute' => 'access',
                    'format' => 'raw',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'access',
                        [0 => 'Скрыто', 1 => 'Преподавателям', 2 => 'Всем'],
                        ['class' => 'form-control', 'prompt' => '-']
                    ),
                    'value' => function (Bonus $bonus) {
                        if ($bonus->access == Bonus::SUCCESS_ALL) {
                            $html = Html::tag('span', 'Всем', ['class' => 'label label-primary']);
                            if (Yii::$app->user->can('admin')) {
                                $html .= ' | ' . Html::a('Преподаватели', ['success', 'id' => $bonus->id, 'success' => Bonus::SUCCESS_TEACHER]);
                                $html .= ' | ' . Html::a('Скрыть', ['success', 'id' => $bonus->id, 'success' => Bonus::SUCCESS_HIDE]);
                            }
                            return $html;
                        } elseif ($bonus->access == Bonus::SUCCESS_TEACHER) {
                            $html = Html::tag('span', 'Преподаватели', ['class' => 'label label-warning']);
                            if (Yii::$app->user->can('admin')) {
                                $html .= ' | ' . Html::a('Всем', ['success', 'id' => $bonus->id, 'success' => Bonus::SUCCESS_ALL]);
                                $html .= ' | ' . Html::a('Скрыть', ['success', 'id' => $bonus->id, 'success' => Bonus::SUCCESS_HIDE]);
                            }
                            return $html;
                        } elseif ($bonus->access == Bonus::SUCCESS_HIDE) {
                            $html = Html::tag('span', 'Скрыто', ['class' => 'label label-danger']);
                            if (Yii::$app->user->can('admin')) {
                                $html .= ' | ' . Html::a('Всем', ['success', 'id' => $bonus->id, 'success' => Bonus::SUCCESS_ALL]);
                                $html .= ' | ' . Html::a('Преподаватели', ['success', 'id' => $bonus->id, 'success' => Bonus::SUCCESS_TEACHER]);
                            }
                            return $html;
                        }
                    }
                ],
                'customer',
                'price',
                [
                    'attribute' => 'date_created',
                    'content' => function ($model) {
                        return date('Y-m-d', strtotime($model->date_created));
                    }
                ],
                [
                    'attribute' => 'date_updated',
                    'content' => function ($model) {
                        return date('Y-m-d', strtotime($model->date_updated));
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}'
                ],
            ],
        ]); ?>
    </div>
</div>
