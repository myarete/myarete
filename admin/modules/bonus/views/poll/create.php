<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BonusPoll */

$this->title = 'Создание опроса';
?>
<div class="bonus-poll-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
