<?php

use common\models\Bonus;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BonusPollSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Опросы';
?>
<div class="ibox-float-e-margins">

	<div class="ibox-title">
		<h5><?= $this->title ?></h5>
		<?php if (!Yii::$app->user->can('admin')): ?>
			<div class="ibox-tools">
				<?= Html::a('', ['create'], ['class' => 'fa fa-plus', 'title' => 'Создать']) ?>
			</div>
		<?php endif ?>
	</div>
	<div class="ibox-content">
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				'name',
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{update} {delete}'
				]
			],
		]); ?>
	</div>

</div>