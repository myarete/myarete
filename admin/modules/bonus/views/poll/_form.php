<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="bonus-poll-form">

    <div class="row">
        <div class="col-md-6">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->widget(\admin\widgets\ckeditor\CKEditor::className(), [
                'enableFinder' => true,
                'preset' => 'full',
                'clientOptions' => [
                    'toolbar' => [
                        [
                            'name' => 'clipboard',
                            'items' => [
                                'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                            ],
                        ],
                        [
                            'name' => 'links',
                            'items' => [
                                'Link', 'Unlink', 'Anchor'
                            ],
                        ],
                        [
                            'name' => 'insert',
                            'items' => [
                                'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                            ],
                        ],
                        [
                            'name' => 'align',
                            'items' => [
                                'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                            ],
                        ],
                        [
                            'name' => 'document',
                            'items' => [
                                'Maximize', 'ShowBlocks', 'Source'
                            ],
                        ],
                        '/',
                        [
                            'name' => 'basicstyles',
                            'items' => [
                                'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                            ],
                        ],
                        [
                            'name' => 'color',
                            'items' => [
                                'TextColor', 'BGColor'
                            ],
                        ],
                        [
                            'name' => 'paragraph',
                            'items' => [
                                'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                            ],
                        ],
                        [
                            'name' => 'styles',
                            'items' => [
                                'Styles', 'Format', 'Font', 'FontSize'
                            ],
                        ],
                        '/',
                        [
                            'name' => 'insert',
                            'items' => [
                                'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                            ],
                        ],
                    ],
                    'height' => 200,
                    'allowedContent' => true,
                    'extraPlugins' => 'mathjax',
                    'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
                ],
            ]); ?>

            <div class="ibox-title">
                <h5>Вопросы</h5>
                <?php if (!Yii::$app->user->can('admin')): ?>
                    <div class="ibox-tools">
                        <?= Html::a('', ['question/create', 'poll_id' => $model->id], ['class' => 'fa fa-plus', 'title' => 'Создать']) ?>
                    </div>
                <?php endif ?>
            </div>

            <div class="form-group">
                <?php
                if ($model->bonusQuestions) {
                    echo '<ul class="list-group" id="sortable">';
                    foreach ($model->bonusQuestions as $question) {
                        echo '<li class="list-group-item" id="' . $question->id . '">' . strip_tags($question->question)
                            . '<div class="pull-right">'
                            . Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['question/update', 'id' => $question->id]) . ' '
                            . Html::a('<span class="glyphicon glyphicon-trash"></span>', ['question/delete', 'id' => $question->id], [
                                'data' => [
                                    'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                    'method' => 'post'
                                ]
                            ]) . '</div></li>';
                    }
                    echo '</ul>';
                    echo Html::hiddenInput('questions', 0, ['id' => 'questions']);
                } ?>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
<script>
    $(function () {
        $('#sortable').sortable({
            update: function (event, ui) {
                var productOrder = $('#sortable').sortable('toArray').toString();
                $('#questions').val(productOrder);
            },
            axis: 'y'
        });
        $('#sortable').disableSelection();
    });
</script>