<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BonusPoll */

$this->title = 'Редактирование опроса';
?>
<div class="bonus-poll-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
