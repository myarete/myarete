<?php

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Видео';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonus-video-index">
    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
        <?php if (!Yii::$app->user->can('admin')): ?>
            <div class="ibox-tools">
                <?= Html::a('', ['create'], ['class' => 'fa fa-plus', 'title' => 'Создать']) ?>
            </div>
        <?php endif ?>
    </div>
    <div class="ibox-content">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'name',
                'video',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}'
                ],
            ],
        ]); ?>
    </div>
</div>
