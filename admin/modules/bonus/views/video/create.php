<?php

$this->title = 'Новое видео';
$this->params['breadcrumbs'][] = ['label' => 'Видео', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonus-video-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
