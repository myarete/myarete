<?php

$this->title = 'Редактирование: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Видео', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="bonus-video-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
