<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
    </div>
    
    <div class="ibox-content">
        
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        
        <?= $form->field($model, 'description')->textarea() ?>
        
        <?php if (!$model->isNewRecord && !empty($model->video)): ?>
            <div class="form-group">
                <label class="control-label">Текущее видео</label>
                <?= \wbraganca\videojs\VideoJsWidget::widget([
                    'options' => [
                        'class' => 'video-js',
                        'controls' => true,
                        'preload' => 'auto',
                    ],
                    'tags' => [
                        'source' => [
                            ['src' => '/upload/bonus/' . $model->video, 'type' => 'video/mp4']
                        ],
                    ]
                ]); ?>
            </div>
        <?php endif; ?>
        
        <?= $form->field($model, 'videoFile')->fileInput()->label('Загрузка видео') ?>
        
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        
        <?php ActiveForm::end(); ?>
        
    </div>
</div>
