<?php

namespace admin\modules\bonus\controllers;

use Yii;
use common\models\BonusAnswer;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AnswerController implements the CRUD actions for BonusAnswer model.
 */
class AnswerController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Creates a new BonusAnswer model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate($question_id)
	{
		$model = new BonusAnswer();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['question/update', 'id' => $question_id]);
		}

		return $this->render('create', [
			'model' => $model,
			'question_id' => $question_id
		]);
	}

	/**
	 * Updates an existing BonusAnswer model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['question/update', 'id' => $model->question_id]);
		}

		return $this->render('update', [
			'model' => $model,
			'question_id' => $model->question_id
		]);
	}

	/**
	 * Deletes an existing BonusAnswer model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the BonusAnswer model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return BonusAnswer the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = BonusAnswer::findOne($id)) !== null) {
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
