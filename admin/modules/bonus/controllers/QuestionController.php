<?php

namespace admin\modules\bonus\controllers;

use common\models\BonusQuestion;
use common\models\BonusAnswer;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class QuestionController extends Controller
{
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	public function actionCreate($poll_id)
	{
		$model = new BonusQuestion();

		$model->is_required = 1;
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['poll/update', 'id' => $poll_id]);
		}

		return $this->render('create', [
			'model' => $model,
			'poll_id' => $poll_id
		]);
	}

	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$new_id = BonusAnswer::find()->select('id')->max('id') + 1;
			if ($answers = Yii::$app->request->post('answers')) {
				foreach (explode(',', $answers) as $answer_id) {
					$answer = BonusAnswer::findOne($answer_id);
					$answer->id = $new_id++;
					$answer->update();
				}
			}
			return $this->redirect(['poll/update', 'id' => $model->poll_id]);
		}

		return $this->render('update', [
			'model' => $model,
			'poll_id' => $model->poll_id
		]);
	}

	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		$model->delete();

		return $this->redirect(['poll/update', 'id' => $model->poll_id]);
	}

	protected function findModel($id)
	{
		if (($model = BonusQuestion::findOne($id)) !== null) {
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
