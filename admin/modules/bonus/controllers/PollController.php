<?php

namespace admin\modules\bonus\controllers;

use Yii;
use common\models\BonusPoll;
use common\models\BonusPollSearch;
use common\models\BonusQuestion;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PollController implements the CRUD actions for BonusPoll model.
 */
class PollController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new BonusPollSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCreate()
    {
        $model = new BonusPoll();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $new_id = BonusQuestion::find()->select('id')->max('id') + 1;
            if ($questions = Yii::$app->request->post('questions')) {
                foreach (explode(',', $questions) as $question_id) {
                    $question = BonusQuestion::findOne($question_id);
                    $question->id = $new_id++;
                    $question->update();
                }
            }
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionReload($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $new_id = BonusQuestion::find()->select('id')->max('id') + 1;
            if ($questions = Yii::$app->request->post('questions')) {
                foreach (explode(',', $questions) as $question_id) {
                    $question = BonusQuestion::findOne($question_id);
                    $question->id = $new_id++;
                    $question->update();
                }
            }
            return $this->redirect(['update', 'id' => $id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = BonusPoll::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
