<?php

namespace admin\modules\bonus\controllers;

use common\models\BonusSearch;
use Yii;
use common\models\BonusStat;
use common\models\BonusStatSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class InfoController extends Controller
{
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	public function actionIndex()
	{
		$searchModel = new BonusSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionView($id)
	{
		$searchModel = new BonusStatSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('view', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	protected function findModel($id)
	{
		if (($model = BonusStat::findOne($id)) !== null) {
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
