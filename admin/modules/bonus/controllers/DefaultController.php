<?php

namespace admin\modules\bonus\controllers;

use common\models\Bonus;
use common\models\BonusSearch;
use common\models\BonusVsPoll;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DefaultController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['delete', 'remove'],
                        'roles' => ['admin']
					],
					[
						'allow' => true,
						'actions' => ['index', 'create', 'update', 'success', 'add'],
                        'roles' => ['teacher']
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	public function actionIndex()
	{
		$searchModel = new BonusSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionCreate()
	{
		$model = new Bonus();

		$model->user_id = Yii::$app->user->id;
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['index']);
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}

	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			if ($polls = Yii::$app->request->post('polls')) {
				foreach (explode(',', $polls) as $poll_id) {
					$bonusVsPoll = BonusVsPoll::findOne($poll_id);
					$newBonusVsPoll = new BonusVsPoll();
					$newBonusVsPoll->attributes = $bonusVsPoll->attributes;
					$newBonusVsPoll->save();
					$bonusVsPoll->delete();
				}
			}
			return $this->redirect(['index']);
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	public function actionAdd($id)
	{
		$model = new BonusVsPoll();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['update', 'id' => $id]);
		}

		return $this->render('add', [
			'model' => $model,
			'id' => $id
		]);
	}

	public function actionRemove($id)
	{
		$model = BonusVsPoll::findOne($id);
		$model->delete();

		return $this->redirect(['update', 'id' => $model->bonus_id]);
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	protected function findModel($id)
	{
		if (($model = Bonus::findOne($id)) !== null) {
			return $model;
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
