<?php
use yii\grid\GridView;

?>


<?php
$this->title = 'Заказать звонок';
?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5><?= $this->title ?></h5>
            </div>

            <div class="ibox-content">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        'fio',
                        [
                            'attribute' => 'phone',
                            'value' => function($data){
                                return \common\components\helpers\FormatHelper::formatPhone($data->phone);
                            },
                        ],
                        [
                            'attribute' => 'date_create',
                            'value' => function($data) {
                                return date('d.m.Y H:i', strtotime($data->date_create));
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['class' => 'action-column'],
                            'template' => '{delete}',
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>
