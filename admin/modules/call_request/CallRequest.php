<?php

namespace admin\modules\call_request;

class CallRequest extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\call_request\controllers';

    public $label = 'Заказать звонок';
}
