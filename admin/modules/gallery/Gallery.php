<?php

namespace admin\modules\gallery;

class Gallery extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\gallery\controllers';

    public $label = 'Галерея';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
