<?php
use common\models\AlbumSearch;
use kartik\grid\GridView;
use yii\helpers\Html;

?>


<style>
    .image-cell {padding: 0 !important;}
    .image-cell img {max-height: 40px;}
</style>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5>Изображения</h5>
                <div class="ibox-tools">
                    <?= Html::a('<i class="fa fa-list"></i> Сортировать', ['sort']) ?>
                </div>
            </div>

            <div class="ibox-content">

                <?= GridView::widget([
                    'id' => 'photo-list',
                    'dataProvider' => (new \common\models\PhotoSearch())->search([]),
                    'layout' => '{items}',
                    'export' => false,
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'options' => ['style' => 'width: 1%'],
                        ],
                        [
                            'attribute' => 'image',
                            'header' => false,
                            'format' => 'raw',
                            'value' => function($data){
                                return $data->getImageTag(['defaultSrc' => '/img/no_image.gif']);
                            },
                            'contentOptions' => ['class' => 'image-cell'],
                        ],
                        [
                            'attribute' => 'album_id',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::activeDropDownList(
                                    $data,
                                    'album_id',
                                    AlbumSearch::getAll(),
                                    [
                                        'prompt' => 'Не установлено',
                                        'class' => 'album-selector',
                                        'data' => [
                                            'id' => $data->id,
                                        ],
                                    ]
                                );
                            },
                        ],
                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'name',
                            'editableOptions' => function ($model, $key, $index) {
                                return [
                                    'valueIfNull' => Yii::$app->formatter->nullDisplay,
                                    'ajaxSettings' => [
                                        'url' => ['/gallery/photo/editable'],
                                    ],
                                ];
                            },
                        ],
                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'description',
                            'editableOptions' => function ($model, $key, $index) {
                                return [
                                    'valueIfNull' => Yii::$app->formatter->nullDisplay,
                                    'ajaxSettings' => [
                                        'url' => ['/gallery/photo/editable'],
                                    ],
                                ];
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'controller' => 'photo',
                            'contentOptions' => ['class' => 'action-column'],
                            'template' => '{delete}',
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>


<script>
    $(function(){

        /**
         *
         */
        window.photoGrid = $('#photo-list');


        /**
         * Delete album
         */
        window.photoGrid.on('click', 'a[title="Delete"]', function() {
            if (!confirm('Вы уверены?')) return false;
            $.get(this.href).success(function(){
                window.photoGrid.reload();
            });
            return false;
        });


        /**
         *
         */
        $('.album-selector').on('change', function(){
            $.post(
                '/gallery/photo-admin/editable',
                {
                    editableIndex   : 0,
                    editableKey     : $(this).data('id'),
                    Photo           : [{album_id : $(this).val()}]
                }
            ).success(function(response) {

                console.log(response);
            });
        });

    });
</script>