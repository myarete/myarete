<?php
use kartik\sortable\Sortable;
?>


<style>
    .sortable li {line-height: 62px;}
    .sortable li img {max-width: 60px; max-height: 60px;}
</style>


<?php
$items = [];
foreach ($album->getPhotos()->ordered()->all() as $i => $photo) {
    $items[] = [
        'content' => $photo->getImageTag([
            'data' => [
                'id' => $photo->id,
            ],
        ]),
    ];
}
?>


<?= Sortable::widget([
    'type' => 'grid',
    'items' => $items,
]) ?>



<script>
    $(function(){

        $('.sortable').sortable().bind('sortupdate', function(e, ui) {

            $.get('/gallery/photod/sort', {
                new_index : ui.item.index(),
                id : ui.item.find('img').data('id')
            });

        });

    });
</script>