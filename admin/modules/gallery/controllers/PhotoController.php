<?php

namespace admin\modules\gallery\controllers;

use common\models\Photo;
use common\components\yii\base\BaseController;
use Yii;
use yii\filters\AccessControl;

/**
 * PhotoController implements the CRUD actions for Album model.
 */
class PhotoController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionUpload()
    {
        Yii::$app->response->format = 'json';
        $model = new Photo;
        return ['success' => $model->save()];
    }


    /**
     * @param $id
     * @return array
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = 'json';
        return ['success' => (bool)Photo::deleteAll(['id' => $id])];
    }


    /**
     * @return array
     */
    public function actionEditable()
    {
        Yii::$app->response->format = 'json';

        if (!isset($_POST['editableKey']))
            return ['success' => false, 'message' => 'Primary key not defined'];

        $model = Photo::findOne($_POST['editableKey']);
        if ($model === null)
            return ['success' => false, 'message' => 'Model not found'];

        if (!isset($_POST['PhotoSearch']) || !isset($_POST['editableIndex']) || !isset($_POST['PhotoSearch'][$_POST['editableIndex']]))
        {
            $this->badRequest();
        }

        $attributes = $_POST['PhotoSearch'][$_POST['editableIndex']];

        $model->attributes = $attributes;

        if (!$model->save())
            return ['success' => false, 'errors' => $model->errors];

        $model->refresh();
        foreach ($attributes as $attribute => $value)
        {
            if ($model->$attribute != $value)
                return ['success' => false, 'message' => "Attribute '{$attribute}' must be equal '{$value}', but has value '{$model->$attribute}'"];
        }

        return ['success' => true];
    }


    /**
     * @param $new_index
     * @param $id
     */
    public function actionSort($new_index, $id)
    {
        $movedPhoto = Photo::findOne($id);

        $models = Photo::find()
            ->ordered()
            ->where(['album_id' => $movedPhoto->album_id])
            ->all();

        foreach ($models as $oldIndex => $model)
        {
            if ($model->id == $id)
                break;
        }

        $out = array_splice($models, $oldIndex, 1);
        array_splice($models, $new_index, 0, $out);

        foreach ($models as $i => $model)
        {
            $model->sort = $i + 1;
            $model->update(false, ['sort']);
        }
    }
}
