<?php

namespace admin\modules\promocodes;

class Promocodes extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\promocodes\controllers';

    public $label = 'Промокоды';

    public function init()
    {
        parent::init();
    }
}
