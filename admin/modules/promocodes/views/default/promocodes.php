<?php


use yii\helpers\Html;
use yii\grid\GridView;

use common\components\other\FirstWords;

?>
<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="article-block">
        <div class="row side-block">
            <h4>Промокоды для регистрации</h4>
            <hr>
            <div class="article-block">
                <div class="row">
                    <p><a href="#" data-toggle="modal" data-target="#create-promocode-modal"><i class="fa fa-plus"></i> Создать промокод</a></p>
                    <?php if(count($promocodes)):?>
                        <?php foreach($promocodes as $promocode):?>
                            <ul>
                                <li><?= $promocode->promocode?> - <?= $promocode->value?> руб. - <?= Html::a('Удалить', ['/promocodes/default/delete-promocode', 'promocode_id' => $promocode->id]) ?></li>
                            </ul>
                        <?php endforeach;?>
                    <?php else:?>
                        <p>Вы пока не создали ни одного промокода</p>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->render('create__promocode__modal', [
    'promocode' => $newPromocode,
    'model' => $model,
]) ?>