<?php

namespace admin\modules\promocodes\controllers;

use common\components\yii\base\BaseController;
use common\models\Promocodes;
use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class DefaultController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionPromocodes()
    {
        $model = new Promocodes();
        $promocodes = Promocodes::findAll(['gift' => 1]);
        $newPromocode = $model->random_string(10);

        return $this->render('promocodes', [
            'model' => $model,
            'promocodes' => $promocodes,
            'newPromocode' => $newPromocode,
        ]);
    }

    public function actionCreateGiftPromocode()
    {
        $model = new Promocodes();

        if ($model->load(Yii::$app->request->post()) && $model->createGiftPromocode()) {
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionSendPromocode()
    {
        $userId = Yii::$app->request->post('user_id');
        $value = Yii::$app->request->post('value');
        $promocode = Yii::$app->request->post('promocode');

        $model = new Promocodes();
        $model->promocode = $promocode;
        $model->user_id = $userId;
        $model->owner_user_id = Yii::$app->user->id;
        $model->value = $value;
        if (!$model->save(false)) {
            throw new Exception('Promocode save error');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDeletePromocode($promocodeId = null)
    {
        $promocodeId = Yii::$app->request->get('promocode_id');

        if (Promocodes::deleteAll(['id' => $promocodeId])) {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            $this->pageNotFound();
        }
    }
}
