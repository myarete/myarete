<?php

namespace admin\modules\exercise;

class Exercise extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\exercise\controllers';

    public function init()
    {
        parent::init();
    }
}
