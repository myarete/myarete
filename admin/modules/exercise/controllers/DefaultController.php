<?php

namespace admin\modules\exercise\controllers;

use common\models\Exercise;
use common\models\ExerciseSearch;
use common\models\ExerciseVariant;
use common\components\yii\base\BaseController;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class DefaultController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['approve', 'delete'],
                        'roles' => ['admin']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'clone'],
                        'roles' => ['teacher']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ExerciseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Exercise();

        if ($model->load(Yii::$app->request->post()) && $model->save())
            return $this->redirect(['view', 'id' => $model->id]);

        return $this->render('create_update', [
            'model' => $model,
        ]);
    }

    public function actionClone($id)
    {
        $model = $this->findModel($id);
        $variants = ExerciseVariant::findAll(['exercise_id' => $id]);
        $new_model = new Exercise;
        $new_model->attributes = $model->attributes;

        if ($new_model->load(Yii::$app->request->post()) && $new_model->save()) {
            foreach ($variants as $variant) {
                $new_variant = new ExerciseVariant;
                $new_variant->attributes = $variant->attributes;
                $new_variant->exercise_id = $new_model->id;
                $new_variant->save();
            }
            return $this->redirect(['view', 'id' => $new_model->id]);
        }

        return $this->render('create_update', [
            'model' => $new_model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!$model->allowEdit())
            $this->forbidden();

        if ($model->load(Yii::$app->request->post()) && $model->save())
            return $this->redirect(['view', 'id' => $model->id]);

        return $this->render('create_update', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (!$model->allowEdit())
            $this->forbidden();

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return bool|int
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionApprove($id)
    {
        $model = $this->findModel($id);
        $model->is_approved = (int)true;
        $model->update(false, ['is_approved']);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param integer $id
     * @return Exercise the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Exercise::findOne($id)) !== null)
            return $model;

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
