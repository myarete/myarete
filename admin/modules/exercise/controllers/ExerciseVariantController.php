<?php

namespace admin\modules\exercise\controllers;

use common\components\yii\base\BaseController;
use common\models\ExerciseVariant;
use Yii;
use yii\filters\AccessControl;

class ExerciseVariantController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['teacher']
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $exercise_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetAllByExercise($exercise_id)
    {
        Yii::$app->response->format = 'json';
        $models = ExerciseVariant::find()
            ->with('theme')
            ->where(['exercise_id' => $exercise_id])
            ->asArray()
            ->all();

        return $models;
    }

    /**
     * @return array|bool
     */
    public function actionSave()
    {
        Yii::$app->response->format = 'json';

        $model = ExerciseVariant::findOne($_POST['ExerciseVariant']['id']);

        if ($model === null)
            $model = new ExerciseVariant;

        $model->load(Yii::$app->request->post());

        if ($model->save())
            return true;
        else
            return $model->errors;
    }

    /**
     * @param $id
     * @return bool
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = 'json';
        return (bool)ExerciseVariant::deleteAll(['id' => $id]);
    }
}