<?php
use common\models\ExerciseType;
use common\models\Subject;
use admin\widgets\ckeditor\CKEditor;
use yii\widgets\ActiveForm;
?>


<?php
$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Задачи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox float-e-margins">

    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
    </div>

    <div class="ibox-content">

        <div class="row">
            <div class="col-xs-9">
                <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'name') ?>

                    <?= $form->field($model, 'question')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'enableFinder' => true,
                        'preset' => 'full',
                        'clientOptions' => [
                            'toolbar' => [
                                [
                                    'name' => 'clipboard',
                                    'items' => [
                                        'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                                    ],
                                ],
                                [
                                    'name' => 'links',
                                    'items' => [
                                        'Link', 'Unlink', 'Anchor'
                                    ],
                                ],
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                                    ],
                                ],
                                [
                                    'name' => 'align',
                                    'items' => [
                                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                                    ],
                                ],
                                [
                                    'name' => 'document',
                                    'items' => [
                                        'Maximize', 'ShowBlocks', 'Source'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'basicstyles',
                                    'items' => [
                                        'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                                    ],
                                ],
                                [
                                    'name' => 'color',
                                    'items' => [
                                        'TextColor', 'BGColor'
                                    ],
                                ],
                                [
                                    'name' => 'paragraph',
                                    'items' => [
                                        'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                                    ],
                                ],
                                [
                                    'name' => 'styles',
                                    'items' => [
                                        'Styles', 'Format', 'Font', 'FontSize'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                                    ],
                                ],
                            ],
                            'height' => 500,
                            'allowedContent' => true,
                            'extraPlugins' => 'mathjax',
                            'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
                        ],
                    ]) ?>

                    <hr>

                    <?= $form->field($model, 'hint')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'clientOptions' => [
                            'toolbar' => [
                                [
                                    'name' => 'clipboard',
                                    'items' => [
                                        'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                                    ],
                                ],
                                [
                                    'name' => 'links',
                                    'items' => [
                                        'Link', 'Unlink', 'Anchor'
                                    ],
                                ],
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                                    ],
                                ],
                                [
                                    'name' => 'align',
                                    'items' => [
                                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                                    ],
                                ],
                                [
                                    'name' => 'document',
                                    'items' => [
                                        'Maximize', 'ShowBlocks', 'Source'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'basicstyles',
                                    'items' => [
                                        'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                                    ],
                                ],
                                [
                                    'name' => 'color',
                                    'items' => [
                                        'TextColor', 'BGColor'
                                    ],
                                ],
                                [
                                    'name' => 'paragraph',
                                    'items' => [
                                        'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                                    ],
                                ],
                                [
                                    'name' => 'styles',
                                    'items' => [
                                        'Styles', 'Format', 'Font', 'FontSize'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                                    ],
                                ],
                            ],
                            'height' => 500,
                            'allowedContent' => true,
                            'extraPlugins' => 'mathjax',
                            'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
                        ],
                    ]) ?>

                    <?= $form->field($model, 'hint_second')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'clientOptions' => [
                            'toolbar' => [
                                [
                                    'name' => 'clipboard',
                                    'items' => [
                                        'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                                    ],
                                ],
                                [
                                    'name' => 'links',
                                    'items' => [
                                        'Link', 'Unlink', 'Anchor'
                                    ],
                                ],
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                                    ],
                                ],
                                [
                                    'name' => 'align',
                                    'items' => [
                                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                                    ],
                                ],
                                [
                                    'name' => 'document',
                                    'items' => [
                                        'Maximize', 'ShowBlocks', 'Source'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'basicstyles',
                                    'items' => [
                                        'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                                    ],
                                ],
                                [
                                    'name' => 'color',
                                    'items' => [
                                        'TextColor', 'BGColor'
                                    ],
                                ],
                                [
                                    'name' => 'paragraph',
                                    'items' => [
                                        'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                                    ],
                                ],
                                [
                                    'name' => 'styles',
                                    'items' => [
                                        'Styles', 'Format', 'Font', 'FontSize'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                                    ],
                                ],
                            ],
                            'height' => 500,
                            'allowedContent' => true,
                            'extraPlugins' => 'mathjax',
                            'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
                        ],
                    ]) ?>


                    <?= $form->field($model, 'hint_third')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'clientOptions' => [
                            'toolbar' => [
                                [
                                    'name' => 'clipboard',
                                    'items' => [
                                        'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                                    ],
                                ],
                                [
                                    'name' => 'links',
                                    'items' => [
                                        'Link', 'Unlink', 'Anchor'
                                    ],
                                ],
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                                    ],
                                ],
                                [
                                    'name' => 'align',
                                    'items' => [
                                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                                    ],
                                ],
                                [
                                    'name' => 'document',
                                    'items' => [
                                        'Maximize', 'ShowBlocks', 'Source'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'basicstyles',
                                    'items' => [
                                        'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                                    ],
                                ],
                                [
                                    'name' => 'color',
                                    'items' => [
                                        'TextColor', 'BGColor'
                                    ],
                                ],
                                [
                                    'name' => 'paragraph',
                                    'items' => [
                                        'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                                    ],
                                ],
                                [
                                    'name' => 'styles',
                                    'items' => [
                                        'Styles', 'Format', 'Font', 'FontSize'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                                    ],
                                ],
                            ],
                            'height' => 500,
                            'allowedContent' => true,
                            'extraPlugins' => 'mathjax',
                            'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
                        ],
                    ]) ?>

                    <hr>

                    <?= $form->field($model, 'decision')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'clientOptions' => [
                            'toolbar' => [
                                [
                                    'name' => 'clipboard',
                                    'items' => [
                                        'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                                    ],
                                ],
                                [
                                    'name' => 'links',
                                    'items' => [
                                        'Link', 'Unlink', 'Anchor'
                                    ],
                                ],
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                                    ],
                                ],
                                [
                                    'name' => 'align',
                                    'items' => [
                                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                                    ],
                                ],
                                [
                                    'name' => 'document',
                                    'items' => [
                                        'Maximize', 'ShowBlocks', 'Source'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'basicstyles',
                                    'items' => [
                                        'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                                    ],
                                ],
                                [
                                    'name' => 'color',
                                    'items' => [
                                        'TextColor', 'BGColor'
                                    ],
                                ],
                                [
                                    'name' => 'paragraph',
                                    'items' => [
                                        'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                                    ],
                                ],
                                [
                                    'name' => 'styles',
                                    'items' => [
                                        'Styles', 'Format', 'Font', 'FontSize'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                                    ],
                                ],
                            ],
                            'height' => 500,
                            'allowedContent' => true,
                            'extraPlugins' => 'mathjax',
                            'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
                        ],
                    ]) ?>

                    <?= $form->field($model, 'decision_detailed')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'clientOptions' => [
                            'toolbar' => [
                                [
                                    'name' => 'clipboard',
                                    'items' => [
                                        'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                                    ],
                                ],
                                [
                                    'name' => 'links',
                                    'items' => [
                                        'Link', 'Unlink', 'Anchor'
                                    ],
                                ],
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                                    ],
                                ],
                                [
                                    'name' => 'align',
                                    'items' => [
                                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                                    ],
                                ],
                                [
                                    'name' => 'document',
                                    'items' => [
                                        'Maximize', 'ShowBlocks', 'Source'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'basicstyles',
                                    'items' => [
                                        'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                                    ],
                                ],
                                [
                                    'name' => 'color',
                                    'items' => [
                                        'TextColor', 'BGColor'
                                    ],
                                ],
                                [
                                    'name' => 'paragraph',
                                    'items' => [
                                        'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                                    ],
                                ],
                                [
                                    'name' => 'styles',
                                    'items' => [
                                        'Styles', 'Format', 'Font', 'FontSize'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                                    ],
                                ],
                            ],
                            'height' => 500,
                            'allowedContent' => true,
                            'extraPlugins' => 'mathjax',
                            'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
                        ],
                    ]) ?>

                    <?= $form->field($model, 'decision_second')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'clientOptions' => [
                            'toolbar' => [
                                [
                                    'name' => 'clipboard',
                                    'items' => [
                                        'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                                    ],
                                ],
                                [
                                    'name' => 'links',
                                    'items' => [
                                        'Link', 'Unlink', 'Anchor'
                                    ],
                                ],
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                                    ],
                                ],
                                [
                                    'name' => 'align',
                                    'items' => [
                                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                                    ],
                                ],
                                [
                                    'name' => 'document',
                                    'items' => [
                                        'Maximize', 'ShowBlocks', 'Source'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'basicstyles',
                                    'items' => [
                                        'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                                    ],
                                ],
                                [
                                    'name' => 'color',
                                    'items' => [
                                        'TextColor', 'BGColor'
                                    ],
                                ],
                                [
                                    'name' => 'paragraph',
                                    'items' => [
                                        'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                                    ],
                                ],
                                [
                                    'name' => 'styles',
                                    'items' => [
                                        'Styles', 'Format', 'Font', 'FontSize'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                                    ],
                                ],
                            ],
                            'height' => 500,
                            'allowedContent' => true,
                            'extraPlugins' => 'mathjax',
                            'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
                        ],
                    ]) ?>

                    <hr>

                    <?= $form->field($model, 'answer')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'clientOptions' => [
                            'toolbar' => [
                                [
                                    'name' => 'clipboard',
                                    'items' => [
                                        'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                                    ],
                                ],
                                [
                                    'name' => 'links',
                                    'items' => [
                                        'Link', 'Unlink', 'Anchor'
                                    ],
                                ],
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                                    ],
                                ],
                                [
                                    'name' => 'align',
                                    'items' => [
                                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                                    ],
                                ],
                                [
                                    'name' => 'document',
                                    'items' => [
                                        'Maximize', 'ShowBlocks', 'Source'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'basicstyles',
                                    'items' => [
                                        'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                                    ],
                                ],
                                [
                                    'name' => 'color',
                                    'items' => [
                                        'TextColor', 'BGColor'
                                    ],
                                ],
                                [
                                    'name' => 'paragraph',
                                    'items' => [
                                        'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                                    ],
                                ],
                                [
                                    'name' => 'styles',
                                    'items' => [
                                        'Styles', 'Format', 'Font', 'FontSize'
                                    ],
                                ],
                                '/',
                                [
                                    'name' => 'insert',
                                    'items' => [
                                        'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                                    ],
                                ],
                            ],
                            'height' => 500,
                            'allowedContent' => true,
                            'extraPlugins' => 'mathjax',
                            'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
                        ],
                    ]) ?>

                </div>
                <div class="col-xs-3">
                    <?= $form->field($model, 'type_id')->dropDownList(ExerciseType::getAll(), ['prompt' => '-- выбрать --']) ?>

                    <?= $form->field($model, 'subject_id')->dropDownList(Subject::getAll(), ['prompt' => '-- выбрать --']) ?>

                    <?= $form->field($model, 'source')->textInput(['maxlength' => true]) ?>
                </div>
            </div>


            <hr>

            <button type="submit" class="btn btn-primary">Сохранить</button>

        <?php ActiveForm::end(); ?>


    </div>
</div>



