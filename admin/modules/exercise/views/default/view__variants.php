<?php
use common\models\ExerciseVariant;
use yii\grid\GridView;
use yii\helpers\Html;

?>


<div class="ibox float-e-margins">

    <div class="ibox-title">
        Варианты
        <div class="ibox-tools">
            <a href="#" data-toggle="modal" data-target="#add-variant-modal"><i class="fa fa-plus"></i> Добавить классификацию</a>
        </div>
    </div>
    <div class="ibox-content">

        <?= GridView::widget([
            'id' => 'variants-grid',
            'layout' => '{items}',
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getVariants(), 'pagination' => false]),
            'columns' => [
                [
                    'attribute' => 'theme_id',
                    'format' => 'raw',
                    'value' => function(ExerciseVariant $variant) {
                        return $variant->theme->getFullName();
                    },
                ],
                [
                    'attribute' => 'complexity',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'controller' => 'exercise-variant',
                    'contentOptions' => ['style' => 'width: 1px; white-space: nowrap;', 'class' => 'action-column'],
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                                'title' => Yii::t('yii', 'Update'),
                                'data-attributes' => json_encode($model->attributes),
                                'class' => 'js-update-variant',
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('yii', 'Delete'),
                                'class' => 'js-delete-variant',
                            ]);
                        }
                    ],
                ],
            ],
        ]) ?>

    </div>
</div>


<?= $this->render('view__variants__modal', [
    'exerciseModel' => $model,
    'variantModel' => new ExerciseVariant(['exercise_id' => $model->id])
]) ?>

