<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ExerciseTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Exercise Types';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox float-e-margins">

    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
        <div class="ibox-tools">
            <?= Html::a('', ['create'], ['class' => 'fa fa-plus', 'title' => 'Создать']) ?>
        </div>
    </div>

    <div class="ibox-content">



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 1px; white-space: nowrap;'],
                'template' => '{update} {delete}',
            ],
        ],
    ]); ?>

    </div>
</div>
