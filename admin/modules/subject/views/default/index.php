<?php

use yii\grid\GridView;
use yii\helpers\Html;
use common\models\Article;
use common\models\SubjectSearch;

?>


<?php
$this->title = 'Предметы';
?>

<div class="ibox">

    <div class="ibox-title">
        <h5>Настройка</h5>
    </div>

    <div class="ibox-content">
        <?= Html::beginForm('set-setting') ?>
            <div class="form-group">
                <?= Html::label('Кол-во предметов на главной', 'Setting')?>
                <?= Html::dropDownList('Setting', Yii::$app->settings->getValue('COUNT_SUBJECT'), ['1'=>'1','2'=>'2','3'=>'3','4'=>'4', '6'=>'6'], ['class'=>'form-control']) ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>
        <?= Html::endForm() ?>
    </div>
</div>

<div class="ibox">

    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
        <div class="ibox-tools">
            <?= Html::a('<i class="fa fa-plus"></i> Создать', ['create']) ?>
        </div>
    </div>

    <div class="ibox-content">

        <?= GridView::widget([
            'layout' => '{items}',
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'name',
                ],

                [
                    'attribute' => 'description',
                ],

                [
                    'format' => 'raw',
                    'header' => 'Учителя',
                    'value' => function($data){
                        $subjects = [];
                        foreach ($data->teachers as $teacherSubject)
                            $subjects[] = Html::a(
                                $teacherSubject->teacher->fio,
                                ['/teacher/default/view', 'id' => $teacherSubject->teacher->id],
                                ['class' => 'label label-success']
                            );

                        return implode('<br>', $subjects);
                    },
                ],
                [
                    'attribute' => 'is_active',
                    'format' => 'raw',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'is_active',
                        [0 => 'Скрыто', 1 => 'Преподавателям', 2 => 'Всем'],
                        ['class' => 'form-control', 'prompt' => '-']
                    ),
                    'value' => function($model) {
                        if ($model->is_active == Article::SUCCESS_ALL)
                        {
                            $html = Html::tag('span', 'Всем', ['class' => 'label label-primary']);
                            if (Yii::$app->user->can('admin'))
                            {
                                $html .= ' | '.Html::a('Преподаватели', ['success', 'id' => $model->id, 'success' => Article::SUCCESS_TEACHER]);
                                $html .= ' | '.Html::a('Скрыть', ['success', 'id' => $model->id, 'success' => Article::SUCCESS_HIDE]);
                            }
                            return $html;
                        }
                        elseif ($model->is_active == Article::SUCCESS_TEACHER)
                        {
                            $html = Html::tag('span', 'Преподаватели', ['class' => 'label label-warning']);
                            if (Yii::$app->user->can('admin'))
                            {
                                $html .= ' | '.Html::a('Всем', ['success', 'id' => $model->id, 'success' => Article::SUCCESS_ALL]);
                                $html .= ' | '.Html::a('Скрыть', ['success', 'id' => $model->id, 'success' => Article::SUCCESS_HIDE]);
                            }
                            return $html;
                        }
                        elseif ($model->is_active == Article::SUCCESS_HIDE)
                        {
                            $html = Html::tag('span', 'Скрыто', ['class' => 'label label-danger']);
                            if (Yii::$app->user->can('admin'))
                            {
                                $html .= ' | '.Html::a('Всем', ['success', 'id' => $model->id, 'success' => Article::SUCCESS_ALL]);
                                $html .= ' | '.Html::a('Преподаватели', ['success', 'id' => $model->id, 'success' => Article::SUCCESS_TEACHER]);
                            }
                            return $html;
                        }
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['class' => 'action-column'],
                ],
            ],
        ]); ?>

    </div>
</div>
