<?php
use common\models\ExerciseClass;
use common\models\Theme;
use kartik\tree\TreeView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
?>


<?php
$this->title = 'Предметы | Просмотр';
?>


<?php
$this->params['ribbonMenu'] = [
    [
        'label'     => '<i class="fa fa-edit"></i>',
        'url'       => ['update', 'id' => $model->id],
        'template'  => '<a href="{url}" title="Обновить">{label}</a>'
    ],
    [
        'label'     => '<i class="fa fa-trash"></i>',
        'url'       => ['delete', 'id' => $model->id],
        'template'  => '<a href="{url}" title="Удалить">{label}</a>',
        'options'   => [
            'onclick' => 'return confirm("Вы уверены?")',
        ]
    ],
];
?>


<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-table"></i>
        <?= $this->title ?>

        <div class="ibox-tools">
            <?= Html::a('', ['update', 'id' => $model->id], ['class' => 'fa fa-edit', 'title' => 'Редактировать']) ?>
        </div>
    </div>

    <div class="ibox-content">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'image',
                    'format' => 'raw',
                    'value' => '
                            <div class="form-group">
                                <label class="control-label">Текущее изображение</label>
                                <img width="200" src="'.Yii::getAlias("@web/upload/subject/").''. $model->image .'" />
                            </div>',
                ],
                'name',
                'description',
                [
                    'format' => 'raw',
                    'label' => 'Учителя',
                    'value' => \admin\widgets\many_many\ManyManyWidget::widget([
                        'allData' => ArrayHelper::map(\common\models\Teacher::find()->orderBy('fio')->all(), 'id', 'fio'),
                        'existsData' => ArrayHelper::map($model->teachers, 'teacher_id', 'teacher_id'),
                        'route' => ['/teacher/default/add-remove-subject', 'subject_id' => $model->id, 'teacher_id' => '{id}'],
                    ]),
                ],
            ],
        ]) ?>

    </div>
</div>



<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-tree"></i> Темы
    </div>

    <div class="ibox-content">

            <?= TreeView::widget([
                'query' => Theme::find()->addOrderBy('root, lft, class')->andWhere(['subject_id' => $model->id]),
                'mainTemplate' => '<div class="row"><div class="col-sm-6">{wrapper}</div><div class="col-sm-6">{detail}</div></div>',
                'treeOptions' => ['style' => 'height:auto'],
                'headingOptions' => ['label' => 'Categories'],
                'fontAwesome' => true,
                'isAdmin' => true,
                'displayValue' => 1,
                'softDelete' => false,
                'cacheSettings' => [
                    'enableCache' => true,
                ]
            ]);
            ?>
    </div>
</div>
