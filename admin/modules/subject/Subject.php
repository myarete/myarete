<?php

namespace admin\modules\subject;

class Subject extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\subject\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
