<?php

namespace admin\modules\css;

class Css extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\css\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
