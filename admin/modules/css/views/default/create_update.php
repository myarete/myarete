<?php
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>


<?php
$this->title = 'HTML | '.($model->isNewRecord ? 'Создать' : 'Обновить');
?>

<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-pencil-square-o"></i> <?= $this->title ?>
    </div>
    <div class="ibox-content">

        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'title') ?>

            <?= $form->field($model, 'code') ?>

            <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'clientOptions' => [
                    'toolbar' => [
                        ['Bold', 'Italic', 'Underline'],
                        ['BulletedList', 'NumberedList'],
                        ['Format'],
                        ['Source'],
                    ],
                    'allowedContent' => true,
                ],
            ]) ?>

            <hr/>

            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
