<?php
$this->title = 'HTML';
?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            
            <div class="ibox-title">
                <h5><?= $this->title ?></h5>
            </div>
            
            <div class="ibox-content">
                <?php foreach ($names as $name => $data): ?>
                    <div class="ibox-title">
                        <h5><?= $name ?></h5>
                    </div>
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="30px">#</th>
                            <th>Файл</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($data as $n => $d): ?>
                            <tr>
                                <td><?= 1 ?></td>
                                <td><?= \yii\helpers\Html::a($n . '.css', ['/css/default/view', 'id' => $n]) ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
