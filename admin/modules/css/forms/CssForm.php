<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 27.06.15
 * Time: 8:00
 */

namespace admin\modules\css\forms;

use yii\base\Model;

class CssForm extends Model
{
    /**
     * @var string
     */
    private $_code;
    
    /**
     * @var string
     */
    private $_file = 'main';
    
    private $_names = [];
    
    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['code', 'required'],
        ];
    }
    
    /**
     * CssForm constructor.
     * @param array $config
     * @param null $file
     */
    public function __construct($file = null, $config = [])
    {
        $dirs = [
            'frontend' => '../../frontend/web/css/',
            'online' => '../../online/web/css/',
            'online/new' => '../../online/web/new/css/',
            'landing' => '../../landing/web/css/',
        ];
        
        $names = [];
        
        foreach ($dirs as $section => $dir) {
            foreach (glob($dir . '*.css') as $f) {
                preg_match('/\/([^\/]+)\.css/', $f, $match);
                if (isset($match[1]))
                    $names[$section][$match[1]] = $dir;
            }
        }
        
        $this->_names = $names;
        
        if ($file && in_array($file, array_keys(array_values($names))))
            $this->_file = $file;
        
        return parent::__construct($config);
    }
    
    /**
     * @return string
     */
    public function getCode()
    {
        if ($this->_code === null) {
            $this->_code = file_get_contents($this->getFile());
        }
        return $this->_code;
    }
    
    public function getNames()
    {
        return $this->_names;
    }
    
    /**
     * @param $value
     */
    public function setCode($value)
    {
        $this->_code = $value;
    }
    
    /**
     * @return bool|string
     */
    protected function getFile()
    {
        $names = [];
        foreach (array_values($this->_names) as $val) {
            $names = array_merge($names, $val);
        }
        
        return $names[$this->_file] . $this->_file . '.css';
    }
    
    /**
     * @return bool
     */
    public function save()
    {
        return (bool)file_put_contents($this->getFile(), $this->getCode());
    }
}
