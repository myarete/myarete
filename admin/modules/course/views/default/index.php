<?php
use yii\grid\GridView;
use yii\helpers\Html;

?>


<?php
$this->title = 'Запись на курсы';
?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5><?= $this->title ?></h5>
            </div>

            <div class="ibox-content">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'id',
                        'fio',
                        'phone',
                        'class',
                        [
                            'attribute' => 'date_create',
                            'value' => function($data) {
                                return date('d.m.Y H:i', strtotime($data->date_create));
                            },
                        ],
                        'type',
                        [
                            'format' => 'raw',
                            'header' => 'Предметы',
                            'value' => function($data) {
                                $subjects = [];;
                                foreach ($data->subjects as $subject)
                                {
                                    $subjects[] = Html::tag('span', $subject->name, ['class' => 'label label-success']);
                                }
                                return implode(' ', $subjects);
                            }
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['class' => 'action-column'],
                            'template' => '{delete}',
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>
