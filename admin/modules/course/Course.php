<?php

namespace admin\modules\course;

class Course extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\course\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
