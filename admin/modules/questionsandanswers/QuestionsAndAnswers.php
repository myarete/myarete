<?php

namespace admin\modules\questionsandanswers;

/**
 * questionsandanswers module definition class
 */
class QuestionsAndAnswers extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'admin\modules\questionsandanswers\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
