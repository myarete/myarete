<?php

namespace admin\modules\main;

class Main extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\main\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
