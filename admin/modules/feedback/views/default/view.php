<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>


<?php
$this->title = 'Отзывы учеников | Просмотр';
?>


<?php
$this->params['ribbonMenu'] = [
    [
        'label'     => '<i class="fa fa-edit"></i>',
        'url'       => ['update', 'id' => $model->id],
        'template'  => '<a href="{url}" title="Обновить">{label}</a>'
    ],
    [
        'label'     => '<i class="fa fa-trash"></i>',
        'url'       => ['delete', 'id' => $model->id],
        'template'  => '<a href="{url}" title="Удалить">{label}</a>',
        'options'   => [
            'onclick' => 'return confirm("Вы уверены?")',
        ]
    ],
];
?>


<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-table"></i>
        <?= $this->title ?>
    </div>

    <div class="ibox-content">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'fio',
                [
                    'attribute' => 'image',
                    'format' => 'raw',
                    'value' => $model->getImageTag(),
                ],
                'content:ntext',
                [
                    'attribute' => 'is_published',
                    'format' => 'raw',
                    'value' => $model->is_published
                        ? Html::a('Опубликован', ['unpublish', 'id' => $model->id], ['class' => 'label label-success'])
                        : Html::a('Не опубликован', ['publish', 'id' => $model->id], ['class' => 'label label-danger']),
                ],
            ],
        ]) ?>

    </div>
</div>