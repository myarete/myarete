<?php

namespace admin\modules\feedback;

class Feedback extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\feedback\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
