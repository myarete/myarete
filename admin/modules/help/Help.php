<?php

namespace admin\modules\help;

class Help extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\help\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
