<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>


<?php
$this->title = 'Создать тип теста';
$this->params['breadcrumbs'][] = ['label' => 'Типы теста', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="ibox float-e-margins">

    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
    </div>

    <div class="ibox-content">

        
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


        <hr>

        <button type="submit" class="btn btn-primary">Сохранить</button>

        <?php ActiveForm::end(); ?>


    </div>
</div>



