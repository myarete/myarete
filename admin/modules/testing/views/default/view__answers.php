<?php
use common\models\TestAnswer;
use yii\grid\GridView;
use yii\helpers\Html;

?>


<div class="ibox float-e-margins">

    <div class="ibox-title">
        Ответы
        <div class="ibox-tools">
            <a href="#" data-toggle="modal" data-target="#add-answer-modal"><i class="fa fa-plus"></i> Добавить ответ</a>
        </div>
    </div>
    <div class="ibox-content">

        <?= GridView::widget([
            'id' => 'answers-grid',
            'layout' => '{items}',
            'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getAnswers(), 'pagination' => false]),
            'columns' => [
                [
                    'attribute' => 'answer',
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'comment',
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'is_right',
                    'format' => 'raw',
                    'contentOptions' =>  ['style' => 'width:20px;'],
                    'value' => function(TestAnswer $answer) {
                        if($answer->is_right) $class = 'fa-check';
                        else $class = 'fa-close';

                        return Html::a('<span class="fa '.$class.'"></span>', ['/testing/test-answer/right', 'id' => $answer->id], [
                            'title' => Yii::t('yii', 'Is right'),
                            'class' => 'js-right-answer',
                        ]);
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'controller' => 'test-answer',
                    'contentOptions' => ['style' => 'width: 1px; white-space: nowrap;', 'class' => 'action-column'],
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                                'title' => Yii::t('yii', 'Update'),
                                'data-attributes' => json_encode($model->attributes),
                                'class' => 'js-update-answer',
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('yii', 'Delete'),
                                'class' => 'js-delete-answer',
                            ]);
                        }
                    ],
                ],
            ],
        ]) ?>

    </div>
</div>



<?= $this->render('view__answers__modal', [
    'testModel' => $model,
    'answerModel' => new TestAnswer(['test_id' => $model->id])
]) ?>

