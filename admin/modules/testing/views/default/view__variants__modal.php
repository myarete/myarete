<?php

use common\models\TestComplexity;
use common\models\Theme;
use kartik\tree\TreeViewInput;
use yii\widgets\ActiveForm;

?>

<div class="modal inmodal" id="add-variant-modal">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Добавить классификацию</h4>
            </div>

            <?php $form = ActiveForm::begin(['id' => 'add-variant-form', 'action' => '/testing/test-variant/save']) ?>

                <div class="modal-body">

                    <!-- id -->
                    <?= $form->field($variantModel, 'id')->label(false)->hiddenInput() ?>

                    <!-- exercise_id -->
                    <?= $form->field($variantModel, 'test_id')->label(false)->hiddenInput() ?>

                    <!-- theme_id -->
                    <div>
                        <?= $form->field($variantModel, 'theme_id')
                            ->error(['class' => 'error-attribute-theme_id'])
                            ->widget(TreeViewInput::className(), [
                                'id' => 'theme-id-input',
                                'query' => Theme::find()
                                    ->addOrderBy('root, lft')
                                    ->where(['subject_id' => $testModel->subject_id]),
                                'headingOptions' => ['label' => 'Тема'],
                                'rootOptions' => ['label' =>'<i class="fa fa-tree text-success"></i>'],
                                'fontAwesome' => true,
                                'asDropdown' => true,
                                'multiple' => false,
                                'options' => [
                                    'disabled' => false,
                                ],
                                'dropdownConfig' => [
                                    'input' => [
                                        'placeholder' => $variantModel->getAttributeLabel('theme_id'),
                                    ],
                                ],
                            ]) ?>
                    </div>

                    <!-- complexity -->
                    <div>
                        <?= $form->field($variantModel, 'complexity')
                            ->error(['class' => 'error-attribute-complexity'])
                            ->dropDownList(TestComplexity::getAll()) ?>
                    </div>

                </div>
                <div class="modal-footer">
                    <a href class="btn btn-white" data-dismiss="modal">Закрыть</a>
                    <a href class="btn btn-primary submit-button">Сохранить</a>
                </div>

            <?php $form->end() ?>

        </div>
    </div>
</div>
