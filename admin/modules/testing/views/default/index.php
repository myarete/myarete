<?php

use common\models\Test;
use common\models\TestType;
use common\models\Theme;
use common\models\Subject;
use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use common\components\other\FirstWords;

$this->title = 'Тесты';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox float-e-margins">

    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
        <div class="ibox-tools">
            <?= Html::a('', ['create'], ['class' => 'fa fa-plus', 'title' => 'Создать']) ?>
        </div>
    </div>

    <div class="ibox-content">

        <div class="row">
            <div class="col-xs-3">
                <label for="search-subject">Предмет</label>
                    <?= Html::activeDropDownList(
                        $searchModel,
                        'subject_id',
                        Subject::getAll(),
                        [
                            'prompt' => '-- all --',
                            'class' => 'form-control filter-selector',
                        ]
                    ) ?>
            </div>
            <div class="col-xs-6">
                <label>Тема</label>
                (<a href="" id="reset-search-theme">Сбросить</a>)
                <?= \kartik\tree\TreeViewInput::widget([
                    'id' => 'search-theme',
                    'model' => $searchModel,
                    'attribute' => 'theme_id',
                    'query' => Theme::find()->where(['subject_id' => $searchModel->subject_id])->addOrderBy('root, lft'),
                    'headingOptions' => ['label' => 'Тема'],
                    'rootOptions' => ['label' =>'<i class="fa fa-tree text-success"></i>'],
                    'fontAwesome' => true,
                    'asDropdown' => true,
                    'multiple' => false,
                    'options' => [
                        'disabled' => empty($searchModel->subject_id),
                        'class' => 'filter-selector'
                    ],
                    'dropdownConfig' => [
                        'input' => [
                            'placeholder' => 'Все',
                        ],
                    ],
                ]) ?>
            </div>
        </div>

        <hr>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'test-grid',
            'filterSelector' => '.filter-selector, #test-grid input, #test-grid select',
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => [
                        'style' => 'width: 50px;'
                    ],
                ],
                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function(Test $test) {
                        return Html::a($test->name, ['view', 'id' => $test->id]);
                    },
                ],
                [
                    'attribute' => 'question',
                    'format' => 'raw',
                    'value' => function(Test $test) {
                        return FirstWords::get($test->question, 30);
                    },
                ],
                [
                    'attribute' => 'subject_id',
                    'filter' => false,
                    'value' => function(Test $test) {
                        return $test->subject->name;
                    },
                ],
                [
                    'attribute' => 'type_id',
                    'value' => function(Test $test) {
                        return $test->type->name;
                    },
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'type_id',
                        TestType::getAll(),
                        ['prompt' => '-- all --', 'class' => 'form-control']
                    ),
                ],
                [
                    'attribute' => 'is_approved',
                    'format' => 'raw',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'is_approved',
                        [1 => 'Одобрено', 0 => 'Не одобрено'],
                        ['prompt' => '-- all --', 'class' => 'form-control']
                    ),
                    'value' => function(Test $test) {
                        if ($test->is_approved)
                        {
                            return Html::tag('span', 'Одобрено', ['class' => 'label label-primary']);
                        }
                        else
                        {
                            $value = Html::tag('span', 'Не одобрено', ['class' => 'label label-danger']);
                            if (Yii::$app->user->can('admin'))
                                $value .= ' '.Html::a('Одобрить', ['approve', 'id' => $test->id]);
                            return $value;
                        }
                    },
                ],
                [
                    'attribute' => 'user_id',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'user_id',
                        User::getAll(),
                        ['prompt' => '-- all --', 'class' => 'form-control']
                    ),
                    'value' => function (Test $test) {
                        return $test->user->email;
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'width: 1px; white-space: nowrap;'],
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            if (!$model->allowEdit())
                                return '';
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                        },
                        'delete' => function ($url, $model, $key) {
                            if (!$model->allowEdit())
                                return '';
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['data' => [
                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                'method' => 'post'
                            ]]);
                        },
                    ],
                ],
            ],
        ]); ?>

    </div>
</div>


<script>
    $(function(){

        $('#reset-search-theme').click(function(){

            $('#search-theme').data('treeinput').$element.val('').trigger('change');

            return false;
        });

    });
</script>
