<?php

namespace admin\modules\testing\controllers;

use common\models\TestAnswer;
use common\components\yii\base\BaseController;
use Yii;
use yii\filters\AccessControl;

class TestAnswerController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['teacher']
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $exercise_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetAllByTest($test_id)
    {
        Yii::$app->response->format = 'json';
        $models = TestAnswer::find()
            ->where(['test_id' => $test_id])
            ->asArray()
            ->all();

        return $models;
    }

    /**
     * @return array|bool
     */
    public function actionSave()
    {
        Yii::$app->response->format = 'json';

        $model = TestAnswer::findOne($_POST['TestAnswer']['id']);

        if ($model === null)
            $model = new TestAnswer;

        $model->load(Yii::$app->request->post());

        if ($model->save())
            return true;
        else
            return $model->errors;
    }

    /**
     * @param $id
     * @return bool
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = 'json';
        return (bool)TestAnswer::deleteAll(['id' => $id]);
    }


    /**
     * @param $id
     * @return bool
     */
    public function actionRight($id)
    {
        Yii::$app->response->format = 'json';
        $model = TestAnswer::findOne($id);
        if ($model === null) return false;

        $model->is_right = (int)!$model->is_right;

        if ($model->save())
            return true;
        else
            return $model->errors;
    }
}