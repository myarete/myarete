<?php

namespace admin\modules\testing\controllers;

use common\models\TestVariant;
use common\components\yii\base\BaseController;
use Yii;
use yii\filters\AccessControl;

class TestVariantController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['teacher']
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $test_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetAllByTest($test_id)
    {
        Yii::$app->response->format = 'json';
        $models = TestVariant::find()
            ->with('theme')
            ->where(['test_id' => $test_id])
            ->asArray()
            ->all();

        return $models;
    }

    /**
     * @return array|bool
     */
    public function actionSave()
    {
        Yii::$app->response->format = 'json';
        $model = TestVariant::findOne($_POST['TestVariant']['id']);

        if (!$model)
            $model = new TestVariant;

        $model->load(Yii::$app->request->post());

        if ($model->save())
            return true;
        else
            return $model->errors;
    }
    /**
     * @param $id
     * @return bool
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = 'json';
        return (bool)TestVariant::deleteAll(['id' => $id]);
    }
}
