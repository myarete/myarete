<?php

namespace admin\modules\testing;

class Testing extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\testing\controllers';

    public function init()
    {
        parent::init();
    }
}
