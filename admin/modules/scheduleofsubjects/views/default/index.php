<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SubjectsScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Расписание предметов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subjects-schedule-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать расписание предмета', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'label' => 'Предмет',
                'value' => 'subject.name',
            ],
            [
                'label' => 'Тип цены',
                'value' => 'price_type.name',
            ],
            'day',
            'time',
            'class_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
