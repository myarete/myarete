<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\TimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\SubjectsSchedule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subjects-schedule-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subject_id')->dropDownList(ArrayHelper::map($subjects,'id','name')) ?>

    <?= $form->field($model, 'price_type')->dropDownList(ArrayHelper::map($priceType,'id','name')) ?>

    <?= $form->field($model, 'day')->dropDownList([ 'Понедельник' => 'Понедельник', 'Вторник' => 'Вторник', 'Среда' => 'Среда', 'Четверг' => 'Четверг', 'Пятница' => 'Пятница', 'Суббота' => 'Суббота', 'Воскресенье' => 'Воскресенье', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'time')->widget(TimePicker::classname(), [
            'pluginOptions' => [
                'showMeridian' => false,
            ]
    ])?>

    <?= $form->field($model, 'class_name')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
