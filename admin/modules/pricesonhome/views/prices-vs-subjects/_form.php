<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\PricesVsSubjects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prices-vs-subjects-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'price_id')->dropDownList(
        ArrayHelper::map($prices, 'id', 'name')
    ) ?>

    <?= $form->field($model, 'subject_id')->dropDownList(
        ArrayHelper::map($subjects, 'id', 'name')
    ) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
