<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PricesVsCategory */

$this->title = 'Создать связку цен и категорий';
$this->params['breadcrumbs'][] = ['label' => 'Цены и категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prices-vs-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'category' => $category,
        'prices' => $prices,
    ]) ?>

</div>
