<?php

namespace admin\modules\pricesonhome;

/**
 * pricesonhome module definition class
 */
class PricesOnHome extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'admin\modules\pricesonhome\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
