<?php

use yii\helpers\Html;
use common\models\ExerciseClass;
use common\models\ExerciseVariant;
use common\models\Theme;

$this->title = "Количество задач";
?>
<div class="container">

    <ul class="nav nav-tabs">
        <?php $i = 0; ?>
        <?php foreach ($subjects as $subject): ?>
            <li <?php if ($i == 0): echo "class='active'"; endif;?>><a data-toggle="tab" href="#subject<?= $subject->id ?>"><?=$subject->name?> (<?= count(ExerciseClass::getExercises($subject->id)) ?>)</a></li>
            <?php $i++; ?>
        <?php endforeach; ?>
    </ul>

    <div class="tab-content">
        <?php $j = 0; ?>
        <?php foreach ($subjects as $subject): ?>
            <?php $classes = ExerciseClass::getClasses($subject->id)?>

            <div id="subject<?= $subject->id ?>" class="tab-pane fade <?php if ($j == 0): echo "in active"; endif;?>">
                <br>
                <ul class="nav nav-tabs">
                    <?php $n = 0; ?>
                    <?php foreach ($classes as $index => $class): ?>
                        <li><?= Html::a($class . ' (' . count(ExerciseClass::getExercises($subject->id, $index)) . ')', ['/information/tasks/view', 'subject_id' => $subject->id, 'class' => $index]) ?></li>
                        <?php $n++; ?>
                    <?php endforeach; ?>
                </ul>

            </div>
            <?php $j++; ?>
        <?php endforeach; ?>
    </div>
</div>
