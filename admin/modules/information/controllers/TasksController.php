<?php

namespace admin\modules\information\controllers;

use common\models\Theme;
use common\models\Subject;
use yii\web\NotFoundHttpException;
use online\modules\training\forms\CreateTrainingForm;
use Yii;

class TasksController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $subjects = Subject::find()
            ->innerJoinWith('exercise')
            ->where(['or', ['is_active' => 1], ['is_active' => 2]])
            ->all();

        return $this->render('index', ['subjects' => $subjects]);
    }

    public function actionView($class, $subject_id)
    {
        $theme = Theme::find()->where(['class' => $class, 'subject_id' => $subject_id, 'lvl' => 0])->one();
        if ($theme === null)
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $this->render('view', [
            'theme' => $theme,
            'sections' => $theme->children(1)->all(),
            'class' => $class
        ]);
    }

}
