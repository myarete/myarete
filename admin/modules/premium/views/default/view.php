<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\modules\premium\models\PremiumPlan */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Премиум планы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox float-e-margins">

    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
    </div>

    <div class="ibox-content">
        <p>
            <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Удалить безвозвратно?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?php if(!empty($model->image)):?>
            <div class="form-group">
                <img width="200" src="<?= \Yii::getAlias('@web/upload/premium/') . $model->image ?>" />
            </div>
        <?php endif ?>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'description:ntext',
                'price',
                [
                    'attribute' => 'validity',
                    'format' => 'raw',
                    'value' => function($model) {
                        $values = ['month' => 'На месяц', 'year' => 'На год', 'to_30_june' => 'До 30-го июня'];
                        return $values[$model->validity];
                    }
                ],
                [
                    'attribute' => 'is_approved',
                    'format' => 'raw',
                    'value' => function($model) {
                        return $model->is_approved ? 'Одобрен' : 'Не одобрен';
                    }
                ],
                [
                    'attribute' => 'dt',
                    'format' => 'raw',
                    'value' => function($model) {
                        return date('d.m.Y H:i:s', strtotime($model->dt));
                    }
                ],
            ],
        ]) ?>
    </div>
</div>
