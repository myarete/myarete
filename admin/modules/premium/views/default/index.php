<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\modules\premium\models\PremiumPlanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Премиум планы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox float-e-margins">

    <div class="ibox-title">
        <div class="ibox-tools">
            <?= Html::a('', ['create'], ['class' => 'fa fa-plus', 'title' => 'Создать']) ?>
        </div>
    </div>

    <div class="ibox-content">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'id:text:№',
                'name',
                'description:ntext',
                'price',
                [
                    'attribute' => 'validity',
                    'format' => 'raw',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'validity',
                        ['month' => 'На месяц', 'year' => 'На год', 'to_30_june' => 'До 30-го июня'],
                        ['prompt' => '-- all --', 'class' => 'form-control control-sm']
                    ),
                    'value' => function($model) {
                        $values = ['month' => 'На месяц', 'year' => 'На год', 'to_30_june' => 'До 30-го июня'];
                        return $values[$model->validity];
                    }
                ],
                [
                    'attribute' => 'is_approved',
                    'format' => 'raw',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'is_approved',
                        [1 => 'Одобрено', 0 => 'Не одобрено'],
                        ['prompt' => '-- all --', 'class' => 'form-control']
                    ),
                    'value' => function($model) {
                        if ($model->is_approved)
                        {
                            return Html::tag('span', 'Одобрено', ['class' => 'label label-primary']);
                        }
                        else
                        {
                            $value = Html::tag('span', 'Не одобрено', ['class' => 'label label-danger']);
                            if (Yii::$app->user->can('admin'))
                                $value .= ' '.Html::a('Одобрить', ['success', 'id' => $model->id]);
                            return $value;
                        }
                    },
                ],
                // 'dt',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>

<script>
    $(function() {
        $('#premiumplan-validity').change(function() {
            var id = $(this).data('id'),
                value = $(this).val();

            $.post('/premium/default/validity', {id: id, value: value}, function(response) {});
        });
    });
</script>
