<?php

namespace admin\modules\teacher\controllers;

use common\components\yii\base\BaseController;
use common\models\TeacherVsSubject;
use Yii;
use common\models\Teacher;
use common\models\TeacherSearch;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * DefaultController implements the CRUD actions for Teacher model.
 */
class DefaultController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Teacher models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TeacherSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Teacher model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Teacher model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Teacher();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create_update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Teacher model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create_update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Teacher model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $teacher_id
     * @param $subject_id
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionAddRemoveSubject($teacher_id, $subject_id)
    {
        $attributes = ['teacher_id' => $teacher_id, 'subject_id' => $subject_id];
        $existModel = TeacherVsSubject::find()->where($attributes)->one();

        if ($existModel === null)
        {
            $model = new TeacherVsSubject;
            $model->attributes = $attributes;
            $model->save(false);
        }
        else
        {
            $existModel->delete();
        }

        $redirectUrl = Yii::$app->request->referrer;
        if ($redirectUrl === null)
            $redirectUrl = ['view', 'id' => $teacher_id];
        return $this->redirect($redirectUrl);
    }

    /**
     * Finds the Teacher model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Teacher the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Teacher::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
