<?php

namespace admin\modules\teacher;

class Teacher extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\teacher\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
