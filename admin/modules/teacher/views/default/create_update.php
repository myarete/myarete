<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

?>


<?php
$this->title = 'Частые вопросы | '.($model->isNewRecord ? 'Создать' : 'Обновить');
?>


<div class="ibox float-e-margins">

    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
    </div>

    <div class="ibox-content">

        <?php $form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>

            <div class="row">

                <div class="col-xs-3">
                    <div class="form-group">
                        <?= $model->getImageUploadWidget($form) ?>
                    </div>
                </div>

                <div class="col-xs-9">
                    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'clientOptions' => [
                            'toolbar' => [
                                ['Bold', 'Italic', 'Underline'],
                                ['BulletedList', 'NumberedList'],
                                ['Format'],
                                ['Source'],
                            ],
                            'allowedContent' => true,
                        ],
                    ]) ?>

                    <?= $form->field($model, 'contacts')->widget(CKEditor::className(), [
                        'options' => ['rows' => 1],
                        'clientOptions' => [
                            'toolbar' => [
                                ['Bold', 'Italic', 'Underline'],
                                ['BulletedList', 'NumberedList'],
                                ['Format'],
                                ['Source'],
                            ],
                            'allowedContent' => true,
                        ],
                    ]) ?>
                </div>

            </div>

            <hr/>

            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
