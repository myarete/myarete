<?php
use yii\grid\GridView;
use yii\helpers\Html;

?>


<?php
$this->title = 'Учителя';
?>



<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5><?= $this->title ?></h5>
                <div class="ibox-tools">
                    <?= Html::a('<i class="fa fa-plus"></i> Создать', ['create']) ?>
                </div>
            </div>

            <div class="ibox-content">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'image',
                            'format' => 'raw',
                            'value' => function($data){
                                return $data->getImageTag(['defaultSrc' => '/img/no_image.gif', 'style' => 'max-height: 100px;']);
                            }
                        ],

                        'fio',
                        'description:ntext',
                        'contacts',

                        [
                            'format' => 'raw',
                            'header' => 'Предметы',
                            'value' => function($data){
                                $subjects = [];
                                foreach ($data->subjects as $teacherSubject)
                                    $subjects[] = Html::a(
                                        $teacherSubject->subject->name,
                                        ['/subject/default/view/', 'id' => $teacherSubject->subject->id],
                                        ['class' => 'label label-success']
                                    );

                                return implode('<br>', $subjects);
                            },
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['class' => 'action-column'],
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>
