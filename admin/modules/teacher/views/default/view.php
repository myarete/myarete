<?php
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
?>


<?php
$this->title = 'Частые вопросы | Просмотр';
?>


<?php
$this->params['ribbonMenu'] = [
    [
        'label'     => '<i class="fa fa-edit"></i>',
        'url'       => ['update', 'id' => $model->id],
        'template'  => '<a href="{url}" title="Обновить">{label}</a>'
    ],
    [
        'label'     => '<i class="fa fa-trash"></i>',
        'url'       => ['delete', 'id' => $model->id],
        'template'  => '<a href="{url}" title="Удалить">{label}</a>',
        'options'   => [
            'onclick' => 'return confirm("Вы уверены?")',
        ]
    ],
];
?>


<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-table"></i>
        <?= $this->title ?>
    </div>

    <div class="ibox-content">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'fio',
                [
                    'attribute' => 'image',
                    'format' => 'raw',
                    'value' => $model->getImageTag(['defaultSrc' => '/img/no_image.gif', 'style' => 'max-height: 100px;']),
                ],
                'description:html',
                'contacts:html',
                [
                    'format' => 'raw',
                    'label' => 'Предметы',
                    'value' => \admin\widgets\many_many\ManyManyWidget::widget([
                        'allData' => ArrayHelper::map(\common\models\Subject::find()->orderBy('name')->all(), 'id', 'name'),
                        'existsData' => ArrayHelper::map($model->subjects, 'subject_id', 'subject_id'),
                        'route' => ['add-remove-subject', 'teacher_id' => $model->id, 'subject_id' => '{id}'],
                    ]),
                ],
            ],
        ]) ?>

    </div>
</div>
