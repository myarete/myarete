<?php

namespace admin\modules\faq;

class Faq extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\faq\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
