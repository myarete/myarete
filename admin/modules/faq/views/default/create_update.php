<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

?>


<?php
$this->title = 'Частые вопросы | '.($model->isNewRecord ? 'Создать' : 'Обновить');
?>


<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-pencil-square-o"></i> <?= $this->title ?>
    </div>
    <div class="ibox-content">

        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'question')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'answer')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'clientOptions' => [
                    'toolbar' => [
                        ['Bold', 'Italic', 'Underline'],
                        ['BulletedList', 'NumberedList'],
                        ['Format'],
                        ['Source'],
                    ],
                    'allowedContent' => true,
                ],
            ]) ?>

            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <hr/>

            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
