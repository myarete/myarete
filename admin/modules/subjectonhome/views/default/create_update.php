<?php
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>


<?php
$this->title = 'Предметы | '.($model->isNewRecord ? 'Создать' : 'Обновить');
?>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <i class="fa fa-pencil-square-o"></i> <?= $this->title ?>
    </div>
    <div class="ibox-content">

        <?php $form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>

            <div class="row">

                <div class="col-xs-3">
                    <div class="form-group">
                        <?php if(!empty($model->image)):?>
                            <div class="form-group">
                                <label class="control-label">Текущее изображение</label>
                                <img width="200" src="<?= \Yii::getAlias('@web/upload/subjectonhome/') . $model->image ?>" />
                            </div>
                        <?php endif;?>

                        <?= $form->field($model, 'imageFile')->fileInput() ?>
                    </div>
                </div>

                <div class="col-xs-9">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'clientOptions' => [
                            'toolbar' => [
                                ['Bold', 'Italic', 'Underline'],
                                ['BulletedList', 'NumberedList'],
                                ['Format'],
                                ['Source'],
                            ],
                            'allowedContent' => true,
                        ],
                    ]) ?>
                </div>
            </div>

            <hr/>

            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
