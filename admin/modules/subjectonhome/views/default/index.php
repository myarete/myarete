<?php
use yii\grid\GridView;
use yii\helpers\Html;


?>


<?php
$this->title = 'Предметы на главной';
?>

<div class="ibox">

    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
        <div class="ibox-tools">
            <?= Html::a('<i class="fa fa-plus"></i> Создать', ['create']) ?>
        </div>
    </div>

    <div class="ibox-content">

        <?= GridView::widget([
            'layout' => '{items}',
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'name',
                ],

                [
                    'attribute' => 'description',
                ],

                [
                    'attribute' => 'is_active',
                    'format' => 'raw',
                    'value' => function($subject) {
                        if ($subject->is_active == 1)
                        {
                            $html = Html::tag('span', 'Опубликовано', ['class' => 'label label-primary']);
                            if (Yii::$app->user->can('admin'))
                            {
                                $html .= ' '.Html::a('Скрыто', ['success', 'id' => $subject->id]);
                            }
                            return $html;
                        }
                        elseif ($subject->is_active == 0)
                        {
                            $html = Html::tag('span', 'Скрыто', ['class' => 'label label-danger']);
                            if (Yii::$app->user->can('admin'))
                            {
                                $html .= ' '.Html::a('Опубликовано', ['success', 'id' => $subject->id]);
                            }
                            return $html;
                        }
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['class' => 'action-column'],
                ],
            ],
        ]); ?>

    </div>
</div>
