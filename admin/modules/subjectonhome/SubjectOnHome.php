<?php

namespace admin\modules\subjectonhome;

/**
 * subjectonhome module definition class
 */
class SubjectOnHome extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'admin\modules\subjectonhome\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
