<?php
use yii\grid\GridView;
use yii\helpers\Html;
?>


<?php
$this->title = 'Новости';
?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5><?= $this->title ?></h5>
                <div class="ibox-tools">
                    <?= Html::a('<i class="fa fa-plus"></i> Создать', ['create']) ?>
                </div>
            </div>

            <div class="ibox-content">

                <?= GridView::widget([
                    'layout' => '{items}',
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'image',
                            'format' => 'raw',
                            'value' => function($data){
                                return $data->getImageTag(['style' => 'max-height: 100px; max-width: 100px;']);
                            },
                        ],
                        [
                            'attribute' => 'title',
                            'contentOptions' => [
                                'style' => 'white-space:nowrap;'
                            ],
                        ],

                        [
                            'attribute' => 'rewrite',
                        ],

                        [
                            'attribute' => 'description',
                            'format' => 'raw',
                            'value' => function($data) {
                                $value = strip_tags($data->description);
                                $value = mb_substr($value, 0, 200, Yii::$app->charset);
                                return trim($value).'...';
                            },
                        ],
                        [
                            'attribute' => 'date_create',
                            'format' => ["date", "php:d-m-Y H:i:s"],
                        ],

                        [
                            'attribute' => 'is_published',
                            'format' => 'raw',
                            'value' => function($data){
                                if ($data->is_published)
                                    return Html::a('Опубликован', ['unpublish', 'id' => $data->id], ['class' => 'label label-success']);
                                else
                                    return Html::a('Не опубликован', ['publish', 'id' => $data->id], ['class' => 'label label-danger']);
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['class' => 'action-column'],
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>
