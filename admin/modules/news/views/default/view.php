<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>


<?php
$this->title = 'Новости | Просмотр';
?>


<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-table"></i>
        <?= $this->title ?>

        <div class="ibox-tools">
            <?= Html::a('', ['update', 'id' => $model->id], ['class' => 'fa fa-edit', 'title' => 'Редактировать']) ?>
        </div>
    </div>

    <div class="ibox-content">


        <div class="row">

            <div class="col-xs-3">
                <?= $model->getImageTag(['style' => 'max-width: 350px;', 'class' => 'img-thumbnail']) ?>
            </div>
            <div class="col-xs-9">
                <div class="row">
                    <h1><?= $model->title; ?></h1>
                </div>
                <div class="row">
                    <?= $model->rewrite; ?>
                </div>
                <br>
                <div class="row">
                    <div class="well">
                        <?= $model->description; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
