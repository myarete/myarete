<?php

namespace admin\modules\news;

class News extends \yii\base\Module
{
    public $controllerNamespace = 'admin\modules\news\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
