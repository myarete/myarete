<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@shared', dirname(dirname(__DIR__)) . '/shared');
Yii::setAlias('@signup', dirname(dirname(__DIR__)) . '/signup');
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@online', dirname(dirname(__DIR__)) . '/online');
Yii::setAlias('@blog', dirname(dirname(__DIR__)) . '/blog');
Yii::setAlias('@admin', dirname(dirname(__DIR__)) . '/admin');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@landing', dirname(dirname(__DIR__)) . '/landing');
