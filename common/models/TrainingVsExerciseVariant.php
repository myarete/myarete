<?php

namespace common\models;

use common\models\ExerciseVariant;
use Yii;

/**
 * This is the model class for table "training_vs_exercise".
 *
 * @property integer $id
 * @property integer $training_id
 * @property integer $exercise_variant_id
 * @property string $date_create
 *
 * @property Training $training
 * @property ExerciseVariant $variant
 */
class TrainingVsExerciseVariant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training_vs_exercise_variant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['training_id', 'exercise_variant_id'], 'required'],
            [['training_id', 'exercise_variant_id'], 'integer'],

            ['answer', 'filter', 'filter' => 'trim'],
            ['answer', 'filter', 'filter' => 'strip_tags'],
            ['answer', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'training_id' => 'Training',
            'exercise_variant_id' => 'Exercise',
            'date_create' => 'Date Create',
            'answer' => 'Решение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTraining()
    {
        return $this->hasOne(Training::className(), ['id' => 'training_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariant()
    {
        return $this->hasOne(ExerciseVariant::className(), ['id' => 'exercise_variant_id']);
    }
}
