<?php

namespace common\models;

use common\models\User;
use common\models\ProfileLanguagesQuery;
use Yii;

/**
 * This is the model class for table "profile_education".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $languages
 * @property User $user
 */
class ProfileLanguages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_languages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang', 'level'], 'required'],
            [['user_id'], 'integer'],
            [['lang', 'level'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'lang' => 'Язык',
            'level' => 'Уровень владения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function afterValidate()
    {
        $this->user_id = Yii::$app->user->identity->id;
    }

    /**
     * @inheritdoc
     * @return profilelanguagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new profilelanguagesQuery(get_called_class());
    }
}
