<?php

namespace common\models;

use common\models\Payment;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "promocodes".
 *
 * @property integer $id
 * @property string $promocode
 * @property integer $user_id
 * @property integer $status
 */
class Promocodes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promocodes';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_created',
                'updatedAtAttribute' => 'date_updated',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promocode', 'value'], 'required'],
            [['owner_user_id', 'user_id', 'status', 'gift', 'register'], 'integer'],
            [['promocode'], 'string', 'max' => 255],
            [['user_id', 'status', 'date_created', 'date_updated'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promocode' => 'Promocode',
            'user_id' => 'User ID',
            'status' => 'Status',
        ];
    }

    public function sendPromocode($userId)
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {
            // Save promocode
            $model = new Promocodes();
            $model->owner_user_id = Yii::$app->user->id;
            $model->user_id = $userId;
            $model->promocode = $this->promocode;
            $model->value = $this->value;
            $model->status = 0;
            if (!$model->save(false))
                throw new Exception('Promocode save error');

            $transaction->commit();
            return true;
        } catch (Exception $e) {
            //$transaction->rollBack();
            echo $e;
        }

        return false;
    }

    public function createGiftPromocode()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {
            // Save promocode
            $model = new Promocodes();
            $model->promocode = $this->promocode;
            $model->value = $this->value;
            $model->owner_user_id = Yii::$app->user->id;
            $model->status = 0;
            $model->gift = 1;

            if (!$model->save(false))
                throw new Exception('Promocode save error');

            $transaction->commit();
            return true;
        } catch (Exception $e) {
            //$transaction->rollBack();
            echo $e;
        }

        return false;
    }

    public static function activatePromocode($promocode)
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            // update promocode fields
            $promoOwner = User::findOne($promocode->owner_user_id);
            if ($promoOwner->role === 'admin') {
                $promocode->status = 1;
                $promocode->register = 0;
                $promocode->user_id = Yii::$app->user->id;
                $promocode->update();
                $model = $promocode;
            } else {
                $model = new static;
                $model->user_id = Yii::$app->user->id;
                $model->owner_user_id = $promocode->owner_user_id;
                $model->promocode = $promocode->promocode;
                $model->value = $promocode->value;
                $model->status = 1;
                $model->register = 1;
                if (!$model->save(false)) {
                    throw new Exception('Promocode activate error');
                }
            }
            $profile = Profile::findOne(['user_id' => Yii::$app->user->id]);
            $profile->cash += $model->value;
            //update owner cash
            $promoOwnerProfile = Profile::findOne(['user_id' => $promocode->owner_user_id]);
            $promoOwnerProfile->cash += $model->value;

            if (!$profile->update()) {
                throw new Exception('Profile save error');
            }

            if (!$promoOwnerProfile->update(false)) {
                throw new Exception('Profile save error');
            }

            $payment = new Payment();
            $payment->user_id = $promoOwnerProfile->user_id;
            $payment->refill_sum = $model->value;
            $payment->is_paid = 0;
            $payment->type_id = 11;
            $payment->save(false);

            $payment = new Payment();
            $payment->user_id = $profile->user_id;
            $payment->refill_sum = $model->value;
            $payment->is_paid = 0;
            $payment->type_id = 10;
            if (!$payment->save(false)) {
                throw new Exception('Payment save error');
            }

            $transaction->commit();
            return true;
        } catch (Exception $e) {
            //$transaction->rollBack();
            echo $e;
        }

        return false;
    }

    public function random_string($str_length)
    {
        $str_characters = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

        $characters_length = count($str_characters) - 1;

        $string = '';

        for ($i = $str_length; $i > 0; $i--) {
            $string .= $str_characters[mt_rand(0, $characters_length)];
        }

        return $string;
    }

    /**
     * @inheritdoc
     * @return PromocodesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PromocodesQuery(get_called_class());
    }
}
