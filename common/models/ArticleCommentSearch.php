<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ArticleComment;

/**
 * ArticleCommentSearch represents the model behind the search form about `online\modules\article\models\ArticleComment`.
 */
class ArticleCommentSearch extends ArticleComment
{
    public $title;

    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'article_id', 'user_id'], 'integer'],
            [['title', 'email'], 'string'],
            [['text', 'create_dt', 'visible'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ArticleComment::find()->joinWith(['user', 'article']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'article_id' => $this->article_id,
            'user_id' => $this->user_id,
            'visible' => $this->visible
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'create_dt', $this->create_dt])
            ->andFilterWhere(['like', 'user.email', $this->email])
            ->andFilterWhere(['like', 'article.title', $this->title]);

        return $dataProvider;
    }
}
