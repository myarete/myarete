<?php

namespace common\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "send_materials".
 *
 * @property integer $id
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property string $trainings
 * @property string $dt
 */
class SendTraining extends \yii\db\ActiveRecord
{
    public $type = 'Тренировка';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'send_training';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_user_id', 'to_user_id'], 'required'],
            [['from_user_id', 'to_user_id', 'training', 'is_ready'], 'integer'],
            [['training_params'], 'string'],
            [['dt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_user_id' => 'From User ID',
            'to_user_id' => 'To User ID',
            'training' => 'Training',
            'training_params' => 'Training params',
            'dt' => 'Dt',
            'is_ready' => 'Is Ready',
        ];
    }

    public function getFromUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    public function getToUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }
}
