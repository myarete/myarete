<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProfileEducation;

/**
 * ProfileEducationSearch represents the model behind the search form about `online\modules\profile\models\ProfileEducation`.
 */
class ProfileEducationSearch extends ProfileEducation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['school', 'degree', 'field_of_study', 'grade', 'activities', 'from_date', 'to_date', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProfileEducation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'from_date' => $this->from_date,
            'to_date' => $this->to_date,
        ]);

        $query->andFilterWhere(['like', 'school', $this->school])
            ->andFilterWhere(['like', 'degree', $this->degree])
            ->andFilterWhere(['like', 'field_of_study', $this->field_of_study])
            ->andFilterWhere(['like', 'grade', $this->grade])
            ->andFilterWhere(['like', 'activities', $this->activities])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
