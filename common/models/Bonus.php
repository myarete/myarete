<?php

namespace common\models;

use Yii;

class Bonus extends \yii\db\ActiveRecord
{
    use \common\models\traits\UploadTrait;

    const SUCCESS_HIDE = 0;
    const SUCCESS_TEACHER = 1;
    const SUCCESS_ALL = 2;

    public static function tableName()
    {
        return 'bonus';
    }

    public function rules()
    {
        return [
            [['user_id', 'name', 'price'], 'required'],
            [['user_id', 'access', 'numbers_of_uses'], 'integer'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['date_created', 'date_updated', 'date_disabled'], 'safe'],
            [['name', 'image', 'customer'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['imageFile', 'image', 'skipOnEmpty' => true]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Наименование',
            'description' => 'Description',
            'image' => 'Image',
            'price' => 'Цена',
            'access' => 'Access',
            'customer' => 'Customer',
            'date_created' => 'Дата создания',
            'date_updated' => 'Date Updated',
            'date_disabled' => 'Дата удаления',
            'numbers_of_uses' => 'Количество использований'
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getBonusStats()
    {
        return $this->hasMany(BonusStat::className(), ['bonus_id' => 'id']);
    }

    public function getBonusVsPolls()
    {
        return $this->hasMany(BonusVsPoll::className(), ['bonus_id' => 'id']);
    }

    public function getPolls()
    {
        return $this->hasMany(BonusPoll::className(), ['id' => 'poll_id'])->via('bonusVsPolls');
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->id;
        }

        $this->uploadImage('@admin/web/upload/bonus/', '@online/web/upload/bonus/');

        return parent::beforeSave($insert);
    }
}
