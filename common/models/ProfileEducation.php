<?php

namespace common\models;

use common\models\User;
use common\models\ProfileEducationQuery;
use Yii;

/**
 * This is the model class for table "profile_education".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $school
 * @property string $degree
 * @property string $field_of_study
 * @property string $grade
 * @property string $activities
 * @property string $from_date
 * @property string $to_date
 * @property string $description
 *
 * @property User $user
 */
class ProfileEducation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_education';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school'], 'required'],
            [['user_id'], 'integer'],
            [['activities', 'description'], 'string'],
            [['from_date', 'to_date'], 'safe'],
            [['school', 'degree', 'field_of_study', 'grade'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'school' => 'Школа',
            'degree' => 'Статус или степень',
            'field_of_study' => 'Область изучения',
            'grade' => 'Класс, группа и т.п.',
            'activities' => 'Проекты и сообщества (увлечения, секции и т.п.)',
            'from_date' => 'Начало обучения',
            'to_date' => 'Окончание обучения',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function afterValidate()
    {
        $this->user_id = Yii::$app->user->identity->id;
    }

    /**
     * @inheritdoc
     * @return profileEducationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new profileEducationQuery(get_called_class());
    }
}
