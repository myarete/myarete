<?php

namespace common\models;

use common\models\Subject;
use Yii;

/**
 * This is the model class for table "course_request".
 *
 * @property integer $id
 * @property string $fio
 * @property string $phone
 * @property integer $subjects
 * @property integer $status
 * @property string $date_create
 *
 */
class CourseRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['fio', 'filter', 'filter' => 'trim'],
            ['fio', 'required', 'message' => 'Обязательно к заполнению'],
            ['fio', 'string', 'max' => 255],
            ['subjects', 'safe'],

            ['phone', 'required', 'message' => 'Обязательно к заполнению'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'phone' => 'Телефон',
            'subjects' => 'Предметы',
            'status' => 'Статус',
            'date_create' => 'Дата',
        ];
    }
}
