<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "exercise_type".
 *
 * @property integer $id
 * @property string $name
 */
class ExerciseType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exercise_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'name' => 'Название',
        ];
    }

    /**
     * @return array
     */
    public static function getAll()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}