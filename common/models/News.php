<?php

namespace common\models;

use common\components\behaviors\ImageBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $rewrite
 * @property string $description
 * @property string $image
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'required', 'message' => 'Не должно быть пустым'],
            ['rewrite', 'required', 'message' => 'Не должно быть пустым'],
            ['rewrite', 'unique', 'message' => 'Уже занят'],
            ['rewrite', 'match', 'pattern' => '~^[_a-z\d]+$~', 'message' => 'Только латинские буквы и нижние подчеркивания (пример: price_block)'],
            ['title', 'string', 'max' => 255],
            ['rewrite', 'string', 'max' => 255],
            ['description', 'string'],
            ['is_published', 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'rewrite' => 'ЧПУ',
            'description' => 'Содержание',
            'image' => 'Изображение',
            'is_published' => 'Опубликован?',
            'date_create' => 'Дата создания'
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'imageBehavior' => [
                'class' => ImageBehavior::className(),
            ],
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @param $rewrite
     * @return mixed|string
     */
    public static function get($rewrite)
    {
        $model = self::find()->where(['rewrite' => $rewrite])->one();
        if ($model === null)
            return '';

        return $model->description;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllNews()
    {
        $model = self::find()
            ->where(['is_published' => 1])
            ->orderBy('date_create DESC')
            ->limit(5)
            ->all();
        foreach ($model as $news) {
            $news->image = $news->getImageSrc();
        }

        if ($model === null)
            return [];

        return $model;
    }
}
