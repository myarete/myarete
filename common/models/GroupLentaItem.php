<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "group_lenta_item".
 *
 * @property int $id
 * @property int $group_id
 * @property int $user_id
 * @property string $title
 * @property string $text
 * @property string $created_dt
 *
 * @property Group $group
 * @property User $user
 */
class GroupLentaItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'group_lenta_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_id', 'user_id', 'title', 'text'], 'required'],
            [['group_id', 'user_id'], 'integer'],
            [['text'], 'string'],
            [['created_dt'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'text' => 'Text',
            'created_dt' => 'Created Dt',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
