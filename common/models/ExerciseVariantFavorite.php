<?php

namespace common\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "exercise_variant_favorite".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $exercise_variant_id
 *
 * @property User $user
 * @property ExerciseVariant $exerciseVariant
 */
class ExerciseVariantFavorite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exercise_variant_favorite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'exercise_variant_id'], 'required'],
            [['user_id', 'exercise_variant_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'exercise_variant_id' => 'Exercise Variant ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExerciseVariant()
    {
        return $this->hasOne(ExerciseVariant::className(), ['id' => 'exercise_variant_id']);
    }
}
