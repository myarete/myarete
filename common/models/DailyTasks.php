<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "daily_tasks".
 *
 * @property integer $id
 * @property string $title
 * @property integer $reward
 * @property integer $condition_id
 * @property string $date_from
 * @property string $date_to
 * @property string $repeat_task
 */
class DailyTasks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daily_tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'reward', 'condition_id', 'date_from', 'date_to', 'repeat_task'], 'required'],
            [['reward', 'condition_id'], 'integer'],
            [['date_from', 'date_to'], 'safe'],
            [['repeat_task'], 'string'],
            [['title'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'reward' => 'Вознаграждение, руб.',
            'condition_id' => 'Условие выполнения',
            'date_from' => 'Дата начала',
            'date_to' => 'Дата завершения',
            'repeat_task' => 'Повтор задачи',
        ];
    }

    public function getConditions()
    {
        return $this->hasOne(DailyTasksConditions::className(), ['id' => 'condition_id']);
    }

    public function getUserVsDailyTasks()
    {
        return $this->hasOne(UserVsDailyTasks::className(), ['task_id' => 'id']);
    }
}
