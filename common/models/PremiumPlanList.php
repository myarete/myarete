<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "premium_plan_list".
 *
 * @property integer $id
 * @property integer $premium_id
 * @property integer $user_id
 * @property string $price
 * @property string $date
 */
class PremiumPlanList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'premium_plan_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['premium_id', 'user_id', 'price', 'status'], 'required'],
            [['premium_id', 'user_id'], 'integer'],
            [['price'], 'number'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'premium_id' => 'Премиум ID',
            'user_id' => 'Пользователь',
            'price' => 'Стоимость',
            'date' => 'Дата',
            'status' => 'Статус'
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getPremiumPlan()
    {
        return $this->hasOne(PremiumPlan::className(), ['id' => 'premium_id']);

    }
}
