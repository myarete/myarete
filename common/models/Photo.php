<?php

namespace common\models;

use common\components\behaviors\ImageBehavior;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * This is the model class for table "photo".
 *
 * @property integer $id
 * @property integer $album_id
 * @property string $image
 * @property integer $sort
 * @property string $name
 * @property string $description
 *
 * @property Album $album
 */
class Photo extends \yii\db\ActiveRecord
{
    /**
     * @return PhotoQuery
     */
    public static function find()
    {
        return new PhotoQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'string', 'max' => 255],

            ['description', 'filter', 'filter' => 'trim'],

            ['album_id', 'exist', 'targetClass' => 'online\modules\gallery\models\Album', 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'album_id' => 'Альбом',
            'image' => 'Изображение',
            'name' => 'Название',
            'description' => 'Описание',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'imageBehavior' => [
                'class' => ImageBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbum()
    {
        return $this->hasOne(Album::className(), ['id' => 'album_id']);
    }
}


class PhotoQuery extends ActiveQuery
{
    /**
     * @return static
     */
    public function ordered()
    {
        return $this->orderBy([new Expression('sort = 0, sort')]);
    }
}