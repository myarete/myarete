<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prices_on_home".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 */
class PricesOnHome extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prices_on_home';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['alias'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'alias' => 'Алиас',
        ];
    }

    public function getPricesVsCategory()
    {
        return $this->hasMany(PricesVsCategory::className(), ['price_id' => 'id'])
            ->with('category');
    }

    public function getPricesVsSubjects()
    {
        return $this->hasMany(PricesVsSubjects::className(), ['price_id' => 'id'])
            ->with('subjects');
    }
}
