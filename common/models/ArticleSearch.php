<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ExerciseSearch represents the model behind the search form about `app\modules\exercise\models\Exercise`.
 */
class ArticleSearch extends Article
{

    public $pagination = ['pageSize' => 10];

    public $categoryId;

    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
            [['email'], 'safe'],
            [['categoryId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find()->where(['active' => 1])
            ->joinWith([
                'user',
                'articleCategories',
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date_created' => SORT_DESC,
                ],
            ],
            'pagination' => !empty($this->pagination) ? $this->pagination : false,
        ]);

        /*$dataProvider->sort->attributes['articleCategories'] = [
            'asc' => ['articleCategories.title' => SORT_ASC],
            'desc' => ['articleCategories.title' => SORT_DESC],
        ];*/

        $dataProvider->sort->attributes['email'] = [
            'asc' => ['user.email' => SORT_ASC],
            'desc' => ['user.email' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $ids = [];

        if ($this->description) {
            $sliders = ArticleSliderItem::find()->where('description like "%' . $this->description . '%"')->all();
            $articles = [];
            foreach ($sliders as $slider) {
                $articles = array_merge($articles, Article::find()->where('description like "%{{sliders=' . $slider->article_slider_id . '}}%"')->all());
            }
            $ids = array_map(function ($obj) {
                return $obj->id;
            }, $articles);
        }

        $query->andFilterWhere(['article.id' => $this->id])
            ->andFilterWhere(['like', 'user.email', $this->email])
            ->andFilterWhere(['article.is_approved' => $this->is_approved])
            ->andFilterWhere(['like', 'article.title', $this->title])
            ->andFilterWhere(['article_category.id' => $this->categoryId])
            ->andFilterWhere(['like', 'article.description', $this->description])
            ->orFilterWhere(['article.id' => $ids]);

        return $dataProvider;
    }
}
