<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BonusStat;

class BonusStatSearch extends BonusStat
{
    public $fullUserName, $userEmail;

    public function rules()
    {
        return [
            [['fullUserName', 'userEmail'], 'safe'],
//            ['stat_id', 'integer']
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = BonusStat::find()
            ->joinWith(['user0', 'user'])
            ->where(['bonus_id' => $params['id']]);
//			->orderBy('stat_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andWhere('profile.first_name LIKE "%' . $this->fullUserName . '%" ' .
            'OR profile.last_name LIKE "%' . $this->fullUserName . '%"'
        )->andWhere('user.email LIKE "%' . $this->userEmail . '%"');

        // grid filtering conditions
//		$query->andFilterWhere([
//			'stat_id' => $this->stat_id,
//		]);

//		$query->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
