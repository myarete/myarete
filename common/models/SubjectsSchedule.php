<?php

namespace common\models;

use common\models\Html;
use common\models\SubjectOnHome;
use Yii;

/**
 * This is the model class_name for table "subjects_schedule".
 *
 * @property integer $id
 * @property integer $subject_id
 * @property string $price_type
 * @property string $day
 * @property string $time
 * @property integer $class_name
 */
class SubjectsSchedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subjects_schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject_id', 'price_type', 'day', 'time', 'class_name'], 'required'],
            [['subject_id'], 'integer'],
            [['day', 'class_name'], 'string'],
            [['time'], 'safe'],
            [['price_type'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject_id' => 'Предмет',
            'price_type' => 'Тип цены',
            'class_name' => 'Класс',
            'day' => 'День недели',
            'time' => 'Время',
        ];
    }

    public function getSubject()
    {
        return $this->hasOne(SubjectOnHome::className(), ['id' => 'subject_id']);
    }

    public function getPricesOnHome()
    {
        return $this->hasOne(PricesOnHome::className(), ['id' => 'price_type']);
    }

    public static function getSchedule($priceType)
    {
        $model = self::find()->where(['price_type' => $priceType])->orderBy('time', 'DESC')->all();
        if ($model === null)
            return null;

        return $model;
    }

    public static function getScheduleForSubject($subjectId)
    {
        $model = self::find()->where(['subject_id' => $subjectId])->orderBy(['time' => SORT_ASC])->all();
        if ($model === null)
            return null;

        return $model;
    }

    public static function getScheduleForPrice($subjectId, $priceType)
    {
        $model = self::find()->where(['subject_id' => $subjectId, 'price_type' => $priceType])->all();
        if ($model === null)
            return null;

        return $model;
    }

}
