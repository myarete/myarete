<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "article_vs_category".
 *
 * @property string $training_id
 * @property string $training_category_id
 *
 * @property TrainingCategory $trainingCategory
 * @property Training $training
 */
class TrainingVsCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training_vs_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['training_id', 'training_category_id'], 'required'],
            [['training_id', 'training_category_id'], 'integer'],
            [['training_id', 'training_category_id'], 'unique', 'targetAttribute' => ['training_id', 'training_category_id'], 'message' => 'The combination of Training ID and Training Category ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'training_id' => 'Статья',
            'training_category_id' => 'Категория',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingCategory()
    {
        return $this->hasOne(TrainingCategory::className(), ['id' => 'training_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTraining()
    {
        return $this->hasOne(Training::className(), ['id' => 'training_id']);
    }

    public function getUserVsTrainingCategory()
    {
        return $this->hasMany(UserVsTrainingCategory::className(), ['training_category_id' => 'training_category_id']);
        //->joinWith('userVsArticle');;

    }
}
