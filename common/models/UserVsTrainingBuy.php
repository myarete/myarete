<?php

namespace common\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "user_vs_training_buy".
 *
 * @property integer $user_id
 * @property integer $training_id
 */
class UserVSTrainingBuy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_vs_training_buy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'training_id'], 'required'],
            [['user_id', 'training_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'training_id' => 'Training ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
