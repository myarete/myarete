<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * ExerciseSearch represents the model behind the search form about `app\modules\exercise\models\Exercise`.
 */
class ExerciseSearch extends Exercise
{
    /**
     * @var int
     */
    public $theme_id;

    /**
     * @var int
     */
    public $class;

    /**
     * @var int
     */
    public $complexity;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
            [['theme_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();
        $query->joinWith([
            'subject',
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort = [
            'defaultOrder' => [
                'id' => SORT_DESC
            ]
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'exercise.id' => $this->id,
            'exercise.subject_id' => $this->subject_id,
            'exercise.type_id' => $this->type_id,
            'variants.class' => $this->class,
            'variants.complexity' => $this->complexity,
            'exercise.user_id' => $this->user_id,
            'exercise.is_approved' => $this->is_approved,
        ]);

        $query->andFilterWhere(['like', 'exercise.question', $this->question])
            ->andFilterWhere(['like', 'exercise.decision', $this->decision])
            ->andFilterWhere(['like', 'exercise.name', $this->name])
            ->andFilterWhere(['like', 'exercise.answer', $this->answer])
            ->andFilterWhere(['like', 'exercise.source', $this->source]);

        if (!empty($this->theme_id)) {
            $theme = Theme::findOne($this->theme_id);
            if ($theme !== null) {
                $query->joinWith(['variants' => function (ActiveQuery $query) {
                    $query->joinWith('theme');
                }], true, 'join');
                $query->andWhere(['root' => $theme->root]);
                $query->andWhere('theme.lft >= :lft AND theme.rgt <= :rgt', [':lft' => $theme->lft, ':rgt' => $theme->rgt]);
            }
        }

        return $dataProvider;
    }
}
