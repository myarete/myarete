<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ExerciseSearch represents the model behind the search form about `app\modules\exercise\models\Exercise`.
 */
class TrainingSaleSearch extends TrainingSale
{

    public $pagination = ['pageSize' => 10];

    public $categoryId;

    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
            [['email'], 'safe'],
            [['categoryId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find()
            ->joinWith([
                'user',
                'trainingCategories',
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date_created' => SORT_DESC,
                ],
            ],
            'pagination' => !empty($this->pagination) ? $this->pagination : false,
        ]);

        $dataProvider->sort->attributes['email'] = [
            'asc' => ['user.email' => SORT_ASC],
            'desc' => ['user.email' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $ids = [];

        $query->andFilterWhere(['training_sale.id' => $this->id])
            ->andFilterWhere(['like', 'user.email', $this->email])
            ->andFilterWhere(['training_sale.is_approved' => $this->is_approved])
            ->andFilterWhere(['like', 'training_sale.title', $this->title])
            ->andFilterWhere(['training_category.id' => $this->categoryId])
            ->andFilterWhere(['like', 'training_sale.description', $this->description])
            ->orFilterWhere(['training_sale.id' => $ids]);

        return $dataProvider;
    }
}
