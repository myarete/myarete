<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "daily_tasks_conditions".
 *
 * @property integer $id
 * @property string $title
 */
class DailyTasksConditions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daily_tasks_conditions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Условие',
        ];
    }
}
