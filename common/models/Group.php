<?php

namespace common\models;

use common\components\behaviors\ImageBehavior;

/**
 * This is the model class for table "group".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $image
 * @property string $description
 *
 * @property User $user
 * @property GroupLentaItem[] $groupLentaItems
 * @property ClientVsGroup[] $clientVsGroups
 * @property TrainingVsGroup[] $trainingVsGroups
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'image'], 'required'],
            [['user_id'], 'integer'],
            [['description'], 'string'],
            [['name', 'image'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Client ID',
            'name' => 'Name',
            'image' => 'Image',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupLentaItems()
    {
        return $this->hasMany(GroupLentaItem::className(), ['group_id' => 'id'])->orderBy('created_dt DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientVsGroups()
    {
        return $this->hasMany(ClientVsGroup::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingVsGroups()
    {
        return $this->hasMany(TrainingVsGroup::className(), ['group_id' => 'id']);
    }

    public function getClients()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->via('clientVsGroups');
    }

    public function getTrainings()
    {
        return $this->hasMany(Training::className(), ['id' => 'training_id'])->via('trainingVsGroups');
    }

    public function behaviors()
    {
        return [
            'imageBehavior' => [
                'class' => ImageBehavior::className(),
            ],
        ];
    }
}
