<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DailyTasks;

/**
 * DailyTasksSearch represents the model behind the search form about `common\models\DailyTasks`.
 */
class DailyTasksSearch extends DailyTasks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'reward', 'condition_id'], 'integer'],
            [['title', 'date_from', 'date_to', 'repeat_task'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DailyTasks::find()->with('conditions');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'reward' => $this->reward,
            'condition_id' => $this->condition_id,
            'date_from' => $this->date_from,
            'date_to' => $this->date_to,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'repeat_task', $this->repeat_task]);

        return $dataProvider;
    }
}
