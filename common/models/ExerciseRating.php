<?php

namespace common\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "exercise_rating".
 *
 * @property string $id
 * @property string $exercise_id
 * @property integer $rating
 * @property string $user_id
 * @property integer $class
 * @property string $date_create
 *
 * @property Exercise $exercise
 * @property User $user
 */
class ExerciseRating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exercise_rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exercise_id', 'rating', 'user_id', 'class'], 'required'],
            [['exercise_id', 'rating', 'user_id', 'class'], 'integer'],
            [['date_create'], 'safe'],
            [['exercise_id', 'user_id', 'class'], 'unique', 'targetAttribute' => ['exercise_id', 'user_id', 'class'], 'message' => 'The combination of Exercise ID, User ID and Class has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exercise_id' => 'Exercise ID',
            'rating' => 'Rating',
            'user_id' => 'User ID',
            'class' => 'Class',
            'date_create' => 'Date Create',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExercise()
    {
        return $this->hasOne(Exercise::className(), ['id' => 'exercise_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
