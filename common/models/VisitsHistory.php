<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "visits_history".
 *
 * @property integer $id
 * @property string $user
 * @property string $page
 * @property string $link
 * @property string $dt
 */
class VisitsHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visits_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user', 'page', 'link'], 'required'],
            [['dt'], 'safe'],
            [['user', 'page', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user' => 'User',
            'page' => 'Page',
            'link' => 'Link',
            'dt' => 'Dt',
        ];
    }
}
