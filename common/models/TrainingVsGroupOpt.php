<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "training_vs_group_opt".
 *
 * @property int $id
 * @property int $trvsgr_id
 * @property int $task_id
 * @property int $f_hint Подсказка
 * @property int $f_decision Решение
 * @property int $f_answer Ответ
 * @property int $f_area Пространство для решения
 *
 * @property TrainingVsGroup $trvsgr
 */
class TrainingVsGroupOpt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'training_vs_group_opt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['trvsgr_id', 'task_id'], 'required'],
            [['trvsgr_id', 'task_id', 'f_hint', 'f_decision', 'f_answer', 'f_area'], 'integer'],
            [['trvsgr_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingVsGroup::className(), 'targetAttribute' => ['trvsgr_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trvsgr_id' => 'Trvsgr ID',
            'task_id' => 'Task ID',
            'f_hint' => 'Подсказка',
            'f_decision' => 'Решение',
            'f_answer' => 'Ответ',
            'f_area' => 'Пространство для решения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrvsgr()
    {
        return $this->hasOne(TrainingVsGroup::className(), ['id' => 'trvsgr_id']);
    }
}
