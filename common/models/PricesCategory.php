<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prices_category".
 *
 * @property integer $id
 * @property string $price
 * @property string $period
 */
class PricesCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prices_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'price', 'period'], 'required'],
            [['price'], 'number'],
            [['title', 'period'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'price' => 'Цена',
            'period' => 'Период',
        ];
    }


    public function getCategory()
    {
        return $this->hasOne(PricesVsCategory::className(), ['category_id' => 'id']);
    }
}
