<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $plan_id
 * @property integer $is_paid
 * @property string $create_dt
 */
class Payment extends \yii\db\ActiveRecord
{
    public $current_result;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'refill_sum'], 'required'],
            [['user_id', 'is_paid'], 'integer'],
            [['create_dt'], 'safe'],
            [['current_result'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'is_paid' => 'Is Paid',
            'refill_sum' => 'Сумма пополнения',
            'create_dt' => 'Create Dt',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(PaymentType::className(), ['id' => 'type_id']);
    }
}
