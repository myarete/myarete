<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property integer $id
 * @property string $question
 * @property string $answer
 * @property string $fio
 * @property string $email
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['question', 'filter', 'filter' => 'trim'],
            ['question', 'required', 'message' => 'Обязательно к заполнению'],

            ['answer', 'filter', 'filter' => 'trim'],

            ['fio', 'filter', 'filter' => 'trim'],
            ['fio', 'required', 'message' => 'Обязательно к заполнению'],
            ['fio', 'string', 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => 'Обязательно к заполнению'],
            ['email', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'fio' => 'ФИО',
            'email' => 'E-mail',
        ];
    }
}
