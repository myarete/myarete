<?php

namespace common\models;

use common\models\Theme;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * ExerciseSearch represents the model behind the search form about `app\modules\exercise\models\Exercise`.
 */
class TestSearch extends Test
{
    /**
     * @var int
     */
    public $theme_id;

    /**
     * @var int
     */
    public $class;

    /**
     * @var int
     */
    public $complexity;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
            [['theme_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();
        $query->joinWith([
            'subject',
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort = [
            'defaultOrder' => [
                'id' => SORT_DESC
            ]
        ];

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'test.id' => $this->id,
            'test.subject_id' => $this->subject_id,
            'test.type_id' => $this->type_id,
            'test.is_approved' => $this->is_approved,
            'test.user_id' => $this->user_id,
            'variants.class' => $this->class,
            'variants.complexity' => $this->complexity,
        ]);

        $query->andFilterWhere(['like', 'test.question', $this->question])
            ->andFilterWhere(['like', 'test.name', $this->name]);

        if (!empty($this->theme_id)) {
            $theme = Theme::findOne($this->theme_id);
            if ($theme !== null) {
                $query->joinWith(['variants' => function (ActiveQuery $query) {
                    $query->joinWith('theme');
                }], true, 'join');
                $query->andWhere(['root' => $theme->root]);
                $query->andWhere('theme.lft >= :lft AND theme.rgt <= :rgt', [':lft' => $theme->lft, ':rgt' => $theme->rgt]);
            }
        }

        return $dataProvider;
    }
}
