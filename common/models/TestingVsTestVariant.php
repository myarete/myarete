<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "testing_vs_test_variant".
 *
 * @property string $id
 * @property string $test_variant_id
 * @property string $testing_id
 * @property string $date_create
 *
 * @property TestVariant $testVariant
 * @property Testing $testing
 */
class TestingVsTestVariant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'testing_vs_test_variant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_variant_id', 'testing_id'], 'required'],
            [['test_variant_id', 'testing_id'], 'integer'],
            [['date_create'], 'safe'],
            [['test_variant_id', 'testing_id'], 'unique', 'targetAttribute' => ['test_variant_id', 'testing_id'], 'message' => 'The combination of Test Variant ID and Testing ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_variant_id' => 'Test Variant ID',
            'testing_id' => 'Testing ID',
            'date_create' => 'Date Create',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariant()
    {
        return $this->hasOne(TestVariant::className(), ['id' => 'test_variant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTesting()
    {
        return $this->hasOne(Testing::className(), ['id' => 'testing_id']);
    }
}
