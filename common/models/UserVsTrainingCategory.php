<?php

namespace common\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "user_vs_training_category".
 *
 * @property integer $user_id
 * @property integer $training_category_id
 *
 * @property UserVsTrainingCategory $trainingCategory
 * @property User $user
 */
class UserVsTrainingCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_vs_training_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'training_category_id'], 'required'],
            [['user_id', 'training_category_id'], 'integer'],
            [['training_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingCategory::className(), 'targetAttribute' => ['training_category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'training_category_id' => 'Training Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
