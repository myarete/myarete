<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "article_slider_item".
 *
 * @property string $id
 * @property string $article_slider_id
 * @property string $title
 * @property string $description
 * @property string $order
 *
 * @property ArticleSlider $articleSlider
 */
class ArticleSliderItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_slider_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_slider_id'], 'required'],
            [['article_slider_id', 'order'], 'integer'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article_slider_id' => 'Article Slider ID',
            'title' => 'Название',
            'description' => 'Текст',
            'order' => 'Сортировка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleSlider()
    {
        return $this->hasOne(ArticleSlider::className(), ['id' => 'article_slider_id']);
    }
}
