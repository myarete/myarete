<?php

namespace common\models;

use Yii;


class BonusQuestion extends \yii\db\ActiveRecord
{
    use \common\models\traits\UploadTrait;

    public static function tableName()
    {
        return 'bonus_question';
    }

    public function rules()
    {
        return [
            [['poll_id', 'question'], 'required'],
            [['poll_id', 'type', 'is_required'], 'integer'],
            [['question', 'description'], 'string'],
            [['poll_id'], 'exist', 'skipOnError' => true, 'targetClass' => BonusPoll::className(), 'targetAttribute' => ['poll_id' => 'id']],
            ['video', 'string', 'max' => 255],
            ['videoFile', 'file', 'skipOnEmpty' => true, 'extensions' => ['mp4']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'poll_id' => 'Poll ID',
            'question' => 'Вопрос',
            'description' => 'Пояснение',
            'type' => 'Тип',
            'is_required' => 'Обязательный',
            'video' => 'Видео'
        ];
    }

    public function getBonusAnswers()
    {
        return $this->hasMany(BonusAnswer::className(), ['question_id' => 'id']);
    }

    public function getPoll()
    {
        return $this->hasOne(BonusPoll::className(), ['id' => 'poll_id']);
    }

    public function getBonusStatValues()
    {
        return $this->hasMany(BonusStatValue::className(), ['question_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        $this->uploadVideo('@admin/web/upload/bonus/', '@online/web/upload/bonus/');

        return parent::beforeSave($insert);
    }
}
