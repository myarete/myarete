<?php

namespace common\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "article_comment".
 *
 * @property integer $id
 * @property integer $article_id
 * @property integer $user_id
 * @property string $text
 * @property string $create_dt
 */
class ArticleComment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'user_id', 'text', 'create_dt'], 'required'],
            [['article_id', 'user_id'], 'integer'],
            [['text'], 'string'],
            [['create_dt', 'visible'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Создатель',
            'article_id' => 'Статья',
            'text' => 'Текст',
            'create_dt' => 'Время создания',
            'visible' => 'Показать'
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }
}
