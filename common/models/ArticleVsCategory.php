<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "article_vs_category".
 *
 * @property string $article_id
 * @property string $article_category_id
 *
 * @property ArticleCategory $articleCategory
 * @property Article $article
 */
class ArticleVsCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_vs_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'article_category_id'], 'required'],
            [['article_id', 'article_category_id'], 'integer'],
            [['article_id', 'article_category_id'], 'unique', 'targetAttribute' => ['article_id', 'article_category_id'], 'message' => 'The combination of Article ID and Article Category ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'article_id' => 'Статья',
            'article_category_id' => 'Категория',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategory()
    {
        return $this->hasOne(ArticleCategory::className(), ['id' => 'article_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    public function getUserVsCategory()
    {
        return $this->hasMany(UserVsCategory::className(), ['article_category_id' => 'article_category_id']);
        //->joinWith('userVsArticle');;

    }
}
