<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * ExerciseSearch represents the model behind the search form about `app\modules\exercise\models\Exercise`.
 */
class ExerciseVariantSearch extends ExerciseVariant
{
    /**
     * @var int
     */
    public $subject_id;

    /**
     * @var int
     */
    public $is_approved;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class', 'complexity', 'theme_id', 'subject_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param $orderBy
     * @return ActiveDataProvider
     */
    public function search($params, $orderBy = null)
    {
        $query = self::find();
        $query->joinWith([
            'exercise' => function (ActiveQuery $query) {
                $query->with('subject');
            },
            'theme',
        ]);

        if ($orderBy !== null)
            $query->orderBy($orderBy);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'class' => $this->class,
            'complexity' => $this->complexity,
            'theme_id' => $this->theme_id,
            'exercise.subject_id' => $this->subject_id,
            'exercise.is_approved' => $this->is_approved,
        ]);

        return $dataProvider;
    }
}
