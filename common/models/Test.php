<?php

namespace common\models;

use common\models\Subject;
use Yii;
use common\models\User;

/**
 * This is the model class for table "test".
 *
 * @property string $id
 * @property string $name
 * @property string $question
 * @property string $type_id
 * @property string $subject_id
 * @property integer $is_approved
 * @property string $user_id
 *
 * @property TestType $type
 * @property Subject $subject
 * @property User $user
 * @property TestAnswer[] $testAnswers
 * @property TestVariant[] $testVariants
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],

            ['question', 'required'],
            ['question', 'string'],

            ['type_id', 'required'],
            ['type_id', 'exist', 'targetClass' => TestType::className(), 'targetAttribute' => 'id'],

            ['subject_id', 'required'],
            ['subject_id', 'exist', 'targetClass' => Subject::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'question' => 'Вопрос',
            'type_id' => 'Тип',
            'subject_id' => 'Предмет',
            'is_approved' => 'Одобрено',
            'user_id' => 'Создатель',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TestType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(TestAnswer::className(), ['test_id' => 'id']);
    }

    public function getRightAnswer()
    {
        return $this->hasOne(TestAnswer::className(), ['test_id' => 'id'])->where(['is_right' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariants()
    {
        return $this->hasMany(TestVariant::className(), ['test_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRate()
    {
        return $this->hasMany(TestRating::className(), ['test_id' => 'id']);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->id;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function allowEdit()
    {
        if (Yii::$app->user->can('admin'))
            return true;

        if (!$this->is_approved)
            return true;

        return Yii::$app->user->id == $this->user_id;
    }
}
