<?php

namespace common\models;

use common\models\Exercise;
use Yii;

/**
 * This is the model class for table "training_sale_vs_exercise".
 *
 * @property integer $id
 * @property integer $training_id
 * @property integer $exercise_id
 * @property string $date_create
 *
 * @property TrainingSale $training
 * @property Exercise $exercise
 */
class TrainingSaleVsExercise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training_sale_vs_exercise';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['training_id', 'exercise_id'], 'required'],
            [['training_id', 'exercise_id'], 'integer'],

            ['answer', 'filter', 'filter' => 'trim'],
            ['answer', 'filter', 'filter' => 'strip_tags'],
            ['answer', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'training_id' => 'Training',
            'exercise_id' => 'Exercise',
            'date_create' => 'Date Create',
            'answer' => 'Решение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingSale()
    {
        return $this->hasOne(TrainingSale::className(), ['id' => 'training_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExercise()
    {
        return $this->hasOne(Exercise::className(), ['id' => 'exercise_id']);
    }
}
