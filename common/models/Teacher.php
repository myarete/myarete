<?php

namespace common\models;

use common\components\behaviors\ImageBehavior;
use Yii;

/**
 * This is the model class for table "teacher".
 *
 * @property integer $id
 * @property string $fio
 * @property string $image
 * @property string $description
 * @property string $contacts
 */
class Teacher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teacher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['fio', 'required', 'message' => 'Обязательно к заполнению'],
            ['fio', 'filter', 'filter' => 'trim'],
            ['fio', 'default', 'value' => null],

            ['description', 'filter', 'filter' => 'trim'],
            ['description', 'default', 'value' => null],

            ['contacts', 'filter', 'filter' => 'trim'],
            ['contacts', 'default', 'value' => null],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'image' => 'Фото',
            'description' => 'Описание',
            'contacts' => 'Контакты',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'imageBehavior' => [
                'class' => ImageBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects()
    {
        return $this->hasMany(TeacherVsSubject::className(), ['teacher_id' => 'id']);
    }
}
