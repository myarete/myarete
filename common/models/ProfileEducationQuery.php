<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[profileEducation]].
 *
 * @see profileEducation
 */
class ProfileEducationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return profileEducation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return profileEducation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
