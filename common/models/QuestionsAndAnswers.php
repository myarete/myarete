<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "questions_and_answers".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $question
 * @property string $answer
 */
class QuestionsAndAnswers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questions_and_answers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'question', 'answer'], 'required'],
            [['page_id'], 'integer'],
            [['question', 'answer'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPages()
    {
        return $this->hasOne(Pages::className(), ['id' => 'page_id']);
    }
}
