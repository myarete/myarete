<?php

namespace common\models;

use common\models\Teacher;
use Yii;

/**
 * This is the model class for table "affiliate".
 *
 * @property integer $id
 * @property integer $name
 * @property string $description
 * @property integer $address
 */
class Affiliate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'affiliate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Обязательно к заполнению'],
            ['name', 'string', 'max' => 255],

            ['description', 'required', 'message' => 'Обязательно к заполнению'],
            ['description', 'required'],

            ['address', 'required', 'message' => 'Обязательно к заполнению'],
            ['address', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'address' => 'Адрес',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAffiliateVsTeacher()
    {
        return $this->hasMany(AffiliateVsTeacher::className(), ['affiliate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeachers()
    {
        return $this->hasMany(Teacher::className(), ['id' => 'teacher_id'])->via('affiliateVsTeacher');
    }
}
