<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

/**
 * @property integer $id
 * @property integer $pid
 * @property string $title
 * @property string $content
 * @property string $publish_status
 * @property integer $post_id
 * @property integer $author_id
 *
 * @property Post $post
 * @property User $author
 */
class Comment extends ActiveRecord
{
    const STATUS_MODERATE = 'moderate';

    const STATUS_PUBLISH = 'publish';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pid', 'post_id', 'author_id'], 'integer'],
            [['title', 'content'], 'required'],
            [['publish_status'], 'string'],
            [['title', 'content'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'pid' => Yii::t('backend', 'Pid'),
            'title' => Yii::t('backend', 'Title'),
            'content' => Yii::t('backend', 'Content'),
            'publish_status' => Yii::t('backend', 'Publish status'),
            'post_id' => Yii::t('backend', 'Post ID'),
            'author_id' => Yii::t('backend', 'Author ID'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @param int $id
     * @return Comment
     * @throws NotFoundHttpException
     */
    public function getComment($id)
    {
        if (
            ($model = Comment::findOne($id)) !== null &&
            $model->isPublished()
        ) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested post does not exist.');
        }
    }

    /**
     * @return bool
     */
    protected function isPublished()
    {
        return $this->publish_status === self::STATUS_PUBLISH;
    }
}
