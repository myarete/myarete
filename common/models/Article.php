<?php

namespace common\models;

use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "article".
 *
 * @property string $id
 * @property string $user_id
 * @property string $title
 * @property string $description
 * @property string $date_created
 * @property string $date_updated
 * @property integer $is_approved
 *
 * @property ArticleCategory[] $articleCategories
 */
class Article extends \yii\db\ActiveRecord
{
    use \common\models\traits\UploadTrait;

    const SUCCESS_HIDE = 0;
    const SUCCESS_TEACHER = 1;
    const SUCCESS_ALL = 2;

    public $category_list;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_created',
                'updatedAtAttribute' => 'date_updated',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'price'], 'required'],
            [['user_id', 'is_approved'], 'integer'],
            [['description', 'description_short'], 'string'],
            [['date_created', 'date_updated', 'category_list', 'image', 'price', 'is_action', 'action_price', 'action_date_start', 'action_date_end'], 'safe'],
            [['title'], 'string', 'max' => 255],
            ['imageFile', 'image', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Создатель',
            'title' => 'Название',
            'description' => 'Содержание',
            'description_short' => 'Краткое описание',
            'imageFile' => 'Изображение',
            'date_created' => 'Дата создания',
            'date_updated' => 'Дата изменения',
            'is_approved' => 'Доступ',
            'category_list' => '',
            'price' => 'Цена',
            'is_action' => 'Акция',
            'action_price' => 'Акционная цена',
            'action_date_start' => 'Начало акции ',
            'action_date_end' => 'Завершение акции ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleVsCategories()
    {
        return $this->hasMany(ArticleVsCategory::className(), ['article_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategories()
    {
        return $this->hasMany(ArticleCategory::className(), ['id' => 'article_category_id'])->viaTable('article_vs_category', ['article_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->id;
        }

        $this->uploadImage('@admin/web/upload/article/', '@online/web/upload/article/');

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {

        $this->unlinkAll('articleCategories', true);

        $cat_arr = explode(',', $this->category_list);

        if (!empty($this->category_list) && !empty($cat_arr)) {
            foreach ($cat_arr as $cat_id) {
                $category = ArticleCategory::findOne($cat_id);
                $this->link('articleCategories', $category);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return bool
     */
    public function allowEdit()
    {
        if (Yii::$app->user->can('teacher'))
            return true;

        return false; //Yii::$app->user->id == $this->user_id;
    }

    /**
     * @return string
     */
    public function categoriesTree()
    {
        $arr = ArrayHelper::getColumn($this->getArticleVsCategories()->select('article_category_id')->asArray()->all(), 'article_category_id');
        return implode(',', $arr);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        $articles = Yii::$app->db->createCommand('SELECT `article`.* FROM `article` LEFT JOIN `article_vs_category` ON `article`.`id` = `article_vs_category`.`article_id` LEFT JOIN `user_vs_category` ON `article_vs_category`.`article_category_id` = `user_vs_category`.`article_category_id` WHERE `user_vs_category`.`user_id` = ' . Yii::$app->user->id)->queryAll();

        return array_reduce($articles, function ($a, $b) {
            static $stored = array();

            $hash = md5(serialize($b));

            if (!in_array($hash, $stored)) {
                $stored[] = $hash;
                $a[] = $b;
            }

            return $a;
        }, array());
    }

    public function getArticleVsCategory()
    {
        return $this->hasMany(ArticleVsCategory::className(), ['article_id' => 'id'])
            ->joinWith('userVsCategory')
            ->where(['user_vs_category.user_id' => Yii::$app->user->id]);

    }

    public function getUserVsArticle()
    {
        return $this->hasMany(UserVsArticle::className(), ['article_id' => 'id'])
            ->where(['user_vs_article.user_id' => Yii::$app->user->id]);

    }
}
