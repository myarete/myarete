<?php

namespace common\models;

use common\components\helpers\FormatHelper;
use Yii;

/**
 * This is the model class for table "call_request".
 *
 * @property integer $id
 * @property string $fio
 * @property string $phone
 * @property string $date_create
 */
class CallRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'call_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['fio', 'required', 'message' => 'Обязательно к заполнению'],
            ['fio', 'string', 'max' => 100],

            ['phone', 'required', 'message' => 'Обязательно к заполнению'],
            ['phone', 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'phone' => 'Телефон',
            'date_create' => 'Дата',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->phone = FormatHelper::purifyPhone($this->phone);
        return parent::beforeSave($insert);
    }
}
