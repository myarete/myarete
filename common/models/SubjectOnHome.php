<?php

namespace common\models;

use common\components\behaviors\ImageBehavior;
use common\models\TeacherVsSubject;
use Yii;

/**
 * This is the model class for table "subject_on_home".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $image
 * @property integer $is_active
 */
class SubjectOnHome extends \yii\db\ActiveRecord
{
    use \common\models\traits\UploadTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subject_on_home';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Не должно быть пустым'],
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'unique', 'message' => 'Уже занято'],
            ['name', 'string', 'max' => 255],

            ['alias', 'required', 'message' => 'Не должно быть пустым'],
            ['alias', 'filter', 'filter' => 'trim'],
            ['alias', 'unique', 'message' => 'Уже занято'],
            ['alias', 'string', 'max' => 20],

            ['description', 'filter', 'filter' => 'trim'],
            ['description', 'default', 'value' => null],
            ['imageFile', 'image', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Алиас',
            'description' => 'Описание',
            'imageFile' => 'Фото',
            'is_active' => 'Статус'
        ];
    }

    /*public function behaviors()
    {
        return [
            'imageBehavior' => [
                'class' => ImageBehavior::className(),
            ],
        ];
    }*/


    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->image = 'default.png';
        }

        $this->uploadImage('@admin/web/upload/subjectonhome/', '@frontend/web/upload/subjectonhome/');

        return parent::beforeSave($insert);
    }

    public static function getSubject($subjectId)
    {
        $model = self::find()->where(['id' => $subjectId])->one();
        if ($model === null)
            return null;

        return $model;
    }

    public function getTeachers()
    {
        return $this->hasMany(TeacherVsSubject::className(), ['subject_id' => 'id']);
    }
}
