<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class ArticleSliderSearch extends ArticleSlider
{
    public $email;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
            [['email'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();
        $query->joinWith([
            'user'
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date_create' => SORT_ASC,
                ],
            ],
        ]);

        $dataProvider->sort->attributes['email'] = [
            'asc' => ['user.email' => SORT_ASC],
            'desc' => ['user.email' => SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['article_slider.id' => $this->id])
            ->andFilterWhere(['like', 'article_slider.name', $this->title])
            ->andFilterWhere(['like', 'user.email', $this->email]);

        return $dataProvider;
    }
}
