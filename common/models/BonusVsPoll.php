<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bonus_vs_poll".
 *
 * @property int $id
 * @property int $bonus_id
 * @property int $poll_id
 *
 * @property Bonus $bonus
 * @property BonusPoll $poll
 */
class BonusVsPoll extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bonus_vs_poll';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bonus_id', 'poll_id'], 'required'],
            [['bonus_id', 'poll_id'], 'integer'],
            [['bonus_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bonus::className(), 'targetAttribute' => ['bonus_id' => 'id']],
            [['poll_id'], 'exist', 'skipOnError' => true, 'targetClass' => BonusPoll::className(), 'targetAttribute' => ['poll_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bonus_id' => 'Bonus ID',
            'poll_id' => 'Poll ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonus()
    {
        return $this->hasOne(Bonus::className(), ['id' => 'bonus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll()
    {
        return $this->hasOne(BonusPoll::className(), ['id' => 'poll_id']);
    }
}
