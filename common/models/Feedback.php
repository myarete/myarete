<?php

namespace common\models;

use common\components\behaviors\ImageBehavior;
use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property integer $id
 * @property string $fio
 * @property string $image
 * @property string $content
 * @property integer $is_published
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            ['fio', 'filter', 'filter' => 'trim'],
            ['fio', 'required', 'message' => 'Обязательно к заполнению'],
            ['fio', 'string', 'max' => 255],

            ['content', 'filter', 'filter' => 'trim'],
            ['content', 'required', 'message' => 'Обязательно к заполнению'],

            ['is_published', 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'image' => 'Фото',
            'content' => 'Отзыв',
            'is_published' => 'Опубликован?',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'imageBehavior' => [
                'class' => ImageBehavior::className(),
            ],
        ];
    }
}
