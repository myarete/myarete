<?php

namespace common\models;

use Yii;

class BonusStat extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'bonus_stat';
    }

    public function rules()
    {
        return [
            [['user_id', 'bonus_id'], 'required'],
            [['user_id', 'bonus_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['bonus_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bonus::className(), 'targetAttribute' => ['bonus_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'bonus_id' => 'Bonus ID',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUser0()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }

    public function getBonus()
    {
        return $this->hasOne(Bonus::className(), ['id' => 'bonus_id']);
    }

    public function getFullUserName()
    {
        return $this->user0->first_name . ' ' . $this->user0->last_name;
    }

    public function getUserEmail()
    {
        return $this->user->email;
    }

    public function getBonusStatValues()
    {
        return $this->hasMany(BonusStatValue::className(), ['stat_id' => 'id']);
    }
}
