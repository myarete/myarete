<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SubjectsSchedule;

/**
 * SubjectsScheduleSearch represents the model behind the search form about `common\models\SubjectsSchedule`.
 */
class SubjectsScheduleSearch extends SubjectsSchedule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'subject_id', 'class_name'], 'integer'],
            [['price_type', 'day', 'time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubjectsSchedule::find()->joinWith('subject')->joinWith('pricesOnHome');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'subject_id' => $this->subject_id,
            'time' => $this->time,
            'class_name' => $this->class_name,
        ]);

        $query->andFilterWhere(['like', 'price_type', $this->price_type])
            ->andFilterWhere(['like', 'day', $this->day]);

        return $dataProvider;
    }
}
