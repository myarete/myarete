<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Promocodes]].
 *
 * @see Promocodes
 */
class PromocodesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Promocodes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Promocodes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
