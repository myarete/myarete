<?php

namespace common\models;

use Yii;
use yii\base\Model;

class ContactForm extends Model
{
    public $name, $email, $phone, $body;
    public $subject = 'Письмо свяжитесь с нами';

    public function rules()
    {
        return [
            [['name', 'email', 'body'], 'required'],
            ['email', 'email'],
            [['phone'], 'safe']
            //['verifyCode', 'captcha', 'captchaAction'=>'index/captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            //'verifyCode' => 'Подтвердите код',
            'name' => 'Как Вас зовут?',
            'email' => 'Ваш e-mail',
            'phone' => 'Ваш номер телефона',
            'body' => 'Здесь вы можете задать интересующий вас вопрос',
        ];
    }

    public function contact($emailto, $emailFrom)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setFrom($emailFrom)
                ->setTo($emailto)
                ->setSubject($this->subject)
                ->setTextBody("\n Почтовый адрес : " . $this->email . "\n Телефон : " . $this->phone . "\n Сообщение : " . $this->body)
                ->send();

            return true;
        } else {
            return false;
        }
    }
    /*
        public function contact($emailto)
        {
            $model = new RegistrationForm;
            if ($model->load(Yii::$app->request->post()) && $model->registration())
            {
                //Yii::$app->session->setFlash('success', 'Ссылка для подтверждения регистрации была отправлена на Ваш E-mail');
                Yii::$app->mailer->compose()
                    ->setFrom(Yii::$app->params['noreplyEmail'])
                    ->setTo(Yii::$app->params['contactsEmail'])
                    ->setSubject('Зарегистрирован пользователь')
                    ->setTextBody('Зарегистрирован новый пользователь '.$model->email."\nhttp://pa.myarete.com/user/default/index")
                    ->send();

                return $this->redirect(Url::home());
            }
        }*/
}