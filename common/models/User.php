<?php

namespace common\models;

use nodge\eauth\ErrorException;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $role
 * @property string $auth_key
 * @property string $confirm_code
 * @property int $is_confirmed
 *
 * @property Profile $profile
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     *
     */
    const PASSWORD_MIN_LENGTH = 5;
    const LOGGED_DURATION = 1296000;

    /**
     * Roles
     */
    const ROLE_CLIENT = 'client';
    const ROLE_FREE = 'free';
    const ROLE_TEACHER = 'teacher';
    const ROLE_DESIGNER = 'designer';
    const ROLE_ADMIN = 'admin';
    const ROLE_PREMIUMTUTOR = 'premiumtutor';
    const ROLE_TL = 'tl';

    /**
     * @var array
     */
    public static $roleLabels = [
        self::ROLE_CLIENT => 'Клиент',
        self::ROLE_FREE => 'Бесплатный пользователь',
        self::ROLE_TEACHER => 'Наш учитель',
        self::ROLE_DESIGNER => 'Дизайнер',
        self::ROLE_ADMIN => 'Админ',
        self::ROLE_PREMIUMTUTOR => 'Премиум репетитор',
        self::ROLE_TL => 'Учитель-ученик',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            [['email', 'password'], 'required'],
            ['email', 'string'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'unique']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'email' => 'E-mail',
            'role' => 'Роль',
        ];
    }

    /**
     * @var array EAuth attributes
     */
    public static function findIdentity($id)
    {

        if (Yii::$app->getSession()->has('user-' . $id)) {
            return Yii::$app->getSession()->get('user-' . $id);
        } else {
            return static::findOne($id);
        }
    }
    /**
     * @param $id
     * @return null|static
     */
//     public static function findIdentity($id)
//     {
//         return static::findOne($id);
//     }

    /**
     * @param mixed $token
     * @param null $type
     * @return null|static
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * @param $password
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function setPassword($password)
    {
        $this->password = \Yii::$app->security->generatePasswordHash($password);
    }

    /**
     *
     */
    public function generateAuthKey()
    {
        $this->auth_key = \Yii::$app->security->generateRandomString();
    }

    /**
     *
     */
    public function generateConfirmCode()
    {
        $this->confirm_code = md5(uniqid() . \Yii::$app->params['salt']);
    }

    /**
     * @param $password
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRate()
    {
        return $this->hasMany(ExerciseRating::className(), ['user_id' => 'id']);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        Exercise::deleteAll(['user_id' => $this->id]);

        return parent::beforeDelete();
    }

    /**
     * @param \nodge\eauth\ServiceBase $service
     * @return User
     * @throws ErrorException
     */
    public static function findByEAuth($service)
    {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        return self::find()->where(['service' => $service->getServiceName(), 'social_id' => $service->getId()])->one();
    }

    public static function findByEmail($email)
    {
        return self::find()->where(['email' => $email])->one();
    }

    public static function getAll()
    {
        $users = self::find()->where('role="admin" OR role="teacher"')->all();

        return ArrayHelper::map($users, 'id', 'email');
    }

    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['user_id' => 'id']);
    }
}
