<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 17.07.15
 * Time: 23:42
 */

namespace common\models;

use Yii;

class TrainingCategory extends \kartik\tree\models\Tree
{
    use \common\models\traits\UploadTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['price', 'status'], 'required'];
        $rules[] = [['description_short', 'image', 'price', 'status'], 'safe'];
        $rules[] = ['imageFile', 'image', 'skipOnEmpty' => true];

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['price'] = 'Цена';
        $labels['status'] = 'Статус';
        $labels['imageFile'] = 'Изображение';
        $labels['description_short'] = 'Краткое описание';

        return $labels;
    }

    public function beforeSave($insert)
    {
        switch ($this->lvl) {
            case 0:
                $this->icon = 'folder';
                break;
            case 1:
                $this->icon = 'folder-o';
                break;
            case 2:
                $this->icon = 'file';
                break;
            case 3:
                $this->icon = 'file-o';
                break;
        }

        $this->collapsed = 1;

        $this->uploadImage('@admin/web/upload/training/', '@online/web/upload/training/');

        return parent::beforeSave($insert);
    }

    public static function getRoots()
    {
        return static::find()
            ->roots()
            ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingVsCategories()
    {
        return $this->hasMany(TrainingVsCategory::className(), ['training_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainings()
    {
        return $this->hasMany(Training::className(), ['id' => 'training_id'])->viaTable('training_vs_category', ['training_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVsTrainingCategory()
    {
        return $this->hasMany(UserVsTrainingCategory::className(), ['root' => 'id']);
    }
}