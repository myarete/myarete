<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prices_vs_category".
 *
 * @property integer $id
 * @property integer $price_id
 * @property integer $category_id
 */
class PricesVsCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prices_vs_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price_id', 'category_id'], 'required'],
            [['price_id', 'category_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_id' => 'Цена на главной',
            'category_id' => 'Ценовая категория',
        ];
    }

    public function getPrices()
    {
        return $this->hasOne(PricesOnHome::className(), ['id' => 'price_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(PricesCategory::className(), ['id' => 'category_id']);
    }
}
