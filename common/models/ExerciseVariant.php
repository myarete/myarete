<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "exercise_variant".
 *
 * @property integer $id
 * @property integer $exercise_id
 * @property integer $theme_id
 * @property integer $complexity
 *
 * @property Exercise $exercise
 * @property Theme $theme
 */
class ExerciseVariant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exercise_variant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['exercise_id', 'required', 'message' => 'Необходимо заполнить'],

            ['theme_id', 'required', 'message' => 'Необходимо заполнить'],
            [
                'theme_id',
                'exist',
                'targetClass' => Theme::className(),
                'targetAttribute' => 'id',
                'filter' => ['lvl' => Theme::LVL_SUBTHEME],
                'message' => 'Необходимо выбрать подтему'
            ],

            ['complexity', 'required', 'message' => 'Необходимо заполнить'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'exercise_id' => 'Задача',
            'theme_id' => 'Тема',
            'complexity' => 'Сложность',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExercise()
    {
        return $this->hasOne(Exercise::className(), ['id' => 'exercise_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheme()
    {
        return $this->hasOne(Theme::className(), ['id' => 'theme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExerciseFavorite()
    {
        return $this->hasOne(ExerciseVariantFavorite::className(), ['exercise_variant_id' => 'id'])
            ->where(['user_id' => Yii::$app->user->id]);
    }
}
