<?php

namespace common\models;

use common\models\Subject;
use common\models\User;
use Yii;
use common\models\TrainingSaleVsExercise;

/**
 * This is the model class for table "exercise".
 *
 * @property integer $id
 * @property string $question
 * @property string $decision
 * @property string $decision_detailed
 * @property string $decision_second
 * @property string $answer
 * @property string $hint
 * @property string $hint_second
 * @property string $hint_third
 * @property integer $type_id
 * @property integer $subject_id
 * @property integer $is_approved
 * @property string $source
 * @property string $name
 * @property int $user_id
 *
 * @property ExerciseType $type
 */
class Exercise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exercise';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],

            ['question', 'required'],
            ['question', 'string'],

            ['hint', 'string'],
            ['hint_second', 'string'],
            ['hint_third', 'string'],

            ['decision', 'string'],
            ['decision_detailed', 'string'],
            ['decision_second', 'string'],

            ['answer', 'string'],

            ['type_id', 'required'],
            ['type_id', 'exist', 'targetClass' => ExerciseType::className(), 'targetAttribute' => 'id'],

            ['subject_id', 'required'],
            ['subject_id', 'exist', 'targetClass' => Subject::className(), 'targetAttribute' => 'id'],

            ['source', 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'name' => 'Название',
            'question' => 'Условие',
            'decision' => 'Решение',
            'decision_detailed' => 'Подробное решение',
            'decision_second' => 'Второе решение',
            'answer' => 'Ответ',
            'hint' => 'Подсказка',
            'hint_second' => 'Вторая подсказка',
            'hint_third' => 'Третья подсказка',
            'type_id' => 'Тип',
            'subject_id' => 'Предмет',
            'source' => 'Источник',
            'is_approved' => 'Одобрено',
            'user_id' => 'Создатель',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(ExerciseType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariants()
    {
        return $this->hasMany(ExerciseVariant::className(), ['exercise_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRate()
    {
        return $this->hasMany(ExerciseRating::className(), ['exercise_id' => 'id']);
    }


    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->id;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return bool
     */
    public function allowEdit()
    {
        if (Yii::$app->user->can('admin'))
            return true;

        if (!$this->is_approved)
            return true;

        return Yii::$app->user->id == $this->user_id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingSaleVsExercise()
    {
        return $this->hasMany(TrainingSaleVsExercise::className(), ['exercise_id' => 'id']);
    }
}
