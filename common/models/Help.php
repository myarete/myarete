<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "help".
 *
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property string $content
 */
class Help extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'required', 'message' => 'Не должно быть пустым'],

            ['code', 'required', 'message' => 'Не должно быть пустым'],
            ['code', 'unique', 'message' => 'Уже занят'],
            ['code', 'match', 'pattern' => '~^[_a-z\d]+$~', 'message' => 'Только латинские буквы и нижние подчеркивания (пример: price_block)'],

            ['content', 'required', 'message' => 'Не должно быть пустым'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'code' => 'Код для вставки',
            'content' => 'Содержание',
        ];
    }

    /**
     * @param $code
     * @return string
     */
    public static function get($code)
    {
        $model = self::find()->where(['code' => $code])->one();
        if ($model === null)
            return '';

        return $model->content;
    }

    /**
     * @param $code
     * @return object
     */
    public static function getModel($code)
    {
        $model = self::find()->where(['code' => $code])->one();
        if ($model === null)
            return null;

        return $model;
    }
}
