<?php

namespace common\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "user_vs_category".
 *
 * @property integer $user_id
 * @property integer $article_category_id
 *
 * @property ArticleCategory $articleCategory
 * @property User $user
 */
class UserVsCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_vs_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'article_category_id'], 'required'],
            [['user_id', 'article_category_id'], 'integer'],
            [['article_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArticleCategory::className(), 'targetAttribute' => ['article_category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'article_category_id' => 'Article Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserVsArticle()
    {
        return $this->hasMany(UserVsArticle::className(), ['category_id' => 'article_category_id']);

    }

}
