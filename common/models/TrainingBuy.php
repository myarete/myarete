<?php

namespace common\models;

use common\models\Exercise;
use common\models\ExerciseVariant;
use common\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "training_buy".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $name
 *
 * @property User $user
 */
class TrainingBuy extends \yii\db\ActiveRecord
{
    use \common\models\traits\UploadTrait;

    const SUCCESS_HIDE = 0;
    const SUCCESS_TEACHER = 1;
    const SUCCESS_ALL = 2;

    public $category_list;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training_buy';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_created',
                'updatedAtAttribute' => 'date_updated',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price'], 'required'],
            [['user_id', 'is_approved'], 'integer'],
            [['description', 'description_short', 'order_by'], 'string'],
            [['date_created', 'date_updated', 'image', 'price', 'category_list', 'order_by', 'is_action', 'action_price', 'action_date_start', 'action_date_end'], 'safe'],
            [['name'], 'string', 'max' => 255],
            ['imageFile', 'image', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Создатель',
            'name' => 'Название',
            'description' => 'Содержание',
            'description_short' => 'Краткое описание',
            'imageFile' => 'Изображение',
            'date_created' => 'Дата создания',
            'date_updated' => 'Дата изменения',
            'is_approved' => 'Доступ',
            'order_by' => 'Номера задач',
            'category_list' => '',
            'price' => 'Цена',
            'is_action' => 'Акция',
            'action_price' => 'Акционная цена',
            'action_date_start' => 'Начало акции ',
            'action_date_end' => 'Завершение акции ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingVsCategories()
    {
        return $this->hasMany(TrainingVsCategory::className(), ['training_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingCategories()
    {
        return $this->hasMany(TrainingCategory::className(), ['id' => 'training_category_id'])->viaTable('training_vs_category', ['training_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->id;
        }

        $this->uploadImage('@admin/web/upload/training/', '@online/web/upload/training/');

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {

        $this->unlinkAll('trainingCategories', true);

        $cat_arr = explode(',', $this->category_list);

        if (!empty($this->category_list) && !empty($cat_arr)) {
            foreach ($cat_arr as $cat_id) {
                $category = TrainingCategory::findOne($cat_id);
                $this->link('trainingCategories', $category);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return bool
     */
    public function allowEdit()
    {
        if (Yii::$app->user->can('teacher'))
            return true;

        return false; //Yii::$app->user->id == $this->user_id;
    }

    /**
     * @return string
     */
    public function categoriesTree()
    {
        $arr = ArrayHelper::getColumn($this->getTrainingVsCategories()->select('training_category_id')->asArray()->all(), 'training_category_id');
        return implode(',', $arr);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVsTrainingBuy()
    {
        return $this->hasMany(UserVsTrainingBuy::className(), ['training_id' => 'id']);
    }

    public function getTrainingVsCategory()
    {
        return $this->hasMany(TrainingVsCategory::className(), ['training_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingExercises()
    {
        $exercise_variants = [];

        if (!empty($this->order_by)) { // если задан порядок задач, то массив заполняется
            foreach (explode(',', $this->order_by) as $id) {
                $id = substr($id, 3);
                if ($variant = TrainingBuyVsExerciseVariant::findOne($id))
                    $exercise_variants[] = $variant;
            }

            return $exercise_variants;
        }

        return $this->hasMany(TrainingBuyVsExerciseVariant::className(), ['training_id' => 'id']);
    }


    public function getTrainingVsCategoryFilter()
    {
        return $this->hasMany(TrainingVsCategory::className(), ['training_id' => 'id'])
            ->joinWith('userVsTrainingCategory')
            ->where(['user_vs_training_category.user_id' => Yii::$app->user->id]);

    }

    public function getUserVsTrainingBuyFilter()
    {
        return $this->hasMany(UserVsTrainingBuy::className(), ['training_id' => 'id'])
            ->where(['user_vs_training_buy.user_id' => Yii::$app->user->id]);

    }

}
