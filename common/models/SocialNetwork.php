<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "social_network".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $url
 *
 * @property User $user
 */
class SocialNetwork extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'social_network';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['url', 'filter', 'filter' => 'trim'],
            ['url', 'filter', 'filter' => 'strip_tags'],
            ['url', 'required'],
            ['url', 'string', 'max' => 255],

            ['user_id', 'required'],
            ['user_id', 'exist', 'targetClass' => '\common\models\User', 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'url' => 'Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
