<?php

namespace common\models;

use Yii;

class BonusStatValue extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'bonus_stat_value';
    }

    public function rules()
    {
        return [
            [['stat_id', 'poll_id', 'question_id', 'answer_id'], 'required'],
            [['stat_id', 'poll_id', 'question_id', 'answer_id'], 'integer'],
            [['video_time'], 'number'],
            [['created_dt'], 'safe'],
            [['text'], 'string', 'max' => 255],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => BonusQuestion::className(), 'targetAttribute' => ['question_id' => 'id']],
            [['answer_id'], 'exist', 'skipOnError' => true, 'targetClass' => BonusAnswer::className(), 'targetAttribute' => ['answer_id' => 'id']],
            [['stat_id'], 'exist', 'skipOnError' => true, 'targetClass' => BonusStat::className(), 'targetAttribute' => ['stat_id' => 'id']],
            [['poll_id'], 'exist', 'skipOnError' => true, 'targetClass' => BonusPoll::className(), 'targetAttribute' => ['poll_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Question ID',
            'answer_id' => 'Answer ID',
            'text' => 'Text',
            'video_time' => 'Video Time',
            'created_dt' => 'Created Dt',
        ];
    }

    public function getBonusStats()
    {
        return $this->hasMany(BonusStat::className(), ['value_id' => 'id']);
    }

    public function getQuestion()
    {
        return $this->hasOne(BonusQuestion::className(), ['id' => 'question_id']);
    }

    public function getAnswer()
    {
        return $this->hasOne(BonusAnswer::className(), ['id' => 'answer_id']);
    }
}
