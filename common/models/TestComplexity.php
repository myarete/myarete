<?php

namespace common\models;

use Yii;

class TestComplexity
{
    /**
     * @return array
     */
    public static function getAll()
    {
        $range = range(1, 9);
        return array_combine($range, $range);
    }
}
