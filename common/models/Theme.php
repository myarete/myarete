<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 17.07.15
 * Time: 23:42
 */

namespace common\models;

use common\models\Subject;
use yii\helpers\Html;

class Theme extends \kartik\tree\models\Tree
{
    /**
     *
     */
    const LVL_CLASS = 0;
    const LVL_SECTION = 1;
    const LVL_THEME = 2;
    const LVL_SUBTHEME = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'theme';
    }

    public function beforeSave($insert)
    {
        switch ($this->lvl) {
            case 0:
                $this->icon = 'folder';
                break;
            case 1:
                $this->icon = 'folder-o';
                break;
            case 2:
                $this->icon = 'file';
                break;
            case 3:
                $this->icon = 'file-o';
                break;
        }

        $this->collapsed = 1;

        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->refresh();

        if (empty($this->subject_id) && empty($this->class)) {
            $parent = $this->parents(1)->one();
            if ($parent !== null) {
                $this->subject_id = $parent->subject_id;
                $this->class = $parent->class;
                $this->update(false, ['subject_id', 'class']);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        $parts = [];
        foreach ($this->parents()->all() as $parent) {
            $parts[] = Html::tag('span', $parent->name, ['class' => 'text-muted']);
        }
        $parts[] = $this->name;

        return implode(' / ', $parts);
    }
}