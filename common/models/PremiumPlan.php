<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "premium_plan".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $price
 * @property string $validity
 * @property integer $is_approved
 * @property string $dt
 */
class PremiumPlan extends \yii\db\ActiveRecord
{
    use \common\models\traits\UploadTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'premium_plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'price', 'validity'], 'required'],
            [['description', 'validity', 'image'], 'string'],
            [['price'], 'number'],
            [['is_approved'], 'integer'],
            [['dt', 'image'], 'safe'],
            [['name', 'image'], 'string', 'max' => 255],
            ['imageFile', 'image', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'description' => 'Описание',
            'price' => 'Цена',
            'validity' => 'Срок действия',
            'is_approved' => 'Одобрен',
            'dt' => 'Дата/время',
            'imageFile' => 'Изображение',
        ];
    }

    public function beforeSave($insert)
    {
        $this->uploadImage('@admin/web/upload/premium/');

        return parent::beforeSave($insert);
    }
}
