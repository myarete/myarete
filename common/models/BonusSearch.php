<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bonus;

class BonusSearch extends Bonus
{
    public function rules()
    {
        return [
            [['id', 'user_id', 'access', 'numbers_of_uses'], 'integer'],
            [['name', 'description', 'image', 'customer', 'date_created', 'date_updated'], 'safe'],
            [['price'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Bonus::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'price' => $this->price,
            'access' => $this->access,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'numbers_of_uses' => $this->numbers_of_uses,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'customer', $this->customer]);

        return $dataProvider;
    }
}
