<?php

namespace common\models\traits;

use Yii;
use yii\web\UploadedFile;

trait UploadTrait
{
    public $imageFile;
    public $videoFile;
    public $additionalImageFile;

    public function uploadImage($path, $additionalPath = null)
    {
        $uploadPath = Yii::getAlias($path);
        $additionalUploadPath = Yii::getAlias($additionalPath);

        $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
        if (!empty($this->imageFile) && $this->validate()) {
            $fileName = Yii::$app->security->generateRandomString(15) . '.' . $this->imageFile->extension;

            if ($this->imageFile->saveAs($uploadPath . $fileName)) {
                if (!empty($additionalUploadPath))
                    copy($uploadPath . $fileName, $additionalUploadPath . $fileName);

                if (!empty($this->image) && file_exists($uploadPath . $this->image)) {
                    unlink($uploadPath . $this->image);
                }
                $this->image = $fileName;
                $this->imageFile = null;

                return true;
            }
        }

        return false;
    }

    public function uploadVideo($path, $additionalPath = null)
    {
        $uploadPath = Yii::getAlias($path);
        $additionalUploadPath = Yii::getAlias($additionalPath);

        $this->videoFile = UploadedFile::getInstance($this, 'videoFile');
        if (!empty($this->videoFile) && $this->validate()) {
            $fileName = $this->videoFile->name;
            if ($this->videoFile->saveAs($uploadPath . $fileName)) {
                if (!empty($additionalUploadPath)) {
                    copy($uploadPath . $fileName, $additionalUploadPath . $fileName);
                }
                $this->video = $fileName;
                $this->videoFile = null;

                return true;
            }
        }

        return false;
    }

    public function getMainImage($path)
    {
        if (!empty($this->image)) {
            return Yii::getAlias($path . $this->image);
        }

        return null;
    }
}
