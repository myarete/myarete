<?php

namespace common\models;

use common\components\behaviors\ImageBehavior;
use common\models\Exercise;
use common\models\Theme;
use common\models\TeacherVsSubject;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "subject".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $image
 */
class Subject extends \yii\db\ActiveRecord
{
    use \common\models\traits\UploadTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Не должно быть пустым'],
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'unique', 'message' => 'Уже занято'],
            ['name', 'string', 'max' => 255],

            ['description', 'filter', 'filter' => 'trim'],
            ['description', 'default', 'value' => null],
            ['imageFile', 'image', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'imageFile' => 'Фото',
            'is_active' => 'Статус'
        ];
    }

    /**
     * @return array
     */
    /*public function behaviors()
    {
        return [
            'imageBehavior' => [
                'class' => ImageBehavior::className(),
            ],
        ];
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeachers()
    {
        return $this->hasMany(TeacherVsSubject::className(), ['subject_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExercise()
    {
        return $this->hasMany(Exercise::className(), ['subject_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getAll()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert)
        {
            $this->createThemes();
        }
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->user_id = Yii::$app->user->id;
        }

        $this->uploadImage('@admin/web/upload/subject/', '@online/web/upload/subject/');

        return parent::beforeSave($insert);
    }

    /**
     * @throws \Exception
     */
    public function createThemes()
    {
        $root = (int)$this->db->createCommand('select max(root) from theme')->queryScalar();
        if ($root <= 0)
            $root = 1;
        else
            $root ++;

        foreach (\online\modules\exercise\models\ExerciseClass::$classes as $class)
        {
            $theme = new Theme();
            $theme->root = $root;
            $theme->subject_id = $this->id;
            $theme->class = $class;
            $theme->name = "Класс {$class}";
            $theme->readonly = 1;
            $theme->movable_u = 0;
            $theme->movable_d = 0;
            $theme->movable_l = 0;
            $theme->movable_r = 0;
            $theme->makeRoot();

            $root ++;
        }
    }
}
