<?php

namespace common\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "chat".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $to_user_id
 * @property string $chat
 * @property string $text
 * @property string $dt
 *
 * @property User $user
 * @property User $toUser
 */
class Chat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'to_user_id', 'chat', 'text'], 'required'],
            [['user_id', 'to_user_id'], 'integer'],
            [['text'], 'string'],
            [['dt'], 'safe'],
            [['chat'], 'string', 'max' => 64],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['to_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['to_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'to_user_id' => 'To User ID',
            'chat' => 'Chat',
            'text' => 'Text',
            'dt' => 'Dt',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }

    public static function chatContents($chat)
    {
        return self::find()->where(['chat' => $chat])->orderBy('dt DESC')->all();
    }
}
