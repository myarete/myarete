<?php

namespace common\models;

use common\models\Subject;
use common\models\SubjectOnHome;
use Yii;

/**
 * This is the model class for table "register_course_list".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $subject
 */
class RegisterCourseList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'register_course_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'subject_id'], 'required'],
            [['name', 'phone', 'subject_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'subject_id' => 'Предмет',
        ];
    }

    public function getSubject()
    {
        return $this->hasOne(SubjectOnHome::className(), ['id' => 'subject_id']);
    }
}
