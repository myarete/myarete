<?php

namespace common\models;

use common\models\SubjectOnHome;
use Yii;

/**
 * This is the model class for table "prices_vs_subjects".
 *
 * @property integer $id
 * @property integer $price_id
 * @property integer $subject_id
 */
class PricesVsSubjects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prices_vs_subjects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price_id', 'subject_id'], 'required'],
            [['price_id', 'subject_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_id' => 'Цена на главной',
            'subject_id' => 'Предмет',
        ];
    }

    public function getPrices()
    {
        return $this->hasOne(PricesOnHome::className(), ['id' => 'price_id']);
    }

    public function getSubjects()
    {
        return $this->hasOne(SubjectOnHome::className(), ['id' => 'subject_id']);
    }
}
