<?php

namespace common\models;

use common\models\Theme;
use Yii;

/**
 * This is the model class for table "test_variant".
 *
 * @property string $id
 * @property string $test_id
 * @property string $theme_id
 * @property string $complexity
 * @property string $date_create
 *
 * @property Test $test
 * @property Theme $theme
 * @property TestingVsTestVariant[] $testingVsTestVariants
 */
class TestVariant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_variant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['test_id', 'required', 'message' => 'Необходимо заполнить'],

            ['theme_id', 'required', 'message' => 'Необходимо заполнить'],
            [
                'theme_id',
                'exist',
                'targetClass' => Theme::className(),
                'targetAttribute' => 'id',
                'filter' => ['lvl' => Theme::LVL_SUBTHEME],
                'message' => 'Необходимо выбрать подтему'
            ],

            ['complexity', 'required', 'message' => 'Необходимо заполнить'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_id' => 'Тест',
            'theme_id' => 'Тема',
            'complexity' => 'Сложность',
            'date_create' => 'Дата создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(Test::className(), ['id' => 'test_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheme()
    {
        return $this->hasOne(Theme::className(), ['id' => 'theme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestingVsTestVariants()
    {
        return $this->hasMany(TestingVsTestVariant::className(), ['test_variant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestFavorite()
    {
        return $this->hasOne(TestVariantFavorite::className(), ['test_variant_id' => 'id'])
            ->where(['user_id' => Yii::$app->user->id]);
    }
}
