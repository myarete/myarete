<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ArticleReady".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $article_id
 * @property integer $ready_at
 */
class ArticleReady extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'article_ready';
    }

    public function rules()
    {
        return [
            [['user_id', 'article_id', 'ready_at'], 'required'],
            [['user_id', 'article_id', 'ready_at'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'article_id' => 'Article ID',
            'ready_at' => 'Ready time',
        ];
    }

    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }
}
