<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 17.07.15
 * Time: 23:42
 */

namespace common\models;

use Yii;

class ArticleCategory extends \kartik\tree\models\Tree
{
    use \common\models\traits\UploadTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['price', 'status'], 'required'];
        $rules[] = [['description_short', 'image', 'price', 'status', 'hide_fr_clients'], 'safe'];
        $rules[] = ['imageFile', 'image', 'skipOnEmpty' => true];

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['price'] = 'Цена';
        $labels['status'] = 'Статус';
        $labels['imageFile'] = 'Изображение';
        $labels['description_short'] = 'Краткое описание';
        $labels['hide_fr_clients'] = 'Скрыть от клиентов';

        return $labels;
    }

    public function beforeSave($insert)
    {
        switch ($this->lvl) {
            case 0:
                $this->icon = 'folder';
                break;
            case 1:
                $this->icon = 'folder-o';
                break;
            case 2:
                $this->icon = 'file';
                break;
            case 3:
                $this->icon = 'file-o';
                break;
        }

        $this->collapsed = 1;

        $this->uploadImage('@admin/web/upload/article/', '@online/web/upload/article/');

        return parent::beforeSave($insert);
    }

    public static function getRoots()
    {
        return static::find()
            ->roots()
            ->all();
    }

    public static function getLeaves()
    {
        return static::find()
            ->leaves()
            ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleVsCategories()
    {
        return $this->hasMany(ArticleVsCategory::className(), ['article_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['id' => 'article_id'])->viaTable('article_vs_category', ['article_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVsCategory()
    {
        return $this->hasMany(UserVsCategory::className(), ['root' => 'id']);
    }
}