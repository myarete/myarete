<?php

namespace common\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "test_variant_favorite".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $test_variant_id
 *
 * @property User $user
 * @property TestVariant $testVariant
 */
class TestVariantFavorite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_variant_favorite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'test_variant_id'], 'required'],
            [['user_id', 'test_variant_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'test_variant_id' => 'Test Variant ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestVariant()
    {
        return $this->hasOne(TestVariant::className(), ['id' => 'test_variant_id']);
    }
}
