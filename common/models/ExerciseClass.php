<?php

namespace common\models;

use Yii;

class ExerciseClass
{
    /**
     * @var array
     */
    public static $classes = [
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
        10 => 10,
        11 => 11,
    ];

    /**
     * @param $subject_id
     * @return array
     */
    public static function getClasses($subject_id)
    {
        $classes = [];
        $models = ThemeModel::find()
            ->select('DISTINCT(theme.class) as class')
            ->innerJoinWith(['exerciseVariants', 'subject'])
            ->where('theme.subject_id = :subject_id', [':subject_id' => $subject_id])
            ->orderBy('theme.class')
            ->all();

        foreach ($models as $model) {
            $classes[$model->class] = Theme::find()->where(['class' => $model->class, 'subject_id' => $subject_id])->one()->name;
        }

        return $classes;
    }

    public static function getExercises($subject_id, $class = null)
    {
        $exercises = [];
        if ($class) {
            $models = ExerciseVariant::find()
                ->innerJoinWith(['exercise', 'theme'])
                ->where('theme.class = :class and exercise.subject_id = :subject_id', [':class' => $class, ':subject_id' => $subject_id])
                ->all();
        } else {
            $models = ExerciseVariant::find()
                ->innerJoinWith(['exercise', 'theme'])
                ->where('exercise.subject_id = :subject_id', [':subject_id' => $subject_id])
                ->all();
        }

        foreach ($models as $model) {
            $exercises[$model->id] = $model->exercise->id . ' - ' . $model->exercise->name;
        }

        return $exercises;
    }

    public static function getExercisesById($id, $subject_id, $class)
    {
        $exercises = [];

        $models = ExerciseVariant::find()
            ->innerJoinWith(['exercise', 'theme'])
            ->where('theme.id = :id and theme.class = :class and exercise.subject_id = :subject_id', [':id' => $id, ':class' => $class, ':subject_id' => $subject_id])
            ->all();

        foreach ($models as $model) {
            $exercises[$model->id] = $model->exercise->id . ' - ' . $model->exercise->name;
        }

        return $exercises;
    }
}
