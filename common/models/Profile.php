<?php

namespace common\models;

use common\components\behaviors\ImageBehavior;
use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $state_id
 * @property string $first_name
 * @property string $last_name
 * @property string $birthdate
 * @property string $cash
 *
 * @property User $user
 * @property State $state
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'filter', 'filter' => 'trim'],
            [['first_name', 'last_name', 'image'], 'string'],
            [['state_id'], 'required'],
            ['state_id', 'exist', 'targetClass' => State::className(), 'targetAttribute' => 'id'],
            [['birthdate', 'cash'], 'safe'],
            [['stateName', 'about_me'], 'string'],
            [['allow_search', 'view_contacts', 'email_confirm', 'policy_confirm', 'first_session'], 'integer'],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'user_id' => 'User ID',
            'state_id' => 'Статус',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'birthdate' => 'Дата рождения',
            'image' => 'Аватар',
            'allow_search' => 'Меня можно найти через «Поиск»',
            'about_me' => 'Обо мне',
            'view_contacts' => 'Видеть контакты',
            'email' => 'Email',
            'email_confirm' => 'Я согласен получать на почту письма от Арете Онлайн.',
            'cash' => 'Баланс',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->image = ltrim(basename($this->image), '/');

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $file = $this->image;
        $dir = $this->getAbsDir() . $this->getHashDir($file);
        FileHelper::createDirectory($dir, 0777);
        $path = Yii::getAlias('@webroot') . '/tmp/';
        if (file_exists($path . $file) && !is_dir($path . $file)) {
            rename($path . $file, $dir . $file);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }

    public function getStateName()
    {
        return $this->state->name;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'imageBehavior' => [
                'class' => ImageBehavior::className(),
            ],
        ];
    }
}
