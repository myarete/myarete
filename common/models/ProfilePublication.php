<?php

namespace common\models;

use common\models\User;
use common\models\ProfilePublicationQuery;
use Yii;

/**
 * This is the model class for table "profile_education".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $skill
 * @property User $user
 */
class ProfilePublication extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_publication';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'publication', 'publication_date', 'url'], 'required'],
            [['user_id'], 'integer'],
            [['description'], 'string'],
            [['publication_date'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['publication'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Название',
            'publication' => 'Издатель',
            'publication_date' => 'Дата публикации',
            'url' => 'Публичная ссылка',
            'description' => 'Описание',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function afterValidate()
    {
        $this->user_id = Yii::$app->user->identity->id;
    }

    /**
     * @inheritdoc
     * @return profileSkillQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new profileSkillQuery(get_called_class());
    }
}
