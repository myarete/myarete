<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_vs_daily_tasks".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $task_id
 * @property integer $status
 */
class UserVsDailyTasks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_vs_daily_tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'task_id', 'status'], 'required'],
            [['user_id', 'task_id', 'status'], 'integer'],
            [['created_at'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'task_id' => 'Task ID',
            'status' => 'Status',
        ];
    }

    public function getTask()
    {
        return $this->hasOne(DailyTasks::className(), ['id' => 'task_id']);
    }
}
