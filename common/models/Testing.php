<?php

namespace common\models;

use Yii;
use common\models\User;
use yii\helpers\Url;

/**
 * This is the model class for table "testing".
 *
 * @property string $id
 * @property string $user_id
 * @property string $name
 * @property string $date_create
 *
 * @property User $user
 * @property TestingVsTestVariant[] $testingVsTestVariants
 */
class Testing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'testing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['date_create'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['order_by', 'note'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'date_create' => 'Date Create',
            'order_by' => 'Order',
            'note' => 'Заметка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        $url = [
            '/testing/default/run',
            'id' => $this->id,
        ];

        return Url::to($url);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestingTests()
    {
        $test_variants = [];

        if (!empty($this->order_by)) { // если задан порядок задач, то массив заполняется
            foreach (explode(',', $this->order_by) as $id) {
                $id = substr($id, 3);
                if ($variant = TestingVsTestVariant::findOne($id))
                    $test_variants[] = $variant;
            }

            return $test_variants;
        }

        return $this->hasMany(TestingVsTestVariant::className(), ['testing_id' => 'id']);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (empty($this->name))
            $this->name = 'Тестирование';

        return parent::beforeSave($insert);
    }
}
