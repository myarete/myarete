<?php

namespace common\models;

use Yii;


class BonusAnswer extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'bonus_answer';
    }

    public function rules()
    {
        return [
            [['question_id', 'answer'], 'required'],
            [['question_id', 'right_answer'], 'integer'],
            [['answer'], 'string', 'max' => 255],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => BonusQuestion::className(), 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Question ID',
            'answer' => 'Answer',
            'right_answer' => 'Right Answer',
        ];
    }

    public function getQuestion()
    {
        return $this->hasOne(BonusQuestion::className(), ['id' => 'question_id']);
    }

    public function getBonusStatValues()
    {
        return $this->hasMany(BonusStatValue::className(), ['answer_id' => 'id']);
    }
}
