<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "training_vs_group".
 *
 * @property int $id
 * @property int $training_id
 * @property int $group_id
 * @property int $create_dt
 *
 * @property Training $training
 * @property Group $group
 * @property TrainingVsGroupOpt[] $trainingVsGroupOpts
 */
class TrainingVsGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'training_vs_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['training_id', 'group_id'], 'required'],
            [['training_id', 'group_id'], 'integer'],
            ['create_dt', 'safe'],
            [['training_id'], 'exist', 'skipOnError' => true, 'targetClass' => Training::className(), 'targetAttribute' => ['training_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'training_id' => 'Training ID',
            'group_id' => 'Group ID',
            'create_dt' => 'Create date time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTraining()
    {
        return $this->hasOne(Training::className(), ['id' => 'training_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingVsGroupOpts()
    {
        return $this->hasMany(TrainingVsGroupOpt::className(), ['trvsgr_id' => 'id']);
    }
}
