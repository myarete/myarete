<?php

namespace common\models;

use common\models\Teacher;
use Yii;

/**
 * This is the model class for table "affiliate_vs_teacher".
 *
 * @property integer $id
 * @property integer $teacher_id
 * @property integer $affiliate_id
 *
 * @property Teacher $teacher
 * @property Affiliate $affiliate
 */
class AffiliateVsTeacher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'affiliate_vs_teacher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_id', 'affiliate_id'], 'required'],
            [['teacher_id', 'affiliate_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teacher_id' => 'Teacher ID',
            'affiliate_id' => 'Affiliate ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'teacher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAffiliate()
    {
        return $this->hasOne(Affiliate::className(), ['id' => 'affiliate_id']);
    }
}
