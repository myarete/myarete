<?php

namespace common\models;

use common\models\User;
use yii\helpers\Url;

/**
 * This is the model class for table "training".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $name
 *
 * @property User $user
 */
class Training extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['order_by', 'note'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Название',
            'order_by' => 'Order',
            'note' => 'Заметка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        $url = [
            '/training/default/run',
            'id' => $this->id,
        ];

        return Url::to($url);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingExercises()
    {
        $exercise_variants = [];

        if (!empty($this->order_by)) { // если задан порядок задач, то массив заполняется
            foreach (explode(',', $this->order_by) as $id) {
                $id = substr($id, 3);
                if ($variant = TrainingVsExerciseVariant::findOne($id))
                    $exercise_variants[] = $variant;
            }

            return $exercise_variants;
        }

        return $this->hasMany(TrainingVsExerciseVariant::className(), ['training_id' => 'id']);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (empty($this->name))
            $this->name = 'Тренировка';

        return parent::beforeSave($insert);
    }
}
