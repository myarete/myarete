<?php

namespace common\models;

use Yii;

class BonusPoll extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'bonus_poll';
    }

    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['description'], 'string'],
            [['create_dt'], 'safe'],
            ['name', 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'description' => 'Описание',
            'create_dt' => 'Создан',
        ];
    }

    public function getBonusQuestions()
    {
        return $this->hasMany(BonusQuestion::className(), ['poll_id' => 'id']);
    }

    public function getBonusVsPolls()
    {
        return $this->hasMany(BonusVsPoll::className(), ['poll_id' => 'id']);
    }
}
