<?php

namespace common\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "send_materials".
 *
 * @property integer $id
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property string $testings
 * @property string $dt
 */
class SendTesting extends \yii\db\ActiveRecord
{
    public $type = 'Тестирование';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'send_testing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_user_id', 'to_user_id'], 'required'],
            [['from_user_id', 'to_user_id', 'testing', 'is_ready'], 'integer'],
            [['dt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_user_id' => 'From User ID',
            'to_user_id' => 'To User ID',
            'testing' => 'Testing',
            'dt' => 'Dt',
            'is_ready' => 'Is Ready',
        ];
    }

    public function getFromUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    public function getToUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }
}

