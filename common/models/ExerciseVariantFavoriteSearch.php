<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ExerciseSearch represents the model behind the search form about `app\modules\exercise\models\Exercise`.
 */
class ExerciseVariantFavoriteSearch extends ExerciseVariantFavorite
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param $orderBy
     * @return ActiveDataProvider
     */
    public function search($params, $orderBy = null)
    {
        $query = self::find();

        if ($orderBy !== null)
            $query->orderBy($orderBy);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_id' => Yii::$app->user->id,
        ]);

        return $dataProvider;
    }
}
