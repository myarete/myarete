<?php

namespace common\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "test_rating".
 *
 * @property string $id
 * @property string $test_id
 * @property integer $rating
 * @property string $user_id
 * @property integer $class
 * @property string $date_create
 *
 * @property User $user
 */
class TestRating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_id', 'rating', 'user_id', 'class'], 'required'],
            [['test_id', 'rating', 'user_id', 'class'], 'integer'],
            [['date_create'], 'safe'],
            [['test_id', 'user_id', 'class'], 'unique', 'targetAttribute' => ['test_id', 'user_id', 'class'], 'message' => 'The combination of Test ID, User ID and Class has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_id' => 'Тест',
            'rating' => 'Рейтинг',
            'user_id' => 'Пользователь',
            'class' => 'Класс',
            'date_create' => 'Дата создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(Test::className(), ['id' => 'test_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
