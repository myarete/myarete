<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "test_answer".
 *
 * @property string $id
 * @property string $test_id
 * @property string $answer
 * @property integer $is_right
 * @property string $comment
 *
 * @property Test $test
 */
class TestAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['test_id', 'required', 'message' => 'Необходимо заполнить'],
            ['answer', 'required', 'message' => 'Необходимо заполнить'],
            [['test_id', 'is_right'], 'integer'],
            [['answer', 'comment'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_id' => 'Тест',
            'answer' => 'Ответ',
            'is_right' => 'Правильный?',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(Test::className(), ['id' => 'test_id']);
    }
}
