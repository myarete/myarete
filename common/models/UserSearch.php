<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{
    public $search = '';
    public $allow_search;
    public $newsletter;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_confirmed', 'allow_search', 'newsletter'], 'integer'],
            [['email', 'password', 'auth_key', 'role', 'date_create', 'confirm_code'], 'safe'],
            [['search'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->joinWith(['profile']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $stateQuery = (new Query())->select('id')->from('state')->where(['like', 'name', $this->search]);

        $role = array_filter(User::$roleLabels, function ($var) {
            if ($this->search)
                return mb_stripos($var, $this->search) !== false;
        });

        $query->filterWhere(['like', 'user.email', $this->search])
            ->filterWhere(['like', 'user.role', key($role)])
            ->orFilterWhere(['like', 'profile.first_name', $this->search])
            ->orFilterWhere(['like', 'profile.last_name', $this->search])
            ->orFilterWhere(['profile.state_id' => $stateQuery]);

        if (preg_match('/[\d-]+/', $this->search)) {
            $query->orFilterWhere(['like', 'profile.birthdate', $this->search]);
        }
        if (isset($this->allow_search)) {
            $query->andFilterWhere(['profile.allow_search' => $this->allow_search]);
        }
        if (isset($this->newsletter)) {
            $query->andFilterWhere(['profile.email_confirm' => $this->newsletter]);
        }

        return $dataProvider;
    }
}
