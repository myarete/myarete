<p>
    <?= \yii\helpers\Html::a('Просмотреть все записи на курсы', \yii\helpers\Url::to(['/course/admin/index'], true)) ?>
</p>

<?= \yii\widgets\DetailView::widget([
    'model' => $courseRequest,
    'attributes' => [
        'fio',
        'phone',
        [
            'label' => 'Предметы',
            'value' => $courseRequest->subjects,
        ],
        'date_create:datetime'
    ]
]) ?>