<p>
    <?= \yii\helpers\Html::a('Просмотреть все заказанные звонки', \yii\helpers\Url::to(['/call_request/admin/index'], true)) ?>
</p>

<?= \yii\widgets\DetailView::widget([
    'model' => $callRequest,
    'attributes' => [
        'fio',
        'phone',
        'date_create:datetime'
    ]
]) ?>