<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 13.07.15
 * Time: 23:25
 */

namespace common\components\user;


/**
 * @property \common\models\User $model
 */
class User extends \yii\web\User
{
    /**
     * @var bool
     */
    private $_model = false;

    /**
     * @return null|\common\models\User
     */
    public function getModel()
    {
        if ($this->_model === false)
            $this->_model = \common\models\User::findOne($this->id);

        return $this->_model;
    }

    /**
     * @param string $name
     * @return bool|mixed
     */
//    public function __get($name)
//    {
//        if ($name !== 'isGuest' && strpos($name, 'is') === 0) {
//            if ($this->getIsGuest())
//                return false;
//
//            $role = strtolower(substr($name, 2));
//            if (isset(\common\models\User::$roleLabels[$role]))
//                return $role == $this->getModel()->role;
//        }
//
//        return parent::__get($name);
//    }

}
