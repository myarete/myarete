<?php

namespace common\components\user\oauth;

class FacebookOAuth2Service extends \nodge\eauth\services\FacebookOAuth2Service
{
    protected $scopes = [self::SCOPE_EMAIL];//, self::SCOPE_USER_BIRTHDAY, self::SCOPE_USER_PHOTOS];

    protected function fetchAttributes()
    {
        $info = $this->makeSignedRequest('me', [
            'query' => [
                'fields' => join(',', [
                    'id',
                    'name',
                    'email',
                    'link',
                    'verified',
                    'first_name',
                    'last_name',
                    'gender',
                    //'birthday',
                    'hometown',
                    'location',
                    'locale',
                    'timezone',
                    'updated_time',
                    //'picture'
                ])
            ]
        ]);

        $this->attributes['id'] = $info['id'];
        $this->attributes['given_name'] = $info['first_name'];
        $this->attributes['family_name'] = $info['last_name'];
        $this->attributes['link'] = $info['link'];
        $this->attributes['email'] = isset($info['email']) ? $info['email'] : '';
        //$this->attributes['picture'] = $info['picture']['data']['url'];
    }
}
