<?php

namespace common\components\user\oauth;

class YandexOAuth2Service extends \nodge\eauth\services\YandexOAuth2Service
{
    protected function fetchAttributes()
    {
        $info = $this->makeSignedRequest('https://login.yandex.ru/info');

        $this->attributes['id'] = $info['id'];
        $this->attributes['given_name'] = $info['first_name'];
        $this->attributes['family_name'] = $info['last_name'];
        $this->attributes['email'] = $info['default_email'];
        $this->attributes['picture'] = 'http://avatars.mds.yandex.net/get-yapic/' . $info['default_avatar_id'] . '/islands-200';

        return true;
    }
}
