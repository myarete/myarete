<?php

namespace common\components\user\oauth;

class GoogleOAuth2Service extends \nodge\eauth\services\GoogleOAuth2Service
{
    protected $scopes = [self::SCOPE_USERINFO_PROFILE, self::SCOPE_USERINFO_EMAIL];

    protected function fetchAttributes()
    {
        $info = $this->makeSignedRequest('https://www.googleapis.com/oauth2/v1/userinfo');
        $this->attributes['id'] = $info['id'];
        $this->attributes['email'] = $info['email'];
        $this->attributes['given_name'] = $info['given_name'];
        $this->attributes['family_name'] = $info['family_name'];
        $this->attributes['picture'] = $info['picture'];
    }
}
