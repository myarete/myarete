<?php

namespace common\components\user\oauth;

class VKontakteOAuth2Service extends \nodge\eauth\services\VKontakteOAuth2Service
{
    const SCOPE_EMAIL = 'email';

    protected $scopes = [self::SCOPE_EMAIL];

    protected function fetchAttributes()
    {
        $tokenData = $this->getAccessTokenData();

        $info = $this->makeSignedRequest('users.get', [
            'query' => [
                'v' => '3.0',
                'uids' => $tokenData['params']['user_id'],
                'fields' => 'nickname, sex, bdate, city, country, timezone, photo, email',
            ],
        ]);

        $info = $info['response'][0];

        $this->attributes['id'] = $info['uid'];
        $this->attributes['email'] = isset($tokenData['params']['email']) ? $tokenData['params']['email'] : '';
        $this->attributes['given_name'] = $info['first_name'];
        $this->attributes['family_name'] = $info['last_name'];
        $this->attributes['picture'] = $info['photo'];
    }
}
