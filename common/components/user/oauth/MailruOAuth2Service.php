<?php

namespace common\components\user\oauth;

class MailruOAuth2Service extends \nodge\eauth\services\MailruOAuth2Service
{
    protected $name = 'mailru';
    protected $title = 'Mail.ru';
    protected $type = 'OAuth2';
    protected $jsArguments = ['popup' => ['width' => 580, 'height' => 400]];

    protected $scopes = [];
    protected $providerOptions = [
        'authorize' => 'https://connect.mail.ru/oauth/authorize',
        'access_token' => 'https://connect.mail.ru/oauth/token',
    ];
    protected $baseApiUrl = 'http://www.appsmail.ru/platform/api';

    protected function fetchAttributes()
    {
        $tokenData = $this->getAccessTokenData();

        $info = $this->makeSignedRequest('/', [
            'query' => [
                'uids' => $tokenData['params']['x_mailru_vid'],
                'method' => 'users.getInfo',
                'app_id' => $this->clientId,
            ],
        ]);

        $info = $info[0];

        $this->attributes['id'] = $info['uid'];
        $this->attributes['given_name'] = $info['first_name'];
        $this->attributes['family_name'] = $info['last_name'];
        $this->attributes['email'] = $info['email'];
        $this->attributes['picture'] = $info['pic_big'];

        return true;
    }
}
