<?php

namespace common\components\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\image\drivers\Image;
use yii\validators\FileValidator;
use yii\web\UploadedFile;
use yii\web\View;

/**
 * Class ImageBehavior
 * @package common\components\behaviors
 */
class ImageBehavior extends Behavior
{
    /**
     * @var string
     */
    public $attribute = 'image';


    /**
     * @var string
     */
    public $uploadDir = '/upload';


    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_VALIDATE => 'afterValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }


    /**
     * @param $event
     */
    public function afterValidate($event)
    {
        $uploadedFile = UploadedFile::getInstance($this->owner, $this->attribute);

        if ($uploadedFile instanceof UploadedFile) {
            $this->owner->{$this->attribute} = $uploadedFile;
            $validator = new FileValidator;
            $validator->extensions = ['jpg', 'jpeg', 'gif', 'png'];
            $validator->wrongExtension = 'Доступны только файлы с расширениями: {extensions}.';
            $validator->validateAttribute($this->owner, $this->attribute);
        }
    }


    /**
     *
     */
    public function beforeSave($event)
    {
        if ($this->owner->{$this->attribute} instanceof UploadedFile) {
            // Set hash to filename
            $this->owner->{$this->attribute}->name = md5(uniqid() . microtime()) . '.' . $this->owner->{$this->attribute}->getExtension();
        } else {
            $className = (new \ReflectionClass($this->owner))->getShortName();
            if (isset($_POST[$className][$this->attribute]) && $_POST[$className][$this->attribute] == 'to_delete') {
                // Delete file
                $file = $this->getAbsDir() . $this->getHashDir($this->owner->oldAttributes[$this->attribute]) . $this->owner->oldAttributes[$this->attribute];
                @unlink($file);
                $this->owner->{$this->attribute} = null;
            }
        }
    }


    /**
     *
     */
    public function afterSave($event)
    {
        if ($this->owner->{$this->attribute} instanceof UploadedFile) {
            $filename = $this->owner->{$this->attribute}->name;
            $dir = $this->getAbsDir() . $this->getHashDir($filename);
            FileHelper::createDirectory($dir, 0777);

            $this->owner->{$this->attribute}->saveAs($dir . $filename);
        }
    }


    /**
     * @return string (e.g. /var/www/site.com/upload/news/)
     */
    public function getAbsDir()
    {
        return Yii::getAlias('@' . Yii::$app->id . '/web') . $this->getRelDir();
    }


    /**
     * @return string (e.g. /upload/news/)
     */
    public function getRelDir()
    {
        return '/' . trim($this->uploadDir, '/') . '/' . strtolower((new \ReflectionClass($this->owner))->getShortName()) . '/';
    }


    /**
     * @param $hash (e.g. 76d80224611fc919a5d54f0ff9fba446.jpg)
     * @return string (e.g. 76d/802/)
     */
    public function getHashDir($hash)
    {
        return substr($hash, 0, 3) . '/' . substr($hash, 3, 3) . '/';
    }


    /**
     * @param array $options
     * @return string /upload/news/76d/802/76d80224611fc919a5d54f0ff9fba446.jpg
     */
    public function getImageSrc($options = [])
    {
        if (empty($this->owner->{$this->attribute})) {
            if (isset($options['defaultSrc']))
                return $options['defaultSrc'];

            return null;
        }

        if (!preg_match('/http|https/', $this->owner->{$this->attribute}))
            $src = $this->getRelDir() . $this->getHashDir($this->owner->{$this->attribute}) . $this->owner->{$this->attribute};
        else
            $src = $this->owner->{$this->attribute};

        if (isset($options['size'])) {
            // /upload/subject/0c6/dc5/0c6dc58fad78bef8f7c450838672b55d_100x200.jpg
            $src = $this->getResizedSrc($src, $options['size']);
        }

        return $src;
    }


    /**
     * @param array $options
     * @return null|string
     * @throws Exception
     */
    public function getImageTag($options = [])
    {
        $src = $this->getImageSrc($options);

        if ($src === null && isset($options['defaultHtml']))
            return $options['defaultHtml'];

        if ($src === null)
            return null;

        return Html::img($src, $options);
    }

    public function getImage($options = [])
    {
        $src = $this->getImageSrc($options);
        if (!$src) {
            $src = $options['defaultHtml'];
        }
        return $src;

    }


    /**
     * @param \yii\widgets\ActiveForm $form
     * @return string
     */
    public function getImageUploadWidget(\yii\widgets\ActiveForm $form)
    {
        $content = $form->field($this->owner, $this->attribute)->fileInput();

        if ($this->getImageSrc() !== null) {
            $content .= $this->getImageTag(['style' => 'max-width: 100px; max-height: 100px; display: block;']);

            // Delete image link
            $id = 'image_upload_widget__delete_link';
            $content .= Html::a('<i class="fa fa-trash"></i> Удалить', '#', ['id' => $id]);
            Yii::$app->view->registerJs(
                "
                    $('#{$id}').click(function(){
                        $(this).siblings('img').remove();
                        $(this).parent().find('input[type=file]').remove();
                        $(this).parent().find('input[type=hidden]').val('to_delete');
                        $(this).remove();
                        return false;
                    });
                ",
                View::POS_READY,
                $id
            );
        }

        return Html::tag('div', $content, ['class' => 'image-upload-widget']);
    }


    /**
     * @return null|string
     */
    public function getOriginSize()
    {
        $src = $this->getImageSrc();
        if ($src === null)
            return null;

        $size = getimagesize(Yii::getAlias('@' . Yii::$app->id . '/web') . $src);
        return $size[0] . 'x' . $size[1];
    }


    /**
     * @param string $srcOrigin
     * @param array $size
     * @return null|string
     */
    private function getResizedSrc($srcOrigin, $size)
    {
        $pathinfo = pathinfo($srcOrigin);
        $sizeStr = implode('x', $size);

        $thumbSrc = $pathinfo['dirname'] . '/' . $pathinfo['filename'] . '_' . $sizeStr . '.' . $pathinfo['extension'];

        $webDir = Yii::getAlias('@' . Yii::$app->id . '/web');
        $fileThumb = $webDir . $thumbSrc;

        if (!file_exists($fileThumb)) {
            $fileOrigin = $webDir . $srcOrigin;
            if (!file_exists($fileOrigin))
                return null;

            Yii::$app->image->load($fileOrigin)->resize($size[0], $size[1], Image::AUTO)->save($fileThumb);
        }

        return $thumbSrc;
    }
}
