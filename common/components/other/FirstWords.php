<?php

namespace common\components\other;

use Yii;

class FirstWords
{
    public static function get($text, $count)
    {
        $retval = [];

        preg_match_all('/<p>(.*)<\/p>/U', $text, $match);

        preg_match_all('/\s?(.*)\s/U', implode(' ', $match[1]), $match);

        if (count($match[1]) < 30)
            return $text;

        for ($i = 0; $i < $count; $i++) {
            if (isset($match[1][$i])) {
                $retval[] = $match[1][$i];
            }
        }

        return implode(' ', $retval) . '...';
    }
}

