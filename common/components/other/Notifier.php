<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 13.07.15
 * Time: 20:56
 */

namespace common\components\other;


use common\models\User;
use common\models\CourseRequest;
use common\models\CallRequest;
use Yii;

class Notifier
{
    /**
     * Подтверждение регистрации
     *
     * @param User $user
     * @return mixed
     */
    public static function sendConfirmEmailLink(User $user)
    {
        return self::sendFromNoReply(
            $user->email,
            'Подтвердите регистрацию',
            'send_confirm_email_link',
            ['user' => $user]
        );
    }

    /**
     * Отправка ссылки для восстановление пароля
     *
     * @param User $user
     * @return mixed
     */
    public static function sendRestorePasswordLink(User $user)
    {
        return self::sendFromNoReply(
            $user->email,
            'Ваша ссылка на восстановление пароля',
            'send_restore_password_link',
            ['user' => $user]
        );
    }

    /**
     * Уведомление о новой записи на курсы
     *
     * @param CourseRequest $courseRequest
     * @return mixed
     */
    public static function createdCourseRequest(CourseRequest $courseRequest)
    {
        return self::sendFromNoReply(
            Yii::$app->params['writeusEmail'],
            'Произведена запись на курсы',
            'send_course_request',
            ['courseRequest' => $courseRequest]
        );
    }

    /**
     * Уведомление о новом заказе на звонок
     *
     * @param CallRequest $callRequest
     * @return mixed
     */
    public static function sendCallRequestEmail(CallRequest $callRequest)
    {
        return self::sendFromNoReply(
            Yii::$app->params['writeusEmail'],
            'Произведен заказ звонка',
            'send_call_request',
            ['callRequest' => $callRequest]
        );
    }

    /**
     * @param $to
     * @param $subject
     * @param $view
     * @param $params
     * @return mixed
     */
    public static function sendFromNoReply($to, $subject, $view, $params)
    {
        return Yii::$app->mailer->compose($view, $params)
            ->setTo($to)
            ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->name])
            ->setSubject($subject)
            ->send();
    }
}