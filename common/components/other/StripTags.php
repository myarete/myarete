<?php

namespace common\components\other;

use Yii;

class StripTags
{
    public static function get($text)
    {
        $retval = [];

        preg_match_all('/<p>(.*)<\/p>/U', $text, $match);

        foreach ($match[1] as $word) {
            $retval[] = $word;
        }

        return implode(' ', $retval);
    }
}


