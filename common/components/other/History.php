<?php

namespace common\components\other;

use Yii;
use common\models\VisitsHistory;

class History
{
    public static function put($user, $page)
    {
        $model = new VisitsHistory;
        $model->user = $user;
        $model->page = $page;
        $model->link = self::getUrl();
        $model->save();
    }

    private static function getUrl()
    {
        $url = empty($_SERVER["HTTPS"]) ? 'http://' . $_SERVER["SERVER_NAME"] : 'https://' . $_SERVER["SERVER_NAME"];
        $url .= ($_SERVER["SERVER_PORT"] != 80) ? ":" . $_SERVER["SERVER_PORT"] : "";
        $url .= $_SERVER["REQUEST_URI"];
        return $url;
    }
}

