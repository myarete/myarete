<?php

namespace common\components\helpers;

use stdClass;

class YandexHelper
{
    /**
     * @param $lg
     * @param $lt
     * @return mixed
     */
    public static function getAddressByCoordinates($lg, $lt)
    {
        $url = sprintf('http://geocode-maps.yandex.ru/1.x/?format=json&geocode=%s,%s', $lg, $lt);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $resDecoded = json_decode($result, true);
        return $resDecoded['response']['GeoObjectCollection']['featureMember'][0]
        ['GeoObject']['metaDataProperty']['GeocoderMetaData']['text'];
    }


    /**
     * @param $address
     * @param array $ll , eg. array(37.618920, 55.756994))
     * @param array $spn , eg. array(0.552069,0.400552)
     * @return stdClass
     */
    public static function getCoordinatesByAddress($address, $ll = array(), $spn = array())
    {
        $url = 'http://geocode-maps.yandex.ru/1.x/?' . http_build_query(['format' => 'json', 'geocode' => $address]);

        if (count($ll) == 2)
            $url .= '&ll=' . implode(',', $ll);

        if (count($spn) == 2)
            $url .= '&spn=' . implode(',', $spn);

        $result = file_get_contents($url);
        $resDecoded = json_decode($result, true);

        if (isset($resDecoded['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'])) {
            $pos = $resDecoded['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
            $coordinates = explode(' ', $pos);

            $object = new stdClass;
            $object->lg = $coordinates[0];
            $object->lt = $coordinates[1];

            return $object;
        }

        return null;
    }


    /**
     * @param $townName
     * @return array
     */
    public static function getCoordinatesByTownName($townName)
    {
        $url = sprintf('http://geocode-maps.yandex.ru/1.x/?format=json&geocode=%s', $townName);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $resDecoded = json_decode($result, true);
        $pointsStr = $resDecoded['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
        return explode(' ', $pointsStr);
    }
}