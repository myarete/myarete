<?php

namespace common\components\helpers;

class FormatHelper
{
    /**
     *
     */
    const PHONE_MASK = '+7(999)999-99-99';

    /**
     * @param $phone
     * @param bool $addPlusBefore800
     * @param bool $hideEnd
     * @return string
     */
    public static function formatPhone($phone, $addPlusBefore800 = false, $hideEnd = false)
    {
        $symbols = preg_replace('~\D+~', '', $phone);

        if (strlen($symbols) == 10)
            $symbols = '7' . $symbols;

        if (strlen($symbols) == 11) {
            $parts = array(
                substr($symbols, 1, 3),
                substr($symbols, 4, 3),
                substr($symbols, 7, 2),
                $hideEnd ? '**' : substr($symbols, 9, 2),
            );

            $number = "({$parts[0]}) {$parts[1]}-{$parts[2]}-{$parts[3]}";

            if ($parts[0] == '800') {
                $number = "8 {$number}";
                if ($addPlusBefore800) {
                    $number = '<span style="color:transparent">+</span>' . $number;
                }
                return $number;
            }

            return "+7 {$number}";
        }

        return $phone;
    }


    /**
     * @param $address
     * @return string
     */
    public static function formatAddress($address)
    {
        $separator = ', ';
        $address = trim($address);
        $address = preg_replace('~\s*,\s*~', $separator, $address);
        $address = preg_replace('~(д|ул)\.\s*~', '$1. ', $address);

        $parts = explode($separator, $address);
        $partsCount = count($parts);
        if ($partsCount) {
            $address = '';
            foreach ($parts as $i => $part) {
                $address .= '<nobr>' . $part . ($i != $partsCount - 1 ? $separator : '') . '</nobr> ';
            }
        }

        return $address;
    }


    /**
     * @param string $phone
     * @return string
     */
    public static function purifyPhone($phone)
    {
        $phone = preg_replace('~\D~', '', $phone);

        if (strlen($phone) == 11)
            $phone = '7' . substr($phone, 1);

        if (strlen($phone) == 10)
            $phone = '7' . $phone;

        return $phone;
    }


    /**
     * @param $cardNumber
     * @return string
     */
    public static function formatCardNumber($cardNumber)
    {
        $cardNumber = preg_replace('~\D~', '', $cardNumber);
        return number_format($cardNumber, 0, '', ' ');
    }
} 