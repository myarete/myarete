<?php

namespace common\components\settings;


use Yii;
use yii\base\Component;
use common\components\settings\SettingsModel;

class Settings extends Component
{
    /**
     * @param $key
     * @return string
     */
    public function getValue($key)
    {
        $model = SettingsModel::findOne(['key' => $key]);
        if (empty($model))
            return '';
        else return $model->value;
    }

    /**
     * @param $key
     * @param $value
     * @return string
     */
    public function setValue($key, $value)
    {
        $model = SettingsModel::findOne(['key' => $key]);
        if (empty($model)) {
            $model = new SettingsModel();
            $model->key = $key;
        }

        $model->value = $value;

        if ($model->save(false))

            return false;
    }
}