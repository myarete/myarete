<?php

namespace common\components\phptex;

use Yii;

class PhpTex
{
    public static function get($string, $template = '', $dpi = '90')
    {
        if (empty($string)) return;

        global $imgfmt, $path_to_latex, $path_to_dvips, $path_to_convert;

        $path_to_latex = '/usr/bin/latex';
        $path_to_dvips = '/usr/bin/dvips';
        $path_to_convert = '/usr/bin/convert';
        $imgfmt = 'png'; #literally used in extensions, and in parameters to convert. Should be either png or gif.

        if ($dpi > 300) $dpi = 300;

        # Figure out TeX, either to get the right cache entry or to, you know, compile
        # Semi-common (ams) symbol packages are included.
        if (file_exists('/phptex/' . $template . '.tex')) {
            $totex = file_get_contents('/phptex/' . $template . '.tex');
        } else {
            $totex = "\\documentclass{minimal}\n" .
                "\\usepackage[utf8]{inputenc}\n" .
                "\\usepackage[T2A]{fontenc}\n" .
                "\\usepackage[english,russian]{babel}\n" .
                "\\usepackage[dvips]{xymtexpdf}\n" .
                "\\begin{document}\n" .
                $string . "\n" .
                "\\end{document}\n";
        }

        $hashfn = sha1($totex . $template) . $dpi;  #file cache entry string:  40-char hash string plus size
        $stralt = str_replace("&", "&amp;", preg_replace("/[\"\n]/", "", $string)); # stuck in the alt and title attributes
        # May need some extra safety.
        $heredir = getcwd();

        # check image cache, return link if exists
        #the vertical-align is to fix text baseline/descender(/tail) details in and on-average sort of way
        if (file_exists($heredir . '/phptex/' . $hashfn . '.' . $imgfmt))
            return '<img title="' . $stralt . '" alt="' . $stralt . '" src="/phptex/' . $hashfn . '.' . $imgfmt . '">';

        # chdir to have superfluous files be created in tmp. (you stiill have to empty this yourself)
        error_reporting(0); # not checking existence myself, that would be double.
        if (chdir($heredir . '/phptex/tmp') === FALSE) {
            return '[directory access error, fix permissions]';
        } #I should chech whether file creation is allowed to give a nice error for that problem case
        error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE); # TODO: set old value

        $tfn = tempnam(getcwd(), 'PTX'); #file in tmp dir

        #write temporary .tex file
        if (($tex = fopen($tfn . '.tex', "w")) == FALSE) {
            return '[file access error] ' . self::phplatex_cleantmp($tfn, $heredir);
        }
        fwrite($tex, $totex);
        fclose($tex);

        # Run latex to create a .dvi.  Have it try to fix minor errors instead of breaking/pausing on them.
        exec('HOME=' . $heredir . ' ' . $path_to_latex . ' -interaction=nonstopmode ' . $tfn . '.tex');
        if (!file_exists($tfn . ".dvi")) {
            $log = file_get_contents($tfn . '.log'); #The log always exists, but now it's actually interesting since it'll contain an error
            return '[latex error, code follows]<pre>' . $totex . '</pre><p><b>Log file:</b><pre>' . $log . '</pre></p> ' . self::phplatex_cleantmp($tfn, $heredir);
        }


        # DVI -> PostScript.   Since dvips uses lpr, which may be configured to actually print by default, force writing to a file with -o
        exec('HOME=' . $heredir . ' ' . $path_to_dvips . ' ' . $tfn . '.dvi -o ' . $tfn . '.ps');
        if (!file_exists($tfn . '.ps')) {
            return '[dvi2ps error] ' . self::phplatex_cleantmp($tfn, $heredir);
        }

        # PostScript -> image.  Also trim based on corner pixel and set transparent color.
        $convert_cmd = $path_to_convert;
        $convert_cmd .= ' -colorspace RGB -density ' . $dpi . ' -trim +page ' . $tfn . '.ps ' . $tfn . '.' . $imgfmt;

        exec($convert_cmd);
        #Note: +page OR -page +0+0 OR +repage moves the image to the cropped area (kills offset)
        #Older code tried: exec('/usr/bin/mogrify -density 90 -trim +page -format $imgfmt '.$tfn.'.ps');
        # It seems some versions of convert may not have -trim. Old versions?

        if (!file_exists($tfn . '.' . $imgfmt)) {
            return '[image convert error] ' . self::phplatex_cleantmp($tfn, $heredir);
        }

        # Copy result image to chache.
        copy($tfn . '.' . $imgfmt, $heredir . '/phptex/' . $hashfn . '.' . $imgfmt);

        # Clean up temporary files, and return link to just-created image
        return self::phplatex_cleantmp($tfn, $heredir) . '<img title="' . $stralt . '" alt="LaTeX formula: ' . $stralt . '" src="/phptex/' . $hashfn . '.' . $imgfmt . '">';
    }

    private function phplatex_cleantmp($tempfname, $todir)
    {
        #specifically removes the various files that probably got created for a specific run, based on the run's filename.
        global $imgfmt;
        if (chdir($todir) === FALSE) {
            return '[directory access error, fix permissions (and empty tmp manually this time)]';
        }
        error_reporting(0); #at least one of these probably will not exist, but disable the error reporting related to that.
        unlink($tempfname);     #the longer/cleaner way would be check for existance for each
        unlink($tempfname . ".tex");
        unlink($tempfname . ".log");
        unlink($tempfname . ".aux");
        unlink($tempfname . ".dvi");
        unlink($tempfname . ".ps");
        unlink($tempfname . "." . $imgfmt);
        error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
        #try-catch would have been nice. This is rather overkill too, the way I use it.
        return '';
    }
}
