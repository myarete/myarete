<?php

namespace common\components\yii\base;

use common\models\ArticleSlider;
use common\models\Exercise;
use common\models\Test;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

abstract class BaseController extends Controller
{
    /**
     * @throws NotFoundHttpException
     */
    public function pageNotFound()
    {
        throw new NotFoundHttpException('The requested page does not exist.');
    }


    /**
     * @throws ForbiddenHttpException
     */
    public function forbidden()
    {
        throw new ForbiddenHttpException('You are not allowed to access this page.');
    }


    /**
     * @throws BadRequestHttpException
     */
    public function badRequest()
    {
        throw new BadRequestHttpException('Bad request');
    }


    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        // Register js route file
        $jsFileRel = "/js/page/{$this->route}.js";
        $jsFileAbs = Yii::getAlias("@app/web{$jsFileRel}");
        if (file_exists($jsFileAbs)) {
            $this->view->registerJsFile($jsFileRel . '?v=' . filemtime($jsFileAbs), ['depends' => [\yii\web\JqueryAsset::className()]]);
        }

        return parent::beforeAction($action);
    }

    /**
     * @param $text string
     * @return string
     */
    public function strReplace($text)
    {
        preg_match_all('/{{\w*=\d*}}/u', $text, $matches);
        $strArr = [];
        $i = 0;
        $j = 0;

        foreach ($matches[0] as $match) {
            $str = str_replace('{', '', $match);
            $str = str_replace('}', '', $str);

            $type = explode('=', $str);

            if ($type[0] === 'exercise') {
                $model = Exercise::findOne(['id' => $type[1]]);
                if (!empty($model)) {
                    $tpl = $tpl = $this->renderPartial('/article/view__exercise', ['exercise' => $model, 'i' => $i]);
                    $i++;
                } else {
                    $tpl = $match;
                }

            } elseif ($type[0] === 'test') {
                $model = Test::findOne(['id' => $type[1]]);
                if (!empty($model)) {
                    $tpl = $this->renderPartial('/article/view__test', ['test' => $model, 'i' => $j]);
                    $j++;
                } else {
                    $tpl = $match;
                }
            } elseif ($type[0] === 'sliders') {
                $model = ArticleSlider::findOne(['id' => $type[1]]);
                if (!empty($model)) {
                    $tpl = $this->strReplace($this->renderPartial('/article/view__slider', ['model' => $model]));
                    $j++;
                } else {
                    $tpl = $match;
                }
            } else {
                $tpl = $match;
            }

            $strArr[] = [
                $match => $tpl
            ];
        }

        foreach ($strArr as $arr) {
            foreach ($arr as $key => $value) {
                $text = preg_replace('/' . $key . '/', $value, $text, 1);
            }
        }

        return $text;
    }
}