<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 25.08.15
 * Time: 18:19
 */

namespace common\components\yii\i18n;

use Yii;

class Formatter extends \yii\i18n\Formatter
{
    /**
     * @param \DateTime|int|string $value
     * @param null $format
     * @return string
     */
    public function asDate($value, $format = null)
    {
        return $this->translateMonth(parent::asDate($value, $format));
    }

    /**
     * @param $month
     * @return string
     */
    private function translateMonth($month)
    {
        $months = [
            'january' => 'января',
            'february' => 'февраля',
            'march' => 'марта',
            'april' => 'апреля',
            'may' => 'мая',
            'june' => 'июня',
            'july' => 'июля',
            'august' => 'августа',
            'september' => 'сентября',
            'october' => 'октября',
            'november' => 'ноября',
            'december' => 'декабря',
        ];

        return strtr(mb_strtolower($month, Yii::$app->charset), $months);
    }
}
