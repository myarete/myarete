<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace online\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/fa/font-awesome.min.css',
        'css/plugins/bootstrap-treeview/bootstrap-treeview.min.css',
        'css/plugins/sweetalert/sweetalert2.min.css',
        'css/style.css',
        'css/tooltip.css',
        'css/ez-space.css'
    ];
    public $js = [
//        'js/plugins/jquery/ui/jquery-ui.min.js',
        'https://use.fontawesome.com/d2e3dc5d54.js',
        'js/function.js',
//        'js/plugins/angular/angular.min.js',
//        'js/plugins/angular/angular-sanitize.min.js',
        'js/plugins/bootbox/bootbox.min.js',
        'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
        'js/plugins/mathjax/config.js',
        'js/custom.js',
//        'js/plugins/zepto/zepto.min.js',
//        'js/plugins/zepto/zepto.dragswap.js',
        'js/plugins/bootstrap-treeview/bootstrap-treeview.min.js',
        'js/plugins/snap/snap.min.js',
        'js/plugins/timeago/jquery.timeago.js',
        'js/plugins/timeago/jquery.timeago.ru.js',
        'js/plugins/sweetalert/sweetalert2.min.js',
        'js/yii.overrides.js',
    ];
//    public $jsOptions = [
//        'position' => View::POS_HEAD,
//    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\jui\JuiAsset'
    ];
}
