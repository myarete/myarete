<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace online\assets;

use yii\web\AssetBundle;
use yii\web\View;
use Yii;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PrintAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/fa/font-awesome.min.css',
        'css/style.css?03',
        'css/print.css?04',
        'css/plugins/bootstrap-treeview/bootstrap-treeview.min.css',
    ];
    public $js = [
        'js/custom.js',
        'js/plugins/snap/snap.min.js',
    ];
    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        array_unshift($this->css, Yii::$app->params['scheme'].'://'.Yii::$app->params['host'].'/css/main.css');

        parent::init();
    }
}
