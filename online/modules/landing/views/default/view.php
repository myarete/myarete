<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var $model \landing\modules\main\models\Landing
 * @var $this \yii\web\View
 */

?>


<?php
$this->title = 'Лэндинг | Просмотр';
?>


<?php
$this->params['ribbonMenu'] = [
    [
        'label'     => '<i class="fa fa-edit"></i>',
        'url'       => ['update', 'id' => $model->id],
        'template'  => '<a href="{url}" title="Обновить">{label}</a>'
    ],
    [
        'label'     => '<i class="fa fa-trash"></i>',
        'url'       => ['delete', 'id' => $model->id],
        'template'  => '<a href="{url}" title="Удалить">{label}</a>',
        'options'   => [
            'onclick' => 'return confirm("Вы уверены?")',
        ]
    ],
];
?>


<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-table"></i>
        <?= $this->title ?>
    </div>

    <div class="ibox-content">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'title',
                'alias',
            ],
        ]) ?>

        <div style="position: relative;">
            <?= $model->content ?>
        </div>
    </div>
</div>
