<?php
use admin\widgets\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>


<?php
$this->title = 'HTML | '.($model->isNewRecord ? 'Создать' : 'Обновить');
?>

<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-pencil-square-o"></i> <?= $this->title ?>
    </div>
    <div class="ibox-content">

        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'title') ?>

            <?= $form->field($model, 'alias') ?>

        <?= $form->field($model, 'content')->widget(CKEditor::className(), [
            'options' => ['rows' => 100],
            'enableFinder' => true,
            'clientOptions' => [
                'toolbar' => [
                    [
                        'name' => 'clipboard',
                        'items' => [
                            'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                        ],
                    ],
                    [
                        'name' => 'links',
                        'items' => [
                            'Link', 'Unlink', 'Anchor'
                        ],
                    ],
                    [
                        'name' => 'insert',
                        'items' => [
                            'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                        ],
                    ],
                    [
                        'name' => 'align',
                        'items' => [
                            'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                        ],
                    ],
                    [
                        'name' => 'document',
                        'items' => [
                            'Maximize', 'ShowBlocks', 'Source'
                        ],
                    ],
                    '/',
                    [
                        'name' => 'basicstyles',
                        'items' => [
                            'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                        ],
                    ],
                    [
                        'name' => 'color',
                        'items' => [
                            'TextColor', 'BGColor'
                        ],
                    ],
                    [
                        'name' => 'paragraph',
                        'items' => [
                            'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                        ],
                    ],
                    [
                        'name' => 'styles',
                        'items' => [
                            'Styles', 'Format', 'Font', 'FontSize'
                        ],
                    ],
                    '/',
                    [
                        'name' => 'insert',
                        'items' => [
                            'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion'
                        ],
                    ],
                ],
                'height' => 500,
                'allowedContent' => true,
                'contentsCss' => [
                    '/css/fa/font-awesome.min.css',
                    '/css/bootstrap.min.css',
                    Yii::$app->params['scheme'].'://'.Yii::$app->params['subdomain.landing'].'.'.Yii::$app->params['host'].'/css/landing.css'
                ],
                'extraPlugins' => 'mathjax,exercise,test,sliders,accordion',
                'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
            ],
        ]) ?>

            <hr/>

            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
