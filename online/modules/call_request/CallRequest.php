<?php

namespace online\modules\call_request;

class CallRequest extends \yii\base\Module
{
    public $controllerNamespace = 'online\modules\call_request\controllers';

    public $label = 'Заказать звонок';
}
