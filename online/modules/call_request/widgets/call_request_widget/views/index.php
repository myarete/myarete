<?php

use yii\widgets\ActiveForm;

$js = <<<JS
    /**
     *
     */
    var modal = $('#call-request-modal'),
        fieldsArea = modal.find('.fields-area'),
        successMessage = modal.find('.success-message'),
        errorMessage = modal.find('.error-message'),
        form = modal.find('form'),
        submitButton = form.find('button[type="submit"]');

    /**
     *
     */
    modal.on('hidden.bs.modal', function (e) {
        form[0].reset();
        fieldsArea.show();
        successMessage.hide();
        errorMessage.hide();
        submitButton.show();
    });

    /**
     *
     */
    form.on('beforeSubmit', function(e){

        $(this).ajaxSubmit({
            success : function (response) {
                if ('success' in response && response.success)
                {
                    fieldsArea.hide();
                    successMessage.show();
                }
                else
                {
                    errorMessage.show();
                }
                submitButton.hide();
            }
        });

        return false;
    });
JS;
$this->registerJs($js);
?>

<div class="modal" id="call-request-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Заказать звонок</h4>
            </div>
            
            <?php $form = ActiveForm::begin([
                'action' => Yii::$app->params['scheme'] . '://' . Yii::$app->params['host'] . '/call_request/default/create',
            ]); ?>
            
            <div class="modal-body">
                
                <div class="row fields-area">
                    <div class="col-xs-6">
                        <?= $form
                            ->field($model, 'fio')
                            ->label('Представьтесь')
                            ->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-xs-6">
                        <?= $form
                            ->field($model, 'phone')
                            ->label(null)
                            ->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '+7 (999) 999-99-99',
                                'options' => [
                                    'placeholder' => '+7 (___) ___-__-__',
                                    'class' => 'form-control',
                                ]
                            ]) ?>
                    </div>
                </div>
                
                <div class="success-message" style="display: none;">
                    Ваш запрос успешно отправлен, ожидайте звонка
                </div>
                
                <div class="error-message" style="display: none;">
                    При отправке запроса произошла ошибка
                </div>
            
            </div>
            
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-success">Заказать</button>
            </div>
            
            <?php ActiveForm::end(); ?>
        
        </div>
    </div>
</div>
