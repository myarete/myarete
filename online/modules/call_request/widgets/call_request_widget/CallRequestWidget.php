<?php

namespace online\modules\call_request\widgets\call_request_widget;

use common\models\CallRequest;
use yii\base\Widget;

class CallRequestWidget extends Widget
{
    /**
     *
     */
    public function init()
    {
        $this->view->registerJsFile('/js/plugins/jquery/form/jquery.form.min.js', ['depends' => ['online\assets\AppAsset']]);
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('index', [
            'model' => new CallRequest,
        ]);
    }
}
