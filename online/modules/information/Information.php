<?php

namespace online\modules\information;

/**
 * information module definition class
 */
class Information extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'online\modules\information\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
