<?php

use yii\helpers\Html;
use common\models\ExerciseClass;
use common\models\ExerciseVariant;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\bootstrap\Modal;

$this->title = "Количество задач";

$first_level = 0;
$second_level = 0;
$third_level = 0;

?>

<div class="container">
    <div class="side-block">
        <h2><?=$theme->subject->name?> - <?=$theme->name?> - <?= count($sections)?> категории - <?= count(ExerciseClass::getExercises($theme->subject->id, $class))?> задач</h2>
        <?php if (count($sections)): ?>
            <div class="tree">
                <ul class="section-list">
                    <?php foreach ($sections as $section): ?>
                        <li>
                            <div>
                                <span class="indicator"><i class="glyphicon glyphicon-plus"></i></span><span class="section-name name"> <?= $section->name ?></span>
                            </div>
                            <ul class="theme-list">
                                <?php foreach ($section->children(1)->all() as $theme): ?>
                                    <li>
                                        <div class="tree-item">
                                            <span class="indicator"><i class="glyphicon glyphicon-plus"></i></span><span class="theme-name name"> <?= $theme->name ?></span>
                                            <!--- COUNT -->
                                            <span>&nbsp;&nbsp;</span>
                                        </div>
                                        <ul class="subtheme-list">
                                            <?php foreach ($theme->children(1)->all() as $subtheme): ?>
                                                <!--?//php if(ExerciseVariant::findAll(['theme_id' => $subtheme->id])): ?-->
                                                    <li>
                                                    <div class="tree-item">
                                                        <span class="name"> <?= $subtheme->name ?> - <b><?= count(ExerciseClass::getExercisesById($subtheme->id, $subtheme->subject_id, $class))?> задач</b></span>
                                                    </div>
                                                    </li>
                                                <!--?//php endif ?-->
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php else: ?>
            <p>Нет разделов</p>
        <?php endif; ?>
    </div>
    <br><br>
</div>
