<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SubjectsSchedule */

$this->title = 'Создать расписание предмета';
$this->params['breadcrumbs'][] = ['label' => 'Расписание предметов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subjects-schedule-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'subjects' => $subjects,
        'priceType' => $priceType
    ]) ?>

</div>
