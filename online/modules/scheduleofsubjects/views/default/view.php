<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SubjectsSchedule */

$this->title = $model->subject->name . ' - ' . $model->class . ' класс - ' . $model->day . ', ' . $model->time;
$this->params['breadcrumbs'][] = ['label' => 'Расписание предметов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subjects-schedule-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Предмет',
                'value' => $model->subject->name,
            ],
            [
                'label' => 'Тип цены',
                'value' => $model->priceType->title,
            ],
            'day',
            'time',
            'class',
        ],
    ]) ?>

</div>
