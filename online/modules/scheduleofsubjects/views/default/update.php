<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SubjectsSchedule */

$this->title = 'Редактировать расписание предмета: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Расписание предметов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subjects-schedule-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'subjects' => $subjects,
        'priceType' => $priceType
    ]) ?>

</div>
