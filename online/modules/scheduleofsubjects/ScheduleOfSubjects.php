<?php

namespace online\modules\scheduleofsubjects;

/**
 * ScheduleOfSubjects module definition class
 */
class ScheduleOfSubjects extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'online\modules\scheduleofsubjects\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
