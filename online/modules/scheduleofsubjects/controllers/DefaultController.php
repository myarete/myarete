<?php

namespace online\modules\scheduleofsubjects\controllers;

use common\models\PricesOnHome;
use common\models\Html;
use common\models\SubjectOnHome;
use Yii;
use common\models\SubjectsSchedule;
use common\models\SubjectsScheduleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for SubjectsSchedule model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SubjectsSchedule models.
     * @return mixed
     */
    public function actionIndex()
    {
        //$model = SubjectsSchedule::find()->joinWith('subject')->joinWith('priceType')->where(['subjects_schedule.id' => $id])->one();


        $searchModel = new SubjectsScheduleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SubjectsSchedule model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = SubjectsSchedule::find()->joinWith('subject')->joinWith('priceType')->where(['subjects_schedule.id' => $id])->one();

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new SubjectsSchedule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SubjectsSchedule();

        $subjects = SubjectOnHome::find()->where(['is_active' => 1])->all();
        $priceType = PricesOnHome::find()->all();//Html::find()->where(['like', 'code', 'price_page__price_%', false] )->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'subjects' => $subjects,
                'priceType' => $priceType
            ]);
        }
    }

    /**
     * Updates an existing SubjectsSchedule model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $subjects = SubjectOnHome::find()->where(['is_active' => 1])->all();
        $priceType = PricesOnHome::find()->all();//Html::find()->where(['like', 'code', 'price_page__price_%', false] )->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'subjects' => $subjects,
                'priceType' => $priceType
            ]);
        }
    }

    /**
     * Deletes an existing SubjectsSchedule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SubjectsSchedule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SubjectsSchedule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SubjectsSchedule::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
