<?php

namespace online\modules\questionsandanswers;

/**
 * questionsandanswers module definition class
 */
class QuestionsAndAnswers extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'online\modules\questionsandanswers\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
