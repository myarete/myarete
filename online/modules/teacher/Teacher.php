<?php

namespace online\modules\teacher;

/**
 * teacher module definition class
 */
class Teacher extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'online\modules\teacher\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
