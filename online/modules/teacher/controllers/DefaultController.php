<?php

namespace online\modules\teacher\controllers;

use common\models\GroupLentaItem;
use common\models\ClientVsGroup;
use common\models\Group;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionClients()
    {
        $myGroups = Group::find()->where(['user_id' => Yii::$app->user->id])->all();

        if ($group_id = Yii::$app->request->post('group_id')) {
            if ($client_ids = Yii::$app->request->post('client_ids')) {
                foreach ($client_ids as $client_id) {
                    if (ClientVsGroup::find()->where(['group_id' => $group_id, 'user_id' => $client_id])->one()) {
                        continue;
                    }
                    $model = new ClientVsGroup();
                    $model->group_id = $group_id;
                    $model->user_id = $client_id;
                    $model->confirmed = 1;
                    $model->save();
                }
            }
        }

        return $this->render('clients', [
            'myGroups' => $myGroups
        ]);
    }

    public function actionGroups()
    {
        $groups = Group::find()->where(['user_id' => Yii::$app->user->id])->all();

        return $this->render('groups', [
            'groups' => $groups,
        ]);
    }

    public function actionViewGroup($id)
    {
        $group = Group::findOne($id);
        $lentaItem = new GroupLentaItem();

        if ($lentaItem->load(Yii::$app->request->post()) && $lentaItem->save()) {
            return $this->redirect(['view-group', 'id' => $id]);
        }

        return $this->render('view-group', [
            'group' => $group,
            'lentaItem' => $lentaItem
        ]);
    }

    public function actionCreateGroup()
    {
        $group = new Group;

        if ($group->load(Yii::$app->request->post()) && $group->save()) {
            return $this->redirect(['groups']);
        }

        return $this->render('create-update-group', [
            'group' => $group,
        ]);
    }

    public function actionEditGroup($id)
    {
        $group = Group::findOne($id);

        if ($post = Yii::$app->request->post()) {
            if ($post['Group']['image'] == '')
                $post['Group']['image'] = $group->image;
        }

        if ($group->load($post) && $group->save()) {
            return $this->redirect(['groups']);
        }

        return $this->render('create-update-group', [
            'group' => $group,
        ]);
    }

    public function actionDeleteGroup($id)
    {
        Group::findOne($id)->delete();

        return $this->redirect(['groups']);
    }

    public function actionRemoveClient()
    {
        if (Yii::$app->request->isPost) {
            $user_id = Yii::$app->request->post('user_id');
            $group_id = Yii::$app->request->post('group_id');
            ClientVsGroup::deleteAll(['user_id' => $user_id, 'group_id' => $group_id]);
        }
        return $this->redirect(['clients']);
    }

    public function actionRemoveFromGroup($id)
    {
        if (Yii::$app->request->isPost) {
            ClientVsGroup::findOne($id)->delete();
        }
        return $this->redirect(['teachers']);
    }

    public function actionTeachers()
    {
        $myGroups = ClientVsGroup::find()->where(['user_id' => Yii::$app->user->id])->all(); //группы в кот. я состою

        if (Yii::$app->request->isPost) {
            $to_group = Yii::$app->request->post('to_group');
            foreach ($to_group as $group_id) {
                $model = new ClientVsGroup();
                $model->user_id = Yii::$app->user->id;
                $model->group_id = $group_id;
                $model->save();
            }
            return $this->redirect(['teachers']);
        }

        return $this->render('teachers', [
            'myGroups' => $myGroups,
        ]);
    }

    public function actionAddClient()
    {
        if (Yii::$app->request->isPost) {
            $user_id = Yii::$app->request->post('user_id');
            $group_id = Yii::$app->request->post('group_id');
            $model = ClientVsGroup::find()->where(['user_id' => $user_id, 'group_id' => $group_id])->one();
            if ($model) {
                $model->confirmed = 1;
                $model->update();
            }
        }
        return $this->redirect(['clients']);
    }
}
