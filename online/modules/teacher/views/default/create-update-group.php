<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use common\models\ClientVsGroup;

if (!$group->isNewRecord) {
    $username = ($group->user->profile->first_name || $group->user->profile->last_name)
        ? $group->user->profile->first_name . ' ' . $group->user->profile->last_name : $group->user->email;
}
$this->title = $group->isNewRecord ? 'Новая группа' : $group->name . ' (' . $username . ')';
?>
<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <h3><?= $this->title ?></h3>

            <p>
                <?= Html::a($group->getImageTag([
                    'defaultSrc' => '/img/default_group.png',
                    'style' => 'max-width: 100px;',
                ]), '') ?>
            </p>

            <?php $form = ActiveForm::begin() ?>

            <?= $form->field($group, 'user_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>

            <?= $form->field($group, 'image')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'showPreview' => false,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false
                ]
            ]);
            ?>

            <?= $form->field($group, 'name') ?>

            <?= $form->field($group, 'description')->textarea() ?>

            <?php if (!$group->isNewRecord): ?>
                <p><?= Html::label('Члены группы:') ?></p>
                <?php foreach ($group->clients as $client): ?>
                    <?php
                    if ($client->profile->first_name || $client->profile->last_name)
                        $name = $client->profile->first_name . ' ' . $client->profile->last_name;
                    else
                        $name = $client->email;
                    ?>
                    <p><?= $name ?></p>
                <?php endforeach ?>
                <br>
            <?php endif; ?>
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>