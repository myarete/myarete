<?php

use yii\helpers\Html;

$this->title = 'Мои группы';

$js = <<<JS
$('.invitation').click(function() {
    var id = $(this).data('id');
    $.post('/teacher/default/send-request', {id: id}, function(response) {
        window.location.reload();
    });
});
$('.confirm').click(function() {
    var id = $(this).data('id'),
        btn = $(this);
    $.post('/teacher/default/confirm', {id: id}, function(response) {
        window.location.reload();
    });
});
$('.not-confirm').click(function() {
    var id = $(this).data('id'),
        btn = $(this);
    yii.confirm('Удалить запрос?', function() {
        $.post('/teacher/default/not-confirm', {id: id}, function(response) {
            if(response) alert('Заявка от ' + response + ' удалена.');
            btn.closest('div.row div.col-sm-6[data-key="'+id+'"]').remove();
        });
    });
});
$('.cancel').click(function() {
    var id = $(this).data('id'),
        btn = $(this);
    yii.confirm('Отказаться от ученика?', function() {
        $.post('/teacher/default/not-confirm', {id: id}, function(response) {
            if(response) alert('Ученик ' + response + ' удален.');
            btn.closest('div.row div.col-sm-6[data-key="'+id+'"]').remove();
        });
    });
});
JS;
$this->registerJs($js);
?>
<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="page__header_title">Мои группы</div>
            <div class="event__content">
                <div class="tab-content">
                    <div class="tab-content__item event__content show" id="tab1">
                        <div class="send__traning">
                            <div class="send__traning_top">
                                <div class="send__traning_left">
                                    <a class="button button__blue button__question" data-toggle="modal"
                                       data-target="#questions_and_answers__modal">Вопросы и ответы
                                        <div class="fa fa-question"></div>
                                    </a>
                                </div>
                                <div class="send__traning_right">
                                    <?= Html::a('Добавить группу', 'create-group', ['class' => 'button button__green--full']) ?>
                                </div>
                            </div>
                        </div>
                        <?php if (Yii::$app->user->can('teacher')): ?>
                            <?php if ($groups): ?>
                                <?php foreach ($groups as $group) : ?>
                                    <div class="table__row">
                                        <div class="table__row_item">
                                            <div class="img-circle">
                                                <?= Html::a($group->getImageTag([
                                                    'defaultSrc' => '/img/default_group.png',
                                                ]), '') ?>
                                            </div>
                                        </div>
                                        <div class="table__row_item">
                                            <?= Html::a($group->name, ['view-group', 'id' => $group->id]) ?>
                                        </div>
                                        <div class="table__row_item">
                                            <div class="button__group">
                                                <?= Html::a('Войти в группу', ['view-group', 'id' => $group->id], ['class' => 'button button__green']) ?>
                                                <?= Html::a('Удалить', ['delete-group', 'id' => $group->id], [
                                                    'class' => 'button button__red',
                                                    'data' => [
                                                        'confirm' => 'Удалить группу?',
                                                        'method' => 'post'
                                                    ]
                                                ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <p>
                                    В настоящее время у вас нет ни одной группы. Для создания нажмите кнопку «Добавить
                                    группу».
                                </p>
                            <?php endif ?>
                        <?php endif ?>
                    </div>
                    <div class="tab-content__item event__content" id="tab2">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    @media screen and (max-width: 768px) {
        .crm__container {
            padding: 10px;
        }

        .crm__content {
            padding: 10px 0 0 0;
        }

        .crm__menu__right {
            position: relative;
            width: 100%;
            margin: 0;
        }
    }
</style>