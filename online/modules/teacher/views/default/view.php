<?php

use kartik\widgets\Typeahead;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\DetailView;

$this->title = 'Информация о ученике';

$subject_array = ArrayHelper::getColumn($subject, 'name');
$selected_array = [];
foreach (explode(', ', $client->subject) as $s)
    $selected_array[$s] = ['selected' => true];

$js = <<<JS
$('.delete').click(function() {
    var id = $(this).data('id'),
        parent = $(this).closest('.thumbnail');
    yii.confirm('Удалить заметку?', function() {
        $.post('/teacher/default/delete-note', {id: id}, function(response) {
            parent.hide();
        });
    });
});

$('.edit').click(function() {
    var id = $(this).data('id');
    $('#edit-note form').attr('action', '/teacher/default/edit-note/' + id);
    $('#edit-note textarea').val($(this).data('value'));
    $('#edit-note').modal('show');
});
JS;
$this->registerJs($js);
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <h2><?= $this->title ?></h2>

            <?= DetailView::widget([
                'model' => $user,
                'attributes' => [
                    [
                        'attribute' => 'profile.image',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->profile->getImageTag([
                                'defaultSrc' => '/img/default_user.png',
                                'style' => 'max-width: 100px;',
                            ]);
                        }
                    ],
                    [
                        'attribute' => 'email',
                        'format' => 'raw',
                        'visible' => $user->profile->view_contacts,
                        'value' => function ($model) {
                            return preg_match('/\@/', $model->email) ? $model->email : $model->profile->email;
                        }
                    ],
                    'profile.first_name',
                    'profile.last_name',
                    'profile.birthdate',
                    'profile.state.name:text:Статус',
                ],
            ]) ?>

            <?= DetailView::widget([
                'model' => $client,
                'attributes' => [
                    'class',
                    'subject',
                    'training_time',
                    'other',
                ]
            ]) ?>

            <?= Html::button('Изменить', [
                'class' => 'btn btn-primary',
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#edit-modal'
                ]
            ]) ?>

            <hr>

            <p>
                <?= Html::button('Создать заметку', [
                    'class' => 'btn btn-primary',
                    'data' => [
                        'toggle' => 'modal',
                        'target' => '#add-note'
                    ]
                ]) ?>
            </p>

            <?php foreach ($client->notes as $note): ?>
                <div class="thumbnail">
                    <?= Html::button('&times;', [
                        'class' => 'close delete',
                        'style' => 'margin-left: 10px;',
                        'data' => ['id' => $note->id]
                    ]) ?>
                    <?= Html::button('<i class="glyphicon glyphicon-pencil" style="font-size: 16px"></i>', [
                        'class' => 'close edit',
                        'data' => [
                            'id' => $note->id,
                            'value' => $note->note
                        ]
                    ]) ?>
                    <p><?= $note->note ?></p>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>

<div class="modal fade" id="add-note" data-backdrop="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Новая заметка</h4>
            </div>
            <?= Html::beginForm(['add-note', 'id' => $client->id]) ?>
            <div class="modal-body">
                <div class="form-group">
                    <?= Html::textarea('note', '', ['class' => 'form-control']) ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
            <?= Html::endForm() ?>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-note" data-backdrop="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Изменить заметку</h4>
            </div>
            <?= Html::beginForm() ?>
            <div class="modal-body">
                <div class="form-group">
                    <?= Html::textarea('note', '', ['class' => 'form-control']) ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
            <?= Html::endForm() ?>
        </div>
    </div>
</div>

<div class="modal fade" id="edit-modal" data-backdrop="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Информация о ученике</h4>
            </div>
            <?= Html::beginForm(['save', 'id' => $client->id]) ?>
            <div class="modal-body">
                <div class="form-group">
                    <?= Html::activeTextInput($client, 'class', ['class' => 'form-control', 'placeholder' => 'Класс']) ?>
                </div>
                <div class="form-group">
                    <?= Typeahead::widget([
                        'model' => $client,
                        'attribute' => 'subject',
                        'dataset' => [
                            [
                                'local' => $subject_array,
                                'source' => new JsExpression(
                                    'function(q, syncResults, asyncResults) {
                                            var matches = [], substrRegex;

                                            q = q.match(/\S*[,\s]{0,2}(\S+)$/i)[1];

                                            substrRegex = new RegExp("^" + q, "i");

                                            $.each(learner_subject_data_1.local, function(i, str) {
                                                if (substrRegex.test(str)) {
                                                    matches.push(str);
                                                }
                                            });
                                            syncResults(matches);
                                        }'
                                ),
                            ],
                        ],
                        'options' => [
                            'placeholder' => 'Предметы',
                        ],
                        'pluginEvents' => [
                            'typeahead:select' => 'function(e, s) {
                                    if(subject == "")
                                        subject = s;
                                    else
                                        subject = subject.replace(/[^,\s]+$/, s);
                                    setTimeout(function() {
                                        $("#learner-subject").val(subject);
                                    }, 20);
                                }',
                            'typeahead:change' => 'function() {
                                    $("#learner-subject").val(subject);
                                }',
                            'typeahead:render' => 'function() {
                                    subject = $("#learner-subject").val();
                                }'
                        ]
                    ]) ?>
                </div>
                <div class="form-group">
                    <?= Html::activeTextInput($client, 'training_time', ['class' => 'form-control', 'placeholder' => 'Время занятий']) ?>
                </div>
                <div class="form-group">
                    <?= Html::activeTextarea($client, 'other', ['class' => 'form-control', 'placeholder' => 'Прочее']) ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
            <?= Html::endForm() ?>
        </div>
    </div>
</div>

