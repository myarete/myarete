<?php

use common\models\ClientVsGroup;
use common\models\User;
use kartik\select2\Select2;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;

$this->title = 'Мои ученики';

$js = <<<JS
$('.remove-client').click(function() {
    var user_id = $(this).data('user_id'),
        group_id = $(this).data('group_id'),
        btn = $(this);
    yii.confirm('Отказаться от ученика?', function() {
        $.post('/teacher/default/remove-client', {user_id: user_id, group_id: group_id}, function(response) {
        });
    });
});
$('button.modal-up').on('click', function (e) {
    e.preventDefault();
    var id = e.target.dataset.id;
    $('[name=group_id]').val(id);
    $('#add-client').modal('show');
});
JS;
$this->registerJs($js);
?>
<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="page__header_title">Мои ученики</div>
            <?php foreach ($myGroups as $group): ?>
                <div class="tab-content__item event__content show lg-pb-15">
                    <div class="send__traning">
                        <div class="send__traning_left">
                            <span class="button button__blue button__question" style="padding-left: 15px"><?= $group->name ?></span>
                        </div>
                        <div class="send__traning_right">
                            <?= Html::button('Добавить ученика', ['class' => 'button button__green--full modal-up', 'data-id' => $group->id]) ?>
                        </div>
                    </div>
                    <?php if ($group->clients): ?>
                        <?php foreach ($group->clients as $key => $client) : ?>
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="img-circle">
                                        <?= $client->profile->getImageTag(['defaultSrc' => '/img/default_user.png']) ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <?php if ($client->profile->first_name || $client->profile->last_name): ?>
                                        <?= Html::a($client->profile->first_name . ' ' . $client->profile->last_name, ['view', 'id' => $client->id]) ?>
                                    <?php else: ?>
                                        <?= Html::a($client->email, ['view', 'id' => $client->id]) ?>
                                    <?php endif ?>
                                </div>
                                <div class="col-sm-4">
                                    <div class="button__group text-right">
                                        <?php if (!ClientVsGroup::find()
                                            ->where(['user_id' => $client->id, 'group_id' => $group->id, 'confirmed' => 1])
                                            ->one()): ?>
                                            <?= Html::button('Принять ученика', [
                                                'class' => 'button button__blue add-client',
                                                'data' => [
                                                    'user_id' => $client->id,
                                                    'group_id' => $group->id
                                                ]
                                            ]) ?>
                                        <?php endif; ?>
                                        <?= Html::button('Отказаться от ученика', [
                                            'class' => 'button button__red remove-client',
                                            'data' => [
                                                'user_id' => $client->id,
                                                'group_id' => $group->id
                                            ]
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <p>
                            1. В настоящее время у вас нет ни одного ученика. Вы можете приглашать учеников из
                            числа
                            своих друзей. Для этого нажмите кнопку «Добавить ученика».
                        </p>
                        <p>
                            2. Вы сможете отслеживать прогресс ученика, вести учет его успеваемости,
                            посещаемости,
                            заданий. Также вы сможете задавать ему домашние и классные задания, диагностики и
                            контрольные с помощью созданных тренировок
                        </p>
                    <?php endif ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<?php
Modal::begin([
    'header' => '<h2>Добавить ученика</h2>',
    'id' => 'add-client',
    'footer' => Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'form' => 'users-form'])
        . ' ' . Html::button('Закрыть', ['class' => 'button button_white', 'data-dismiss' => 'modal', 'aria-hidden' => true]),
    'options' => [
        'class' => 'modal-sm'
    ]
]);

$clients = User::find()->where(['not', ['id' => Yii::$app->user->id]])->all();
$clients_array = [];
foreach ($clients as $client) {
    if ($client->profile->first_name || $client->profile->last_name) {
        $clients_array[$client->id] = $client->profile->first_name . ' ' . $client->profile->last_name;
    } else {
        $clients_array[$client->id] = $client->email;
    }
}
?>
<?= Html::beginForm('clients', 'post', ['id' => 'users-form']); ?>
<?= Html::hiddenInput('group_id'); ?>
<div class="row">
    <div class="col-md-12">
        <label>Выбрать клиентов</label>
        <?= Select2::widget([
            'name' => 'client_ids',
            'data' => $clients_array,
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => ['placeholder' => 'Клиенты ...', 'multiple' => true, 'autocomplete' => 'off'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>
</div>
<?= Html::endForm(); ?>
<?php Modal::end(); ?>

