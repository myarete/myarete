<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;

if (!$group->isNewRecord) {
    $username = ($group->user->profile->first_name || $group->user->profile->last_name)
        ? $group->user->profile->first_name . ' ' . $group->user->profile->last_name : $group->user->email;
}
$this->title = $group->isNewRecord ? 'Новая группа' : $group->name . ' (' . $username . ')';
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <h3><?= $this->title ?></h3>
            <div>
                <?= Html::a($group->getImageTag([
                    'defaultSrc' => '/img/default_group.png',
                    'style' => 'max-width: 100px;',
                ]), '') ?>
                <?php if (Yii::$app->user->can('teacher')): ?>
                    <div class="pull-right">
                        <?= Html::a('Редактировать группу', ['edit-group', 'id' => $group->id], ['class' => 'btn btn-success visible-lg-block']) ?>
                        <?= Html::a('Удалить группу', ['delete-group', 'id' => $group->id], ['class' => 'btn btn-danger visible-lg-block lg-mt-10']) ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="panel panel-default lg-p-20 lg-mt-20" style="background-color: #f1f1f1">
                <?= $group->description ?>
            </div>

            <ul class="nav nav-tabs">
                <li class="nav-item active">
                    <a class="nav-link" data-toggle="tab" href="#tab1">Лента</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab2">Заметки</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab3">Состав группы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab4">Материалы</a>
                </li>
            </ul>

            <div class="tab-content lg-pt-10">
                <div class="tab-pane fade active in" id="tab1">
                    <div class="text-right">
                        <?= Html::button('Добавить запись', [
                            'class' => 'btn btn-success',
                            'data' => [
                                'toggle' => 'modal',
                                'target' => '#add-record'
                            ]
                        ]); ?>
                    </div>
                    <?php foreach ($group->groupLentaItems as $_lentaItem): ?>
                        <div class="item-data">
                            <div class="row">
                                <div class="col-sm-1" style="font-size: 12px">
                                    <?= date('Y-m-d', strtotime($_lentaItem->created_dt)); ?>
                                </div>
                                <div class="col-sm-2" style="font-size: 12px">
                                    <?php if ($_lentaItem->user->profile->first_name || $_lentaItem->user->profile->last_name) {
                                        echo $_lentaItem->user->profile->first_name . ' ' . $_lentaItem->user->profile->last_name;
                                    } else {
                                        echo $_lentaItem->user->email;
                                    } ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" style="font-weight: bold;padding-top: 10px">
                                    <?= $_lentaItem->title ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $_lentaItem->text ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="tab-pane fade" id="tab2">
                    tab2
                </div>
                <div class="tab-pane fade" id="tab3">
                    <?php foreach ($group->clients as $client): ?>
                        <p>
                            <?php if ($client->profile->first_name || $client->profile->last_name)
                                echo $client->profile->first_name . ' ' . $client->profile->last_name;
                            else
                                echo $client->email; ?>
                        </p>
                    <?php endforeach; ?>
                </div>
                <div class="tab-pane fade" id="tab4">
                    <div class="form-group">
                        <div class="text-right">
                            <a class="btn btn-success" href="/training/default/saved">Сохраненные</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php foreach ($group->trainings as $training): ?>
                            <?= $training->name . '<br>' ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php Modal::begin([
        'header' => '<h2>Добавить запись</h2>',
        'id' => 'add-record',
        'footer' => Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'form' => 'lenta-form'])
            . ' ' . Html::button('Закрыть', ['class' => 'button button_white', 'data-dismiss' => 'modal', 'aria-hidden' => true]),
        'options' => [
            'class' => 'modal-sm'
        ]
    ]); ?>
    <?php $form = ActiveForm::begin(['id' => 'lenta-form']) ?>
    <?= $form->field($lentaItem, 'group_id')->hiddenInput(['value' => $group->id])->label(false); ?>
    <?= $form->field($lentaItem, 'user_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false); ?>
    <?= $form->field($lentaItem, 'title')->textInput(); ?>
    <?= $form->field($lentaItem, 'text')->textarea(); ?>
    <?php ActiveForm::end(); ?>
    <?php Modal::end(); ?>
