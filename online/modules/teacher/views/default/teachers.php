<?php

use yii\bootstrap\Html;
use yii\bootstrap\Modal;

$this->title = 'Мои учителя';
?>
<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="page__header_title">Мои группы</div>
            <div class="event__content">
                <div class="tab-content">
                    <div class="tab-content__item event__content show" id="tab1">
                        <div class="send__traning">
                            <div class="send__traning_top">
                                <div class="send__traning_left">
                                    <a class="button button__blue button__question" data-toggle="modal"
                                       data-target="#questions_and_answers__modal">Вопросы и ответы
                                        <div class="fa fa-question"></div>
                                    </a>
                                </div>
                                <div class="send__traning_right">
                                    <?= Html::button('Стать учеником в группе', ['class' => 'button button__green--full', 'data' => ['toggle' => 'modal', 'target' => '#add-group']]) ?>
                                </div>
                            </div>
                        </div>
                        <?php if (count($myGroups)): ?>
                            <?php foreach ($myGroups as $key => $group) : ?>
                                <div class="row lg-mb-10">
                                    <div class="col-sm-5 lg-mt-10">
                                        <?= $group->group->name ?>
                                    </div>
                                    <div class="col-sm-4 lg-mt-10">
                                        <?php if ($group->group->user->profile->first_name || $group->group->user->profile->last_name)
                                            echo $group->group->user->profile->first_name . ' ' . $group->group->user->profile->last_name;
                                        else
                                            echo $group->group->user->email;
                                        ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="button__group pull-right">
                                            <?= Html::a('Открыть группу', ['view-group', 'id' => $group->group->id], ['class' => 'button button__green--full']) ?>
                                            <?= Html::a('Выйти из группы', ['remove-from-group', 'id' => $group->id], [
                                                'class' => 'button button__red',
                                                'data' => [
                                                    'confirm' => 'Выйти?',
                                                    'method' => 'post'
                                                ]
                                            ]) ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <p>Вы пока не вошли ни в одну группу.</p>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
\yii\bootstrap\Modal::begin([
    'header' => '<h2>Стать учеником в группе</h2>',
    'id' => 'add-group',
    'footer' => Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'form' => 'groups-form'])
        . ' ' . Html::button('Закрыть', ['class' => 'button button_white', 'data-dismiss' => 'modal', 'aria-hidden' => true]),
    'options' => [
        'class' => 'modal-sm'
    ]
]); ?>

<?= Html::beginForm('teachers', 'post', ['id' => 'groups-form']) ?>
<?php
$groups_ids = \yii\helpers\ArrayHelper::getColumn(\common\models\Group::find()->where(['user_id' => Yii::$app->user->id])->select('id')->all(), 'id');
$groups_ids2 = \yii\helpers\ArrayHelper::getColumn(\common\models\ClientVsGroup::find()->where(['user_id' => Yii::$app->user->id])->select('group_id')->all(), 'group_id');
$groups_ids = array_merge($groups_ids, $groups_ids2);
$groups = \common\models\Group::find()->where(['not in', 'id', $groups_ids])->all()
?>
<?php if ($groups): ?>
    <?php foreach ($groups as $group): ?>
        <div class="row">
            <div class="col-sm-5 lg-mt-10">
                <?= $group->name ?>
            </div>
            <div class="col-sm-4 lg-mt-10">
                <?php if ($group->user->profile->first_name || $group->user->profile->last_name)
                    echo $group->user->profile->first_name . ' ' . $group->user->profile->last_name;
                else
                    echo $group->user->email;
                ?>
            </div>
            <div class="col-sm-3">
                <div class="checkbox">
                    <label for="checkbox<?= $group->id ?>">
                        <?= Html::checkbox('to_group[]', false, ['value' => $group->id, 'id' => 'checkbox' . $group->id]) ?>
                        Войти в группу
                    </label>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<?= Html::endForm() ?>
<?php Modal::end(); ?>

