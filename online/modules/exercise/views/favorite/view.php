<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="crm__container">
    <div class="crm__content">
        <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
            <?= \kartik\alert\AlertBlock::widget([
                'delay' => false
            ]) ?>
        <?php endif; ?>
        <div class="wihite__box shadow">
            <div class="traning">
                <div class="traning__title"><?= '№' . $model->exercise->id . ' ' . $model->exercise->name ?></div>
                <div class="traning__list">
                    <div class="traning__list_item" id="id_<?= $model->exercise->id ?>">
                        <div class="traning__list_action">
                            <a class="button button__round button__green traning__btn_more" href="#">
                                <span class="traning__btn_more--show">Подробнее </span>
                                <span class="traning__btn_more--hide">Скрыть </span>
                            </a>
                            <?= Html::a(
                                '<div class="fa '.(($model->exerciseFavorite) ? 'fa-star' : 'fa-star-o').'"></div>',
                                [(($model->exerciseFavorite) ? '/exercise/favorite/delete' : '/exercise/favorite/add'),
                                    'id' => (($model->exerciseFavorite) ? $model->exerciseFavorite->id : $model->exercise->exercise_variant_id)],
                                [
                                    'class' => 'button button__round button__icon '.(($model->exerciseFavorite) ? 'button__red' : 'button__blue'),
                                ]
                            ) ?>

                        </div>
                        <div class="traning__list_center" class="collapse" data-id="<?= $model->exercise->id ?>">
                            <div class="traning__list_row">
                                <div class="traning__task">
                                    <p><?=$model->exercise->question?></p>
                                </div>
                            </div>
                            <?php if (!empty($model->exercise->hint)): ?>
                                <div class="traning__list_row">
                                    <a class="traning__link" href="#<?= "hint-{$model->exercise->id}" ?>" data-traning-link="<?= "hint-{$model->exercise->id}" ?>" data-toggle="collapse">
                                        <span class="traning__link--show">Показать подсказку</span>
                                        <span class="traning__link--hide">Скрыть подсказку</span>
                                    </a>
                                    <div class="traning__link_slide collapse" id="<?= "hint-{$model->exercise->id}" ?>"  data-id="<?= $model->exercise->id?>">
                                        <p><?= $model->exercise->hint ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($model->exercise->hint_second)): ?>
                                <div class="traning__list_row">
                                    <a class="traning__link" href="#<?= "hint_second-{$model->exercise->id}" ?>" data-traning-link="<?= "hint_second-{$model->exercise->id}" ?>" data-toggle="collapse">
                                        <span class="traning__link--show">Показать вторую подсказку</span>
                                        <span class="traning__link--hide">Скрыть вторую подсказку</span>
                                    </a>
                                    <div class="traning__link_slide collapse" id="<?= "hint_second-{$model->exercise->id}" ?>"  data-id="<?= $model->exercise->id?>">
                                        <p><?= $model->exercise->hint_second ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($model->exercise->hint_third)): ?>
                                <div class="traning__list_row">
                                    <a class="traning__link" href="#<?= "hint_third-{$model->exercise->id}" ?>" data-traning-link="<?= "hint_third-{$model->exercise->id}" ?>" data-toggle="collapse">
                                        <span class="traning__link--show">Показать третью подсказку</span>
                                        <span class="traning__link--hide">Скрыть третью подсказку</span>
                                    </a>
                                    <div class="traning__link_slide collapse" id="<?= "hint_third-{$model->exercise->id}" ?>"  data-id="<?= $model->exercise->id?>">
                                        <p><?= $model->exercise->hint_third ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($model->exercise->decision)): ?>
                                <div class="traning__list_row">
                                    <a class="traning__link" href="#<?= "decision-{$model->exercise->id}" ?>" data-traning-link="<?= "decision-{$model->exercise->id}" ?>" data-toggle="collapse">
                                        <span class="traning__link--show">Показать решение</span>
                                        <span class="traning__link--hide">Скрыть решение</span>
                                    </a>
                                    <div class="traning__link_slide collapse" id="<?= "decision-{$model->exercise->id}" ?>"  data-id="<?= $model->exercise->id?>">
                                        <p><?= $model->exercise->decision ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($model->exercise->decision_detailed)): ?>
                                <div class="traning__list_row">
                                    <a class="traning__link" href="#<?= "decision_detailed-{$model->exercise->id}" ?>" data-traning-link="<?= "decision_detailed-{$model->exercise->id}" ?>" data-toggle="collapse">
                                        <span class="traning__link--show">Показать подробное решение</span>
                                        <span class="traning__link--hide">Скрыть подробное решение</span>
                                    </a>
                                    <div class="traning__link_slide collapse" id="<?= "decision_detailed-{$model->exercise->id}" ?>"  data-id="<?= $model->exercise->id?>">
                                        <p><?= $model->exercise->decision_detailed ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($model->exercise->decision_second)): ?>
                                <div class="traning__list_row">
                                    <a class="traning__link" href="#<?= "decision_second-{$model->exercise->id}" ?>" data-traning-link="<?= "decision_second-{$model->exercise->id}" ?>" data-toggle="collapse">
                                        <span class="traning__link--show">Показать второе решение</span>
                                        <span class="traning__link--hide">Скрыть второе решение</span>
                                    </a>
                                    <div class="traning__link_slide collapse" id="<?= "decision_second-{$model->exercise->id}" ?>"  data-id="<?= $model->exercise->id?>">
                                        <p><?= $model->exercise->decision_second ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($model->exercise->answer)): ?>
                                <div class="traning__list_row">
                                    <a class="traning__link" href="#<?= "answer-{$model->exercise->id}" ?>" data-traning-link="<?= "answer-{$model->exercise->id}" ?>" data-toggle="collapse">
                                        <span class="traning__link--show">Показать ответ</span>
                                        <span class="traning__link--hide">Скрыть ответ</span>
                                    </a>
                                    <div class="traning__link_slide collapse" id="<?= "answer-{$model->exercise->id}" ?>" data-id="<?= $model->exercise->id?>">
                                        <p><?= $model->exercise->answer ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="traning__list_row"> <a class="traning__link" href="#" data-traning-link="t2"> <span class="traning__link--show">Добавить пространство для решения</span><span class="traning__link--hide">Скрыть пространство для решения</span></a>
                                <div class="traning__link_slide" id="t2">
                                    <div class="traning__field_wrap">
                                        <div class="traning__field_toggle">
                                            <div class="traning__field_toggle-item traning__field_toggle-item--blank" data-style="blank"></div>
                                            <div class="traning__field_toggle-item traning__field_toggle-item--line" data-style="line"></div>
                                            <div class="traning__field_toggle-item traning__field_toggle-item--cage" data-style="cage"></div>
                                        </div>
                                        <div class="traning__field">
                                            <textarea> </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="traning__sublist">
                            <div class="traning__sublist_row">
                                <div class="traning__sublist_name">Задача №<?= $model->exercise->id ?> <?= $model->exercise->name ?></div>
                            </div>
                            <div class="traning__sublist_row">
                                <div class="traning__sublist_name">Тема:</div>
                                <div class="traning__sublist_text"><?=$model->theme->getFullName()?></div>
                            </div>
                            <div class="traning__sublist_row">
                                <div class="traning__sublist_name">Сложность:</div>
                                <div class="traning__sublist_text"><?=$model->complexity?></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>