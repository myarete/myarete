<?php
use yii\helpers\Html;

if(!$model) return;

$favorite = null;

foreach($model->variants as $variant) {
	if($variant->exerciseFavorite) {
		$favorite = $variant;
		break;
    }
}
?>

<div class="crm__container">
    <div class="crm__content">
        <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
            <?= \kartik\alert\AlertBlock::widget([
                'delay' => false
            ]) ?>
        <?php endif; ?>
        <div class="wihite__box shadow">
            <div class="traning">
                <div class="traning__title">Задача #<?=$model->id?></div>
                <div class="traning__list">
                    <?php if(isset($variant)): ?>
                    <div class="traning__list_item">
                        <div class="traning__list_action">
                            <?= Html::a(
                                '<div class="fa '.(($favorite) ? 'fa-star' : 'fa-star-o').'"></div>',
                                [(($favorite) ? '/exercise/favorite/delete' : '/exercise/favorite/add'),
                                    'id' => $favorite ? $favorite->exerciseFavorite->id : $variant->id],
                                [
                                    'class' => 'button button__round button__icon '.(($favorite) ? 'button__red' : 'button__blue'),
                                ]
                            ) ?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="traning__list_item">
                        <div class="traning__list_center" class="collapse">
                            <div class="traning__list_row">
                                <div class="traning__task">
                                    <p><?=$model->question?></p>
                                </div>
                            </div>


                            <?php if (!empty($model->hint)): ?>
                                <div class="traning__list_row">
                                    <a class="traning__link" href="#<?= "hint-{$model->id}" ?>" data-traning-link="<?= "hint-{$model->id}" ?>" data-toggle="collapse">
                                        <span class="traning__link--show">Показать подсказку</span>
                                        <span class="traning__link--hide">Скрыть подсказку</span>
                                    </a>
                                    <div class="traning__link_slide collapse" id="<?= "hint-{$model->id}" ?>"  data-id="<?= $model->id?>">
                                        <p><?= $model->hint ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($model->hint_second)): ?>
                                <div class="traning__list_row">
                                    <a class="traning__link" href="#<?= "hint_second-{$model->id}" ?>" data-traning-link="<?= "hint_second-{$model->id}" ?>" data-toggle="collapse">
                                        <span class="traning__link--show">Показать вторую подсказку</span>
                                        <span class="traning__link--hide">Скрыть вторую подсказку</span>
                                    </a>
                                    <div class="traning__link_slide collapse" id="<?= "hint_second-{$model->id}" ?>"  data-id="<?= $model->id?>">
                                        <p><?= $model->hint_second ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($model->hint_third)): ?>
                                <div class="traning__list_row">
                                    <a class="traning__link" href="#<?= "hint_third-{$model->id}" ?>" data-traning-link="<?= "hint_third-{$model->id}" ?>" data-toggle="collapse">
                                        <span class="traning__link--show">Показать третью подсказку</span>
                                        <span class="traning__link--hide">Скрыть третью подсказку</span>
                                    </a>
                                    <div class="traning__link_slide collapse" id="<?= "hint_third-{$model->id}" ?>"  data-id="<?= $model->id?>">
                                        <p><?= $model->hint_third ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($model->decision)): ?>
                                <div class="traning__list_row">
                                    <a class="traning__link" href="#<?= "decision-{$model->id}" ?>" data-traning-link="<?= "decision-{$model->id}" ?>" data-toggle="collapse">
                                        <span class="traning__link--show">Показать решение</span>
                                        <span class="traning__link--hide">Скрыть решение</span>
                                    </a>
                                    <div class="traning__link_slide collapse" id="<?= "decision-{$model->id}" ?>"  data-id="<?= $model->id?>">
                                        <p><?= $model->decision ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($model->decision_detailed)): ?>
                                <div class="traning__list_row">
                                    <a class="traning__link" href="#<?= "decision_detailed-{$model->id}" ?>" data-traning-link="<?= "decision_detailed-{$model->id}" ?>" data-toggle="collapse">
                                        <span class="traning__link--show">Показать подробное решение</span>
                                        <span class="traning__link--hide">Скрыть подробное решение</span>
                                    </a>
                                    <div class="traning__link_slide collapse" id="<?= "decision_detailed-{$model->id}" ?>"  data-id="<?= $model->id?>">
                                        <p><?= $model->decision_detailed ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($model->decision_second)): ?>
                                <div class="traning__list_row">
                                    <a class="traning__link" href="#<?= "decision_second-{$model->id}" ?>" data-traning-link="<?= "decision_second-{$model->id}" ?>" data-toggle="collapse">
                                        <span class="traning__link--show">Показать второе решение</span>
                                        <span class="traning__link--hide">Скрыть второе решение</span>
                                    </a>
                                    <div class="traning__link_slide collapse" id="<?= "decision_second-{$model->id}" ?>"  data-id="<?= $model->id?>">
                                        <p><?= $model->decision_second ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if (!empty($model->answer)): ?>
                                <div class="traning__list_row">
                                    <a class="traning__link" href="#<?= "answer-{$model->id}" ?>" data-traning-link="<?= "answer-{$model->id}" ?>" data-toggle="collapse">
                                        <span class="traning__link--show">Показать ответ</span>
                                        <span class="traning__link--hide">Скрыть ответ</span>
                                    </a>
                                    <div class="traning__link_slide collapse" id="<?= "answer-{$model->id}" ?>" data-id="<?= $model->id?>">
                                        <p><?= $model->answer ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if($model->variants && $trainings): ?>
                                <div>
                                    <?= Html::a('Добавить в тренировку', "#training-{$model->id}", ['data-toggle' => 'collapse']) ?>
                                    <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "training-{$model->id}"])?>
                                    <?= Html::beginForm(['add-to-training']) ?>
                                    <?= Html::hiddenInput('variant_id', $variant->id) ?>
                                    <div class="form">
                                        <div class="form__row">
                                        <?php foreach($trainings as $training): ?>

                                                <div class="form__label"><?=$training->name?></div>
                                                <div class="form__control">
                                                    <?= Html::checkbox('trainings[]', false, ['value' => $training->id]) ?>
                                                </div>

                                        <?php endforeach; ?>
                                        </div>
                                        <div class="form__row">
                                            <?= Html::submitButton('Добавить', ['class' => 'button button__green']) ?>
                                        </div>
                                    </div>
                                    <?= Html::endForm() ?>
                                    <?= Html::endTag('div') ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

