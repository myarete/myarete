<?php
  use common\models\ExerciseVariantFavoriteSearch;
  use yii\grid\GridView;
  use common\models\Training;
  use yii\helpers\Html;
  use common\components\other\FirstWords;

$this->title = 'Избранные задачи';
?>

<div class="crm__container">
    <div class="crm__content">
        <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
            <?= \kartik\alert\AlertBlock::widget([
                'delay' => false
            ]) ?>
        <?php endif; ?>
        <div class="wihite__box shadow">
            <div class="page__header_title">Избранные задачи</div>
            <div class="event__content">
                <?php if(count($trainings)):?>
                    <?php foreach($trainings as $training) : ?>
                        <div class="table__row">
                            <div class="table__row_item">
                                <?= '№' . $training->exerciseVariant->exercise->id . ' ' . Html::a($training->exerciseVariant->exercise->name, ['/exercise/favorite/view', 'id' => $training->exerciseVariant->id]) ?>
                            </div>



                            <div class="table__row_item">
                                <div class="button__group">
                                    <?= Html::a('Открыть', ['/exercise/favorite/view', 'id' => $training->exerciseVariant->id], ['class' => 'button button__green']); ?>
                                    <?= Html::a('<i class="fa fa-trash-o"></i>', ['/exercise/favorite/delete', 'id' => $training->id], ['class' => 'button button__red button__icon']); ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <p>Вы пока не создали ни одной тренировки</p>
                <?php endif?>
            </div>
        </div>
    </div>
</div>

