<?php

use common\models\Exercise;
use common\models\ExerciseType;
use common\models\Theme;
use common\models\User;
use common\models\Subject;
use yii\helpers\Html;
use yii\grid\GridView;
use common\components\other\FirstWords;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ExerciseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Задачи';
$this->params['breadcrumbs'][] = $this->title;

//$js = <<<JS
//    $('#reset-search-theme').click(function(){
//
//        $('#search-theme').data('treeinput').$element.val('').trigger('change');
//
//        return false;
//    });
//JS;
//$this->registerJs($js);
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="ibox-title">
                <h5><?= $this->title ?></h5>
                <div class="ibox-tools">
                    <?= Html::a('', ['create'], ['class' => 'fa fa-plus', 'title' => 'Создать']) ?>
                </div>
            </div>

            <div class="ibox-content">

                <div class="row">
                    <div class="col-xs-3">
                        <label for="search-subject">Предмет</label>
                        <?= Html::activeDropDownList(
                            $searchModel,
                            'subject_id',
                            Subject::getAll(),
                            [
                                'prompt' => '-- all --',
                                'class' => 'form-control filter-selector',
                            ]
                        ) ?>
                    </div>
                    <div class="col-xs-6">
                        <label>Тема</label>
                        (<a href="" id="reset-search-theme">Сбросить</a>)
                        <?= \kartik\tree\TreeViewInput::widget([
                            'id' => 'search-theme',
                            'model' => $searchModel,
                            'attribute' => 'theme_id',
                            'query' => Theme::find()->where(['subject_id' => $searchModel->subject_id])->addOrderBy('root, lft'),
                            'headingOptions' => ['label' => 'Тема'],
                            'rootOptions' => ['label' => '<i class="fa fa-tree text-success"></i>'],
                            'fontAwesome' => true,
                            'asDropdown' => true,
                            'multiple' => false,
                            'options' => [
                                'disabled' => empty($searchModel->subject_id),
                                'class' => 'filter-selector'
                            ],
                            'dropdownConfig' => [
                                'input' => [
                                    'placeholder' => 'Все',
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>

                <hr>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'id' => 'exercise-grid',
                    'filterSelector' => '.filter-selector, #exercise-grid input, #exercise-grid select',
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'id',
                            'contentOptions' => [
                                'style' => 'width: 50px;'
                            ],
                        ],
                        [
                            'attribute' => 'name',
                            'format' => 'raw',
                            'value' => function (Exercise $exercise) {
                                return Html::a($exercise->name, ['view', 'id' => $exercise->id]);
                            },
                        ],
                        [
                            'attribute' => 'question',
                            'filter' => false,
                            'format' => 'raw',
                            'value' => function (Exercise $exercise) {
                                return FirstWords::get($exercise->question, 30);
                            },
                        ],
                        [
                            'attribute' => 'subject_id',
                            'filter' => false,
                            'value' => function (Exercise $exercise) {
                                return $exercise->subject->name;
                            },
                        ],
                        [
                            'attribute' => 'type_id',
                            'value' => function (Exercise $exercise) {
                                return $exercise->type->name;
                            },
                            'filter' => Html::activeDropDownList(
                                $searchModel,
                                'type_id',
                                ExerciseType::getAll(),
                                ['prompt' => '-- all --', 'class' => 'form-control']
                            ),
                        ],
                        [
                            'attribute' => 'is_approved',
                            'format' => 'raw',
                            'filter' => Html::activeDropDownList(
                                $searchModel,
                                'is_approved',
                                [1 => 'Одобрено', 0 => 'Не одобрено'],
                                ['prompt' => '-- all --', 'class' => 'form-control']
                            ),
                            'value' => function (Exercise $exercise) {
                                if ($exercise->is_approved) {
                                    return Html::tag('span', 'Одобрено', ['class' => 'label label-primary']);
                                } else {
                                    $value = Html::tag('span', 'Не одобрено', ['class' => 'label label-danger']);
                                    if (Yii::$app->user->can('admin'))
                                        $value .= ' ' . Html::a('Одобрить', ['approve', 'id' => $exercise->id]);
                                    return $value;
                                }
                            },
                        ],
                        [
                            'attribute' => 'user_id',
                            'filter' => Html::activeDropDownList(
                                $searchModel,
                                'user_id',
                                User::getAll(),
                                ['prompt' => '-- all --', 'class' => 'form-control']
                            ),
                            'value' => function (Exercise $exercise) {
                                return $exercise->user->email;
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['style' => 'width: 1px; white-space: nowrap;'],
                            'buttons' => [
                                'update' => function ($url, $model, $key) {
                                    if (!$model->allowEdit())
                                        return '';
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                                },
                                'delete' => function ($url, $model, $key) {
                                    if (!$model->allowEdit())
                                        return '';
                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['data' => [
                                        'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                        'method' => 'post'
                                    ]]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
