<?php
use yii\helpers\Html;
?>
<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="side-block">
        <h2>Избранное</h2>

        <hr>

        <div>

            <div class="exercise-header">
                <div class="row">
                    <div class="col-xs-3">
                        <strong>Задача #<?=$model->exercise->id?></strong>
                    </div>
                </div>
                <div>
                    <strong>Тема:</strong> <?=$model->theme->getFullName()?>
                </div>
                <div>
                    <strong>Сложность:</strong> <?=$model->complexity?>
                </div>


            </div>

            <div>
                <?=$model->exercise->question?>
            </div>

            <?php if (!empty($model->exercise->hint)): ?>
                <div>
                    <?= Html::a('Показать подсказку', "#hint-{$model->exercise->id}", ['data-toggle' => 'collapse']) ?>
                    <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "hint-{$model->exercise->id}"])?>
                    <?= $model->exercise->hint ?>
                    <?= Html::endTag('div') ?>
                </div>
            <?php endif; ?>

            <?php if (!empty($model->exercise->hint_second)): ?>
                <div>
                    <?= Html::a('Показать вторую подсказку', "#hint-second-{$model->exercise->id}", ['data-toggle' => 'collapse']) ?>
                    <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "hint-second-{$model->exercise->id}"])?>
                    <?= $model->exercise->hint_second ?>
                    <?= Html::endTag('div') ?>
                </div>
            <?php endif; ?>

            <?php if (!empty($model->exercise->hint_third)): ?>
                <div>
                    <?= Html::a('Показать третью подсказку', "#hint-third-{$model->exercise->id}", ['data-toggle' => 'collapse']) ?>
                    <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "hint-third-{$model->exercise->id}"])?>
                    <?= $model->exercise->hint_third ?>
                    <?= Html::endTag('div') ?>
                </div>
            <?php endif; ?>

            <?php if (!empty($model->exercise->decision)): ?>
                <div>
                    <?= Html::a('Показать решение', "#decision-{$model->exercise->id}", ['data-toggle' => 'collapse']) ?>
                    <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "decision-{$model->exercise->id}"])?>
                    <?= $model->exercise->decision ?>
                    <?= Html::endTag('div') ?>
                </div>
            <?php endif; ?>

            <?php if (!empty($model->exercise->decision_detailed)): ?>
                <div>
                    <?= Html::a('Показать подробное решение', "#decision-detailed-{$model->exercise->id}", ['data-toggle' => 'collapse']) ?>
                    <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "decision-detailed-{$model->exercise->id}"])?>
                    <?= $model->exercise->decision_detailed ?>
                    <?= Html::endTag('div') ?>
                </div>
            <?php endif; ?>

            <?php if (!empty($model->exercise->decision_second)): ?>
                <div>
                    <?= Html::a('Показать второе решение', "#decision-second-{$model->exercise->id}", ['data-toggle' => 'collapse']) ?>
                    <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "decision-second-{$model->exercise->id}"])?>
                    <?= $model->exercise->decision_second?>
                    <?= Html::endTag('div') ?>
                </div>
            <?php endif; ?>

            <?php if (!empty($model->exercise->answer)): ?>
                <div>
                    <?= Html::a('Показать ответ', "#answer-{$model->exercise->id}", ['data-toggle' => 'collapse']) ?>
                    <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "answer-{$model->exercise->id}"])?>
                    <?= $model->exercise->answer ?>
                    <?= Html::endTag('div') ?>
                </div>
            <?php endif; ?>

        </div>
    </div>
    <br><br>
</div>
