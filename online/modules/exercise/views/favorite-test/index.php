<?php
  use common\models\ExerciseVariantFavoriteTestSearch;
  use yii\grid\GridView;
  use common\models\Training;
  use yii\helpers\Html;

?>
<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="side-block">
      <h2>Избранные тесты</h2>

      <hr>

      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
          //                    [
          //                        'class' => \yii\grid\SerialColumn::className(),
          //                    ],
          [
            'header' => '#',
            'format' => 'raw',
            'value'  => function (ExerciseVariantFavoriteTestSearch $model)
            {
              return $model->exerciseVariant->id;
            },
          ],
          [
            'header' => 'Название',
            'format' => 'raw',
            'value'  => function (ExerciseVariantFavoriteTestSearch $model)
            {
              return Html::a($model->exerciseVariant->exercise->name, ['/exercise/favorite/view', 'id' => $model->exerciseVariant->id]);
            },
          ],
          [
            'contentOptions' => ['class' => 'action-column'],
            'template'       => '{delete}',
            'class'          => \yii\grid\ActionColumn::className(),
          ],
        ],
      ]); ?>
    </div>
    <br><br>
</div>


