<?php

namespace online\modules\exercise;

class Exercise extends \yii\base\Module
{
    public $controllerNamespace = 'online\modules\exercise\controllers';

    public function init()
    {
        parent::init();
    }
}
