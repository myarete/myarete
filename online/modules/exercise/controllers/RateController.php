<?php

namespace online\modules\exercise\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\ExerciseRating;

class RateController extends \yii\web\Controller
{
    public $layout = '/old';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }


    /**
     * @param $exercise_id
     * @param $class
     * @param $rating
     * @return string
     */
    public function actionAdd($exercise_id, $class, $rating)
    {
        $model = ExerciseRating::findOne([
            'user_id' => Yii::$app->user->id,
            'exercise_id' => $exercise_id,
        ]);

        if(!$model)
            $model = new ExerciseRating();

        $model->user_id = Yii::$app->user->id;
        $model->exercise_id = $exercise_id;
        $model->class = $class;
        $model->rating = $rating;

        $model->save();

        return $this->redirect(Yii::$app->request->referrer);
    }
}
