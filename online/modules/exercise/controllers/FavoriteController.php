<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 19.07.15
 * Time: 14:03
 */

namespace online\modules\exercise\controllers;

use common\components\yii\base\BaseController;
use common\models\Exercise;
use common\models\ExerciseVariant;
use common\models\ExerciseVariantFavorite;
use common\models\ExerciseVariantFavoriteSearch;
use common\models\Training;
use common\models\TrainingVsExerciseVariant;
use Yii;
use yii\filters\AccessControl;

class FavoriteController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $trainings = ExerciseVariantFavorite::find()->with('exerciseVariant')->where(['user_id' => Yii::$app->user->id])->all();

        $searchModel = new ExerciseVariantFavoriteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'trainings' => $trainings
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionAdd($id)
    {
        $model = new ExerciseVariantFavorite;
        $model->exercise_variant_id = $id;
        $model->user_id = Yii::$app->user->id;
        $model->save();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {

        $model = ExerciseVariantFavorite::findOne(['id' => $id]);

        $model->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => ExerciseVariant::findOne($id),
            'trainings' => Training::findAll(['user_id' => Yii::$app->user->id])
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionSearchExercise($id)
    {
        $model = Exercise::findOne($id);

        if(!$model){
            Yii::$app->session->setFlash('warning', 'Задачи с таким номером не существует. Попробуйте другой номер задачи.');
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('view-exercise', [
            'model' => $model,
            'trainings' => Training::findAll(['user_id' => Yii::$app->user->id])
        ]);
    }

    public function actionAddToTraining()
    {
		$variant_id = Yii::$app->request->post('variant_id');

		if(Yii::$app->request->post('trainings')) {
			foreach(Yii::$app->request->post('trainings') as $training_id) {

				$model = TrainingVsExerciseVariant::findAll(['exercise_variant_id' => $variant_id, 'training_id' => $training_id]);

				if(!$model) {
					$model = new TrainingVsExerciseVariant;

					$model->exercise_variant_id = $variant_id;
					$model->training_id = $training_id;
					$model->save(false);
				}
			}
		}

		return $this->redirect(Yii::$app->request->referrer);
    }
}
