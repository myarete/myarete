<?php
/**
 * Created by PhpStorm.
 * User: Aleksey Andreyanov (wrewolf@gmail.com)
 * Date: 19.07.15
 * Time: 14:03
 */

namespace online\modules\exercise\controllers;

use common\components\yii\base\BaseController;
use common\models\ExerciseVariant;
use common\models\ExerciseVariantFavoriteTest;
use common\models\ExerciseVariantFavoriteTestSearch;
use Yii;
use yii\filters\AccessControl;

class FavoriteTestController extends BaseController
{
    public $layout = '/old';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ExerciseVariantFavoriteTestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionAdd($id)
    {
        $model = new ExerciseVariantFavoriteTest;
        $model->exercise_variant_id = $id;
        $model->user_id = Yii::$app->user->id;
        $model->save();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {

        $model = ExerciseVariantFavoriteTest::findOne(['id' => $id]);

        $model->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => ExerciseVariant::findOne($id),
        ]);
    }
}
