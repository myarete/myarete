<?php

namespace online\modules\subject;

class Subject extends \yii\base\Module
{
    public $controllerNamespace = 'online\modules\subject\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
