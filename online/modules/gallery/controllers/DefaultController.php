<?php

namespace online\modules\gallery\controllers;

use common\components\yii\base\BaseController;
use Yii;
use common\models\Album;
use yii\filters\AccessControl;

/**
 * DefaultController implements the CRUD actions for Album model.
 */
class DefaultController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param null $album_id
     * @return mixed
     */
    public function actionSort($album_id = null)
    {
        $album = $album_id === null ? null : Album::findOne($album_id);

        return $this->render('sort', [
            'album' => $album,
        ]);
    }
}
