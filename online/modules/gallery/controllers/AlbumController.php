<?php

namespace online\modules\gallery\controllers;

use common\components\yii\base\BaseController;
use Yii;
use common\models\Album;
use yii\filters\AccessControl;

/**
 * AlbumController implements the CRUD actions for Album model.
 */
class AlbumController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actionCreate()
    {
        Yii::$app->response->format = 'json';
        $model = new Album;
        $success = $model->load(Yii::$app->request->post()) && $model->save();
        return ['success' => $success];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = 'json';
        return ['success' => (bool)Album::deleteAll(['id' => $id])];
    }

    /**
     * @return array
     */
    public function actionEditable()
    {
        Yii::$app->response->format = 'json';

        if (!isset($_POST['editableKey']))
            return ['success' => false, 'message' => 'Primary key not defined'];

        $model = Album::findOne($_POST['editableKey']);
        if ($model === null)
            return ['success' => false, 'message' => 'Model not found'];

        if (!isset($_POST['AlbumSearch']) || !isset($_POST['editableIndex']) || !isset($_POST['AlbumSearch'][$_POST['editableIndex']]))
        {
            $this->badRequest();
        }

        $attributes = $_POST['AlbumSearch'][$_POST['editableIndex']];

        $model->attributes = $attributes;

        if (!$model->save())
            return ['success' => false, 'errors' => $model->errors];

        $model->refresh();
        foreach ($attributes as $attribute => $value)
        {
            if ($model->$attribute != $value)
                return ['success' => false, 'message' => "Attribute '{$attribute}' must be equal '{$value}', but has value '{$model->$attribute}'"];
        }

        return ['success' => true];
    }
}
