<?php

use common\models\Photo;
use kartik\file\FileInput;

$js = <<<JS
    $('#upload-image-field').on('filebatchuploadcomplete', function(event, file, previewId, index, reader) {
        window.photoGrid.reload();
        $(this).fileinput('refresh');
    });
JS;
$this->registerJs($js);
?>


<style>
    .file-drop-zone-title {
        font-size: 18px;
        padding: 0;
    }
    
    .file-thumbnail-footer {
        display: none;
    }
    
    .file-preview-frame {
        height: auto;
        padding: 2px;
        margin: 3px;
        box-shadow: 1px 1px 1px 1px #A2958A;
    }
</style>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            
            <div class="ibox-title">
                <h5>Добавить изображение</h5>
            </div>
            
            <div class="ibox-content">
                
                <?= FileInput::widget([
                    'model' => new Photo(),
                    'attribute' => 'image',
                    'language' => 'ru',
                    'options' => [
                        'multiple' => true,
                        'id' => 'upload-image-field',
                    ],
                    'pluginOptions' => [
                        'allowedFileExtensions' => ['jpg', 'jpeg', 'gif', 'png'],
                        'uploadUrl' => '/gallery/photo/upload',
                        'showCaption' => false,
                        'previewSettings' => [
                            'image' => ['width' => 'auto', 'height' => '40px']
                        ],
                    ],
                ]) ?>
            
            </div>
        </div>
    </div>
</div>
