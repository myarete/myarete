<?php

use common\models\Album;
use yii\bootstrap\ActiveForm;
use kartik\grid\GridView;

$js = <<<JS
    /**
     *
     */
    var modal = $('#add-album-modal'),
        form = $('#add-album-form'),
        grid = $('#album-list');


    /**
     *
     */
    form.on('beforeValidate', function() {

        $.post(
            this.getAttribute('action'),
            $(this).serializeArray()
        ).success(function(response){
            if ('success' in response && response.success)
            {
                modal.modal('hide');
                grid.reload()
            }
            else
            {
                alert('Error');
            }
        });

        return false;
    });


    /**
     *
     */
    modal.on('shown.bs.modal', function () {
        form.find('input[type="text"]').focus();
    });


    /**
     *
     */
    modal.on('hidden.bs.modal', function () {
        form[0].reset();
    });


    /**
     * Delete album
     */
    grid.on('click', 'a[title="Delete"]', function() {
        if (!confirm('Вы уверены?')) return false;
        $.get(this.href).success(function(){
            grid.reload();
        });
        return false;
    });
JS;
$this->registerJs($js);
?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            
            <div class="ibox-title">
                <h5>Альбомы</h5>
                <div class="ibox-tools">
                    <a class="header-button fa fa-plus" href="#" data-toggle="modal" data-target="#add-album-modal"></a>
                </div>
            </div>
            
            <div class="ibox-content">
                
                <?= GridView::widget([
                    'id' => 'album-list',
                    'dataProvider' => (new \common\models\AlbumSearch())->search([]),
                    'layout' => '{items}',
                    'export' => false,
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'options' => ['style' => 'width: 1%'],
                        ],
                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'name',
                            'editableOptions' => function ($model, $key, $index) {
                                return [
                                    'valueIfNull' => Yii::$app->formatter->nullDisplay,
                                    'ajaxSettings' => [
                                        'url' => ['/gallery/album/editable'],
                                    ],
                                ];
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'controller' => 'album',
                            'contentOptions' => [
                                'class' => 'action-column',
                                'style' => 'padding: 8px 6px;',
                            ],
                            'template' => '{delete}',
                        ],
                    ],
                ]); ?>
            
            </div>
        </div>
    </div>
</div>


<div class="modal" id="add-album-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Добавить альбом</h4>
            </div>
            
            <?php $form = ActiveForm::begin([
                'id' => 'add-album-form',
                'action' => ['album/create'],
            ]); ?>
            
            <div class="modal-body">
                <?= $form->field(new Album, 'name') ?>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-primary" data-loading-text="Загрузка...">Сохранить</button>
            </div>
            
            <?php ActiveForm::end(); ?>
        
        </div>
    </div>
</div>
