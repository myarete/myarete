<?php
$this->title = Yii::$app->getModule('gallery')->label;
?>


<div class="row">
    <div class="col-xs-6">
        <?= $this->render('index__add_photos') ?>
    </div>
    <div class="col-xs-6 pl0">
        <?= $this->render('index__albums') ?>
    </div>
</div>

<?= $this->render('index__photos') ?>
