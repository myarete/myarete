<?php
use common\models\AlbumSearch;
use yii\helpers\Html;

$this->title = Yii::$app->getModule('gallery')->label.' | Сортировка';
?>


<?php
$this->params['ribbonMenu'] = [
    [
        'label'     => 'Управление',
        'url'       => ['index'],
    ],
];
?>


<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-table"></i>
        <?= $this->title ?>
    </div>

    <div class="ibox-content">

        <form method="get">
            <?= Html::dropDownList(
                'album_id',
                Yii::$app->request->get('album_id'),
                AlbumSearch::getAll(),
                ['prompt' => '-- Выберите альбом --', 'onchange' => 'this.form.submit()']
            ) ?>
        </form>

        <hr class="dashed" />

        <?php if ($album === null): ?>

            <p>Изображений не найдено</p>

        <?php else: ?>

            <?= $this->render('sort__images', ['album' => $album]) ?>

        <?php endif; ?>

    </div>
</div>