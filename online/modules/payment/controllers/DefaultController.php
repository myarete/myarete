<?php

namespace online\modules\payment\controllers;

use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\ViewAction;
use common\models\PremiumPlan;
use common\models\Payment;
use common\models\Profile;

/**
 * Default controller for the `payment` module
 */
class DefaultController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'page' => [
                'class' => ViewAction::className(),
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = '/main';

        $dataProvider = new ActiveDataProvider([
            'query' => Payment::find()
                ->where(['user_id' => Yii::$app->user->id])
                ->orderBy('create_dt DESC'),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        $payment = $dataProvider->models;

        $sum = 0;
        foreach ($payment as $i => $p) {
            if ($p->is_paid) {
                $sum -= $p->refill_sum;
            } else {
                $sum += $p->refill_sum;
            }
            $p->current_result = $sum;
        }

        $profile = Profile::findOne(['user_id' => Yii::$app->user->id]);
        $profile->cash = $sum;
        $profile->update();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'sum' => $sum
        ]);
    }

    public function actionRefillCash($id = null)
    {
        return $this->render('refill-cash', ['cash' => $id]);

    }

    public function actionAddToCash()
    {
        Yii::$app->response->format = 'json';

        $payment = Payment::find()->where(['user_id' => Yii::$app->user->id, 'is_paid' => 0])->one();

        if (!$payment && Yii::$app->request->post('refill_sum') >= 1) {

            $payment = new Payment;
            $payment->refill_sum = Yii::$app->request->post('refill_sum');
            $payment->user_id = Yii::$app->user->id;
            $payment->save();

            return ['order' => $payment->id];

        } else if ($payment && Yii::$app->request->post('refill_sum') >= 1) {
            $payment->delete();
            $payment = new Payment;
            $payment->refill_sum = Yii::$app->request->post('refill_sum');
            $payment->user_id = Yii::$app->user->id;
            $payment->save();

            return ['order' => $payment->id];
        }

        return ['order' => 0];
    }

    public function actionInfo()
    {
        return $this->render('info');
    }
}
