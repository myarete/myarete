<?php

namespace online\modules\payment;

/**
 * payment module definition class
 */
class Payment extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'online\modules\payment\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
