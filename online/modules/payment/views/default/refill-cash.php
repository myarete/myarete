<?php

use yii\helpers\Html;

$this->title = 'Пополнить бумажник';
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="page__header_title">Пополнить бумажник</div>
            <div class="traning__top">
                <a class="button button__blue button__question" data-toggle="modal" data-target="#questions_and_answers__modal">Вопросы и ответы
                    <div class="fa fa-question"></div>
                </a>
            </div>
            <div class="event__content">
                <div class="form">
                    <div class="form__row">
                        <div class="form__label">Сумма для пополнения </div>
                        <div class="form__control">
                            <div class="form__field_icon">
                                <?php if($cash == null):?>
                                    <?= Html::textInput('sum', '0.00', ['id' => 'sum', 'class' => 'form__input']) ?>
                                    <div class="fa fa-rub"></div>
                                <?php else: ?>
                                    <?= Html::textInput('sum', $cash, ['id' => 'sum', 'class' => 'form__input']) ?>
                                    <div class="fa fa-rub"></div>
                                <?php endif; ?>
                                <?= Html::a('Продолжить', 'javascript:void(0)', [
                                    'onclick' => '
                                                yii.confirm("Оплатить?", function(result) {
                                                    if(result) {
                                                        makePayment(
                                                            $("#sum").val(),
                                                            "0",
                                                            "",
                                                            "'.Yii::$app->user->model->profile->first_name.' '.Yii::$app->user->model->profile->last_name.'",
                                                            "'.Yii::$app->user->model->email.'",
                                                            ""
                                                        )
                                                    }
                                                })',
                                'class' => 'button']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJsFile('https://securepay.tinkoff.ru/html/payForm/js/tinkoff.js', [
    'depends' => ['online\assets\AppAsset'],
    'position' => yii\web\View::POS_HEAD
]) ?>

<?php $this->registerJs('

	function makePayment(amount, orderId, description, name, email, phone) {
	
		var params = {
			TerminalKey: "1497393080064",
			Amount: amount * 100,
			OrderId: orderId,
			Description: description,
			DATA: "Email=" + email + "|Phone=" + phone + "|Name=" + name,
			Frame: true,
			Language: "ru"
		};
		
		$.post("/payment/default/add-to-cash", {refill_sum: amount}, function(response) {
		
            if(response.order != 0) {
            
                params.OrderId = response.order;
                console.log(response.order);
                console.log(params.OrderId);
                doPay(params);
            }
        });
    }', yii\web\View::POS_HEAD) ?>

