<?php

use yii\helpers\Html;

$this->title = 'История платежей';
?>
<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="page__header_title">История платежей</div>
            <div class="event__content">
                <div class="send__traning">
                    <div class="send__traning_top">
                        <a class="button button__blue button__question" data-toggle="modal"
                           data-target="#questions_and_answers__modal">Вопросы и ответы
                            <div class="fa fa-question"></div>
                        </a>
                        <div class="send__traning_right">
                            <span>В бумажнике <?= Yii::$app->user->model->profile->cash ?> ₽</span>&nbsp
                            <?= Html::a('Пополнить бумажник', ['/payment/default/refill-cash'], ['class' => 'button button__green--full']) ?>
                        </div>
                    </div>
                </div>
                <?php if ($dataProvider): ?>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Итого:</th>
                            <th><?= number_format($sum, 2) ?> ₽</th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th>Дата</th>
                            <th>Тип операции</th>
                            <th>Сумма</th>
                            <th>Результат операции</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?= \yii\widgets\ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => '_listItem'
                        ]);
                        ?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                <?php else: ?>
                    <div class="text-center"><p>Вы пока не сделали ни одного платежа</p></div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
