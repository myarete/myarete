<?php

use yii\helpers\Html;

$this->title = 'Мы принимаем к оплате';
?>

<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="side-block">
        <h2><?= $this->title ?></h2>

        <div class="text-center">
            <img width="200" src="/images/visa_mastercard.png" alt="">
        </div>
        <div class="text-center">
            <?= Html::a('Договор-оферта', ['/payment/default/page', 'view' => 'offer']) ?>&nbsp;
            <?= Html::a('Наши реквизиты', ['/payment/default/page', 'view' => 'requisites']) ?>
        </div>
    </div>
    <br><br>
</div>