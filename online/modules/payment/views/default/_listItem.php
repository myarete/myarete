<?php
if ($model->is_paid) {
    $style = 'style="background-color: #ff000024"';
} else {
    $style = 'style="background-color: #00800024"';
}
?>
<tr <?= $style ?>>
    <td>
        <?= date('d.m.Y', strtotime($model->create_dt)) ?>
    </td>
    <td>
        <?= $model->type->name ?>
    </td>
    <td>
        <?= $model->is_paid ? '-' . $model->refill_sum : $model->refill_sum ?> ₽
    </td>
    <td>
        <?= number_format($model->current_result, 2) ?> ₽
    </td>
</tr>
