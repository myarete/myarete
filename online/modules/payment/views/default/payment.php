<?php

use yii\helpers\Html;

$this->title = 'Оплата';
?>

<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="side-block">
        <h1>Вы подключаете: <?= $plan->name ?></h1>
        <br>
        <?php if($plan->validity == 'month'): ?>
            <p class="font-increased">Стоимость подписки: <?= $plan->price ?> рублей в месяц.</p>
        <?php else: ?>
            <p class="font-increased">Стоимость подписки: <?= $plan->price ?> рублей в год.</p>
        <?php endif ?>
        <br>
        <br>
        <p><?= Html::a('Добавить еще 5 учеников всего за 400 рублей/месяц', '', ['class' => 'font-increased']) ?></p>
        <hr>
        <p>Выберите способ оплаты:</p>
        <div class="row">
            <div class="col-sm-4 text-center">
                <?= Html::a('<img src="/images/visa_mastercard.png" alt="">', 'javascript:void(0)', [
                    'onclick' => '
                    yii.confirm("Оплатить?", function(result) {
                        if(result) {
                            makePayment(
                                '.$plan->price.',
                                "0",
                                "",
                                "'.Yii::$app->user->model->profile->first_name.' '.Yii::$app->user->model->profile->last_name.'",
                                "'.Yii::$app->user->model->email.'",
                                ""
                            )
                        }
                    })',
                ]) ?>
            </div>
            <div class="col-sm-4 text-center">
                <?= Html::a('<img src="/images/yandex_money.png" alt="">', '') ?>
            </div>
            <div class="col-sm-4 text-center">
                <?= Html::a('<img src="/images/qiwi_logo.png" alt="">', '') ?>
            </div>
        </div>
        <hr>
        <div class="text-center">
            <?= Html::a('Договор-оферта', ['/payment/default/page', 'view' => 'offer']) ?>&nbsp;&nbsp;&nbsp;&nbsp;
            <?= Html::a('Наши реквизиты', ['/payment/default/page', 'view' => 'requisites']) ?>
        </div>

        <?php $this->registerJsFile('https://securepay.tinkoff.ru/html/payForm/js/tinkoff.js', [
            'depends' => ['online\assets\AppAsset'],
            'position' => yii\web\View::POS_HEAD
        ]) ?>

        <?php $this->registerJs('
            function makePayment(amount, orderId, description, name, email, phone) {
                var params = {
                    //Код магазина (обязательный параметр), выдается банком.
                    TerminalKey: "Идентификатор магазина из ЛК",
                    //Сумма заказа в копейках (обязательный параметр)
                    Amount: amount,
                    //Номер заказа (если не передан, принудительно устанавливается timestamp)
                    OrderId: orderId,
                    //Описание заказа (не обязательный параметр)
                    Description: description,
                    //Дополнительные параметры платежа
                    DATA: "Email=" + email + "|Phone=" + phone + "|Name=" + name,
                    //Флаг открытия платежной формы во фрейме: false - в отдельном окне, true - в текущем окне.
                    Frame: true,
                    //Язык формы оплаты: ru - русский язык, en - английский язык
                    Language: "ru"
                };
                $.post("/payment/default/add-payment", {id: '.$plan->id.'}, function(response) {
                    if(response.order != 0) {
                        params.OrderId = response.order;
                        doPay(params);
                    }
                });
            }', yii\web\View::POS_HEAD) ?>

    </div>
    <br><br>
</div>