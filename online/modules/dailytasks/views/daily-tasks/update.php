<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DailyTasks */

$this->title = 'Редактировать ежедневное задание: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Ежедневные задания', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="daily-tasks-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'conditions' => $conditions
    ]) ?>

</div>
