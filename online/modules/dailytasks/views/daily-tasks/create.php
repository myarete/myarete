<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DailyTasks */

$this->title = 'Создать ежедневное задания';
$this->params['breadcrumbs'][] = ['label' => 'Ежедневные задания', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-tasks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'conditions' => $conditions
    ]) ?>

</div>
