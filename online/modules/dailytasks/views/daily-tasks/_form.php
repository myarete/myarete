<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\DailyTasks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="daily-tasks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reward')->textInput() ?>

    <?= $form->field($model, 'condition_id')->dropDownList(ArrayHelper::map($conditions,'id','title')) ?>

    <?= $form->field($model, 'date_from')->widget(\kartik\widgets\DatePicker::className(), [
        'type' => DatePicker::TYPE_INPUT,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ],
    ]);
    ?>

    <?= $form->field($model, 'date_to')->widget(\kartik\widgets\DatePicker::className(), [
        'type' => DatePicker::TYPE_INPUT,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ],
    ]);
    ?>

    <?= $form->field($model, 'repeat_task')->dropDownList([ 'everyday' => 'Каждый день', 'everyweek' => 'Каждую неделю', 'everymonth' => 'Каждый год', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
