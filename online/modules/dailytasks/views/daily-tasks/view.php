<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DailyTasks */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Ежедневные задания', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-tasks-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'reward',
            'condition_id',
            'date_from',
            'date_to',
            'repeat_task',
        ],
    ]) ?>

</div>
