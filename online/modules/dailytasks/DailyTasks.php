<?php

namespace online\modules\dailytasks;

/**
 * dailytasks module definition class
 */
class DailyTasks extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'online\modules\dailytasks\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
