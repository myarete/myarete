<?php

namespace online\modules\news;

class News extends \yii\base\Module
{
    public $controllerNamespace = 'online\modules\news\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
