<?php

use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

$this->title = 'Новости | ' . ($model->isNewRecord ? 'Создать' : 'Обновить');

$js = <<<JS
    a = {"\"":"","\'":""," ":"_","-":"_","$":"","#":"","№":"","@":"","!":"","?":"","(":"",")":"","[":"","]":"","{":"","}":"","<":"",">":"","/":"","\\":"","+":"","=":"","*":"","^":"",":":"",";":"","%":"",",":"",".":"","|":"","~":"","`":"","Ё":"YO","Й":"I", "Ц":"TS","У":"U","К":"K","Е":"E","Н":"N","Г":"G","Ш":"SH","Щ":"SCH","З":"Z","Х":"H","Ъ":"","ё":"yo","й":"i","ц":"ts","у":"u","к":"k","е":"e","н":"n","г":"g","ш":"sh","щ":"sch","з":"z","х":"h","ъ":"","Ф":"F","Ы":"I","В":"V","А":"a","П":"P","Р":"R","О":"O","Л":"L","Д":"D","Ж":"ZH","Э":"E","ф":"f","ы":"i","в":"v","а":"a","п":"p","р":"r","о":"o","л":"l","д":"d","ж":"zh","э":"e","Я":"Ya","Ч":"CH","С":"S","М":"M","И":"I","Т":"T","Ь":"","Б":"B","Ю":"YU","я":"ya","ч":"ch","с":"s","м":"m","и":"i","т":"t","ь":"","б":"b","ю":"yu"};

    transliterate = function(word){
        return word.split('').map(function(char){
            return typeof a[char] !== 'undefined' ? a[char] : char;
        }).join("").toLowerCase();
    };
JS;
$this->registerJs($js);
?>

<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-pencil-square-o"></i> <?= $this->title ?>
    </div>
    <div class="ibox-content">
        
        <?php $form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>
        
        <div class="row">
            
            <div class="col-xs-3">
                <div class="form-group">
                    <?= $model->getImageUploadWidget($form) ?>
                </div>
            </div>
            
            <div class="col-xs-9">
                <?= $form->field($model, 'title')->textInput(['onchange' => '$("#news-rewrite").val(transliterate($(this).val()))']) ?>
                
                <?= $form->field($model, 'rewrite'); ?>
                
                <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                    'options' => ['rows' => 6],
                    'clientOptions' => [
                        'toolbar' => [
                            ['Bold', 'Italic', 'Underline'],
                            ['BulletedList', 'NumberedList'],
                            ['Format'],
                            ['Source'],
                        ],
                        'allowedContent' => true,
                    ],
                ]) ?>
            </div>
        </div>
        
        <hr/>
        
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        
        <?php ActiveForm::end(); ?>
    
    </div>
</div>
