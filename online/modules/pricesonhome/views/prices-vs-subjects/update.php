<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PricesVsSubjects */

$this->title = 'Обновить предметы в ценах: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Предметы в ценах', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="prices-vs-subjects-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'subjects' => $subjects,
        'prices' => $prices,
    ]) ?>

</div>
