<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PricesVsSubjects */

$this->title = 'Создать предметы в ценах';
$this->params['breadcrumbs'][] = ['label' => 'Предметы в ценах', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prices-vs-subjects-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'subjects' => $subjects,
        'prices' => $prices,
    ]) ?>

</div>
