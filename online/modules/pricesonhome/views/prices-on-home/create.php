<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PricesOnHome */

$this->title = 'Создать цены на главной';
$this->params['breadcrumbs'][] = ['label' => 'Цены на главной', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prices-on-home-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
