<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PricesVsCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Цены и категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prices-vs-category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать связку цен и категорий', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'prices.name',
            'category.price',
            'category.period',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
