<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PricesVsCategory */

$this->title = 'Обновить связку цен и категорий: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Цены и категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="prices-vs-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'category' => $category,
        'prices' => $prices,
    ]) ?>

</div>
