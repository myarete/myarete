<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\PricesVsCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prices-vs-category-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'price_id')->dropDownList(
        ArrayHelper::map($prices, 'id', 'name')
    ) ?>

    <?= $form->field($model, 'category_id')->dropDownList(
        ArrayHelper::map($category, 'id', 'price', 'period')
    ) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Удалить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
