<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PricesCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ценовые категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prices-category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать ценовую категорию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'price',
            'period',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
