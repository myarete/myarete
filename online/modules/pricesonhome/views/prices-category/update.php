<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PricesCategory */

$this->title = 'Обновить ценовую категорию: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Prices Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="prices-category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
