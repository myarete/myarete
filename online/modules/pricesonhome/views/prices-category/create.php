<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PricesCategory */

$this->title = 'Создать ценовую категорию';
$this->params['breadcrumbs'][] = ['label' => 'Ценовая категория', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prices-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
