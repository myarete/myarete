<?php

namespace online\modules\premium\controllers;

use common\models\User;
use common\models\Payment;
use common\models\PremiumPlanList;
use Yii;
use yii\db\Expression;
use yii\web\Controller;
use common\models\PremiumPlan;
use common\models\Profile;

/**
 * Default controller for the `premium` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $model_month = PremiumPlan::findAll(['validity' => 'month']);
        $model_year = PremiumPlan::findAll(['validity' => 'year']);
        $model_30june = PremiumPlan::findAll(['validity' => 'to_30_june']);

        return $this->render('index', [
            'model_month' => $model_month,
            'model_year' => $model_year,
            'model_30june' => $model_30june
        ]);
    }

    public function actionPayment($id)
    {
        $premiumPlan = PremiumPlan::findOne(['id' => $id]);
        $myPremiumPlan = PremiumPlanList::findOne(['user_id' => Yii::$app->user->id, 'status' => 'active']);

        return $this->render('payment', [
            'premiumPlan' => $premiumPlan,
            'myPremiumPlan' => $myPremiumPlan
        ]);
    }

    public function actionBuy($id)
    {
        $premiumPlan = PremiumPlan::findOne(['id' => $id]);

        $user = User::find()->where(['id' => Yii::$app->user->id])->one();
        $profile = Profile::find()->where(['user_id' => Yii::$app->user->id])->one();
        if($profile->cash >= $premiumPlan->price) {
            if(!PremiumPlanList::findOne(['user_id' => Yii::$app->user->id])) {
                $premiumPlanList = new PremiumPlanList();
                $premiumPlanList->premium_id = $premiumPlan->id;
                $premiumPlanList->user_id = Yii::$app->user->id;
                $premiumPlanList->price = $premiumPlan->price;
                $premiumPlanList->status = 'active';

                if ($premiumPlanList->save()) {
                    $user->role = User::ROLE_PREMIUMTUTOR;
                    $user->update();

                    $profile->cash -= $premiumPlan->price;
                    $profile->update();
                    $payment = new Payment();
                    $payment->user_id = $user->id;
                    $payment->type_id = 5;
                    $payment->refill_sum = $premiumPlan->price;
                    $payment->is_paid = 1;
                    $payment->save();
    
                    $this->redirect('/premium/default/my');
                } else {
                    return false;
                }
            } else if(PremiumPlanList::findOne(['user_id' => Yii::$app->user->id, 'status' => 'suspended'])) {
                $premiumPlanList = PremiumPlanList::findOne(['user_id' => Yii::$app->user->id, 'status' => 'suspended']);
                $premiumPlanList->premium_id = $id;
                $premiumPlanList->date = new Expression('NOW()');
                $premiumPlanList->status = 'active';

                if ($premiumPlanList->update()) {
                    $user->role = User::ROLE_PREMIUMTUTOR;
                    $user->update();

                    $profile->cash -= $premiumPlan->price;
                    $profile->update();
                    $payment = new Payment();
                    $payment->user_id = $user->id;
                    $payment->type_id = 5;
                    $payment->refill_sum = $premiumPlan->price;
                    $payment->is_paid = 1;
                    $payment->save();

                    $this->redirect('/premium/default/my');
                } else {
                    return false;
                }
            } else {
                $this->redirect('/premium/default/my');
            }
        }

    }

    public function actionMy()
    {
        $premiumPlan = PremiumPlanList::find()->with('premiumPlan')->where(['user_id' => Yii::$app->user->id])->one();

        if(empty($premiumPlan)) {
            return $this->redirect('/premium/default/index');
        }

        return $this->render('my', ['premiumPlan' => $premiumPlan]);
    }

}
