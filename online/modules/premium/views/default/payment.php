<?php

use yii\helpers\Html;

$this->title = 'Мой Премиум';
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="page__header_title">Мой Премиум</div>
            <div class="event__content">
                <div class="send__traning">
                    <div class="send__traning_top">
                        <a class="button button__blue button__question" data-toggle="modal" data-target="#questions_and_answers__modal">Вопросы и ответы
                            <div class="fa fa-question"></div>
                        </a>
                    </div>
                </div>
                <div class="text-center">
                    <?php if(empty($myPremiumPlan)):?>
                        <?php if(Yii::$app->user->model->profile->cash >= $premiumPlan->price):?>
                            <p>Вы хотите оплатить план "<?= $premiumPlan->name?>" стоимостью <b><?= $premiumPlan->price?> р.</b> Данная сумма будет списана с вашего бумажника.</p>
                            <?= Html::a('Подтвердить оплату', ['/premium/default/buy', 'id' => $premiumPlan->id], ['class' => 'button']) ?>
                        <?php else: ?>
                            <p>Вы хотите оплатить план "<?= $premiumPlan->name?>" стоимостью <b><?= $premiumPlan->price?> р.</b> Данная сумма будет списана с вашего бумажника.</p>
                            <p>На вашем бумажнике недостаточно средств для покупки данного плана, пополните бумажник на сумму <b><?= $rest = $premiumPlan->price - Yii::$app->user->model->profile->cash ?> р.</b></p>
                            <?= Html::a('Пополнить бумажник', ['/payment/default/refill-cash/', 'id' => $rest], ['class' => 'button', 'target' => '_blank']) ?>
                        <?php endif;?>
                    <?php else:?>
                        <p>У вас уже есть активный премиум план.</p>
                        <?= Html::a('Мой премиум', ['/premium/default/my'], ['class' => 'button']) ?>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>
