<?php

use yii\helpers\Html;

$this->title = 'Мой Премиум';
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="page__header_title">Мой Премиум</div>
            <div class="event__content">
                <div class="send__traning">
                    <div class="send__traning_top">
                        <a class="button button__blue button__question" data-toggle="modal" data-target="#questions_and_answers__modal">Вопросы и ответы
                            <div class="fa fa-question"></div>
                        </a>
                    </div>
                </div>
                <?php if($premiumPlan->status == 'active'):?>
                    <p><b>Ваш текущий план:</b> <?= $premiumPlan->premiumPlan->name?></p>
                    <p><b>Описание плана:</b> <?= $premiumPlan->premiumPlan->description?></p>
                    <?php if($premiumPlan->premiumPlan->validity == 'month'):?>
                        <p><b>Активен до:</b> <?= date('d.m.Y', strtotime($premiumPlan->date) + 30 * 24 * 3600);?></p>
                    <?php elseif($premiumPlan->premiumPlan->validity == 'year'): ?>
                        <p><b>Активен до:</b> <?= date('d.m.Y', strtotime($premiumPlan->date) + 365 * 24 * 3600);?></p>
                    <?php elseif($premiumPlan->premiumPlan->validity == 'to_30_june'):?>
                        <?php
                        //$current = strtotime(date('d.m.Y'));
                        $june = "30.06.2018";
                        //$result = $end - $current;
                        ?>
                        <p><b>Активен до:</b> <?= $june?></p>
                    <?php endif;?>
                <?php elseif($premiumPlan->status == 'suspended'):?>
                    <div class="text-center">
                        <p>Срок действия Вашего премиум плана уже закончился, успейте продлить его.</p>
                        <?= Html::a('Продлить план', ['/premium/default/index'], ['class' => 'btn btn-primary']) ?>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>
