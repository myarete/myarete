<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use admin\widgets\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model admin\modules\premium\models\PremiumPlan */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<?php if(!$model->isNewRecord && !empty($model->image)):?>
    <div class="form-group">
        <img width="200" src="<?= \Yii::getAlias('@web/upload/premium/') . $model->image ?>" />
    </div>
<?php endif;?>

<?= $form->field($model, 'imageFile')->fileInput() ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'description')->widget(CKEditor::className(), [
    'options' => ['rows' => 100],
    'enableFinder' => true,
    'clientOptions' => [
        'toolbar' => [
            [
                'name' => 'clipboard',
                'items' => [
                    'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                ],
            ],
            [
                'name' => 'links',
                'items' => [
                    'Link', 'Unlink', 'Anchor'
                ],
            ],
            [
                'name' => 'insert',
                'items' => [
                    'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                ],
            ],
            [
                'name' => 'align',
                'items' => [
                    'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                ],
            ],
            [
                'name' => 'document',
                'items' => [
                    'Maximize', 'ShowBlocks', 'Source'
                ],
            ],
            '/',
            [
                'name' => 'basicstyles',
                'items' => [
                    'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                ],
            ],
            [
                'name' => 'color',
                'items' => [
                    'TextColor', 'BGColor'
                ],
            ],
            [
                'name' => 'paragraph',
                'items' => [
                    'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                ],
            ],
            [
                'name' => 'styles',
                'items' => [
                    'Styles', 'Format', 'Font', 'FontSize'
                ],
            ],
            '/',
            [
                'name' => 'insert',
                'items' => [
                    'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                ],
            ],
        ],
        'height' => 500,
        'allowedContent' => true,
        'contentsCss' => [
            '/css/fa/font-awesome.min.css',
            '/css/bootstrap.min.css',
            Yii::$app->params['scheme'].'://'.Yii::$app->params['host'].'/css/main.css',
            Yii::$app->params['scheme'].'://'.Yii::$app->params['subdomain.online'].'.'.Yii::$app->params['host'].'/css/article.css'
        ],
        'extraPlugins' => 'mathjax,exercise,test,sliders,accordion,vkshare',
        'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML'
    ],
]) ?>

<?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'validity')->dropDownList([ 'month' => 'Month', 'year' => 'Year', 'to_30_june' => 'До 30-го июня'], ['prompt' => '']) ?>

<div class="form-group">
    <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

