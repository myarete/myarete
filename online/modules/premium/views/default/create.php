<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model admin\modules\premium\models\PremiumPlan */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Премиум планы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ibox float-e-margins">

    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
    </div>

    <div class="ibox-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
