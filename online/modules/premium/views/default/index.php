<?php

use yii\helpers\Html;

$this->title = 'Премиум-учитель';
?>

<div class="crm__container">
    <!-- begin aside-->
    <!-- end aside-->
    <div class="crm__content">
        <div class="premium">
            <div class="store__ban"><img src="/img/premium.jpg" alt=""/>
                <div class="store__ban_title-wrap">
                    <div class="store__ban_title">
                        <div class="store__ban_title-text">Премиум возможности для <br>работы и учебы</div>
                    </div>
                </div>
            </div>
            <div class="premium__title"><?= $this->title ?></div>
            <!-- begin premium__grid-->
            <div class="grid premium__grid" data-grid-match="data-grid-match">
                <div class="grid__wrapper">
                    <div class="grid__item">
                        <div class="premium__grid_title">На месяц</div>
                        <div class="premium__grid_price">399 <span>Рублей</span></div>
                        <div class="premium__grid_list">
                            <div class="premium__grid_list-item">Добавляйте себе учеников</div>
                            <div class="premium__grid_list-item">Объединяйте их в группы</div>
                            <div class="premium__grid_list-item">Создавайте заметки</div>
                            <div class="premium__grid_list-item">Ведите учет занятий, домашних заданий и успехов своих учеников</div>
                        </div>
                        <div class="premium__grid_action">
                            <a class="button button__blue--full button__round" href="/premium/default/payment/1">Подключить</a>
                        </div>
                    </div>
                </div>
                <div class="grid__wrapper">
                    <div class="grid__item">
                        <div class="premium__grid_title">На год</div>
                        <div class="premium__grid_price">4499 <span>Рублей</span></div>
                        <div class="premium__grid_list">
                            <div class="premium__grid_list-item">Добавляйте себе учеников</div>
                            <div class="premium__grid_list-item">Объединяйте их в группы</div>
                            <div class="premium__grid_list-item">Создавайте заметки</div>
                            <div class="premium__grid_list-item">Ведите учет занятий, домашних заданий и успехов своих учеников</div>
                            <div class="premium__grid_list-item">Обменивайтесь с ними тренировками</div>
                        </div>
                        <div class="premium__grid_action">
                            <a class="button button__blue--full button__round" href="/premium/default/payment/2">Подключить</a>
                        </div>
                    </div>
                </div>
                <div class="grid__wrapper">
                    <div class="grid__item">
                        <div class="premium__grid_title">До 30-го июня 2018 года</div>
                        <div class="premium__grid_price">4499 <span>Рублей</span></div>
                        <div class="premium__grid_list">
                            <div class="premium__grid_list-item">Добавляйте себе учеников</div>
                            <div class="premium__grid_list-item">Объединяйте их в группы</div>
                            <div class="premium__grid_list-item">Создавайте заметки</div>
                            <div class="premium__grid_list-item">Ведите учет занятий, домашних заданий и успехов своих учеников</div>
                            <div class="premium__grid_list-item">Обменивайтесь с ними тренировками</div>
                        </div>
                        <div class="premium__grid_action">
                            <a class="button button__blue--full button__round" href="/premium/default/payment/6">Подключить</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end premium__grid-->
        </div>
    </div>
</div>
