<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel online\modules\main\models\MessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сообщения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="messages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name:ntext:Имя',
            'email:email',
            [
                'attribute' => 'read',
                'label' => 'Прочитано/непрочитано',
                'content' => function($model) {
                    return $model->read ? 'Прочитано' : 'Непрочитано';
                }
            ],
            [
                'attribute' => 'create_dt',
                'label' => 'Дата/время создания',
                'content' => function($model) {
                    return date('d.m.Y H:i:s', strtotime($model->create_dt));
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}  {delete}'
            ],
        ],
    ]); ?>
</div>
