<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model online\modules\main\models\Messages */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Сообщения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="messages-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name:ntext:Имя',
            'email:email',
            'message:ntext',
            [
                'attribute' => 'create_dt',
                'label' => 'Дата/время создания',
                'value' => function($model) {
                    return date('d.m.Y H:i:s', strtotime($model->create_dt));
                }
            ]
        ],
    ]) ?>

</div>
