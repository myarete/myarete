<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>


<?php
$this->title = Yii::$app->getModule('affiliate')->label.' | '.($model->isNewRecord ? 'Создать' : 'Обновить');
?>

<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-pencil-square-o"></i> <?= $this->title ?>
    </div>
    <div class="ibox-content">

        <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-xs-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                </div>
                <div class="col-xs-6">
                    <?= $this->render('create_update__map', ['form' => $form, 'model' => $model]) ?>
                </div>
            </div>

            <hr/>

            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
