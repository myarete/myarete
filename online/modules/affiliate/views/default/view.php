<?php
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
?>


<?php
$this->title = Yii::$app->getModule('affiliate')->label.' | Просмотр';
?>


<?php
$this->params['ribbonMenu'] = [
    [
        'label'     => '<i class="fa fa-edit"></i>',
        'url'       => ['update', 'id' => $model->id],
        'template'  => '<a href="{url}" title="Обновить">{label}</a>'
    ],
    [
        'label'     => '<i class="fa fa-trash"></i>',
        'url'       => ['delete', 'id' => $model->id],
        'template'  => '<a href="{url}" title="Удалить">{label}</a>',
        'options'   => [
            'onclick' => 'return confirm("Вы уверены?")',
        ]
    ],
];
?>


<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-table"></i>
        <?= $this->title ?>
    </div>

    <div class="ibox-content">

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'description:ntext',
                'address',
                [
                    'format' => 'raw',
                    'label' => 'Преподаватели',
                    'value' => \admin\widgets\many_many\ManyManyWidget::widget([
                        'allData' => ArrayHelper::map(\common\models\Teacher::find()->orderBy('fio')->all(), 'id', 'fio'),
                        'existsData' => ArrayHelper::map($model->teachers, 'id', 'id'),
                        'route' => ['/affiliate/default/add-remove-teacher', 'affiliate_id' => $model->id, 'teacher_id' => '{id}'],
                    ]),
                ],

            ],
        ]) ?>

    </div>
</div>
