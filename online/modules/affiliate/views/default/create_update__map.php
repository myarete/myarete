<?php
$this->registerJsFile('//api-maps.yandex.ru/2.0.31/?load=package.full&lang=ru-RU', ['position' => \yii\web\View::POS_HEAD]);

$js = <<<JS
    /**
     *
     */
    var lt = 55.7540;
    var lg = 37.6186;
    var map;
    var placemark;


    /**
     *
     */
    ymaps.ready(function(){

        // Map init
        map = new ymaps.Map('map', {
            center      : [lt, lg],
            zoom        : 12,
            behaviors   : ['default', 'scrollZoom']
        });
        map.controls.add('smallZoomControl', {top: 5, left: 5});

        // Add placemark
        placemark = new ymaps.Placemark([lt, lg], {}, {})
        map.geoObjects.add(placemark);
    });


    /**
     *
     */
    $('#search-address-link').click(function(){

        console.log($('#affiliate-address').val());

        $.get('https://geocode-maps.yandex.ru/1.x/', {
            format : 'json',
            geocode : $('#affiliate-address').val()
        })
        .success(function(data) {
            if (data.response.GeoObjectCollection.featureMember.length == 0)
            {
                alert('Ничего не найдено');
            }
            else
            {
                var coords = data.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos.split(' ');
                lt = parseFloat(coords[1]);
                lg = parseFloat(coords[0]);
                map.setCenter([lt, lg]);
                placemark.geometry.setCoordinates([lt, lg]);
            }
        });

        return false;
    });


    /**
     *
     */
    $('#affiliate-address').keypress(function(e){
        if (e.which == 13)
        {
            $('#search-address-link').trigger('click');
            return false;
        }
    });


    /**
     *
     */
    if ($('#affiliate-address').val() != '')
    {
        $('#search-address-link').trigger('click');
    }
JS;
$this->registerJs($js);
?>

<div class="row">
    <div class="col-xs-10">
        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-xs-2">
        <a id="search-address-link" class="btn btn-info" style="margin-top: 25px;" href="#">Найти</a>
    </div>
</div>


<div id="map" style="height: 270px; "></div>
