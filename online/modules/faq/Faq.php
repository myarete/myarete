<?php

namespace online\modules\faq;

class Faq extends \yii\base\Module
{
    public $controllerNamespace = 'online\modules\faq\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
