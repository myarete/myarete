<?php

use online\assets\AppAsset;
use yii\helpers\Html;

/* @var $model common\models\Post */
/* @var \frontend\models\CommentForm $commentForm \;
/* @var TagPost $post */

$this->title = "Новости";
?>

<div class="crm__container">
    <!-- begin aside-->
    <!-- end aside-->
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="page__header_title"><?= $model->title ?></div>
            <div class="lesson-full">
                <div class="lesson-full__title"><?= Html::a($model->category->title, ['category/view', 'id' => $model->category->id]) ?> <div class="blog__list_date"><?= date("d-m-Y", strtotime($model->publish_date)) ?> в <?= date("H:i", strtotime($model->publish_date)) ?></div></div>
                <div class="lesson-full__p"><?= $model->content ?></div>
                <div class="lesson-full__p"><?php
                    $tags = [];
                    foreach($model->getTagPost()->all() as $postTag) {
                        $tag = $postTag->getTag()->one();
                        $tags[] = Html::a($tag->title, ['tag/view', 'id' => $tag->id]);
                    } ?>

                    <?= implode($tags, ', ') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end content -->