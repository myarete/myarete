<?php

namespace online\modules\course;

class Course extends \yii\base\Module
{
    public $controllerNamespace = 'online\modules\course\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
