<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use common\components\other\History;
use kartik\alert\AlertBlock;

History::put(Yii::$app->user->identity->email, $training->name);

$this->title = $training->name;

$js = <<<JS
    $("#success-settings").click(function (e) {

        e.preventDefault();
        var form = $("#settings_form");
        var data = form.serializeArray();
        var hint = data[0]['value'];
        var solution = data[1]['value'];
        var answers = data[2]['value'];
        var field = data[3]['value'];

        if (hint === 'close') {
            $(".hint_block").removeClass('in');
            $(".hint_block").hide();
            $(".close_hint").hide();
            $(".open_hint").show();
        } else {
            $(".hint_block").addClass('in');
            $(".hint_block").show();
            $(".open_hint").hide();
            $(".close_hint").show();
        }

        if (solution === 'close') {
            $(".decision_block").removeClass('in');
            $(".decision_block").hide();
            $(".close_decision").hide();
            $(".open_decision").show();
        } else {
            $(".decision_block").addClass('in');
            $(".decision_block").show();
            $(".open_decision").hide();
            $(".close_decision").show();
        }

        if (answers === 'close') {
            $(".answers_block").removeClass('in');
            $(".answers_block").hide();
            $(".close_answers").hide();
            $(".open_answers").show();
        } else {
            $(".answers_block").addClass('in');
            $(".answers_block").show();
            $(".open_answers").hide();
            $(".close_answers").show();
        }

        var fieldDiv = $(".field_div");
        var textarea = $(".traning__field");

        switch (field) {

            case 'close':
                fieldDiv.hide();
                $(".field_open").show();
                $(".field_close").hide();
                break;
            case 'cell':
                fieldDiv.show();
                textarea.removeClass('traning__field--blank');
                textarea.removeClass('traning__field--line');
                textarea.removeClass('traning__field--cage');
                textarea.addClass('traning__field--cage');
                $(".field_open").hide();
                $(".field_close").show();
                break;
            case 'white':
                fieldDiv.show();
                textarea.removeClass('traning__field--blank');
                textarea.removeClass('traning__field--line');
                textarea.removeClass('traning__field--cage');
                textarea.addClass('traning__field--blank');
                $(".field_open").hide();
                $(".field_close").show();
                break;
            case 'line':
                fieldDiv.show();
                textarea.removeClass('traning__field--blank');
                textarea.removeClass('traning__field--line');
                textarea.removeClass('traning__field--cage');
                textarea.addClass('traning__field--line');
                $(".field_open").hide();
                $(".field_close").show();
                break;

        }
    });

    Zepto('#sortable').dragswap({
        element: 'div.draggable', // the child element you are targeting
        overClass: 'over', // class when element goes over another element
        moveClass: 'moving', // class when element is moving
        dropClass: 'drop', // the class to add when the element is dropped
        dropAnimation: true, // do you want to detect animation end?
        exclude: '.disabled',  // excluded elements selector, here we can add array of excluded classes ['.exclude', '.exclude2']
        dropComplete: saveOrder
    });

    $('#showSetup').click(function () {
        $('#trainingSetup').modal();
    });

    $('#setupBtn').click(function () {
        if ($('#showHint').is(':checked')) {
            $('a[href ^= #hint-]').removeClass('collapsed').attr('aria-expanded', true);
            $('div[id ^= hint-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
        } else {
            $('a[href ^= #hint-]').addClass('collapsed').attr('aria-expanded', false);
            $('div[id ^= hint-]').removeClass('in').attr('aria-expanded', false);
        }
        if ($('#showDecision').is(':checked')) {
            $('a[href ^= #decision-]').removeClass('collapsed').attr('aria-expanded', true);
            $('div[id ^= decision-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
        } else {
            $('a[href ^= #decision-]').addClass('collapsed').attr('aria-expanded', false);
            $('div[id ^= decision-]').removeClass('in').attr('aria-expanded', false);
        }
        if ($('#showAnswer').is(':checked')) {
            $('a[href ^= #answer-]').removeClass('collapsed').attr('aria-expanded', true);
            $('div[id ^= answer-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
        } else {
            $('a[href ^= #answer-]').addClass('collapsed').attr('aria-expanded', false);
            $('div[id ^= answer-]').removeClass('in').attr('aria-expanded', false);
        }
        if ($('#addAreas').is(':checked')) {
            $('a[href ^= #solutions-]').removeClass('collapsed').attr('aria-expanded', true);
            $('div[id ^= solutions-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
        } else {
            $('a[href ^= #solutions-]').addClass('collapsed').attr('aria-expanded', false);
            $('div[id ^= solutions-]').removeClass('in').attr('aria-expanded', false);
        }
        if ($('#shuffleExercises').is(':checked')) {
            $.post('/training/default/shuffle-exercises', {id: '<?= $training->id ?>'});
            setTimeout(function () {
                location.reload();
            }, 500);
        }
    });

    $('#screenshot').click(function () {
        $('.traning__top').hide();
        $('.traning__list_action').hide();

        html2canvas($('.wihite__box'), {
            onrendered: function (canvas) {
                canvas.toBlob(function (blob) {
                    saveAs(blob, "<?= $training->name ?>.png");
                }, "image/png");
            }
        });

        $('.traning__top').show();
        $('.traning__list_action').show();
    });

    $('#printscreen').click(function () {
        var content = $('.traning__list').html();

        $.post('/training/default/print-screen', {content: content}, function (resp) {
            if (resp != '') location.href = '/training/default/print-screen?id=' + '<?= $training->id ?>' + '&tmpfile=' + resp;
        });
    });

    $('.bground-btn').click(function () {
        if ($(this).hasClass('clean-background')) {
            $(this).closest('.row').find('textarea').css('background-image', 'none');
            $(this).closest('.row').find('textarea').css('background-color', '#fff');
        } else if ($(this).hasClass('cage-background')) {
            $(this).closest('.row').find('textarea').css('background-image', 'url(/../images/cage.jpg)');
        } else if ($(this).hasClass('linear-background')) {
            $(this).closest('.row').find('textarea').css('background-image', 'url(/../images/linear.png)');
        }
    });

    $('#similar-training').click(function () {
        yii.prompt('Название тренировки', function (name) {
            var id = '<?= $training->id ?>';
            $.post('/training/default/create-similar', {id: id, name: name}, function (response) {
                location.href = '/training/default/run/' + response;
            });
        });
    });

    $('#edit-btn').click(function () {
        $('#view-note').hide();
        $('#edit-note').show();
        $('#save-btn').show();
        $(this).hide();
    });

    $('#delete-btn').click(function () {
        var id = $(this).closest('form').find('input[type=hidden]').val();
        $.post('/training/default/delete-note', {id: id}, function (response) {
            location.reload();
        });
    });

    function saveOrder() {
        var data = [];
        $.each($('#sortable .draggable'), function (i, e) {
            data.push($(e).attr('id'));
        });
        $.post('/training/default/save-order', {
            training_id: '<?= $training->id ?>',
            data: data
        });
    }

    saveOrder();
JS;
$this->registerJs($js);
?>

<?php if ($add == 1): ?>
    <div class="crm__container">
        <div class="crm__content">
            <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
                <?= AlertBlock::widget([
                    'delay' => false
                ]) ?>
            <?php endif; ?>
            <style>
                .alert-success {
                    color: #222
                }

                .action-price-text {
                    margin-left: 10px;
                    font-size: 24px;
                    color: #4785fc
                }
            </style>
            <div class="wihite__box shadow">

                <div class="traning">
                    <div class="traning__title"><?= $training->name ?></div>
                    <div class="traning__top">
                        <a class="button button__blue button__question" data-toggle="modal"
                           data-target="#questions_and_answers__modal">Вопросы и ответы
                            <div class="fa fa-question"></div>
                        </a>
                        <div class="pull-right">
                            <?php if (isset($params) && $training->user_id == Yii::$app->user->id): ?>
                                <?= Html::a('Посмотреть полную версию', ['/training/default/run', 'id' => $training->id],
                                    ['class' => 'button button__round button__green']) ?>
                            <?php endif ?>

                            <?php
                            Modal::begin([
                                'header' => '<h2>Управление</h2>',
                                'toggleButton' => [
                                    'tag' => 'button',
                                    'class' => 'button button__round button__blue',
                                    'label' => 'Управление',
                                ],
                                'options' => [
                                    'class' => 'modal-sm'
                                ]
                            ]); ?>
                            <form action="" id="settings_form">

                                <div class="form">
                                    <div class="form__row">
                                        <div class="form__control">
                                            <div class="form__label" style="font-weight: 700;">Подсказки:</div>
                                            <div class="form__label">
                                                <label for="hint-close" style="font-weight: 400;">Скрыть все </label>
                                                <input style="margin: 0;" type="radio" id="hint-close" name="hint"
                                                       value="close"
                                                       checked/>
                                            </div>
                                            <div class="form__label">
                                                <label for="hint-open" style="font-weight: 400;">Показать все </label>
                                                <input style="margin: 0;" type="radio" id="hint-open" name="hint"
                                                       value="open"/>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="form__control">
                                            <div class="form__label" style="font-weight: 700;">Решения:</div>
                                            <div class="form__label">
                                                <label for="solution-close" style="font-weight: 400;">Скрыть
                                                    все </label>
                                                <input style="margin: 0;" type="radio" id="solution-close"
                                                       name="solution"
                                                       value="close" checked/>
                                            </div>
                                            <div class="form__label">
                                                <label for="solution-open" style="font-weight: 400;">Показать
                                                    все </label>
                                                <input style="margin: 0;" type="radio" id="solution-open"
                                                       name="solution"
                                                       value="open"/>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="form__control">
                                            <div class="form__label" style="font-weight: 700;">Ответы:</div>
                                            <div class="form__label">
                                                <label for="answers-close" style="font-weight: 400;">Скрыть все </label>
                                                <input style="margin: 0;" type="radio" id="answers-close" name="answers"
                                                       value="close" checked/>
                                            </div>
                                            <div class="form__label">
                                                <label for="answers-open" style="font-weight: 400;">Показать
                                                    все </label>
                                                <input style="margin: 0;" type="radio" id="answers-open" name="answers"
                                                       value="open"/>
                                            </div>

                                        </div>
                                        <hr>
                                        <div class="form__control">
                                            <div class="form__label" style="font-weight: 700;">Пространство для<br>решения
                                                ко
                                                всем задачам:
                                            </div>
                                            <div class="form__label">
                                                <label for="field-close" style="font-weight: 400;">Нет </label>
                                                <input style="margin: 0;" type="radio" id="field-close" name="field"
                                                       value="close" checked/>
                                            </div>
                                            <div class="form__label">
                                                <label for="field-white" style="font-weight: 400;">Белое</label>
                                                <input style="margin: 0;" type="radio" id="field-white" name="field"
                                                       value="white"/>
                                            </div>
                                            <div class="form__label">
                                                <label for="field-cell" style="font-weight: 400;">Клетка</label>
                                                <input style="margin: 0;" type="radio" id="field-cell" name="field"
                                                       value="cell"/>
                                            </div>
                                            <div class="form__label">
                                                <label for="field-line" style="font-weight: 400;">Линейка</label>
                                                <input style="margin: 0;" type="radio" id="field-line" name="field"
                                                       value="line"/>
                                            </div>
                                        </div>
                                        <br><br><br>
                                        <div class="text-center">
                                            <button class="btn btn-success" id="success-settings">Применить</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <?php
                            Modal::end();
                            ?>


                            <div class="button__group_flip" style="margin-left: 10px">

                                <?= Html::button('<div class="fa fa-copy"></div>', [
                                    'id' => 'similar-training',
                                    'class' => 'button button__round button__yellow button__icon',
                                ]) ?>

                                <?php if (Yii::$app->user->can('user')): ?>
                                    <?= Html::button('<div class="fa fa-camera"></div>', [
                                        'id' => 'screenshot',
                                        'class' => 'button button__round button__blue button__icon',
                                    ]) ?>
                                <?php endif; ?>

                                <?= Html::button('<div class="fa fa-print"></div>', [
                                    'id' => 'printscreen',
                                    'class' => 'button button__round button__green button__icon',
                                ]) ?>
                            </div>

                        </div>
                    </div>
                    <div class="traning__list">
                        <?php $i = 0 ?>
                        <?php foreach ($training->trainingExercises as $trainingExercise): ?>
                            <?php if (isset($params) && $params[$trainingExercise->variant->exercise->id] && in_array('exercise', $params[$trainingExercise->variant->exercise->id])) {
                                continue;
                            }
                            if (!$trainingExercise->variant) {
                                continue;
                            }
                            ?>
                            <div class="traning__list_item" id="id_<?= $trainingExercise->id ?>">
                                <div class="traning__list_count"><?= $i + 1 ?> </div>
                                <div class="traning__list_action">
                                    <a class="button button__round button__green traning__btn_more" href="#">
                                        <span class="traning__btn_more--show">Подробнее </span>
                                        <span class="traning__btn_more--hide">Скрыть </span>
                                    </a>
                                    <?= Html::a(
                                        '<div class="fa ' . (($trainingExercise->variant->exerciseFavorite) ? 'fa-star' : 'fa-star-o') . '"></div>',
                                        [(($trainingExercise->variant->exerciseFavorite) ? '/exercise/favorite/delete' : '/exercise/favorite/add'),
                                            'id' => (($trainingExercise->variant->exerciseFavorite) ? $trainingExercise->variant->exerciseFavorite->id : $trainingExercise->exercise_variant_id)],
                                        [
                                            'class' => 'button button__round button__icon ' . (($trainingExercise->variant->exerciseFavorite) ? 'button__red' : 'button__blue'),
                                        ]
                                    ) ?>
                                    <?php
                                    if (!isset($params))
                                        echo Html::a(
                                            '<div class="fa fa-trash"></div>',
                                            ['/training/training-exercise/delete', 'id' => $trainingExercise->id],
                                            [
                                                'class' => 'button button__round button__icon button__red',
                                                'data' => [
                                                    'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                                ]
                                            ]
                                        );
                                    ?>
                                </div>
                                <div class="traning__list_center" id="exercise-info-<?= $i ?>" class="collapse"
                                     data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                    <div class="traning__list_row">
                                        <div class="traning__task">
                                            <p><?= $trainingExercise->variant->exercise->question ?></p>
                                        </div>
                                    </div>
                                    <?php if (!empty($trainingExercise->variant->exercise->hint)): ?>
                                        <div class="traning__list_row">
                                            <a class="traning__link hint_href"
                                               href="#<?= "hint-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                               data-traning-link="<?= "hint-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                               data-toggle="collapse">
                                                    <span
                                                        class="traning__link--show open_hint">Показать подсказку</span>
                                                <span class="traning__link--hide close_hint">Скрыть подсказку</span>
                                            </a>
                                            <div class="traning__link_slide collapse hint_block"
                                                 id="<?= "hint-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                                <p><?= $trainingExercise->variant->exercise->hint ?></p>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (!empty($trainingExercise->variant->exercise->hint_second)): ?>
                                        <div class="traning__list_row">
                                            <a class="traning__link"
                                               href="#<?= "hint_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                               data-traning-link="<?= "hint_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                               data-toggle="collapse">
                                                <span class="traning__link--show">Показать вторую подсказку</span>
                                                <span class="traning__link--hide">Скрыть вторую подсказку</span>
                                            </a>
                                            <div class="traning__link_slide collapse"
                                                 id="<?= "hint_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                                <p><?= $trainingExercise->variant->exercise->hint_second ?></p>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (!empty($trainingExercise->variant->exercise->hint_third)): ?>
                                        <div class="traning__list_row">
                                            <a class="traning__link"
                                               href="#<?= "hint_third-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                               data-traning-link="<?= "hint_third-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                               data-toggle="collapse">
                                                <span class="traning__link--show">Показать третью подсказку</span>
                                                <span class="traning__link--hide">Скрыть третью подсказку</span>
                                            </a>
                                            <div class="traning__link_slide collapse"
                                                 id="<?= "hint_third-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                                <p><?= $trainingExercise->variant->exercise->hint_third ?></p>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (!empty($trainingExercise->variant->exercise->decision)): ?>
                                        <div class="traning__list_row">
                                            <a class="traning__link"
                                               href="#<?= "decision-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                               data-traning-link="<?= "decision-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                               data-toggle="collapse">
                                                    <span
                                                        class="traning__link--show open_decision">Показать решение</span>
                                                <span
                                                    class="traning__link--hide close_decision">Скрыть решение</span>
                                            </a>
                                            <div class="traning__link_slide collapse decision_block"
                                                 id="<?= "decision-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                                <p><?= $trainingExercise->variant->exercise->decision ?></p>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (!empty($trainingExercise->variant->exercise->decision_detailed)): ?>
                                        <div class="traning__list_row">
                                            <a class="traning__link"
                                               href="#<?= "decision_detailed-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                               data-traning-link="<?= "decision_detailed-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                               data-toggle="collapse">
                                                <span class="traning__link--show">Показать подробное решение</span>
                                                <span class="traning__link--hide">Скрыть подробное решение</span>
                                            </a>
                                            <div class="traning__link_slide collapse"
                                                 id="<?= "decision_detailed-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                                <p><?= $trainingExercise->variant->exercise->decision_detailed ?></p>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (!empty($trainingExercise->variant->exercise->decision_second)): ?>
                                        <div class="traning__list_row">
                                            <a class="traning__link"
                                               href="#<?= "decision_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                               data-traning-link="<?= "decision_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                               data-toggle="collapse">
                                                <span class="traning__link--show">Показать второе решение</span>
                                                <span class="traning__link--hide">Скрыть второе решение</span>
                                            </a>
                                            <div class="traning__link_slide collapse"
                                                 id="<?= "decision_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                                <p><?= $trainingExercise->variant->exercise->decision_second ?></p>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (!empty($trainingExercise->variant->exercise->answer)): ?>
                                        <div class="traning__list_row">
                                            <a class="traning__link"
                                               href="#<?= "answer-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                               data-traning-link="<?= "answer-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                               data-toggle="collapse">
                                                <span class="traning__link--show open_answers">Показать ответ</span>
                                                <span class="traning__link--hide close_answers">Скрыть ответ</span>
                                            </a>
                                            <div class="traning__link_slide collapse answers_block"
                                                 id="<?= "answer-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                                <p><?= $trainingExercise->variant->exercise->answer ?></p>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <div class="traning__list_row"><a class="traning__link" href="#"
                                                                      data-traning-link="<?= "blank-{$i}-{$trainingExercise->variant->exercise->id}" ?>">
                                            <span class="traning__link--show field_open">Добавить пространство для решения</span><span
                                                class="traning__link--hide field_close">Скрыть пространство для решения</span></a>
                                        <div class="traning__link_slide field_div"
                                             id="<?= "blank-{$i}-{$trainingExercise->variant->exercise->id}" ?>">
                                            <div class="traning__field_wrap">
                                                <div class="traning__field_toggle">
                                                    <div
                                                        class="traning__field_toggle-item traning__field_toggle-item--blank"
                                                        data-style="blank"></div>
                                                    <div
                                                        class="traning__field_toggle-item traning__field_toggle-item--line"
                                                        data-style="line"></div>
                                                    <div
                                                        class="traning__field_toggle-item traning__field_toggle-item--cage"
                                                        data-style="cage"></div>
                                                </div>
                                                <div class="traning__field">
                                                    <textarea> </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="traning__sublist">
                                    <div class="traning__sublist_row">
                                        <div class="traning__sublist_name">Задача
                                            №<?= $trainingExercise->variant->exercise->id ?> <?= $trainingExercise->variant->exercise->name ?></div>
                                    </div>
                                    <div class="traning__sublist_row">
                                        <div class="traning__sublist_name">Тема:</div>
                                        <div
                                            class="traning__sublist_text"><?= $trainingExercise->variant->theme->getFullName() ?></div>
                                    </div>
                                    <div class="traning__sublist_row">
                                        <div class="traning__sublist_name">Сложность:</div>
                                        <div
                                            class="traning__sublist_text"><?= $trainingExercise->variant->complexity ?></div>
                                    </div>

                                </div>
                            </div>
                            <?php $i++ ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div class="modal fade training-setup" id="trainingSetup" data-backdrop="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span
                                    aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                            <h4 class="modal-title">Настройка тренинга</h4>
                        </div>
                        <div class="modal-body replace_item_modal_body">
                            <ul>
                                <li><label><input type="checkbox" id="showHint"> Показать подсказки</label></li>
                                <li><label><input type="checkbox" id="showDecision"> Показать решения</label></li>
                                <li><label><input type="checkbox" id="showAnswer"> Показать ответы</label></li>
                                <li><label><input type="checkbox" id="shuffleExercises"> Перемешать задачи</label></li>
                                <li><label><input type="checkbox" id="addAreas"> Добавить области для решения</label>
                                </li>
                            </ul>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="setupBtn" class="btn btn-white" data-dismiss="modal">Ок</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="use-training" data-backdrop="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Как пользоваться тренировками?</h4>
                        </div>
                        <div class="modal-body">
                            <p><?= \common\models\Html::get('use_training') ?></p>
                            <p><?= Html::a('Подробнее', ['/training/default/page', 'view' => 'use_training_detail']) ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php $this->registerJsFile('/js/html2canvas.js') ?>
    <?php $this->registerJsFile('/js/FileSaver.min.js') ?>
    <?php $this->registerJsFile('/js/canvas-to-blob.min.js') ?>

<?php else: ?>
    <div class="crm__container">
        <div class="crm__content">
            <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
                <?= AlertBlock::widget([
                    'delay' => false
                ]) ?>
            <?php endif; ?>
            <style>
                .alert-success {
                    color: #222
                }

                .action-price-text {
                    margin-left: 10px;
                    font-size: 24px;
                    color: #4785fc
                }
            </style>
            <div class="wihite__box shadow">
                <div class="traning">
                    <div class="page__header_title"><?= $training->name ?></div>
                    <div class="lesson-full">
                        <p><?= $training->description_short ?></p><br>
                    </div>
                    <div class="lesson-full">
                        <?php if ($training->is_action == 0): ?>
                            <?php if ($training->price == 0 || $training->price == NULL): ?>
                                <div class="lesson-full__title">Для просмотра данной тренировки, просто добавьте ее
                                </div><?= Html::a('Добавить тренировку', ['/training/default/add-training', 'training_id' => $training->id, 'training_price' => $training->price], [
                                    'data' => ['confirm' => 'Вы уверены?', 'method' => 'post'], 'class' => 'button button__green']); ?>
                            <?php else: ?>
                                <div class="lesson-full__title">Для просмотра данной тренировки, просто купите ее
                                </div><?= Html::a('Купить тренировку за ' . $training->price . ' руб.', ['/training/default/buy-training', 'training_id' => $training->id, 'training_price' => $training->price], [
                                    'data' => ['confirm' => 'С вашего Арете Кошелька будет списано ' . $training->price . ' рублей. Вы уверены?', 'method' => 'post'], 'class' => 'button button__green']); ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php if ($training->action_price == 0 || $training->action_price == NULL): ?>
                                <div class="lesson-full__title">Для просмотра данной тренировки, просто добавьте ее
                                </div><?= Html::a('Добавить тренировку', ['/training/default/add-training', 'training_id' => $training->id, 'training_price' => $training->action_price], [
                                    'data' => ['confirm' => 'Вы уверены?', 'method' => 'post'], 'class' => 'button button__green']); ?>
                            <?php else: ?>
                                <div class="lesson-full__title">Для просмотра данной тренировки, просто купите ее
                                </div><?= Html::a('Купить тренировку за ' . $training->action_price . ' руб.', ['/training/default/buy-training', 'training_id' => $training->id, 'training_price' => $training->action_price], [
                                    'data' => ['confirm' => 'С вашего Арете Кошелька будет списано ' . $training->action_price . ' рублей. Вы уверены?', 'method' => 'post'], 'class' => 'button button__green']); ?>
                            <?php endif; ?>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
