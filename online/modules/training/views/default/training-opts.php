<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

?>

<div class="crm__container">
    <div class="crm__content">
        <div id="white-box" class="wihite__box shadow">
            <div class="traning">
                <div class="traning__title"><?= $training->name ?> (опции)</div>
                <div class="task__list">
                    <?= Html::beginForm('', 'post', ['id' => 'submit-form']) ?>
                    <?php foreach ($training->trainingExercises as $i => $trainingExercise): ?>
                        <div class="traning__list_item">
                            <?= Html::hiddenInput('TrainingVsGroupOpt[' . $i . '][task_id]', $trainingExercise->id) ?>
                            <div class="traning__list_count"><?= $i + 1 ?> </div>
                            <div class="traning__list_center">
                                <div class="traning__task">
                                    <p><?= $trainingExercise->variant->exercise->question ?></p>
                                </div>
                                <label class="opt__link">
                                    <?= Html::checkbox('TrainingVsGroupOpt[' . $i . '][f_hint]', null) ?> Показать
                                    подсказку
                                </label>
                                <label class="opt__link">
                                    <?= Html::checkbox('TrainingVsGroupOpt[' . $i . '][f_decision]', null) ?> Показать
                                    решение
                                </label>
                                <label class="opt__link">
                                    <?= Html::checkbox('TrainingVsGroupOpt[' . $i . '][f_answer]', null) ?> Показать
                                    ответ
                                </label>
                                <label class="opt__link">
                                    <?= Html::checkbox('TrainingVsGroupOpt[' . $i . '][f_area]', null) ?> Добавить
                                    пространство
                                    для решения
                                </label>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="traning__list_item">
                        <?= Html::button('Далее', ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#select-group']) ?>
                    </div>
                    <?php Modal::begin([
                        'id' => 'select-group',
                        'header' => '<h2>Отправить в группу</h2>',
                        'options' => [
                            'class' => 'modal-sm',
                        ],
                        'footer' => \yii\bootstrap\Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'form' => 'submit-form'])
                            . ' ' . Html::button('Закрыть', ['class' => 'btn btn-default', 'data-dismiss' => 'modal', 'aria-hidden' => true]),
                    ]); ?>
                    <?= Html::hiddenInput('TrainingVsGroup[training_id]', $training->id) ?>
                    <?php foreach ($groups as $group): ?>
                        <div class="form__control">
                            <div class="form__label">
                                <?= Html::checkbox('group_ids[]', 0, ['value' => $group->id, 'id' => 'id-' . $group->id]) ?>
                                <label for="id-<?= $group->id ?>"><?= $group->name ?></label>
                            </div>
                        </div>
                    <?php
                    endforeach;
                    Modal::end();
                    Html::endForm() ?>
                </div>
            </div>
        </div>
    </div>
</div>

