<?php

use common\models\ExerciseClass;
use yii\helpers\Html;

$this->title = 'Создать тренировку';
?>

<!-- begin content-->
<div class="crm__container">
    <!-- begin aside-->
    <div class="crm__content">
        <div class="store__ban" style="background-image: url(/img/ban-bg-green.jpg)">
            <div class="store__ban_title-wrap">
                <div class="store__ban_title">
                    <div class="store__ban_title-text">
                        Попробуйте готовые тренировки <br>из нашего магазина
                        <p><?= Html::a('Перейти в магазин тренировок', ['/training/default/store'], ['class' => 'button button__brown-blue button__round']) ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="store">
            <div class="grid grid__cube grid__cube_tree" data-grid-match="">
                <?php foreach ($subjects as $subject): ?>
                    <div class="grid__wrapper">
                        <div class="grid__item">
                            <div class="grid__cube_img">
                                <img width="70" src="<?= \Yii::getAlias('@web/upload/subject/') . $subject->image ?>"/>
                            </div>
                            <div class="grid__cube_name"><?= $subject->name ?> </div>
                            <div class="store__create">
                                <div class="button button__green store__create_btn">Создать тренировку</div>
                                <div class="store__create_field">
                                    <div class="fa fa-signal"></div>
                                    <div class="fa fa-caret-down"></div>
                                    <?= Html::beginForm(['create'], 'get') ?>
                                    <?= Html::hiddenInput('subject_id', $subject->id) ?>
                                    <?= Html::dropDownList(
                                        'class',
                                        null,
                                        ExerciseClass::getClasses($subject->id),
                                        [
                                            'prompt' => 'Выберите класс',
                                            'onchange' => "this.form.submit()"
                                        ]
                                    ) ?>
                                    <?= Html::endForm() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>