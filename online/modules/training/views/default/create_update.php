<?php

use admin\widgets\ckeditor\CKEditor;
use kartik\date\DatePicker;
use kartik\tree\TreeViewInput;
use common\models\TrainingCategory;
use yii\widgets\ActiveForm;

$this->registerJs('jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
        jQuery(this).html("Address: " + (index + 1))
    });
});

jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
        jQuery(this).html("Address: " + (index + 1))
    });
});');

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Тренировки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$('.kv-node-checkbox, .kv-node-label, .kv-node-icon').click(function() {

    var ul = $(this).closest('ul'),
        li = $(this).closest('li');

    if(ul.hasClass('kv-tree'))
        return;

    if(li.hasClass('kv-selected')) {
        for(var i = 0; i < 10; i++) {
            ul.parent().removeClass('kv-selected');
            ul = ul.parent().parent();
            if(ul.hasClass('kv-tree'))
                break;
        }
    } else {
        for(var i = 0; i < 10; i++) {
            ul.parent().addClass('kv-selected');
            ul = ul.parent().parent();
            if(ul.hasClass('kv-tree'))
                break;
        }
    }
});
JS;
$this->registerJs($js);
?>

<div class="ibox float-e-margins">
    
    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
    </div>
    
    <div class="ibox-content">
        <div class="row">
            <div class="col-xs-12">
                <?php //echo $this->render('@admin/views/_formula'); ?>
            </div>
            <div class="col-xs-8">
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'price')->textInput(['type' => 'number']) ?>
                
                <?= $form->field($model, 'is_action')->checkbox() ?>
                
                <?= $form->field($model, 'action_price')->textInput(['type' => 'number', 'value' => 0]) ?>
                
                <?= $form->field($model, 'action_date_start')->widget(\kartik\widgets\DatePicker::className(), [
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ],
                ]);
                ?>
                
                <?= $form->field($model, 'action_date_end')->widget(\kartik\widgets\DatePicker::className(), [
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ],
                ]);
                ?>
                
                <?php if (!empty($model->image)): ?>
                    <div class="form-group">
                        <label class="control-label">Текущее изображение</label>
                        <img width="200" src="<?= \Yii::getAlias('@web/upload/training/') . $model->image ?>"/>
                    </div>
                <?php endif; ?>
                
                <?= $form->field($model, 'imageFile')->fileInput() ?>
                
                <?= $form->field($model, 'description_short')->widget(CKEditor::className(), [
                    'options' => ['rows' => 100],
                    'enableFinder' => true,
                    'clientOptions' => [
                        'toolbar' => [
                            [
                                'name' => 'clipboard',
                                'items' => [
                                    'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                                ],
                            ],
                            [
                                'name' => 'links',
                                'items' => [
                                    'Link', 'Unlink', 'Anchor'
                                ],
                            ],
                            [
                                'name' => 'insert',
                                'items' => [
                                    'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                                ],
                            ],
                            [
                                'name' => 'align',
                                'items' => [
                                    'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                                ],
                            ],
                            [
                                'name' => 'document',
                                'items' => [
                                    'Maximize', 'ShowBlocks', 'Source'
                                ],
                            ],
                            '/',
                            [
                                'name' => 'basicstyles',
                                'items' => [
                                    'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                                ],
                            ],
                            [
                                'name' => 'color',
                                'items' => [
                                    'TextColor', 'BGColor'
                                ],
                            ],
                            [
                                'name' => 'paragraph',
                                'items' => [
                                    'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                                ],
                            ],
                            [
                                'name' => 'styles',
                                'items' => [
                                    'Styles', 'Format', 'Font', 'FontSize'
                                ],
                            ],
                            '/',
                            [
                                'name' => 'insert',
                                'items' => [
                                    'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                                ],
                            ],
                        ],
                        'height' => 500,
                        'allowedContent' => true,
                        'contentsCss' => [
                            '/css/fa/font-awesome.min.css',
                            '/css/bootstrap.min.css',
                            Yii::$app->params['scheme'] . '://' . Yii::$app->params['host'] . '/css/main.css',
                            Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host'] . '/css/article.css'
                        ],
                        'extraPlugins' => 'mathjax,exercise,test,sliders,accordion,vkshare',
                        'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML'
                    ],
                ]) ?>
                
                
                <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                    'options' => ['rows' => 100],
                    'enableFinder' => true,
                    'clientOptions' => [
                        'toolbar' => [
                            [
                                'name' => 'clipboard',
                                'items' => [
                                    'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                                ],
                            ],
                            [
                                'name' => 'links',
                                'items' => [
                                    'Link', 'Unlink', 'Anchor'
                                ],
                            ],
                            [
                                'name' => 'insert',
                                'items' => [
                                    'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                                ],
                            ],
                            [
                                'name' => 'align',
                                'items' => [
                                    'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                                ],
                            ],
                            [
                                'name' => 'document',
                                'items' => [
                                    'Maximize', 'ShowBlocks', 'Source'
                                ],
                            ],
                            '/',
                            [
                                'name' => 'basicstyles',
                                'items' => [
                                    'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                                ],
                            ],
                            [
                                'name' => 'color',
                                'items' => [
                                    'TextColor', 'BGColor'
                                ],
                            ],
                            [
                                'name' => 'paragraph',
                                'items' => [
                                    'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                                ],
                            ],
                            [
                                'name' => 'styles',
                                'items' => [
                                    'Styles', 'Format', 'Font', 'FontSize'
                                ],
                            ],
                            '/',
                            [
                                'name' => 'insert',
                                'items' => [
                                    'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                                ],
                            ],
                        ],
                        'height' => 500,
                        'allowedContent' => true,
                        'contentsCss' => [
                            '/css/fa/font-awesome.min.css',
                            '/css/bootstrap.min.css',
                            Yii::$app->params['scheme'] . '://' . Yii::$app->params['host'] . '/css/main.css',
                            Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host'] . '/css/article.css'
                        ],
                        'extraPlugins' => 'mathjax,exercise,test,sliders,accordion,vkshare',
                        'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML'
                    ],
                ]) ?>
            
            </div>
            <div class="col-xs-4">
                <?= $form->field($model, 'category_list')->widget(TreeViewInput::classname(), [
                    // single query fetch to render the tree
                    // use the Product model you have in the previous step
                    'query' => TrainingCategory::find()->addOrderBy('root, lft'),
                    'headingOptions' => ['label' => 'Классификация'],
                    'name' => 'category_list',
                    'asDropdown' => false,   // will render the tree input widget as a dropdown.
                    'multiple' => true,     // set to false if you do not need multiple selection
                    'cascadeSelectChildren' => false,
                    'fontAwesome' => true,  // render font awesome icons
                    'rootOptions' => [
                        'label' => '<i class="fa fa-tree"></i>',  // custom root label
                        'class' => 'text-success'
                    ],
                    'options' => ['class' => 'form-control show'],
                ]); ?>
            </div>
        </div>
        
        <hr>
        
        <button type="submit" class="btn btn-primary">Сохранить</button>
        
        <?php ActiveForm::end(); ?>
    
    </div>
</div>
