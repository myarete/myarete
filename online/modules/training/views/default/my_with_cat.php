<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\UserVsTrainingCategory;

$path = '@web/upload/training/';
$this->title = 'Статьи';
?>

<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">


    <div class="article-block">
        <div class="row side-block">
            <div class="col-md-12">
                <h3>Мои категории</h3>
                <?php if(!empty($childCategory)) : ?>
                    <h2><?= $childCategory->name ?></h2>
                <?php endif; ?>

                <?php if(!empty($categories)) : ?>
                    <div class="article-block">
                        <div class="row">
                            <?php foreach($categories as $category):
                                if ($category->active!=1): continue; endif;?>
                                <?php if (UserVsTrainingCategory::findOne(['user_id' => Yii::$app->user->id, 'training_category_id' => $category->id]) || UserVsTrainingCategory::findOne(['user_id' => Yii::$app->user->id, 'pid' => $category->id]) || UserVsTrainingCategory::findOne(['user_id' => Yii::$app->user->id, 'root' => $category->id])):?>
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                    <div class="thumbnail">
                                        <?php if(!empty($category->getMainImage($path))): ?>
                                            <a href="<?= Url::to(['/training/default/my', 'categoryId' => $category->id]) ?>"><img src="<?= $category->getMainImage($path) ?>"></a>
                                        <?php endif; ?>
                                        <div class="caption">
                                            <a href="<?= Url::to(['/training/default/my', 'categoryId' => $category->id]) ?>"><h3><?= $category->name ?></h3> </a>
                                            <?php if(!empty($category->description_short)): ?>
                                                <p class="desc-short"><?= $category->description_short ?></p>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if(!empty($dataProvider->models) && !empty($categoryId)) : ?>
                    <div class="article-block">
                        <div class="row">
                            <?php foreach($dataProvider->models as $model) : ?>
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                    <div class="thumbnail">
                                        <?php if(!empty($model->getMainImage($path))): ?>
                                            <img src="<?= $model->getMainImage($path) ?>">
                                        <?php endif; ?>
                                        <div class="caption">
                                            <div class="article-title"><?= Html::a($model->name, ['/training/default/view', 'id' => $model->id]) ?></div>
                                            <p><?= $category->description_short ?></p>

                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <div class="col-md-12">
                <h3>Список тренировок</h3>
                <b>Сортировать:</b> <?= $sort->link('name') . ' | ' . $sort->link('id');?> <br><br>
                <?php foreach($trainingsFromCategories as $training) : ?>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <div class="thumbnail">
                            <?php if(!empty($training->getMainImage($path))): ?>
                                <?= Html::a('<img src="'.$training->getMainImage($path).'">', ['/training/default/view', 'id' => $training->id]) ?>

                            <?php endif; ?>
                            <div class="caption">
                                <div class="article-title"><?= Html::a($training->name, ['/training/default/view', 'id' => $training->id]) ?></div>
                                <?php if(!empty($training->description_short)): ?>
                                    <p class="desc-short"><?= $training->description_short ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

                <?php foreach($trainings as $training) : ?>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <div class="thumbnail">
                            <?php if(!empty($training->getMainImage($path))): ?>
                                <?= Html::a('<img src="'.$training->getMainImage($path).'">', ['/training/default/view', 'id' => $training->id]) ?>

                            <?php endif; ?>
                            <div class="caption">
                                <div class="article-title"><?= Html::a($training->name, ['/training/default/view', 'id' => $training->id]) ?></div>
                                <?php if(!empty($training->description_short)): ?>
                                    <p class="desc-short"><?= $training->description_short ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
        <br><br>
    </div>
</div>