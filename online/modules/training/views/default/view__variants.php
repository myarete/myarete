<?php
use common\models\ExerciseVariant;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Theme;

?>


<div class="ibox float-e-margins">

    <div class="ibox-title">
        Задачи
        <div class="ibox-tools">
            <a href="#" data-toggle="modal" data-target="#add-variant-modal"><i class="fa fa-plus"></i> Добавить задачу</a>
        </div>
    </div>
    <div class="ibox-content">
        <?php if(!empty($exercises)):?>
        <table class="table">
            <thead>
            <tr>
                <th>Номер</th>
                <th>Задача</th>
                <th>Тема</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <?php foreach($exercises as $exercise):?>
                    <tr>
                        <td><?= $exercise['exercise_id']?></td>
                        <td><?= $exercise['name']?></td>
                        <td><?= $exercise['theme']?></td>
                        <td><?= Html::a('Редактировать', Url::to('/training/default/update-exercise?id='.$exercise['exercise_id'].'&exercise_id='.$exercise['id'].'&training_id='.$training_id), ['class' => 'btn btn-primary col-xs-12']);?></td>
                        <td><?= Html::a('Удалить', Url::to('/training/default/delete-exercise?id='.$exercise['id'].'&training_id='.$training_id), ['class' => 'btn btn-primary col-xs-12']);?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        <?php else:?>
            <p>Еще не добавленно ни одной задачи.</p>
        <?php endif;?>

    </div>
</div>


<?= $this->render('view__variants__modal', [
    'exerciseModel' => $model,
    'variantModel' => new ExerciseVariant(['exercise_id' => $model->id]),
    'training_id' => $training_id,
]) ?>



