<?php

use kartik\select2\Select2;
use common\models\ExerciseClass;
use common\models\ExerciseVariant;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$this->title = "Создать тренировку";

$js = <<<JS
$.each($('.tree li > div'), function(i, e) {
    if((i % 2) == 0)
        $(e).css('background', '#fff');
    else
        $(e).css('background', '#f9f9f9');
});

$('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Свернуть');
$('.tree li.parent_li > div span').on('click', function (e) {
    var children = $(this).parent().parent('li.parent_li').find(' > ul > li');
    if (children.is(":visible")) {
        children.hide('fast');
        $(this).attr('title', 'Развернуть').find(' > i').addClass('glyphicon-plus').removeClass('glyphicon-minus');
    } else {
        children.show('fast');
        $(this).attr('title', 'Свернуть').find(' > i').addClass('glyphicon-minus').removeClass('glyphicon-plus');
    }
    e.stopPropagation();
    $.each($('.tree li > div:visible'), function(i, e) {
        if((i % 2) == 0)
            $(e).css('background', '#fff');
        else
            $(e).css('background', '#f9f9f9');
    });
});

$.each($('.subtheme-list'), function(i, e) {
    if($(e).find('li').length == 0) {
        $(e).parent().remove();
    }
});
$.each($('.theme-list'), function(i, e) {
    if($(e).find('li').length == 0) {
        $(e).parent().remove();
    }
});

$('#create-empty').click(function() {
    $('#create-training-form').attr('action', 'create-empty');
    $('#create-training-form').submit();
});
JS;
$this->registerJs($js);
?>

<div class="crm__container">
    <!-- begin aside-->
    <div class="aside aside__left">
        <div class="discipline">
            <div class="discipline__img">
                <img width="70" src="<?= \Yii::getAlias('@web/upload/subject/') . $theme->subject->image ?>"/>
            </div>
            <div class="discipline__name"><?= $theme->subject->name ?></div>
            <div class="discipline__sub"><?= $theme->name ?> <a href="#">Изменить</a></div>
            <div class="store__create">
                <div class="store__create_field">
                    <div class="fa fa-signal"></div>
                    <div class="fa fa-caret-down"></div>
                    <?= Html::beginForm(['create'], 'get') ?>
                    <?= Html::hiddenInput('subject_id', $theme->subject->id) ?>
                    <?= Html::dropDownList(
                        'class',
                        null,
                        ExerciseClass::getClasses($theme->subject->id),
                        [
                            'prompt' => 'Выберите класс',
                            'onchange' => "this.form.submit()"
                        ]
                    ) ?>
                    <?= Html::endForm() ?>
                </div>
            </div>
        </div>
    </div>
    <!-- end aside-->
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="traning">
                <form method="post" id="create-training-form">
                    <div class="traning__top">
                        <a class="button button__blue button__question" data-toggle="modal"
                           data-target="#questions_and_answers__modal">Вопросы и ответы
                            <div class="fa fa-question"></div>
                        </a>
                        <?= Html::a('Создать пустую тренировку', ['#'], ['id' => 'create-empty', 'class' => 'button button__round']) ?>
                        <div class="button__group_flip">
                            <button class="button button__round button__green--full pull-right">Создать</button>
                        </div>
                    </div>
                    <?php if (count($sections)): ?>
                        <?= Html::errorSummary($model, ['class' => 'error-summary']) ?>
                        <?= Html::activeHiddenInput($model, 'name', ['id' => 'training-name']) ?>
                        <div class="task__content">
                            <?php foreach ($sections as $section): ?>
                                <div class="task__row_wrap">
                                    <div class="task__row">
                                        <div class="task__row_item task__row_item--left"><a class="icon-plus"
                                                                                            href="#"><?= $section->name ?></a>
                                        </div>
                                        <div class="task__row_item task__row_item--center"></div>
                                        <div class="task__row_item task__row_item--right task__count_add">
                                            <!--div class="task__count_add-label show">Введите кол-во заданий</div>
                                            <div class="task__count_add-rezult">0</div-->
                                            <div class="task__count_add-field show">
                                                <?= Html::activeInput(
                                                    'number',
                                                    $model,
                                                    "quantity[$section->id]",
                                                    ['placeholder' => 'Кол-во задач']
                                                ) ?>
                                                <div class="fa fa-book"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php foreach ($section->children(1)->all() as $theme): ?>
                                        <div class="task__row_wrap">
                                            <div class="task__row task__row_sub">
                                                <div class="task__row_item task__row_item--left"><a class="icon-plus"
                                                                                                    href="#"><?= $theme->name ?></a>
                                                </div>
                                                <div class="task__row_item task__row_item--center"></div>
                                                <div class="task__row_item task__row_item--right task__count_add">
                                                    <!--div class="task__count_add-label show">Введите кол-во заданий</div>
                                                    <div class="task__count_add-rezult">0</div-->
                                                    <div class="task__count_add-field show">
                                                        <?= Html::activeInput(
                                                            'number',
                                                            $model,
                                                            "quantity[$theme->id]",
                                                            ['placeholder' => 'Кол-во задач']
                                                        ) ?>
                                                        <div class="fa fa-book"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php foreach ($theme->children(1)->all() as $subtheme): ?>
                                                <?php if (ExerciseVariant::findAll(['theme_id' => $subtheme->id])): ?>
                                                    <div class="task__row_wrap">
                                                        <div class="task__row task__row_sub">
                                                            <div class="task__row_item task__row_item--left"><a
                                                                    class="icon-plus icon-plus--empty"
                                                                    href="#"><?= $subtheme->name ?></a></div>
                                                            <div class="task__row_item task__row_item--center"></div>
                                                            <div
                                                                class="task__row_item task__row_item--right task__count_add">
                                                                <!--div class="task__count_add-label show">Введите кол-во заданий</div>
                                                                <div class="task__count_add-rezult">0</div-->
                                                                <div class="task__count_add-field show">
                                                                    <?= Html::activeInput(
                                                                        'number',
                                                                        $model,
                                                                        "quantity[$subtheme->id]",
                                                                        ['placeholder' => 'Кол-во задач']
                                                                    ) ?>
                                                                    <div class="fa fa-book"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif ?>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php else: ?>
                        <p>Нет разделов</p>
                    <?php endif; ?>
                    <p>
                        <button class="button button__round button__green--full">Создать</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'options' => [
        'id' => 'kartik-modal',
        'tabindex' => false, // important for Select2 to work properly
        'data-backdrop' => 'false'
    ],
    'header' => '<h4 class="modal-title" id="myModalLabel">Добавить задачу вручную</h4>',
    'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="submit" form="addExerciseForm" class="btn btn-primary">Сохранить</button>'
]); ?>

<?= Html::beginForm(['/training/default/manual-exercise'], 'post', ['id' => 'addExerciseForm']) ?>
<?= Select2::widget([
    'name' => 'name',
    'options' => ['placeholder' => 'Введите название тренировки для добавления в неё или создания новой...'],
    'pluginOptions' => [
        'tags' => true,
        'allowClear' => true,
        'minimumInputLength' => 1,
        'ajax' => [
            'url' => Url::to(['/training/default/manual-training-list']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term}; }')
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('function(training) { if(training.id) return training.id + ": " + training.text; else return training.text;}'),
        'templateSelection' => new JsExpression('function (training) { return training.text; }'),
    ],
]); ?>
<br>
<?= Select2::widget([
    'name' => 'exercise_variant_id',
    'options' => ['placeholder' => 'Введите номер или название задачи...'],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 1,
        'ajax' => [
            'url' => Url::to(['/training/default/manual-exercise-list']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term, subject_id:' . $theme->subject_id . ', class:' . $theme->class . '}; }')
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('function(exercise) { if(exercise.id) return exercise.id + ": " + exercise.text; else return exercise.text;}'),
        'templateSelection' => new JsExpression('function (exercise) { return exercise.text; }'),
    ],
]); ?>
<?php ExerciseClass::getExercises($theme->subject_id, $theme->class) ?>
<?= Html::endForm() ?>
<?php Modal::end(); ?>
