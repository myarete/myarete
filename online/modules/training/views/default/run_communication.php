<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use common\components\other\History;

History::put(Yii::$app->user->identity->email, $training->name);

$this->title = $training->name;

$js=<<<JS
$('.replace-btn').click(function() {
    $('#ReplaceModal').modal();
    $('#ReplaceModal form').attr('action', '/training/training-exercise/replace?id=' + $(this).data('id'))
});

$('#ReplaceModal').keypress(function(ev) {
    if(ev.keyCode == 13)
        $('#SubmitBtn').click();
});

$('#showSetup').click(function() {
    $('#trainingSetup').modal();
});

$('#setupBtn').click(function() {
    if($('#showHint').is(':checked')) {
        $('a[href ^= #hint-]').removeClass('collapsed').attr('aria-expanded', true);
        $('div[id ^= hint-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
    } else {
        $('a[href ^= #hint-]').addClass('collapsed').attr('aria-expanded', false);
        $('div[id ^= hint-]').removeClass('in').attr('aria-expanded', false);
    }
    if($('#showDecision').is(':checked')) {
        $('a[href ^= #decision-]').removeClass('collapsed').attr('aria-expanded', true);
        $('div[id ^= decision-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
    } else {
        $('a[href ^= #decision-]').addClass('collapsed').attr('aria-expanded', false);
        $('div[id ^= decision-]').removeClass('in').attr('aria-expanded', false);
    }
    if($('#showAnswer').is(':checked')) {
        $('a[href ^= #answer-]').removeClass('collapsed').attr('aria-expanded', true);
        $('div[id ^= answer-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
    } else {
        $('a[href ^= #answer-]').addClass('collapsed').attr('aria-expanded', false);
        $('div[id ^= answer-]').removeClass('in').attr('aria-expanded', false);
    }
    if($('#addAreas').is(':checked')) {
        $('a[href ^= #solutions-]').removeClass('collapsed').attr('aria-expanded', true);
        $('div[id ^= solutions-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
    } else {
        $('a[href ^= #solutions-]').addClass('collapsed').attr('aria-expanded', false);
        $('div[id ^= solutions-]').removeClass('in').attr('aria-expanded', false);
    }
    if($('#shuffleExercises').is(':checked')) {
        $.post('/training/default/shuffle-exercises', {id: '<?= $training->id ?>'});
        setTimeout(function() {
            location.reload();
        }, 500);
    }
});

$('#screenshot').click(function() {
    $('.traning__top').hide();
    $('.traning__list_action').hide();

    html2canvas($('.wihite__box'), {
        onrendered: function(canvas) {
            canvas.toBlob(function(blob) {
                saveAs(blob ,"<?= $training->name ?>.png");
            }, "image/png");
        }
    });

    $('.traning__top').show();
    $('.traning__list_action').show();
});

$('#printscreen').click(function() {
    var content = $('.traning__list').html();

    $.post('/training/default/print-screen', {content: content}, function(resp) {
        if(resp != '') location.href = '/training/default/print-screen?id=' + '<?= $training->id ?>' + '&tmpfile=' + resp;
    });
});

$('.bground-btn').click(function() {
    if($(this).hasClass('clean-background')) {
        $(this).closest('.row').find('textarea').css('background-image', 'none');
        $(this).closest('.row').find('textarea').css('background-color', '#fff');
    } else if($(this).hasClass('cage-background')) {
        $(this).closest('.row').find('textarea').css('background-image', 'url(/../images/cage.jpg)');
    } else if($(this).hasClass('linear-background')) {
        $(this).closest('.row').find('textarea').css('background-image', 'url(/../images/linear.png)');
    }
});

$('#similar-training').click(function() {
    yii.prompt('Название тренировки', function(name) {
        var id = '<?= $training->id ?>';
        $.post('/training/default/create-similar', {id: id, name: name}, function(response) {
            location.href = '/training/default/run/' + response;
        });
    });
});

$('#edit-btn').click(function() {
    $('#view-note').hide();
    $('#edit-note').show();
    $('#save-btn').show();
    $(this).hide();
});

$('#delete-btn').click(function() {
    var id = $(this).closest('form').find('input[type=hidden]').val();
    $.post('/training/default/delete-note', {id: id}, function(response) {
        location.reload();
    });
});
JS;
$this->registerJs($js);
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="traning">
                <div class="traning__title"><?=$training->name?></div>
                <div class="traning__top">
                    <a class="button button__blue button__question" data-toggle="modal" data-target="#questions_and_answers__modal">Вопросы и ответы
                        <div class="fa fa-question"></div>
                    </a>
                    <div class="button__group_flip">
                        <?php if(isset($params) && $training->user_id == Yii::$app->user->id): ?>
                            <?= Html::a('Посмотреть полную версию', ['/training/default/run', 'id' => $training->id], ['class' => 'button button__round button__green']) ?>
                        <?php endif ?>
                        <?= Html::button($training->note ? 'Посмотреть заметку' : 'Создать заметку', [
                            'class' => 'button button__round button__green',
                            'data' => [
                                'toggle' => 'modal',
                                'target' => '#create-note'
                            ]
                        ]) ?>
                        <?= Html::a('<div class="fa fa-copy"></div>', '#', [
                            'id' => 'similar-training',
                            'class' => 'button button__round button__yellow button__icon',
                        ]) ?>
                        <?= Html::button('<div class="fa fa-camera"></div>', [
                            'id' => 'screenshot',
                            'class' => 'button button__round button__blue button__icon',
                        ]) ?>
                        <?= Html::button('<div class="fa fa-print"></div>', [
                            'id' => 'printscreen',
                            'class' => 'button button__round button__green button__icon',
                        ]) ?>

                    </div>
                </div>
                <div class="traning__list">
                    <?php $i = 0 ?>
                    <?php foreach ($training->trainingExercises as $trainingExercise): ?>
                        <?php if(isset($params[$trainingExercise->variant->exercise->id]) && in_array('exercise', $params[$trainingExercise->variant->exercise->id])) {
                            continue;
                        }
                        ?>
                        <div class="traning__list_item" id="id_<?= $trainingExercise->id ?>">
                            <div class="traning__list_count"><?= $i + 1 ?> </div>
                            <?php if(isset($params[$trainingExercise->variant->exercise->id]) && in_array('info', $params[$trainingExercise->variant->exercise->id]))
                                $display = 'style="display:none"';
                            else
                                $display = '';
                            ?>
                            <div class="traning__list_action" <?= $display ?>>
                                <a class="button button__round button__green traning__btn_more" href="#">
                                    <span class="traning__btn_more--show">Подробнее </span>
                                    <span class="traning__btn_more--hide">Скрыть </span>
                                </a>

                                <?= Html::a(
                                    '<div class="fa '.(($trainingExercise->variant->exerciseFavorite) ? 'fa-star' : 'fa-star-o').'"></div>',
                                    [(($trainingExercise->variant->exerciseFavorite) ? '/exercise/favorite/delete' : '/exercise/favorite/add'),
                                        'id' => (($trainingExercise->variant->exerciseFavorite) ? $trainingExercise->variant->exerciseFavorite->id : $trainingExercise->exercise_variant_id)],
                                    [
                                        'class' => 'button button__round button__icon '.(($trainingExercise->variant->exerciseFavorite) ? 'button__red' : 'button__blue'),
                                    ]
                                ) ?>

                                <?php
                                if(!isset($params))
                                    echo Html::a(
                                        '<div class="fa fa-refresh"></div>',
                                        ['/training/training-exercise/replace-variants', 'id' => $trainingExercise->id],
                                        [
                                            'class' => 'button button__round button__yellow button__icon replace-btn',
                                            'onclick' => 'return false',
                                            'data' => [
                                                'id' => $trainingExercise->id,
                                            ]
                                        ]
                                    );
                                ?>
                                <?php
                                if(!isset($params))
                                    echo Html::a(
                                        '<div class="fa fa-trash"></div>',
                                        ['/training/training-exercise/delete', 'id' => $trainingExercise->id],
                                        [
                                            'class' => 'button button__round button__icon button__red',
                                            'data' => [
                                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                            ]
                                        ]
                                    );
                                ?>
                            </div>
                            <div class="traning__list_center" id="exercise-info-<?= $i ?>" class="collapse" data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                <div class="traning__list_row">
                                    <div class="traning__task">
                                        <p><?=$trainingExercise->variant->exercise->question?></p>
                                    </div>
                                </div>
                                <?php if (!empty($trainingExercise->variant->exercise->hint) && !(isset($params[$trainingExercise->variant->exercise->id]) && in_array('hint', $params[$trainingExercise->variant->exercise->id]))): ?>
                                    <div class="traning__list_row">
                                        <a class="traning__link" href="#<?= "hint-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-traning-link="<?= "hint-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-toggle="collapse">
                                            <span class="traning__link--show">Показать подсказку</span>
                                            <span class="traning__link--hide">Скрыть подсказку</span>
                                        </a>
                                        <div class="traning__link_slide collapse" id="<?= "hint-{$i}-{$trainingExercise->variant->exercise->id}" ?>"  data-id="<?= $trainingExercise->variant->exercise->id?>">
                                            <p><?= $trainingExercise->variant->exercise->hint ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($trainingExercise->variant->exercise->hint_second) && !(isset($params[$trainingExercise->variant->exercise->id]) && in_array('hint_second', $params[$trainingExercise->variant->exercise->id]))): ?>
                                    <div class="traning__list_row">
                                        <a class="traning__link" href="#<?= "hint_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-traning-link="<?= "hint_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-toggle="collapse">
                                            <span class="traning__link--show">Показать вторую подсказку</span>
                                            <span class="traning__link--hide">Скрыть вторую подсказку</span>
                                        </a>
                                        <div class="traning__link_slide collapse" id="<?= "hint_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"  data-id="<?= $trainingExercise->variant->exercise->id?>">
                                            <p><?= $trainingExercise->variant->exercise->hint_second ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($trainingExercise->variant->exercise->hint_third) && !(isset($params[$trainingExercise->variant->exercise->id]) && in_array('hint_third', $params[$trainingExercise->variant->exercise->id]))): ?>
                                    <div class="traning__list_row">
                                        <a class="traning__link" href="#<?= "hint_third-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-traning-link="<?= "hint_third-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-toggle="collapse">
                                            <span class="traning__link--show">Показать третью подсказку</span>
                                            <span class="traning__link--hide">Скрыть третью подсказку</span>
                                        </a>
                                        <div class="traning__link_slide collapse" id="<?= "hint_third-{$i}-{$trainingExercise->variant->exercise->id}" ?>"  data-id="<?= $trainingExercise->variant->exercise->id?>">
                                            <p><?= $trainingExercise->variant->exercise->hint_third ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($trainingExercise->variant->exercise->decision) && !(isset($params[$trainingExercise->variant->exercise->id]) && in_array('decision', $params[$trainingExercise->variant->exercise->id]))): ?>
                                    <div class="traning__list_row">
                                        <a class="traning__link" href="#<?= "decision-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-traning-link="<?= "decision-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-toggle="collapse">
                                            <span class="traning__link--show">Показать решение</span>
                                            <span class="traning__link--hide">Скрыть решение</span>
                                        </a>
                                        <div class="traning__link_slide collapse" id="<?= "decision-{$i}-{$trainingExercise->variant->exercise->id}" ?>"  data-id="<?= $trainingExercise->variant->exercise->id?>">
                                            <p><?= $trainingExercise->variant->exercise->decision ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($trainingExercise->variant->exercise->decision_detailed) && !(isset($params[$trainingExercise->variant->exercise->id]) && in_array('decision_detailed', $params[$trainingExercise->variant->exercise->id]))): ?>
                                    <?php if(isset($params[$trainingExercise->variant->exercise->id]) && in_array('decision_detailed', $params[$trainingExercise->variant->exercise->id]))
                                        $display = 'style="display:none"';
                                    else
                                        $display = '';
                                    ?>
                                    <div class="traning__list_row">
                                        <a class="traning__link" href="#<?= "decision_detailed-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-traning-link="<?= "decision_detailed-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-toggle="collapse">
                                            <span class="traning__link--show">Показать подробное решение</span>
                                            <span class="traning__link--hide">Скрыть подробное решение</span>
                                        </a>
                                        <div class="traning__link_slide collapse" id="<?= "decision_detailed-{$i}-{$trainingExercise->variant->exercise->id}" ?>"  data-id="<?= $trainingExercise->variant->exercise->id?>">
                                            <p><?= $trainingExercise->variant->exercise->decision_detailed ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($trainingExercise->variant->exercise->decision_second) && !(isset($params[$trainingExercise->variant->exercise->id]) && in_array('decision_second', $params[$trainingExercise->variant->exercise->id]))): ?>
                                    <div class="traning__list_row">
                                        <a class="traning__link" href="#<?= "decision_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-traning-link="<?= "decision_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-toggle="collapse">
                                            <span class="traning__link--show">Показать второе решение</span>
                                            <span class="traning__link--hide">Скрыть второе решение</span>
                                        </a>
                                        <div class="traning__link_slide collapse" id="<?= "decision_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"  data-id="<?= $trainingExercise->variant->exercise->id?>">
                                            <p><?= $trainingExercise->variant->exercise->decision_second ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($trainingExercise->variant->exercise->answer) && !(isset($params[$trainingExercise->variant->exercise->id]) && in_array('answer', $params[$trainingExercise->variant->exercise->id]))): ?>
                                    <div class="traning__list_row">
                                        <a class="traning__link" href="#<?= "answer-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-traning-link="<?= "answer-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-toggle="collapse">
                                            <span class="traning__link--show">Показать ответ</span>
                                            <span class="traning__link--hide">Скрыть ответ</span>
                                        </a>
                                        <div class="traning__link_slide collapse" id="<?= "answer-{$i}-{$trainingExercise->variant->exercise->id}" ?>" data-id="<?= $trainingExercise->variant->exercise->id?>">
                                            <p><?= $trainingExercise->variant->exercise->answer ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <div class="traning__list_row"> <a class="traning__link" href="#" data-traning-link="<?= "blank-{$i}-{$trainingExercise->variant->exercise->id}" ?>"> <span class="traning__link--show">Добавить пространство для решения</span><span class="traning__link--hide">Скрыть пространство для решения</span></a>
                                    <div class="traning__link_slide" id="<?= "blank-{$i}-{$trainingExercise->variant->exercise->id}" ?>">
                                        <div class="traning__field_wrap">
                                            <div class="traning__field_toggle">
                                                <div class="traning__field_toggle-item traning__field_toggle-item--blank" data-style="blank"></div>
                                                <div class="traning__field_toggle-item traning__field_toggle-item--line" data-style="line"></div>
                                                <div class="traning__field_toggle-item traning__field_toggle-item--cage" data-style="cage"></div>
                                            </div>
                                            <div class="traning__field">
                                                <textarea> </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="traning__sublist">
                                <div class="traning__sublist_row">
                                    <div class="traning__sublist_name">Задача №<?= $trainingExercise->variant->exercise->id ?> <?= $trainingExercise->variant->exercise->name ?></div>
                                </div>
                                <div class="traning__sublist_row">
                                    <div class="traning__sublist_name">Тема:</div>
                                    <div class="traning__sublist_text"><?=$trainingExercise->variant->theme->getFullName()?></div>
                                </div>
                                <div class="traning__sublist_row">
                                    <div class="traning__sublist_name">Сложность:</div>
                                    <div class="traning__sublist_text"><?=$trainingExercise->variant->complexity?></div>
                                </div>
                                <?php $rating = $trainingExercise->variant->exercise->getRate()->where(['user_id'=> Yii::$app->user->identity->id])->one(); ?>
                                <?php if(!$rating): ?>
                                    <br>
                                    <button id="rate-btn-<?= $trainingExercise->variant->exercise->id ?>" class="button button__round button__green"
                                            onclick="
                                                    $('.rate-<?= $trainingExercise->variant->exercise->id ?>').show();
                                                    $('#close-rate-<?= $trainingExercise->variant->exercise->id ?>').show();
                                                    $(this).hide()">Оценить задачу</button>
                                <?php endif; ?>

                                <div class="traning__sublist_row rate-<?= $trainingExercise->variant->exercise->id; ?>" <?= (($rating)?'':'style="display:none;"') ?>>
                                    <div class="traning__sublist_name">Оцененная сложность задачи: <?= ($rating)?$rating->rating.'/10':'' ?> &nbsp</div>
                                    <div class="traning__sublist_text">
                                        <div class="traning__rate">
                                            <?php for ($n=1; $n <= 10; $n++): ?>
                                                <?= Html::a('<i class="fa fa-star'.(($rating && $n <= $rating->rating) ? '' : '-o').'"></i>', Url::to([
                                                    '/exercise/rate/add',
                                                    'exercise_id' => $trainingExercise->variant->exercise->id,
                                                    'class' => $training->user->profile->state_id,
                                                    'rating' => $n,
                                                    'training_id' => $training->id,
                                                ])) ?>
                                            <?php endfor; ?>
                                        </div>
                                    </div>
                                    <br>
                                    <button class="button button__round button__red btn-close-rate" id="close-rate-<?= $trainingExercise->variant->exercise->id ?>"
                                            onclick="
                                                    $('.rate-<?= $trainingExercise->variant->exercise->id ?>').hide();
                                                    $('#rate-btn-<?= $trainingExercise->variant->exercise->id ?>').show();
                                                    $(this).hide()">Скрыть</button>
                                </div>
                            </div>
                        </div>
                        <?php $i++ ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-sm fade" id="ReplaceModal">
    <div class="modal-dialog">
        <div class="modal-content"><a class="modal-close" href="#" data-dismiss="modal"></a>
            <div class="modal__promocode_wrap">
                <div class="modal-header">Заменить задачу
                    <div class="modal-header__sub"> Вы легко можете заменить непонравившуюся Вам задачу на другую.</div>
                </div>
            </div>
            <div class="modal-body">
                <?= Html::beginForm(['/training/training-exercise/replace', 'id' => $trainingExercise->id]) ?>
                <div class="form">
                    <div class="form__row">
                        <div class="form__label">Из того же раздела</div>
                        <div class="form__control">
                            <input type="radio" name="theme" value="section" checked/>
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__label">Из той же темы</div>
                        <div class="form__control">
                            <input type="radio" name="theme" value="theme"/>
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__label">Из той же подтемы</div>
                        <div class="form__control">
                            <input type="radio" name="theme" value="subtheme"/>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="form__row">
                        <div class="form__label">Такого же типа </div>
                        <div class="form__control">
                            <input type="checkbox" name="type" id="type"/>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="form__row">
                        <div class="form__label">Посложнее</div>
                        <div class="form__control">
                            <input type="radio" name="complexity" value="up" checked/>
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__label">Попроще</div>
                        <div class="form__control">
                            <input type="radio" name="complexity" value="down"/>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" id="SubmitBtn" class="button">Заменить</button>
                    </div>
                </div>
                <?= Html::endForm() ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade training-setup" id="trainingSetup" data-backdrop="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                <h4 class="modal-title">Настройка тренинга</h4>
            </div>
            <div class="modal-body replace_item_modal_body">
                <ul>
                    <li><label><input type="checkbox" id="showHint"> Показать подсказки</label></li>
                    <li><label><input type="checkbox" id="showDecision"> Показать решения</label></li>
                    <li><label><input type="checkbox" id="showAnswer"> Показать ответы</label></li>
                    <li><label><input type="checkbox" id="shuffleExercises"> Перемешать задачи</label></li>
                    <li><label><input type="checkbox" id="addAreas"> Добавить области для решения</label></li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" id="setupBtn" class="btn btn-white" data-dismiss="modal">Ок</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-sm fade" id="create-note">
    <div class="modal-dialog">
        <div class="modal-content"><a class="modal-close" href="#" data-dismiss="modal"></a>
            <div class="modal__promocode_wrap">
                <div class="modal-header"><?= $training->note ? 'Редактировать заметку' : 'Создать заметку' ?>
                    <div class="modal-header__sub"> Вы легко можете создать и отредактировать заметку для текущей тренировки.</div>
                </div>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin(['action' => '/training/default/note']) ?>
                <?= $form->field($training, 'id')->hiddenInput()->label(false) ?>
                <div id="view-note" <?= $training->note ? '' : 'style="display:none"' ?>>
                    <?= $training->note ?>
                </div>
                <div class="form" id="edit-note" <?= $training->note ? 'style="display:none"' : '' ?>>
                    <div class="form__row">
                        <?= $form->field($training, 'note')->widget(CKEditor::className(), [
                            'options' => ['rows' => 10],
                            'preset' => 'custom',
                            'clientOptions' => [
                                'toolbarGroups' => [
                                    ['name' => 'basicstyles', 'groups' => ['undo', 'basicstyles', 'align', 'list', 'indent', 'colors']],
                                ],
                            ]
                        ])->label(false) ?>
                    </div>
                    <div class="form__row">
                        <div class="text-center">
                            <button type="submit" id="save-btn" class="button" <?= $training->note ? 'style="display:none"' : '' ?>>Сохранить</button>
                            <button type="button" id="delete-btn" class="button button__red">Удалить</button>
                        </div>
                    </div>
                </div>
                <div class="text-center" <?= $training->note ? '' : 'style="display:none"' ?>>
                    <br>
                    <button type="button" id="edit-btn" class="button button__blue">Редактировать</button>
                </div>

                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJsFile('/js/html2canvas.js') ?>
<?php $this->registerJsFile('/js/FileSaver.min.js') ?>
<?php $this->registerJsFile('/js/canvas-to-blob.min.js') ?>
