<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Exercise;
use common\models\ExerciseComplexity;
use common\models\Theme;
use kartik\tree\TreeViewInput;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\web\JsExpression;
use yii\helpers\Url;

?>

<table class="table">
    <thead>
    <tr>
        <th>Задача</th>
        <th>Тема</th>
        <th>Действие</th>
        <th>Порядок</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td><?= $exercise['name']?></td>
            <?php $form = ActiveForm::begin() ?>
            <td>
                <div class="modal-body">
                    <div>
                        <?= $form->field($trainingBuyVsExerciseVariant, 'exercise_variant_id')->dropDownList(ArrayHelper::map($themes,'exercise_id','name'))->label(false); ?>
                    </div>
                </div>
            </td>
            <td><?= $form->field($trainingBuyVsExerciseVariant, 'order_by')
                    ->label(false)
                    ->textInput(['type' => 'number', 'placeholder' => 'Порядковый номер']) ?></td>
            <td><?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary col-xs-12']) ?></td>

            <?php $form->end() ?>
        </tr>
    </tbody>
</table>






