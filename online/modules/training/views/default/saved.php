<?php

use kartik\alert\AlertBlock;
use yii\bootstrap\Html;

$this->title = 'Сохраненные тренировки';
?>

<div class="crm__container">
    <div class="crm__content">
        <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
            <?= AlertBlock::widget([
                'delay' => false
            ]) ?>
        <?php endif; ?>
        <div class="wihite__box shadow">
            <div class="page__header_title">Сохраненные тренировки</div>
            <div class="event__content">
                <?php if (count($trainings)): ?>
                    <?php foreach ($trainings as $training) : ?>
                        <div class="table__row">
                            <div class="table__row_item">
                                <?= Html::a($training->name, ['/training/default/run', 'id' => $training->id]) ?>
                            </div>
                            <div class="table__row_item">
                                <div class="button__group">
                                    <?php if (Yii::$app->user->can('teacher')): ?>
                                        <?= Html::a('Переслать в группу', ['training-opts', 'id' => $training->id], ['class' => 'button button__green select-group']); ?>
                                    <?php endif; ?>
                                    <?= Html::a('Открыть', ['/training/default/run', 'id' => $training->id], ['class' => 'button button__green']); ?>
                                    <?= Html::a('<i class="fa fa-trash-o"></i>', ['/training/default/delete', 'id' => $training->id], [
                                        'data' => ['confirm' => 'Вы уверены?', 'method' => 'post'], 'class' => 'button button__red button__icon']) ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <p>Вы пока не создали ни одной тренировки</p>
                <?php endif ?>
            </div>
            <p>
                <?= Html::a('Создать тренировку', ['/training/default/choose-class-subject'], ['class' => 'btn btn-primary']) ?>
            </p>
        </div>
        <?php if (count($trainings)): ?>
            <div class="wihite__box shadow">
                <div class="page__header_title">Полученные тренировки</div>
                <div class="event__content">
                    <?php foreach ($received_trainings as $training) : ?>
                        <div class="table__row">
                            <div class="table__row_item">
                                <?= Html::a($training->name, ['/training/default/run', 'id' => $training->id]) ?>
                            </div>
                            <div class="table__row_item">
                                <div class="button__group">
                                    <?php if (Yii::$app->user->can('teacher')): ?>
                                        <?= Html::a('Переслать в группу', ['training-opts', 'id' => $training->id], ['class' => 'button button__green select-group']); ?>
                                    <?php endif; ?>
                                    <?= Html::a('Открыть', ['/training/default/run', 'id' => $training->id], ['class' => 'button button__green']); ?>
                                    <?= Html::a('<i class="fa fa-trash-o"></i>', ['/training/default/delete', 'id' => $training->id], [
                                        'data' => ['confirm' => 'Вы уверены?', 'method' => 'post'], 'class' => 'button button__red button__icon']) ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
