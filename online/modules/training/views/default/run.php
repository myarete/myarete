<?php

use common\components\other\History;
use dosamigos\ckeditor\CKEditor;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

History::put(Yii::$app->user->identity->email, $training->name);

$this->title = $training->name;
$this->registerJsFile('/js/html2canvas.js');
$this->registerJsFile('/js/FileSaver.min.js');
$this->registerJsFile('/js/canvas-to-blob.min.js');

$js = <<<JS
    function saveOrder() {
        var data = [];
        $.each($('#sortable .traning__list_item'), function (i, e) {
            if($(e).attr('id') !== undefined) {
                data.push($(e).attr('id'));
            }
        });
        $.post('/training/default/save-order', {
            training_id: :training_id,
            data: data
        });
    }

    $("#success-settings").click(function (e) {
        e.preventDefault();
        var form = $("#settings_form");
        var data = form.serializeArray();
        var hint = data[0]['value'];
        var solution = data[1]['value'];
        var answers = data[2]['value'];
        var field = data[3]['value'];

        if (hint === 'close') {
            $(".hint_block").removeClass('in');
            $(".hint_block").hide();
            $(".close_hint").hide();
            $(".open_hint").show();
        } else {
            $(".hint_block").addClass('in');
            $(".hint_block").show();
            $(".open_hint").hide();
            $(".close_hint").show();
        }

        if (solution === 'close') {
            $(".decision_block").removeClass('in');
            $(".decision_block").hide();
            $(".close_decision").hide();
            $(".open_decision").show();
        } else {
            $(".decision_block").addClass('in');
            $(".decision_block").show();
            $(".open_decision").hide();
            $(".close_decision").show();
        }

        if (answers === 'close') {
            $(".answers_block").removeClass('in');
            $(".answers_block").hide();
            $(".close_answers").hide();
            $(".open_answers").show();
        } else {
            $(".answers_block").addClass('in');
            $(".answers_block").show();
            $(".open_answers").hide();
            $(".close_answers").show();
        }

        var fieldDiv = $(".field_div");
        var textarea = $(".traning__field");

        switch (field) {
            case 'close':
                fieldDiv.hide();
                $(".field_open").show();
                $(".field_close").hide();
                break;
            case 'cell':
                fieldDiv.show();
                textarea.removeClass('traning__field--blank');
                textarea.removeClass('traning__field--line');
                textarea.removeClass('traning__field--cage');
                textarea.addClass('traning__field--cage');
                $(".field_open").hide();
                $(".field_close").show();
                break;
            case 'white':
                fieldDiv.show();
                textarea.removeClass('traning__field--blank');
                textarea.removeClass('traning__field--line');
                textarea.removeClass('traning__field--cage');
                textarea.addClass('traning__field--blank');
                $(".field_open").hide();
                $(".field_close").show();
                break;
            case 'line':
                fieldDiv.show();
                textarea.removeClass('traning__field--blank');
                textarea.removeClass('traning__field--line');
                textarea.removeClass('traning__field--cage');
                textarea.addClass('traning__field--line');
                $(".field_open").hide();
                $(".field_close").show();
                break;
        }
        $('#settings-modal').modal('hide');
    });

    $('#screenshot').click(function () {
        $('.traning__top').hide();
        $('.traning__list_action').hide();
        
        var div = $('#white-box')[0];
        var rect = div.getBoundingClientRect();
        var canvas = document.createElement('canvas');
        canvas.width = rect.width;
        canvas.height = rect.height;
        var ctx = canvas.getContext('2d');
        ctx.translate(-rect.left, -rect.top);
        
        html2canvas(div, {
            canvas: canvas,
            height: rect.height,
            width: rect.width,
            onrendered: function (canvas) {
                canvas.toBlob(function (blob) {
                    saveAs(blob, ":training_name.jpg");
                }, "image/jpg");
            }
        });

        $('.traning__top').show();
        $('.traning__list_action').show();
    });
    
    $('#random').on('click', function() {
        $.post('/training/default/shuffle-exercises', {id: :training_id}, function f() {
            location.reload();
        });
    });

    $('#printscreen').click(function () {
        var content = $('.traning__list').html();

        $.post('/training/default/print-screen', {content: content}, function (resp) {
            if (resp != '') location.href = '/training/default/print-screen?id=' + :training_id + '&tmpfile=' + resp;
        });
    });

    $('body').on('click', '.replace-btn', function () {
        $('#ReplaceModal').modal();
        $('#ReplaceModal form').attr('action', '/training/training-exercise/replace?id=' + $(this).data('id'))
    });

    $('#ReplaceModal').keypress(function (ev) {
        if (ev.keyCode == 13)
            $('#SubmitBtn').click();
    });

    $('#sortable').sortable({
        update: function () {
            saveOrder();
        }
    });

    $('#showSetup').click(function () {
        $('#trainingSetup').modal();
    });

    $('#setupBtn').click(function () {
        if ($('#showHint').is(':checked')) {
            $('a[href ^= #hint-]').removeClass('collapsed').attr('aria-expanded', true);
            $('div[id ^= hint-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
        } else {
            $('a[href ^= #hint-]').addClass('collapsed').attr('aria-expanded', false);
            $('div[id ^= hint-]').removeClass('in').attr('aria-expanded', false);
        }
        if ($('#showDecision').is(':checked')) {
            $('a[href ^= #decision-]').removeClass('collapsed').attr('aria-expanded', true);
            $('div[id ^= decision-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
        } else {
            $('a[href ^= #decision-]').addClass('collapsed').attr('aria-expanded', false);
            $('div[id ^= decision-]').removeClass('in').attr('aria-expanded', false);
        }
        if ($('#showAnswer').is(':checked')) {
            $('a[href ^= #answer-]').removeClass('collapsed').attr('aria-expanded', true);
            $('div[id ^= answer-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
        } else {
            $('a[href ^= #answer-]').addClass('collapsed').attr('aria-expanded', false);
            $('div[id ^= answer-]').removeClass('in').attr('aria-expanded', false);
        }
        if ($('#addAreas').is(':checked')) {
            $('a[href ^= #solutions-]').removeClass('collapsed').attr('aria-expanded', true);
            $('div[id ^= solutions-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
        } else {
            $('a[href ^= #solutions-]').addClass('collapsed').attr('aria-expanded', false);
            $('div[id ^= solutions-]').removeClass('in').attr('aria-expanded', false);
        }
        if ($('#shuffleExercises').is(':checked')) {
            $.post('/training/default/shuffle-exercises', {id: :training_id});
            setTimeout(function () {
                location.reload();
            }, 500);
        }
    });

    $('.bground-btn').click(function () {
        if ($(this).hasClass('clean-background')) {
            $(this).closest('.row').find('textarea').css('background-image', 'none');
            $(this).closest('.row').find('textarea').css('background-color', '#fff');
        } else if ($(this).hasClass('cage-background')) {
            $(this).closest('.row').find('textarea').css('background-image', 'url(/../images/cage.jpg)');
        } else if ($(this).hasClass('linear-background')) {
            $(this).closest('.row').find('textarea').css('background-image', 'url(/../images/linear.png)');
        }
    });

    $('#similar-training').click(function () {
        yii.prompt('Название тренировки', function (name) {
            var id = :training_id;
            $.post('/training/default/create-similar', {id: id, name: name}, function (response) {
                location.href = '/training/default/run/' + response;
            });
        }, $('.traning__title').text() + ' - Копия');
    });

    $('#edit-btn').click(function () {
        $('#view-note').hide();
        $('#edit-note').show();
        $('#save-btn').show();
        $(this).hide();
    });

    $('#delete-btn').click(function () {
        var id = $(this).closest('form').find('input[type=hidden]').val();
        $.post('/training/default/delete-note', {id: id}, function (response) {
            location.reload();
        });
    });
JS;
$js = str_replace(':training_id', $training->id, $js);
$js = str_replace(':training_name', $training->name, $js);
$this->registerJs($js);
?>
<style>
    a.traning__link {
        color: #00c15a;
        text-decoration: underline;
        font-size: 18px;
    }
</style>
<div class="crm__container">
    <div class="crm__content">
        <div id="white-box" class="wihite__box shadow">
            <div class="traning">
                <div class="traning__title"><?= $training->name ?></div>
                <div class="traning__top">
                    <a class="button button__blue button__question" data-toggle="modal"
                       data-target="#questions_and_answers__modal">Вопросы и ответы
                        <div class="fa fa-question"></div>
                    </a>
                    <div class="pull-right">
                        <?php if (isset($params) && $training->user_id == Yii::$app->user->id): ?>
                            <?= Html::a('Посмотреть полную версию', ['/training/default/run', 'id' => $training->id], ['class' => 'button button__round button__green']) ?>
                        <?php endif ?>
                        <?= Html::button($training->note ? 'Посмотреть заметку' : 'Создать заметку', [
                            'class' => 'button button__round button__green',
                            'data' => [
                                'toggle' => 'modal',
                                'target' => '#create-note',
                                'title' => $training->note ? 'Посмотреть заметку' : 'Создать заметку'
                            ],
                        ]) ?>
                        <?php
                        Modal::begin([
                            'id' => 'settings-modal',
                            'header' => '<h2>Управление</h2>',
                            'toggleButton' => [
                                'tag' => 'button',
                                'class' => 'button button__round button__blue',
                                'label' => 'Управление',
                                'data-title' => 'Управление тренировкой'
                            ],
                            'options' => [
                                'class' => 'modal-sm',
                            ]
                        ]); ?>
                        <form action="" id="settings_form">
                            <div class="form">
                                <div class="form__row">
                                    <div class="form__control">
                                        <div class="form__label" style="font-weight: 700;">Подсказки:</div>
                                        <div class="form__label">
                                            <input style="margin: 0;" type="radio" id="hint-close" name="hint"
                                                   value="close"
                                                   checked/>
                                            <label for="hint-close" style="font-weight: 400;">Скрыть все </label>
                                        </div>
                                        <div class="form__label">
                                            <input style="margin: 0;" type="radio" id="hint-open" name="hint"
                                                   value="open"/>
                                            <label for="hint-open" style="font-weight: 400;">Показать все </label>
                                        </div>

                                    </div>
                                    <hr>
                                    <div class="form__control">
                                        <div class="form__label" style="font-weight: 700;">Решения:</div>
                                        <div class="form__label">
                                            <input style="margin: 0;" type="radio" id="solution-close" name="solution"
                                                   value="close" checked/>
                                            <label for="solution-close" style="font-weight: 400;">Скрыть все </label>
                                        </div>
                                        <div class="form__label">
                                            <input style="margin: 0;" type="radio" id="solution-open" name="solution"
                                                   value="open"/>
                                            <label for="solution-open" style="font-weight: 400;">Показать все </label>
                                        </div>

                                    </div>
                                    <hr>
                                    <div class="form__control">
                                        <div class="form__label" style="font-weight: 700;">Ответы:</div>
                                        <div class="form__label">
                                            <input style="margin: 0;" type="radio" id="answers-close" name="answers"
                                                   value="close" checked/>
                                            <label for="answers-close" style="font-weight: 400;">Скрыть все </label>
                                        </div>
                                        <div class="form__label">
                                            <input style="margin: 0;" type="radio" id="answers-open" name="answers"
                                                   value="open"/>
                                            <label for="answers-open" style="font-weight: 400;">Показать все </label>
                                        </div>

                                    </div>
                                    <hr>
                                    <div class="form__control">
                                        <div class="form__label" style="font-weight: 700;">Пространство для<br>решения
                                            ко
                                            всем задачам:
                                        </div>
                                        <div class="form__label">
                                            <input style="margin: 0;" type="radio" id="field-close" name="field"
                                                   value="close" checked/>
                                            <label for="field-close" style="font-weight: 400;">Нет </label>
                                        </div>
                                        <div class="form__label">
                                            <input style="margin: 0;" type="radio" id="field-white" name="field"
                                                   value="white"/>
                                            <label for="field-white" style="font-weight: 400;">Белое</label>
                                        </div>
                                        <div class="form__label">
                                            <input style="margin: 0;" type="radio" id="field-cell" name="field"
                                                   value="cell"/>
                                            <label for="field-cell" style="font-weight: 400;">Клетка</label>
                                        </div>
                                        <div class="form__label">
                                            <input style="margin: 0;" type="radio" id="field-line" name="field"
                                                   value="line"/>
                                            <label for="field-line" style="font-weight: 400;">Линейка</label>
                                        </div>
                                    </div>
                                    <br><br><br>
                                    <div class="text-center">
                                        <button class="btn btn-success" id="success-settings">Применить</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <?php
                        Modal::end();
                        ?>

                        <div class="button__group_flip" style="margin-left: 10px">
                            <?= Html::button('<div class="fa fa-copy"></div>', [
                                'id' => 'similar-training',
                                'class' => 'button button__round button__yellow button__icon',
                                'data-title' => 'Создать аналогичную тренировку'
                            ]) ?>

                            <?php if (Yii::$app->user->can('user')): ?>
                                <?= Html::button('<div class="fa fa-camera"></div>', [
                                    'id' => 'screenshot',
                                    'class' => 'button button__round button__blue button__icon',
                                    'data-title' => 'Сделать скриншот'
                                ]) ?>
                            <?php endif; ?>

                            <?= Html::button('<div class="fa fa-random"></div>', [
                                'id' => 'random',
                                'class' => 'button button__round button__brown button__icon',
                                'data-title' => 'Перемешать задачи'
                            ]) ?>

                            <?= Html::button('<div class="fa fa-print"></div>', [
                                'id' => 'printscreen',
                                'class' => 'button button__round button__green button__icon',
                                'data-title' => 'Версия для печати'
                            ]) ?>
                        </div>

                    </div>
                </div>
                <div class="traning__list" id="sortable">
                    <?php foreach ($training->trainingExercises as $i => $trainingExercise): ?>
                        <?php if (isset($params[$trainingExercise->variant->exercise->id]) && in_array('exercise', $params[$trainingExercise->variant->exercise->id])) {
                            continue;
                        }
                        ?>
                        <div class="traning__list_item" id="id_<?= $trainingExercise->id ?>">
                            <div class="traning__list_count"><?= $i + 1 ?> </div>
                            <div class="traning__list_action">
                                <a class="button button__round button__green traning__btn_more" href="#"
                                   data-title="Информация о задаче">
                                    <span class="traning__btn_more--show">Подробнее </span>
                                    <span class="traning__btn_more--hide">Скрыть </span>
                                </a>

                                <?= Html::a(
                                    '<div class="fa ' . (($trainingExercise->variant->exerciseFavorite) ? 'fa-star' : 'fa-star-o') . '"></div>',
                                    [(($trainingExercise->variant->exerciseFavorite) ? '/exercise/favorite/delete' : '/exercise/favorite/add'),
                                        'id' => (($trainingExercise->variant->exerciseFavorite) ? $trainingExercise->variant->exerciseFavorite->id : $trainingExercise->exercise_variant_id)],
                                    [
                                        'class' => 'button button__round button__icon ' . (($trainingExercise->variant->exerciseFavorite) ? 'button__red' : 'button__blue'),
                                        'data-title' => 'Добавить в/убрать из избранного'
                                    ]
                                ) ?>

                                <?php
                                if (!isset($params))
                                    echo Html::a(
                                        '<div class="fa fa-refresh"></div>',
                                        ['/training/training-exercise/replace-variants', 'id' => $trainingExercise->id],
                                        [
                                            'class' => 'button button__round button__yellow button__icon replace-btn',
                                            'onclick' => 'return false',
                                            'data' => [
                                                'id' => $trainingExercise->id,
                                                'title' => 'Заменить задачу на похожую'
                                            ]
                                        ]
                                    );
                                ?>
                                <?php
                                if (!isset($params))
                                    echo Html::a(
                                        '<div class="fa fa-trash"></div>',
                                        ['/training/training-exercise/delete', 'id' => $trainingExercise->id],
                                        [
                                            'class' => 'button button__round button__icon button__red',
                                            'data' => [
                                                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                                'title' => 'Удалить задачу из тренировки'
                                            ]
                                        ]
                                    );
                                ?>
                            </div>
                            <div class="traning__list_center" id="exercise-info-<?= $i ?>" class="collapse"
                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                <div class="traning__list_row">
                                    <div class="traning__task">
                                        <p><?= $trainingExercise->variant->exercise->question ?></p>
                                    </div>
                                </div>
                                <?php if (!empty($trainingExercise->variant->exercise->hint)): ?>
                                    <div class="traning__list_row">
                                        <a class="traning__link hint_href"
                                           href="#<?= "hint-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                           data-traning-link="<?= "hint-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                           data-toggle="collapse">
                                            <span class="traning__link--show open_hint">Показать подсказку</span>
                                            <span class="traning__link--hide close_hint">Скрыть подсказку</span>
                                        </a>
                                        <div class="traning__link_slide collapse hint_block"
                                             id="<?= "hint-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                             data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                            <p><?= $trainingExercise->variant->exercise->hint ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($trainingExercise->variant->exercise->hint_second)): ?>
                                    <div class="traning__list_row">
                                        <a class="traning__link"
                                           href="#<?= "hint_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                           data-traning-link="<?= "hint_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                           data-toggle="collapse">
                                            <span class="traning__link--show">Показать вторую подсказку</span>
                                            <span class="traning__link--hide">Скрыть вторую подсказку</span>
                                        </a>
                                        <div class="traning__link_slide collapse"
                                             id="<?= "hint_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                             data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                            <p><?= $trainingExercise->variant->exercise->hint_second ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($trainingExercise->variant->exercise->hint_third)): ?>
                                    <div class="traning__list_row">
                                        <a class="traning__link"
                                           href="#<?= "hint_third-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                           data-traning-link="<?= "hint_third-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                           data-toggle="collapse">
                                            <span class="traning__link--show">Показать третью подсказку</span>
                                            <span class="traning__link--hide">Скрыть третью подсказку</span>
                                        </a>
                                        <div class="traning__link_slide collapse"
                                             id="<?= "hint_third-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                             data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                            <p><?= $trainingExercise->variant->exercise->hint_third ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($trainingExercise->variant->exercise->decision)): ?>
                                    <div class="traning__list_row">
                                        <a class="traning__link"
                                           href="#<?= "decision-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                           data-traning-link="<?= "decision-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                           data-toggle="collapse">
                                            <span class="traning__link--show open_decision">Показать решение</span>
                                            <span class="traning__link--hide close_decision">Скрыть решение</span>
                                        </a>
                                        <div class="traning__link_slide collapse decision_block"
                                             id="<?= "decision-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                             data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                            <p><?= $trainingExercise->variant->exercise->decision ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($trainingExercise->variant->exercise->decision_detailed)): ?>
                                    <div class="traning__list_row">
                                        <a class="traning__link"
                                           href="#<?= "decision_detailed-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                           data-traning-link="<?= "decision_detailed-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                           data-toggle="collapse">
                                            <span class="traning__link--show">Показать подробное решение</span>
                                            <span class="traning__link--hide">Скрыть подробное решение</span>
                                        </a>
                                        <div class="traning__link_slide collapse"
                                             id="<?= "decision_detailed-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                             data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                            <p><?= $trainingExercise->variant->exercise->decision_detailed ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($trainingExercise->variant->exercise->decision_second)): ?>
                                    <div class="traning__list_row">
                                        <a class="traning__link"
                                           href="#<?= "decision_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                           data-traning-link="<?= "decision_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                           data-toggle="collapse">
                                            <span class="traning__link--show">Показать второе решение</span>
                                            <span class="traning__link--hide">Скрыть второе решение</span>
                                        </a>
                                        <div class="traning__link_slide collapse"
                                             id="<?= "decision_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                             data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                            <p><?= $trainingExercise->variant->exercise->decision_second ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if (!empty($trainingExercise->variant->exercise->answer)): ?>
                                    <div class="traning__list_row">
                                        <a class="traning__link"
                                           href="#<?= "answer-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                           data-traning-link="<?= "answer-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                           data-toggle="collapse">
                                            <span class="traning__link--show open_answers">Показать ответ</span>
                                            <span class="traning__link--hide close_answers">Скрыть ответ</span>
                                        </a>
                                        <div class="traning__link_slide collapse answers_block"
                                             id="<?= "answer-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                             data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                            <p><?= $trainingExercise->variant->exercise->answer ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <div class="traning__list_row"><a class="traning__link" href="#"
                                                                  data-traning-link="<?= "blank-{$i}-{$trainingExercise->variant->exercise->id}" ?>">
                                        <span
                                                class="traning__link--show field_open">Добавить пространство для решения</span><span
                                                class="traning__link--hide field_close">Скрыть пространство для решения</span></a>
                                    <div class="traning__link_slide field_div"
                                         id="<?= "blank-{$i}-{$trainingExercise->variant->exercise->id}" ?>">
                                        <div class="traning__field_wrap">
                                            <div class="traning__field_toggle">
                                                <div
                                                        class="traning__field_toggle-item traning__field_toggle-item--blank"
                                                        data-style="blank"></div>
                                                <div class="traning__field_toggle-item traning__field_toggle-item--line"
                                                     data-style="line"></div>
                                                <div class="traning__field_toggle-item traning__field_toggle-item--cage"
                                                     data-style="cage"></div>
                                            </div>
                                            <div class="traning__field">
                                                <textarea> </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="traning__sublist">
                                <div class="traning__sublist_row">
                                    <div class="traning__sublist_name">Задача
                                        №<?= $trainingExercise->variant->exercise->id ?> <?= $trainingExercise->variant->exercise->name ?></div>
                                </div>
                                <div class="traning__sublist_row">
                                    <div class="traning__sublist_name">Тема:</div>
                                    <div
                                            class="traning__sublist_text"><?= $trainingExercise->variant->theme->getFullName() ?></div>
                                </div>
                                <div class="traning__sublist_row">
                                    <div class="traning__sublist_name">Сложность:</div>
                                    <div class="traning__sublist_text"><?= $trainingExercise->variant->complexity ?></div>
                                </div>
                            </div>
                        </div>
                        <?php $i++ ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-sm fade" id="ReplaceModal">
    <div class="modal-dialog">
        <div class="modal-content"><a class="modal-close" href="#" data-dismiss="modal"></a>
            <div class="modal__promocode_wrap">
                <div class="modal-header">Заменить задачу
                    <div class="modal-header__sub"> Вы легко можете заменить непонравившуюся Вам задачу на другую.</div>
                </div>
            </div>
            <div class="modal-body">
                <?= Html::beginForm(['/training/training-exercise/replace', 'id' => $trainingExercise->id]) ?>
                <div class="form">
                    <div class="form__row">
                        <div class="form__label">Из того же раздела</div>
                        <div class="form__control">
                            <input type="radio" name="theme" value="section" checked/>
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__label">Из той же темы</div>
                        <div class="form__control">
                            <input type="radio" name="theme" value="theme"/>
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__label">Из той же подтемы</div>
                        <div class="form__control">
                            <input type="radio" name="theme" value="subtheme"/>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="form__row">
                        <div class="form__label">Такого же типа</div>
                        <div class="form__control">
                            <input type="checkbox" name="type" id="type"/>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="form__row">
                        <div class="form__label">Посложнее</div>
                        <div class="form__control">
                            <input type="radio" name="complexity" value="up"/>
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__label">Попроще</div>
                        <div class="form__control">
                            <input type="radio" name="complexity" value="down"/>
                        </div>
                    </div>
                    <div class="form__row">
                        <div class="form__label">Той же сложности</div>
                        <div class="form__control">
                            <input type="radio" name="complexity" value="equally" checked/>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" id="SubmitBtn" class="button">Заменить</button>
                    </div>
                </div>
                <?= Html::endForm() ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="use-training" data-backdrop="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Как пользоваться тренировками?</h4>
            </div>
            <div class="modal-body">
                <p><?= \common\models\Html::get('use_training') ?></p>
                <p><?= Html::a('Подробнее', ['/training/default/page', 'view' => 'use_training_detail']) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-sm fade" id="create-note">
    <div class="modal-dialog">
        <div class="modal-content"><a class="modal-close" href="#" data-dismiss="modal"></a>
            <div class="modal__promocode_wrap">
                <div class="modal-header"><?= $training->note ? 'Редактировать заметку' : 'Создать заметку' ?>
                    <div class="modal-header__sub"> Вы легко можете создать и отредактировать заметку для текущей
                        тренировки.
                    </div>
                </div>
            </div>
            <div class="modal-body note_modal">
                <?php $form = ActiveForm::begin(['action' => '/training/default/note']) ?>
                <?= $form->field($training, 'id')->hiddenInput()->label(false) ?>
                <div id="view-note" <?= $training->note ? '' : 'style="display:none"' ?>>
                    <?= $training->note ?>
                </div>
                <div class="form" id="edit-note" <?= $training->note ? 'style="display:none"' : '' ?>>
                    <div class="form__row">
                        <?= $form->field($training, 'note')->widget(CKEditor::className(), [
                            'options' => ['rows' => 10],
                            'preset' => 'custom',
                            'clientOptions' => [
                                'toolbarGroups' => [
                                    ['name' => 'basicstyles', 'groups' => ['undo', 'basicstyles', 'align', 'list', 'indent', 'colors']],
                                ],
                            ]
                        ])->label(false) ?>
                    </div>
                    <div class="form__row">
                        <div class="text-center">
                            <button type="submit" id="save-btn"
                                    class="button" <?= $training->note ? 'style="display:none"' : '' ?>>Сохранить
                            </button>
                            <button type="button" id="delete-btn" class="button button__red">Удалить</button>
                        </div>
                    </div>
                </div>
                <div class="text-center" <?= $training->note ? '' : 'style="display:none"' ?>>
                    <br>
                    <button type="button" id="edit-btn" class="button button__blue">Редактировать</button>
                </div>

                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>
