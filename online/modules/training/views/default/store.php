<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\alert\AlertBlock;
use common\models\UserVsTrainingCategory;
use common\models\UserVsTrainingBuy;
use yii\widgets\Breadcrumbs;
use common\models\TrainingCategory;

$path = '@web/upload/training/';
$this->title = 'Магазин тренировок';

if(!empty($childCategory)) {
    $this->params['breadcrumbs'][] = [
        'template' => "<li>{link}</li>\n",
        'label' => $this->title,
        'url' => ['/training/default/store']
    ];

    $rootCategory = TrainingCategory::find()->where(['id' => $category->root])->one();
    $parentCategory = TrainingCategory::find()->where(['id' => $category->pid])->one();

    if (!empty($rootCategory) && $rootCategory->id != $childCategory->id) {
        $this->params['breadcrumbs'][] = ['label' => $rootCategory->name, 'url' => ['/training/default/store?categoryId=' . $rootCategory->id]];
    }
    if (!empty($parentCategory) && $rootCategory->id != $parentCategory->id) {
        $this->params['breadcrumbs'][] = ['label' => $parentCategory->name, 'url' => ['/training/default/store?categoryId=' . $parentCategory->id]];
    }

    $this->params['breadcrumbs'][] = ['label' => $childCategory->name, 'url' => ['/training/default/store?categoryId=' . $childCategory->id]];
}
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="store">
            <?php if(!empty($childCategory)) :?>
                <div class="store__ban"  style="background-image: url(/img/store1.jpg)">
                    <div class="store__ban_title-wrap">
                        <div class="store__ban_title">
                            <div class="store__ban_title-text store__ban_title-text--bottom">
                                <div class="store__ban_title-left">
                                    <div class="store__ban_title-name"><?= $childCategory->name; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <div class="store__ban" style="background-image: url(/img/store1.jpg)">
                    <div class="store__ban_title-wrap">
                        <div class="store__ban_title">
                            <div class="store__ban_title-text">Готовьтесь к экзаменам<br>по тренировкам Арете Онлайн</div>
                        </div>
                    </div>
                </div>
                <!--div class="store__filter">
                    <div class="store__filter_btn button button__green">Найти уроки </div>
                    <div class="store__filter_search">
                        <input type="text" placeholder="Введите предмет или отдельную тему для поиска"/><i class="icon-search"> </i>
                    </div>
                    <div class="store__filter_bottom">
                        <div class="store__filter_bottom-label">Популярное: </div>
                        <div class="store__filter_bottom-tags"> <a class="button button__tag" href="#">Английский язык </a><a class="button button__tag" href="#">Китайский язык </a><a class="button button__tag" href="#">Математика</a><a class="button button__tag" href="#">ЕГЭ</a><a class="button button__tag" href="#">ОГЭ </a></div>
                    </div>
                </div-->
            <?php endif; ?>

            <div class="event__content">
                <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
                    <?= AlertBlock::widget([
                        'delay' => false
                    ]) ?>
                <?php endif; ?>

                <style>
                    .alert-success {
                        color: #222
                    }
                    .action-price-text {
                        margin-left:10px; font-size:24px; color:#4785fc
                    }
                </style>

                <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], ]);?>

                <!-- begin grid__cube-->
                <?php if(!empty($categories)) : ?>
                    <div class="grid grid__cube" data-grid-match="">
                        <?php foreach($categories as $category):
                            if ($category->active!=1): continue; endif;?>
                            <div class="grid__wrapper">
                                <div class="grid__item">
                                    <?php if(!empty($category->getMainImage($path))): ?>
                                        <div class="grid__cube_img"> <a href="<?= Url::to(['/training/default/store', 'categoryId' => $category->id]) ?>"><img width="70" src="<?= $category->getMainImage($path) ?>"></a></div>
                                    <?php endif; ?>
                                    <div class="grid__cube_name"><?= $category->name ?></div>
                                    <a href="<?= Url::to(['/training/default/store', 'categoryId' => $category->id]) ?>" class="button button__green">Подробнее</a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <?php if(empty($categories) && empty($dataProvider->models)): ?>
                    <div class="wihite__box shadow">
                        <div class="tab-content">
                            <div class="tab-content__item event__content show" id="tab1">
                                <p>В настоящее время эта категория пуста. Мы ведем активную работу по созданию контента. Очень скоро здесь появятся тренировки.</p>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <!-- end grid__cube-->

                <?php if(!empty($dataProvider->models) && !empty($categoryId)) : ?>
                    <div class="wihite__box shadow">
                        <div class="tab-content">

                            <div class="tab-content__item event__content show" id="tab1">
                                <?php $this->title = $category->name ?>
                                <?php foreach($dataProvider->models as $i => $model) : ?>
                                    <div class="table__row">
                                        <div class="table__row_item">
                                            <?= Html::a($model->name, ['/training/default/view', 'id' => $model->id]) ?>
                                        </div>
                                        <?php if (UserVsTrainingBuy::findOne(['user_id' => Yii::$app->user->id, 'training_id' => $model->id]) || UserVsTrainingCategory::findOne(['user_id' => Yii::$app->user->id, 'training_category_id' => $category->id])):?>
                                            <div class="table__row_item">Добавленно</div>
                                        <?php else:?>
                                            <?php if ($model->is_action == 0):?>
                                                <?php if ($model->price == 0):?>
                                                    <div class="table__row_item"><span style="margin-right:30px">Бесплатно</span>
                                                        <?= Html::a('Добавить', ['/training/default/add-training', 'training_id' => $model->id, 'training_price' => $model->price], [
                                                            'data' => ['confirm' => 'Вы уверены?', 'method' => 'post'], 'class' => 'button button__green']); ?>
                                                    </div>
                                                <?php else:?>
                                                    <div class="table__row_item"><span style="margin-right:30px"><?= $model->price ?> руб.</span>
                                                        <?= Html::a('Купить', ['/training/default/buy-training', 'training_id' => $model->id, 'training_price' => $model->price], [
                                                            'data' => ['confirm' => 'С вашего Арете Кошелька будет списано ' . $model->price . ' рублей. Вы уверены?', 'method' => 'post'], 'class' => 'button button__green']); ?>
                                                    </div>
                                                <?php endif; ?>
                                            <?php else:?>
                                                <?php if ($model->action_price == 0):?>
                                                    <div class="table__row_item"><span><s><?= $model->price ?> руб.</s><span class="action-price-text">Бесплатно</span></span>
                                                        <?= Html::a('Добавить', ['/training/default/add-training', 'training_id' => $model->id, 'training_price' => $model->action_price], [
                                                            'data' => ['confirm' => 'Вы уверены?', 'method' => 'post'], 'class' => 'button button__green']); ?>
                                                    </div>
                                                <?php else:?>
                                                    <div class="table__row_item"><span><s><?= $model->price ?>  руб.</s><span class="action-price-text"><?= $model->action_price ?> руб.</span></span>
                                                        <?= Html::a('Купить', ['/training/default/buy-training', 'training_id' => $model->id, 'training_price' => $model->action_price], [
                                                            'data' => ['confirm' => 'С вашего Арете Кошелька будет списано ' . $model->action_price . ' рублей. Вы уверены?', 'method' => 'post'], 'class' => 'button button__green']); ?>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                <?php else: ?>

                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
