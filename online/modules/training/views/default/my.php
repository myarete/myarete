<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\UserVsTrainingCategory;

$path = '@web/upload/training/';
$this->title = 'Купленные тренировки';
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="event__content">
                <div class="page__header_title">Купленные тренировки</div>
                <b>Сортировать:</b> <?= $sort->link('name') . ' | ' . $sort->link('id');?> <br><br>
                <?php foreach($trainingsFromCategories as $training) : ?>
                    <div class="table__row">
                        <div class="table__row_item">
                            <?= Html::a($training->name, ['/training/default/view', 'id' => $training->id]) ?>
                        </div>
                        <div class="table__row_item"><?= Html::a('Открыть', ['/training/default/view', 'id' => $training->id], ['class' => 'button button__green']); ?></div>
                    </div>
                <?php endforeach; ?>
                <?php foreach($trainings as $training) : ?>
                    <div class="table__row">
                        <div class="table__row_item">
                            <?= Html::a($training->name, ['/training/default/view', 'id' => $training->id]) ?>
                        </div>
                        <div class="table__row_item"><?= Html::a('Открыть', ['/training/default/view', 'id' => $training->id], ['class' => 'button button__green']); ?></div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>