<?php
use common\models\Exercise;
use common\models\ExerciseComplexity;
use common\models\Theme;
use kartik\tree\TreeViewInput;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\Html;


$url_exercise = Url::to(['exercise-list']);
$url_variant = Url::to(['variant-list'])
?>

<div class="modal inmodal" id="add-variant-modal">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Добавить задачу</h4>
            </div>

            <?php $form = ActiveForm::begin(['action' => '/training/default/add-exercise?training_id='.$training_id]) ?>

                <div class="modal-body">

                    <div>
                        <?php
                        echo $form->field($variantModel, 'exercise_id')->widget(Select2::classname(), [
                            'initValueText' => '', // set the initial display text
                            'options' => ['placeholder' => 'Выберите задачу', 'id'=>'cat-id'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Пожалуйста, подождите...'; }"),
                                ],
                                'ajax' => [
                                    'url' => $url_exercise,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(exercise) { return exercise.id+" - "+exercise.text; }'),
                                'templateSelection' => new JsExpression('function (exercise) { return exercise.text; }'),
                            ],
                        ]);?>
                    </div>
                    <div>
                        <?php
                        echo $form->field($variantModel, 'theme_id')->widget(DepDrop::classname(), [
                                'options'=>['id'=>'subcat-id'],
                                'pluginOptions'=>[
                                    'depends'=>['cat-id'],
                                    'placeholder'=>'Выберите вариант...',
                                    'url'=>$url_variant
                                ]
                            ]);
                        ?>
                    </div>

                </div>
                <div class="modal-footer">
                    <?= Html::submitButton('Добавить задачу', ['class' => 'btn btn-primary col-xs-12']) ?>
                </div>

            <?php $form->end() ?>

        </div>
    </div>
</div>

<style>
    .select2-container {
        z-index:9999;
    }
</style>