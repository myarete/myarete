<?php
use yii\helpers\Html;
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
            <div class="side-block">
                <h2><?=$theme->subject->name?> - <?=$theme->class?> класс</h2>

                <hr>

                <form method="post" id="create-training-form">

                    <?php if (count($sections)): ?>

                        <?= Html::errorSummary($model, ['class' => 'error-summary']) ?>

                        <?= Html::activeHiddenInput($model, 'name', ['id' => 'training-name']) ?>

                        <ul class="section-list">

                            <?php foreach ($sections as $section): ?>

                                <li>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <span class="section-name name"><?= $section->name ?></span>
                                        </div>
                                        <div class="col-xs-6">
                                            <?= Html::activeTextInput(
                                                $model,
                                                "quantity[$section->id]",
                                                ['class' => 'form-control', 'placeholder' => 'Кол-во']
                                            ) ?>
                                        </div>
                                    </div>

                                    <ul class="theme-list">

                                        <?php foreach ($section->children(1)->all() as $theme): ?>

                                            <li>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <span class="theme-name name"><?= $theme->name ?></span>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <?= Html::activeTextInput(
                                                            $model,
                                                            "quantity[$theme->id]",
                                                            ['class' => 'form-control', 'placeholder' => 'Кол-во']
                                                        ) ?>
                                                    </div>
                                                </div>

                                                <ul class="subtheme-list">


                                                    <?php foreach ($theme->children(1)->all() as $subtheme): ?>

                                                        <li class="row">
                                                            <div class="col-xs-6">
                                                                <span class="name"><?= $subtheme->name ?></span>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <?= Html::activeTextInput(
                                                                    $model,
                                                                    "quantity[$subtheme->id]",
                                                                    ['class' => 'form-control', 'placeholder' => 'Кол-во']
                                                                ) ?>
                                                            </div>
                                                        </li>

                                                    <?php endforeach; ?>

                                                </ul>

                                            </li>

                                        <?php endforeach; ?>

                                    </ul>

                                </li>

                            <?php endforeach; ?>

                        </ul>

                        <hr>

                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-success pull-right">Добавить задач в тренировку "<?= $name; ?>"</button>
                            </div>
                        </div>

                    <?php else: ?>

                        <p>Нет разделов</p>

                    <?php endif; ?>

                </form>
            </div>
            <br><br>
        </div>
    </div>
</div>