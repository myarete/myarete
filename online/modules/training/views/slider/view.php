<?php

use yii\helpers\Html;
use common\models\ArticleVsCategory;

$this->title = 'Слайдер: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Слайдеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile(Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host'] . '/js/plugins/slick/slick.min.js', ['depends' => ['admin\assets\AppAsset']]);
$this->registerCssFile(Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host'] . '/css/plugins/slick/slick.css', ['depends' => ['admin\assets\AppAsset']]);
$this->registerCssFile(Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host'] . '/css/plugins/slick/slick-theme.css', ['depends' => ['admin\assets\AppAsset']]);

$js=<<<JS
    $('.article_slider_view').slick({
        dots: true,
        draggable: false,
        swipe: false,
        swipeToSlide: false,
        accessibility: false
    });
JS;
$this->registerJs($js);
?>

<div class="ibox ">
    <div class="ibox-title">
        <?= $this->title ?>
        <div class="ibox-tools">
            <?php if ($model->allowEdit()): ?>
                <?= Html::a(
                    '<i class="fa fa-edit"></i> Редактировать</a>',
                    ['update', 'id' => $model->id]
                ) ?>
                
                <?= Html::a(
                    '<i class="fa fa-trash"></i> Удалить</a>',
                    ['delete', 'id' => $model->id],
                    ['onclick' => 'return confirm("Вы уверены?")']
                ) ?>
            <?php endif; ?>
        
        </div>
    </div>
    <div class="ibox-content">
        <h2><?= $model->name ?></h2>
    </div>
</div>

<div class="row">
    <div class="col-lg-10 col-lg-offset-1">
        <?= $this->render('/article/view__slider', [
            'model' => $model
        ]); ?>
    </div>
</div>
