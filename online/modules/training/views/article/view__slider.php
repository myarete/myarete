<?php
/**
 * User: Evgeniy Karpeev [karpeevea@gmail.com]
 * Date: 27.01.2016
 * Time: 16:12
 */
?>

<div class="ibox">
    <h4 class="text-center m">
        <?= $model->name ?>
    </h4>
    <div class="article_slider_view">
        <?php foreach($model->articleSliderItems as $item):?>
        <div>
            <div class="ibox-content">
                <h2><?= $item->title?></h2>
                <?= $item->description ?>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
