<?php
use yii\helpers\Html;
?>

<div class="article-exercise">
        <div class="exercise-header">
            <strong>Задача #<?=$exercise->id?></strong>
        </div>

        <div class="article-exercise-question">
            <?=$exercise->question?>
        </div>

        <?php if (!empty($exercise->hint)): ?>
            <div>
                <?= Html::a('Показать подсказку', "#hint-{$i}-{$exercise->id}", ['data-toggle' => 'collapse']) ?>
                <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "hint-{$i}-{$exercise->id}"])?>
                <?= $exercise->hint ?>
                <?= Html::endTag('div') ?>
            </div>
        <?php endif; ?>

        <?php if (!empty($exercise->hint_second)): ?>
            <div>
                <?= Html::a('Показать вторую подсказку', "#hint-second-{$i}-{$exercise->id}", ['data-toggle' => 'collapse']) ?>
                <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "hint-second-{$i}-{$exercise->id}"])?>
                <?= $exercise->hint_second ?>
                <?= Html::endTag('div') ?>
            </div>
        <?php endif; ?>

        <?php if (!empty($exercise->hint_third)): ?>
            <div>
                <?= Html::a('Показать третью подсказку', "#hint-third-{$i}-{$exercise->id}", ['data-toggle' => 'collapse']) ?>
                <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "hint-third-{$i}-{$exercise->id}"])?>
                <?= $exercise->hint_third ?>
                <?= Html::endTag('div') ?>
            </div>
        <?php endif; ?>

        <?php if (!empty($exercise->decision)): ?>
            <div>
                <?= Html::a('Показать решение', "#decision-{$i}-{$exercise->id}", ['data-toggle' => 'collapse']) ?>
                <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "decision-{$i}-{$exercise->id}"])?>
                <?= $exercise->decision ?>
                <?= Html::endTag('div') ?>
            </div>
        <?php endif; ?>

        <?php if (!empty($exercise->decision_detailed)): ?>
            <div>
                <?= Html::a('Показать подробное решение', "#decision-detailed-{$i}-{$exercise->id}", ['data-toggle' => 'collapse']) ?>
                <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "decision-detailed-{$i}-{$exercise->id}"])?>
                <?= $exercise->decision_detailed ?>
                <?= Html::endTag('div') ?>
            </div>
        <?php endif; ?>

        <?php if (!empty($exercise->decision_second)): ?>
            <div>
                <?= Html::a('Показать второе решение', "#decision-second-{$i}-{$exercise->id}", ['data-toggle' => 'collapse']) ?>
                <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "decision-second-{$i}-{$exercise->id}"])?>
                <?= $exercise->decision_second?>
                <?= Html::endTag('div') ?>
            </div>
        <?php endif; ?>

        <?php if (!empty($exercise->answer)): ?>
            <div>
                <?= Html::a('Показать ответ', "#answer-{$i}-{$exercise->id}", ['data-toggle' => 'collapse']) ?>
                <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "answer-{$i}-{$exercise->id}"])?>
                <?= $exercise->answer ?>
                <?= Html::endTag('div') ?>
            </div>
        <?php endif; ?>
</div>