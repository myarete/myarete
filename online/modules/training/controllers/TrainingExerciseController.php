<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 19.07.15
 * Time: 14:03
 */

namespace online\modules\training\controllers;

use common\components\yii\base\BaseController;
use common\models\ExerciseVariant;
use common\models\TrainingVsExerciseVariant;
use Yii;
use yii\filters\AccessControl;

class TrainingExerciseController extends BaseController
{
    public $layout = '/old';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = TrainingVsExerciseVariant::findOne($id);

        if ($model === null || $model->training->user_id !== Yii::$app->user->id)
            $this->pageNotFound();

        $model->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return array|bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReplace($id)
    {
        $oldModel = TrainingVsExerciseVariant::findOne($id);

        if ($oldModel === null)
            $this->pageNotFound();

        if ($oldModel->training->user_id !== Yii::$app->user->id)
            $this->forbidden();

		$allSubTheme = [];
        $themeIds = [];

		switch(Yii::$app->request->post('theme')) {
			case 'subtheme':
				$allSubTheme[] = $oldModel->variant->theme;
			break;
			case 'theme':
				$allSubTheme = $oldModel->variant->theme->parents(1)->one()->children(1)->all();
			break;
			case 'section':
				$allSection = $oldModel->variant->theme->parents(2)->one()->children(1)->all();
				foreach($allSection as $section) {
					$subThemes = $section->children(1)->all();
					foreach ($subThemes as $subTheme) {
						$allSubTheme[] = $subTheme;
					}
				}
			break;
		}

		foreach ($allSubTheme as $subTheme) {
			$themeIds[] = $subTheme->id;
		}

		$query = ExerciseVariant::find()
			->joinWith(['exercise'])
			->where([
				'exercise.subject_id' => $oldModel->variant->exercise->subject_id,
				'exercise.is_approved' => (int)true,
			])
			->andWhere([
				'in',
				'theme_id',
				$themeIds
			])
			->andWhere([
				'not in',
				'exercise_variant.id',
				Yii::$app->db
					->createCommand('select exercise_variant_id from training_vs_exercise_variant where training_id = :training_id')
					->bindValue(':training_id', $oldModel->training_id)
					->queryColumn()
			]);

        if(Yii::$app->request->post('type'))
 			$query = $query->andWhere(['exercise.type_id' => $oldModel->variant->exercise->type_id]);

		if(Yii::$app->request->post('complexity') && Yii::$app->request->post('complexity') == 'up')
			$query = $query->andWhere(['>', 'complexity', $oldModel->variant->complexity]);

		if(Yii::$app->request->post('complexity') && Yii::$app->request->post('complexity') == 'down')
			$query = $query->andWhere(['<', 'complexity', $oldModel->variant->complexity]);

		$newModel = $query
			->orderBy('RAND()')
			->one();

		if($newModel && $oldModel->exercise_variant_id != $newModel->id) {
			$oldModel->exercise_variant_id = $newModel->id;
			$oldModel->save(false, ['exercise_variant_id']);
        } else {
			Yii::$app->session->setFlash('warning', 'Подходящей задачи не найдено');
        }

		return $this->redirect(['/training/default/run/', 'id' => $oldModel->training_id]);
    }
}
