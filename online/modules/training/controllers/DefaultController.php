<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 19.07.15
 * Time: 14:03
 */

namespace online\modules\training\controllers;

use common\components\yii\base\BaseController;
use common\models\Exercise;
use common\models\ExerciseVariant;
use common\models\Group;
use common\models\Payment;
use common\models\Profile;
use common\models\Subject;
use common\models\Theme;
use common\models\Training;
use common\models\TrainingBuy;
use common\models\TrainingBuySearch;
use common\models\TrainingCategory;
use common\models\TrainingVsCategory;
use common\models\TrainingVsExerciseVariant;
use common\models\TrainingVsGroup;
use common\models\TrainingVsGroupOpt;
use common\models\UserVsTrainingBuy;
use common\models\UserVsTrainingCategory;
use online\modules\training\forms\CreateTrainingForm;
use Yii;
use yii\data\Sort;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\ViewAction;

class DefaultController extends BaseController
{

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'page' => [
                'class' => ViewAction::className(),
            ],
        ];
    }

    public function actionMy($categoryId = null, $articleId = null)
    {
        $trainings = null;
        $category = null;
        if (empty($categoryId)) {
            //$categories = ArticleCategory::find()->with('userVsCategory')->where(['id' => 119])->all();
            $model = new TrainingCategory();
            $categoriesRoots = TrainingCategory::getRoots();
            $categories = $model->getUserVsTrainingCategory();
            $categories = $categories->all();
            $categories = TrainingCategory::find()->joinWith('userVsTrainingCategory')->where(['user_vs_training_category.user_id' => Yii::$app->user->id])->all();
        } else {
            $category = TrainingCategory::findOne($categoryId);
            $categories = $category->children(1)->all();
            $categoriesRoots = $categories;
        }

        $sort = new Sort([
            'attributes' => [
                'id' => [
                    'label' => 'Номер тренировки',
                ],
                'name' => [
                    'label' => 'Название',
                ],
            ],
        ]);

        $trainingsFromCategories = TrainingBuy::find()->joinWith('trainingVsCategoryFilter')->orderBy($sort->orders)->all();
        //$articles = Article::find()->joinWith('userVsArticle')->orderBy($sort->orders)->all();

        $trainings = TrainingBuy::find()->joinWith('userVsTrainingBuyFilter')->all();

        $approvedBy = [TrainingBuy::SUCCESS_ALL];
        if (Yii::$app->user->can('teacher'))
            $approvedBy[] = TrainingBuy::SUCCESS_TEACHER;

        $searchModel = new TrainingBuySearch([
            'is_approved' => $approvedBy,
            'pagination' => null,
            'categoryId' => $categoryId
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('my', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'trainings' => $trainings,
            'childCategory' => $category,
            'categoryId' => $categoryId,
            'categories' => $categories,
            'category' => $category,
            'trainingsFromCategories' => $trainingsFromCategories,
            'categoriesRoots' => $categoriesRoots,
            'sort' => $sort
        ]);
    }

    public function actionStore($categoryId = null)
    {
        $category = null;
        $categories_buy = TrainingCategory::find()->joinWith('userVsTrainingCategory')->where(['user_id' => Yii::$app->user->id])->all();
        if (empty($categoryId)) {
            $categories = TrainingCategory::getRoots();
        } else {
            $category = TrainingCategory::findOne($categoryId);
            $categories = $category->children(1)->all();
        }

        $approvedBy = [TrainingBuy::SUCCESS_ALL];
        if (Yii::$app->user->can('teacher'))
            $approvedBy[] = TrainingBuy::SUCCESS_TEACHER;

        $searchModel = new TrainingBuySearch([
            'is_approved' => $approvedBy,
            'pagination' => null,
            'categoryId' => $categoryId
        ]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        foreach ($dataProvider->models as $model) {
            if ($model->is_action == 1) {
                $actionDateStart = strtotime($model->action_date_start);
                $actionDateEnd = strtotime($model->action_date_end);

                if ($model->action_date_start != null && $model->action_date_end != null) {
                    if (time() < $actionDateStart) {
                        $model->is_action = 0;
                    } else if (time() > $actionDateEnd) {
                        $model->is_action = 0;
                        $model->action_price = 0;
                        $model->action_date_start = null;
                        $model->action_date_end = null;
                        $model->update();
                    }
                }
            }
        }

        return $this->render('store', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'childCategory' => $category,
            'categoryId' => $categoryId,
            'categories' => $categories,
            'category' => $category,
            'categories_buy' => $categories_buy
        ]);
    }

    public function actionAddTraining()
    {
        $trainingId = Yii::$app->request->get('training_id');
        $trainingPrice = Yii::$app->request->get('training_price');
        $training = TrainingBuy::find()->where(['id' => $trainingId])->one();
        $trainingVsCategory = TrainingVsCategory::find()->where(['training_id' => $trainingId])->one();

        if ($trainingId != '') {
            if ($trainingPrice == 0) {
                if (!UserVSTrainingBuy::find()->where(['user_id' => Yii::$app->user->id, 'training_id' => $trainingId])->one()) {
                    $model = new UserVSTrainingBuy();
                    $model->user_id = Yii::$app->user->id;
                    $model->training_id = $training->id;
                    $model->category_id = $trainingVsCategory->training_category_id;
                    $model->save();
                    Yii::$app->session->setFlash('success', 'Вы добавили тренировку - <a href="/training/default/view/' . $training->id . '">'
                        . $training->name . '</a><br>Вы сможете найти его в разделе <a href="/training/default/my">Купленные тренировки</a>');
                }
            } else {
                return $this->redirect(['store']);
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionBuyTraining()
    {
        $trainingId = Yii::$app->request->get('training_id');
        $trainingPrice = Yii::$app->request->get('training_price');
        $training = TrainingBuy::find()->where(['id' => $trainingId])->one();

        $profile = Profile::find()->where(['user_id' => Yii::$app->user->id])->one();

        if ($trainingId != '') {
            if ($trainingPrice != 0) {
                if ($profile->cash > $trainingPrice) {
                    if (!UserVSTrainingBuy::find()->where(['user_id' => Yii::$app->user->id, 'training_id' => $trainingId])->one()) {
                        $profile->cash -= $trainingPrice;
                        $profile->save();
                        $payment = new Payment();
                        $payment->user_id = Yii::$app->user->id;
                        $payment->type_id = 7;
                        $payment->refill_sum = $trainingPrice;
                        $payment->is_paid = 1;
                        $payment->save();
                        $model = new UserVSTrainingBuy();
                        $model->user_id = Yii::$app->user->id;
                        $model->training_id = $training->id;
                        $model->save();
                        Yii::$app->session->setFlash('success', 'Вы купили тренировку - <a href="/training/default/view/' . $training->id . '">'
                            . $training->name . '</a> за '
                            . $trainingPrice . ' рублей.<br>Вы сможете найти его в разделе <a href="/training/default/my">Купленные тренировки</a>');
                    }
                } else {
                    Yii::$app->session->setFlash('info', 'В вашем бумажнике не хватает средств для покупки данной тренировки - <a href="/payment/default/refill-cash">Пополнить бумажник</a>');
                }
            } else {
                return $this->redirect(['store']);
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionAddCategory()
    {
        $categoryId = Yii::$app->request->get('category_id');
        $category = TrainingCategory::find()->where(['id' => $categoryId])->one();

        $categoriesChildren = $category->children(5)->all();

        if ($categoryId != '') {
            if ($category->price == 0 || $category->price == NULL) {

                if (!UserVsTrainingCategory::find()->where(['user_id' => Yii::$app->user->id, 'training_category_id' => $category->id])->one()) {
                    UserVsTrainingBuy::deleteAll(['category_id' => $category->id]);
                    $model = new UserVsTrainingCategory();
                    $model->user_id = Yii::$app->user->id;
                    $model->training_category_id = $categoryId;
                    $model->pid = $category->pid;
                    $model->root = $category->root;
                    $model->save();

                    foreach ($categoriesChildren as $child) {
                        UserVsTrainingBuy::deleteAll(['user_id' => Yii::$app->user->id, 'category_id' => $category->id]);
                        $cat = new UserVsTrainingCategory();
                        $cat->user_id = Yii::$app->user->id;
                        $cat->training_category_id = $child->id;
                        $cat->pid = $child->pid;
                        $cat->root = $child->root;
                        $cat->save();
                    }

                    Yii::$app->session->setFlash('success', 'Категория была успешно добавленна в раздел - <a href="/training/default/my">Купленные тренировки</a>');

                }
            } else {
                return $this->redirect(['store']);
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionBuyCategory()
    {
        $categoryId = Yii::$app->request->get('category_id');
        $category = TrainingCategory::find()->where(['id' => $categoryId])->one();

        $categoriesChildren = $category->children(5)->all();

        $profile = Profile::find()->where(['user_id' => Yii::$app->user->id])->one();

        if ($categoryId != '') {
            if ($category->price != 0 || $category->price != NULL) {
                if ($profile->cash > $category->price) {
                    if (!UserVsTrainingCategory::find()->where(['user_id' => Yii::$app->user->id, 'training_category_id' => $category->id])->one()) {
                        $profile->cash -= $category->price;
                        $profile->save();
                        $payment = new Payment();
                        $payment->user_id = Yii::$app->user->id;
                        $payment->type_id = 8;
                        $payment->refill_sum = $category->price;
                        $payment->is_paid = 1;
                        $payment->save();
                        UserVsTrainingBuy::deleteAll(['category_id' => $category->id]);
                        //UserVsTrainingCategory::deleteAll(['pid' => $category->id]);
                        $model = new UserVsTrainingCategory();
                        $model->user_id = Yii::$app->user->id;
                        $model->training_category_id = $categoryId;
                        $model->pid = $category->pid;
                        $model->root = $category->root;
                        $model->save();

                        foreach ($categoriesChildren as $child) {
                            UserVsTrainingBuy::deleteAll(['user_id' => Yii::$app->user->id, 'category_id' => $category->id]);
                            //list($id, $name) = explode("_", $key);
                            $cat = new UserVsTrainingCategory();
                            $cat->user_id = Yii::$app->user->id;
                            $cat->training_category_id = $child->id;
                            $cat->pid = $child->pid;
                            $cat->root = $child->root;
                            $cat->save();
                        }

                        Yii::$app->session->setFlash('success', 'Категория была успешно куплена и добавленна в раздел - <a href="/training/default/my">Купленные тренировки</a><br>С баланса было списана сумма - ' . $category->price . ' ₽');
                    }
                } else {
                    Yii::$app->session->setFlash('info', 'В вашем бумажнике не хватает средств для покупки данной категории - <a href="/payment/default/refill-cash">Пополнить бумажник</a>');
                }
            } else {
                return $this->redirect(['store']);
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRun($id)
    {
        $training = Training::findOne($id);

        if (!$training->trainingExercises) {
            Yii::$app->session->setFlash('error', 'Для этой тренировки задачи не найдены.');
            //$training->delete();
            return $this->redirect(['/training/default/saved']);
        }

        if ($training === null)
            $this->pageNotFound();

        return $this->render('run', [
            'training' => $training,
        ]);
    }

    public function actionView($id)
    {
        $training = TrainingBuy::findOne($id);
        $add = 0;

        if (!$training->trainingExercises) {
            Yii::$app->session->setFlash('error', 'Для этой тренировки задачи не найдены.');
            //$training->delete();
            return $this->redirect(['/training/default/store']);
        }

        if ($training === null)
            $this->pageNotFound();

        if (UserVSTrainingBuy::find()->where(['user_id' => Yii::$app->user->id, 'training_id' => $id])->one()) {
            $add = 1;
        }

        return $this->render('view', [
            'training' => $training,
            'add' => $add
        ]);
    }

    /**
     * @return string
     */
    public function actionChooseClassSubject()
    {
        if (Yii::$app->user->can('user')) {
            $query = Subject::find()
                ->innerJoinWith('exercise')
                ->where(['is_active' => 2]);
        } else {
            $query = Subject::find()
                ->innerJoinWith('exercise')
                ->where(['>=', 'is_active', 1]);
        }

        return $this->render('choose_class_subject', [
            'subjects' => $query->all(),
        ]);
    }

    /**
     * @param $class
     * @param $subject_id
     * @return string
     */
    public function actionCreate($class, $subject_id)
    {
        $theme = Theme::find()->where(['class' => $class, 'subject_id' => $subject_id, 'lvl' => 0])->one();
        if ($theme === null) {
            $this->pageNotFound();
        }

        $model = new CreateTrainingForm;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->redirect($model->getTraining()->getUrl());
        }

        return $this->render('create', [
            'theme' => $theme,
            'sections' => $theme->children(1)->all(),
            'model' => $model,
        ]);
    }

    public function actionCreateEmpty()
    {
        $model = new CreateTrainingForm;

        $model->empty = 1;
        $model->load(Yii::$app->request->post());
        $model->save();

        return $this->redirect(['saved']);
    }

    public function actionCreateSimilar()
    {
        Yii::$app->response->format = 'json';

        $id = Yii::$app->request->post('id');
        $name = Yii::$app->request->post('name');

        $training = Training::findOne($id);
        $new_training = new Training;
        $new_training->attributes = $training->attributes;
        $new_training->name = $name;
        $new_training->order_by = '';
        $new_training->save();

        $subQuery = (new Query())->select('exercise_variant_id')->from('training_vs_exercise_variant')->where(['training_id' => $new_training->id]);
        foreach ($training->trainingExercises as $variant) {
            $new_variant = ExerciseVariant::find()
                ->where(['theme_id' => $variant->variant->theme_id])
                ->andWhere(['not', ['id' => $subQuery]])
                ->orderBy('rand()')
                ->one();
            $new_training_vs_ex_variant = new TrainingVsExerciseVariant;
            $new_training_vs_ex_variant->training_id = $new_training->id;
            $new_training_vs_ex_variant->exercise_variant_id = $new_variant->id;
            $new_training_vs_ex_variant->save(false);
        }

        return $new_training->id;
    }

    public function actionSaveTraining()
    {
        Yii::$app->response->format = 'json';

        $id = Yii::$app->request->post('id');
        $name = Yii::$app->request->post('name');

        $training = TrainingBuy::findOne($id);
        $new_training = new Training;
        $new_training->attributes = $training->attributes;
        $new_training->name = $name;
        $new_training->order_by = '';
        $new_training->save();

        $subQuery = (new Query())->select('exercise_variant_id')->from('training_buy_vs_exercise_variant')->where(['training_id' => $new_training->id]);
        foreach ($training->trainingExercises as $variant) {
            $new_variant = ExerciseVariant::find()
                ->where(['theme_id' => $variant->variant->theme_id])
                ->andWhere(['not', ['id' => $subQuery]])
                ->orderBy('rand()')
                ->one();
            $new_training_vs_ex_variant = new TrainingVsExerciseVariant;
            $new_training_vs_ex_variant->training_id = $new_training->id;
            $new_training_vs_ex_variant->exercise_variant_id = $new_variant->id;
            $new_training_vs_ex_variant->save(false);
        }

        return $new_training->id;
    }

    /**
     * @param $class
     * @param $subject_id
     * @return string
     */
    public function actionAddexercise($class, $subject_id, $training_id)
    {
        $theme = Theme::find()->where(['class' => $class, 'subject_id' => $subject_id])->one();
        if ($theme === null) {
            $this->pageNotFound();
        }

        $model = new CreateTrainingForm;
        if ($model->load($_POST) && $model->save($training_id)) {
            $this->redirect($model->getTraining()->getUrl());
        }

        return $this->render('addexercise', [
            'theme' => $theme,
            'sections' => $theme->children(1)->all(),
            'model' => $model,
            'name' => Training::find()->where('id = :id', [':id' => $training_id])->one()->name,
        ]);
    }

    public function actionManualExercise()
    {
        $training_id = null;
        $params = Yii::$app->request->post();
        if (empty($params['exercise_variant_id'])) $this->pageNotFound();
        if (empty($params['name'])) $params['name'] = 'Тренировка';

        $traning = Training::findOne($params['name']);

        if (is_numeric($params['name']) && !empty($traning)) {
            $training_id = $traning->id;
        } else {
            $traning = new Training;
            $traning->name = $params['name'];
            $traning->user_id = Yii::$app->user->id;
            if ($traning->save(false)) {
                $training_id = $traning->id;
            }
        }

        if ($training_id) {
            $model = new TrainingVsExerciseVariant();
            $model->training_id = $training_id;
            $model->exercise_variant_id = $params['exercise_variant_id'];
            $model->save(false);
            $this->redirect($traning->getUrl());
        } else {
            $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * @param null $q
     * @param null $subject_id
     * @param null $class
     * @return array
     */
    public function actionManualExerciseList($q = null, $subject_id = null, $class = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $query = new Query();
            $query->select('DISTINCT(exercise_variant.id), exercise.name AS text')
                ->from('exercise_variant')
                ->innerJoin('exercise', 'exercise_variant.exercise_id = exercise.id')
                ->innerJoin('theme', 'exercise_variant.theme_id = theme.id')
                ->andfilterWhere(['like', 'exercise.name', $q])
                ->orFilterWhere(['like', 'exercise.id', $q])
                ->andWhere('theme.class = :class and exercise.subject_id = :subject_id', [':class' => $class, ':subject_id' => $subject_id])
                ->limit(20);

            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }

        return $out;
    }

    /**
     * @param null $q
     * @param null $id
     * @return array
     */
    public function actionManualTrainingList($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, name AS text')
                ->from('training')
                ->andFilterWhere(['like', 'name', $q])
                ->andWhere(['=', 'user_id', Yii::$app->user->id])
                ->limit(20);

            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Exercise::find($id)->name];
        }
        return $out;
    }

    /**
     * @param $id
     * @return Response
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = Training::find()
            ->where([
                'id' => $id,
                'user_id' => Yii::$app->user->id,
            ])
            ->one();

        if ($model === null)
            $this->pageNotFound();

        $model->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return bool
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        Yii::$app->response->format = 'json';

        $model = Training::find()
            ->where([
                'id' => $id,
                'user_id' => Yii::$app->user->id,
            ])
            ->one();

        if ($model === null)
            $this->pageNotFound();

        $model->name = Yii::$app->request->get('name');

        $model->update(true, ['name']);

        return $model->attributes;
    }

    /**
     * @return string
     */
    public function actionSaved()
    {
        $trainings = Training::find()->where(['user_id' => Yii::$app->user->id])->orderBy(['id' => SORT_DESC])->all();
        $sub_query = TrainingVsGroup::find()->select('training_id')->where(['user_id' => Yii::$app->user->id]);
        $received_trainings = Training::find()->where(['id' => $sub_query])->all();

        return $this->render('saved', [
            'trainings' => $trainings,
            'received_trainings' => $received_trainings
        ]);
    }

    /**
     * @param $id
     * @return array|bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReplaceExerciseVariant($id)
    {
        /** @var $oldModel TrainingVsExerciseVariant */
        $oldModel = TrainingVsExerciseVariant::findOne($id);
        if ($oldModel === null)
            $this->pageNotFound();

        $newModel = ExerciseVariant::find()
            ->joinWith(['exercise'])
            ->where([
                'exercise.subject_id' => $oldModel->variant->exercise->subject_id,
                'class' => $oldModel->variant->class,
                'theme_id' => $oldModel->variant->theme_id,
                'complexity' => $oldModel->variant->complexity,
            ])
            ->andWhere('exercise_variant.id != :id', [':id' => $oldModel->exercise_variant_id])
            ->one();

        Yii::$app->response->format = 'json';

        if ($newModel === null)
            return ['error' => 'Похожей задачи не найдено'];

        $oldModel->exercise_variant_id = $newModel->id;
        return $oldModel->save(false, ['exercise_variant_id']);
    }

    public function actionSaveOrder()
    {
        $model = Training::findOne(Yii::$app->request->post('training_id'));
        $model->order_by = implode(',', Yii::$app->request->post('data'));
        $model->save();
    }

    public function actionShuffleExercises()
    {
        $id = Yii::$app->request->post('id');
        $training = Training::findOne($id);
        $order = explode(',', $training->order_by);
        $old_order = [];
        foreach ($order as $item)
            $old_order[] = $item;
        while ($order == $old_order)
            shuffle($order);
        $training->order_by = implode(',', $order);
        $training->save();
    }

    public function actionPrintScreen($id = null, $tmpfile = null)
    {
        $this->layout = '/print';

        if ($content = Yii::$app->request->post('content')) {
            $tmpfile = tempnam('/tmp', 'log');
            file_put_contents($tmpfile, $content);
            return $tmpfile;
        }

        $training = Training::findOne($id);

        return $this->render('printscreen', [
            'training' => $training,
            'tmpfile' => $tmpfile
        ]);
    }

    public function actionNote()
    {
        //print_r($_POST);
        $id = Yii::$app->request->post('Training')['id'];
        $note = Yii::$app->request->post('Training')['note'];
        $training = Training::findOne($id);
        $training->note = $note;
        $training->update();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPreviewNote()
    {
        $id = Yii::$app->request->post('training_id');
        $note = Yii::$app->request->post('note');
        $testing = Training::findOne($id);
        $testing->note = $note;
        $testing->update();

        return $this->redirect(['saved']);
    }

    public function actionDeleteNote()
    {
        $id = Yii::$app->request->post('id');
        $testing = Training::findOne($id);
        $testing->note = '';
        $testing->update();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionTrainingOpts($id)
    {
        $training = Training::findOne($id);
        $groups = Group::findAll(['user_id' => Yii::$app->user->id]);

        if ($post = Yii::$app->request->post()) {
            $group_ids = $post['group_ids'];
            foreach ($group_ids as $group_id) {
                $trainingVsGroup = new TrainingVsGroup();
                $post['TrainingVsGroup']['group_id'] = $group_id;
                if ($trainingVsGroup->load($post) && $trainingVsGroup->save()) {
                    $opts = $post['TrainingVsGroupOpt'];
                    foreach ($opts as $opt) {
                        $option['TrainingVsGroupOpt'] = $opt;
                        $option['TrainingVsGroupOpt']['trvsgr_id'] = $trainingVsGroup->id;
                        $trainingVsGroupOpt = new TrainingVsGroupOpt();
                        if ($trainingVsGroupOpt->load($option)) {
                            $trainingVsGroupOpt->save();
                        }
                    }
                }
            }
            return $this->redirect(['saved']);
        }

        return $this->render('training-opts', [
            'training' => $training,
            'groups' => $groups
        ]);
    }

//    public function actionAddTrainingToGroup()
//    {
//        $post = Yii::$app->request->post();
//        if (!empty($post['Opts'])) {
//            $group_ids = $post['group_ids'];
//            foreach ($group_ids as $group_id) {
//                $trainingVsGroup = new TrainingVsGroup();
//                $post['TrainingVsGroup']['group_id'] = $group_id;
//                if ($trainingVsGroup->load($post) && $trainingVsGroup->save()) {
//                    echo $group_id;
//                }
//            }
//        }
//        $training = Training::findOne($post['TrainingVsGroup']['training_id']);
//
//        $this->render('training-opts', ['training' => $training]);
//    }
}
