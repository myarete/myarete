<?php

namespace online\modules\training;

class Training extends \yii\base\Module
{
    public $controllerNamespace = 'online\modules\training\controllers';

    public function init()
    {
        parent::init();
    }
}
