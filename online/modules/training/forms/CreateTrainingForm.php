<?php

namespace online\modules\training\forms;

use common\models\ExerciseVariant;
use common\models\Theme;
use common\models\Training;
use common\models\TrainingVsExerciseVariant;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class CreateTrainingForm extends Model
{
    /**
     * @var array
     */
    public $quantity = [];

    /**
     * @var string
     */
    public $name = 'Тренировка';

    public $empty = 0;

    /**
     * @var Training
     */
    private $_training;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['quantity', 'checkQuantity'],
            ['name', 'required'],
        ];
    }

    /**
     *
     */
    public function checkQuantity()
    {
        foreach ($this->quantity as $id => $quantity)
        {
            if ((int)$quantity > 0 || $this->empty)
                return;
        }
        $this->addError('quanity', 'Вы должны указать количество');
    }

    /**
     * @return bool
     */
    public function save($training_id = null)
    {
        if (!$this->validate()) {
            return false;
        }

        $themes = Theme::find()->where(['in', 'id', array_keys($this->quantity)])->orderBy('lft')->indexBy('id')->all();
        $trainingVsExerciseVariant = [];


        if($training_id) {
            $this->_training = Training::find()->where('id = :id', [':id' => $training_id])->one();
            $trainingVsExerciseVariant = TrainingVsExerciseVariant::find()
                                                ->select('exercise_variant_id')
                                                ->where('training_id = :training_id', [':training_id' => $training_id])
                                                ->all();
            $trainingVsExerciseVariant = ArrayHelper::getColumn($trainingVsExerciseVariant, 'exercise_variant_id');
        } else {
            $this->_training = new Training;
            $this->_training->name = $this->name;
            $this->_training->user_id = Yii::$app->user->id;
            $this->_training->save(false);
        }

        $variants = [];

        foreach ($this->quantity as $id => $quantity)
        {
            if ($quantity <= 0)
                continue;

            $theme = $themes[$id];
            $variants += ExerciseVariant::find()
                ->joinWith(['theme'])
                ->where('theme.lft >= :lft AND theme.rgt <= :rgt AND root = :root')
                ->andWhere(['not in', 'exercise_variant.id', $trainingVsExerciseVariant])
                ->params(['lft' => $theme->lft, 'rgt' => $theme->rgt, 'root' => $theme->root])
                ->limit($quantity)
                ->indexBy('id')
                ->orderBy('rand()')
                ->all();
        }
        //echo '<pre>';
        //print_r($variants);
        //echo '</pre>';

        foreach ($variants as $variant)
        {

            $model = new TrainingVsExerciseVariant;
            $model->training_id = $this->_training->id;
            $model->exercise_variant_id = $variant->id;
            $model->save(false);
        }

        return true;
    }

    /**
     * @return Training
     */
    public function getTraining()
    {
        return $this->_training;
    }
}
