<?php
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>


<?php
$this->title = 'Отзывы учеников | '.($model->isNewRecord ? 'Создать' : 'Обновить');
?>

<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-pencil-square-o"></i> <?= $this->title ?>
    </div>
    <div class="ibox-content">

        <?php $form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>

            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'clientOptions' => [
                    'toolbar' => [
                        ['Bold', 'Italic', 'Underline'],
                        ['BulletedList', 'NumberedList'],
                        ['Format'],
                        ['Source'],
                    ],
                    'allowedContent' => true,
                ],
            ]) ?>

            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>
