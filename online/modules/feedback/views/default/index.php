<?php
use yii\grid\GridView;
use yii\helpers\Html;

?>


<?php
$this->title = 'Отзывы учеников';
?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5><?= $this->title ?></h5>
            </div>

            <div class="ibox-content">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'image',
                            'format' => 'raw',
                            'value' => function($data){
                                return $data->getImageTag(['style' => 'max-height: 100px; max-width: 100px;']);
                            },
                        ],

                        'fio',
                        'content:ntext',
                        [
                            'attribute' => 'is_published',
                            'format' => 'raw',
                            'value' => function($data){
                                if ($data->is_published)
                                    return Html::a('Опубликован', ['unpublish', 'id' => $data->id], ['class' => 'label label-success']);
                                else
                                    return Html::a('Не опубликован', ['publish', 'id' => $data->id], ['class' => 'label label-danger']);
                            },
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['class' => 'action-column'],
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>
