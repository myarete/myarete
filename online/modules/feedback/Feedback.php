<?php

namespace online\modules\feedback;

class Feedback extends \yii\base\Module
{
    public $controllerNamespace = 'online\modules\feedback\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
