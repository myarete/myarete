<?php
use common\models\Exercise;
use common\models\ExerciseComplexity;
use common\models\Theme;
use kartik\tree\TreeViewInput;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="modal inmodal" id="create-promocode-modal">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Создать промокод</h4>
            </div>
            <?php $form = ActiveForm::begin(['action' => '/promocodes/default/create-gift-promocode']) ?>

                <div class="modal-body">
                    <div>
                        <?= $form->field($model, 'value')
                            ->label(false)
                            ->textInput(['type' => 'number', 'placeholder' => '0.00']) ?>

                        <?= $form->field($model, 'promocode')
                            ->label(false)
                            ->textInput(['type' => 'text', 'value' => $promocode]) ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <?= Html::submitButton('Создать промокод', ['class' => 'btn btn-primary col-xs-12']) ?>
                </div>

            <?php $form->end() ?>

        </div>
    </div>
</div>