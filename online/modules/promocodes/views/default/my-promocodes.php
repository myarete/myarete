<?php


use yii\helpers\Html;
use yii\grid\GridView;

use common\components\other\FirstWords;

$this->title = 'Промокоды';

?>
<div class="crm__container">
    <div class="crm__content">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
            <div class="article-block">
                <div class="row side-block">
                    <h4>Мои промокоды</h4>
                    <hr>
                    <div class="article-block">
                        <div class="row">
                            <?php if(count($promocodes)):?>
                                <?php foreach($promocodes as $promocode):?>
                                    <ul>
                                        <?php if($promocode->status == 0):?>
                                            <li><?= $promocode->promocode?> - <?= $promocode->value?> руб. - <?= Html::a('Активировать', ['/promocodes/default/activate-promocode', 'promocode_id' => $promocode->id]) ?></li>
                                        <?php else:?>
                                            <li><?= $promocode->promocode?> - <?= $promocode->value?> руб. - данный промокод уже активирован</li>
                                        <?php endif;?>
                                    </ul>
                                <?php endforeach;?>
                            <?php else:?>
                                <p>У вас пока нет промокодов</p>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

