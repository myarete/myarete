<?php


use yii\helpers\Html;
use yii\grid\GridView;

?>
<div class="crm__container">
    <div class="crm__content">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
            <div class="article-block">
                <div class="row side-block">
                    <h4>Промокод для друзей</h4>
                    <hr>
                    <div class="article-block">
                        <div class="row">
                            <?php if(empty($promocode)):?>
                                <p>У вас пока нет промокода. Вы хотите его <a href="/promocodes/default/generate-promocode">сгенерировать</a>?</p>
                            <?php else:?>
                                <p>Ваш промокод: <?= $promocode->promocode?></p>
                            <?php endif;?>
                            <p>Отправьте этот промокод вашему другу и пригласите его зарегистрироваться в Арете Онлайн. Как только он зарегистрирует учетную запись с этим промокодом, и он, и вы получите по 200 рублей в Арете-Бумажник.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
