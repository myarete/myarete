<?php


use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;

use common\components\other\FirstWords;

$this->title = 'Промокоды';

$js=<<<JS
    $("#copyButton").click(function () {
        copyToClipboard(document.getElementById("copyTarget"));
        alert('Ваш промокод скопирован');
    });

    function copyToClipboard(elem) {
        // create hidden text element, if it doesn't already exist
        var targetId = "_hiddenCopyText_";
        var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            // can just use the original source element for the selection and copy
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            // must use a temporary form element for the selection and copy
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        // select the content
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);

        // copy the selection
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch (e) {
            succeed = false;
        }
        // restore original focus
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }

        if (isInput) {
            // restore prior selection
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            // clear temporary content
            target.textContent = "";
        }
        return succeed;
    }
JS;
$this->registerJs($js);
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="event__content">
                <div class="tab-menu__wrap tab-menu__wrap-with--btn">
                    <div class="tab-menu tab-menu-left">
                        <div class="tab-menu__label tab-menu__label-left active" data-tab="tab5">Мои промокоды</div>
                        <div class="tab-menu__label tab-menu__label-left" data-tab="tab6">Активированные</div>
                    </div>
                </div>
                <div class="tab-content" id="width-check">
                    <div class="tab-content__item event__content show" id="tab5">
                        <?= Html::beginForm('/promocodes/default/activate-promocode'); ?>
                        <div class="promocode grid">
                            <?php if (count($promocodes)): ?>
                                <?php foreach ($promocodes as $promocode): ?>
                                    <?php if ($promocode->status == 0): ?>
                                        <div class="grid__wrapper promocode__wrap">
                                            <input class="promocode__radio" type="radio" name="promo"
                                                   value="<?= $promocode->id ?>" id="r<?= $promocode->id ?>"/>
                                            <label class="promocode__item promocode__item--active"
                                                   for="r<?= $promocode->id ?>">
                                                <div class="promocode__left">
                                                    <div class="promocode__code"><?= $promocode->promocode ?></div>
                                                    <div class="promocode__gift"><?= $promocode->value ?> р. на
                                                        бумажник
                                                    </div>
                                                    <div class="promocode__time">Срок использования
                                                        <div class="promocode__date">неограничен</div>
                                                    </div>
                                                </div>
                                                <a class="promocode__right" href="#"></a>
                                            </label>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <p style="padding-left: 15px;">У вас пока нет промокодов</p>
                            <?php endif; ?>
                        </div>
                        <div class="promocode__bottom">
                            <div class="col-sm-12">
                                <div class="col-lg-4 col-md-6 col-sm-6">
                                    <input style="width: 100%;border-radius: 0; box-shadow: none; font-size: 16px;"
                                           type="text" name="promo_field"
                                           class="form-control promocode__field text-center"
                                           placeholder="А если есть, то введите его здесь!">

                                </div>
                                <div class="col-lg-5 col-md-4 col-sm-4 text-left">
                                    <?= Html::submitButton('Активировать промокод', ['class' => 'button button__green--full button__round']); ?>
                                </div>
                            </div>
                        </div>
                        <?= Html::endForm(); ?>

                        <div class="clearfix"></div>

                        <div class="modal__promocode">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal__promocode_wrap">
                                        <div class="modal-header">Получайте бонусы за каждого приглашенного друга</div>
                                        <div class="modal-body">
                                            <div class="modal-body_p">Отправьте промокод вашему другу и пригласите его
                                                зарегистрироваться в Арете Онлайн. Как только он зарегистрирует учетную
                                                запись с этим промокодом, и он, и вы получите по 200 рублей в
                                                Арете-Бумажник.
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="modal-footer-promocode"
                                                 id="copyTarget"><?= $my_promocode->promocode ?></div>
                                            <a class="button button__green--full button__round" href="#"
                                               id="copyButton">Скопировать промокод</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-content__item event__content" id="tab6">
                        <div class="promocode grid">
                            <?php if (count($promocodes)): ?>
                                <?php foreach ($promocodes as $index => $promocode): ?>
                                    <?php if ($promocode->status != 0): ?>
                                        <div class="grid__wrapper promocode__wrap">
                                            <div class="promocode__item">
                                                <div class="promocode__left">
                                                    <div class="promocode__code"><?= $promocode->promocode ?></div>
                                                    <div class="promocode__gift"><?= $promocode->value ?> р. на
                                                        бумажник
                                                    </div>
                                                    <div class="promocode__time">Срок использования
                                                        <div class="promocode__date">неограничен</div>
                                                    </div>
                                                </div>
                                                <a class="promocode__right" href="#"></a>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <p style="padding-left: 15px;">У вас пока нет промокодов</p>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
