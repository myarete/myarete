<?php

namespace online\modules\promocodes\controllers;

use common\components\yii\base\BaseController;
use common\models\Promocodes;
use Yii;

class DefaultController extends BaseController
{
    
    public function actionPromocodes()
    {
        $promocodes = Promocodes::find()->where(['user_id' => Yii::$app->user->id, 'register' => 0])->orderBy(['date_created' => SORT_DESC])->all();
        
        $my_promocode = Promocodes::find()->where(['user_id' => Yii::$app->user->id, 'register' => 1])->one();
        
        if (empty($my_promocode)) {
            $my_promocode = new Promocodes();
            $my_promocode->promocode = $my_promocode->random_string(10);
            $my_promocode->value = 200;
            $my_promocode->owner_user_id = Yii::$app->user->id;
            $my_promocode->user_id = Yii::$app->user->id;
            $my_promocode->register = 1;
            $my_promocode->save();
            return $this->render('promocodes', [
                'promocodes' => $promocodes,
                'my_promocode' => $my_promocode
            ]);
        } else {
            return $this->render('promocodes', [
                'promocodes' => $promocodes,
                'my_promocode' => $my_promocode
            ]);
        }
        
    }
    
    public function actionFriendsPromocode()
    {
        $promocode = Promocodes::find()->select('promocode')->where(['owner_user_id' => Yii::$app->user->id, 'user_id' => Yii::$app->user->id, 'register' => 1])->one();
        
        return $this->render('friends-promocode', [
            'promocode' => $promocode
        ]);
    }
    
    public function actionMyPromocodes()
    {
        $promocodes = Promocodes::find()->where(['user_id' => Yii::$app->user->id, 'register' => 0])->orderBy(['date_created' => SORT_DESC])->all();
        
        return $this->render('my-promocodes', [
            'promocodes' => $promocodes,
        ]);
    }
    
    public function actionGeneratePromocode()
    {
        $promocode = Promocodes::find()->select('promocode')->where(['owner_user_id' => Yii::$app->user->id, 'user_id' => Yii::$app->user->id, 'register' => 1])->one();
        
        if (empty($promocode)) {
            $model = new Promocodes();
            $model->promocode = $model->random_string(10);
            $model->value = 200;
            $model->owner_user_id = Yii::$app->user->id;
            $model->user_id = Yii::$app->user->id;
            $model->status = 0;
            $model->register = 1;
            $model->save();
            
            Yii::$app->session->setFlash('success', 'Ваш промокод был успешно сгенерирован, теперь вы можете им возпользоваться!');
            $this->redirect('friends-promocode');
        }
        
        return false;
    }
    
    public function actionActivatePromocode()
    {
        if ($promoField = Yii::$app->request->post('promo_field')) {
            $promocode = Promocodes::find()->where(['promocode' => $promoField])->one();
        } else {
            Yii::$app->session->setFlash('error', 'Вы не ввели промокод');
            return $this->redirect(Yii::$app->request->referrer);
        }
        
        $maxNumOfUsersPromo = Yii::$app->params['maxNumOfUsersPromo'];
        
        if ($this->isPromocodeActivated($promoField) || $promocode->register == 1) {
            Yii::$app->session->setFlash('error', 'Этот промокод уже активирован, введите другой промокод');
            return $this->redirect(Yii::$app->request->referrer);
        }
        
        if ($this->currNumOfPromo() >= $maxNumOfUsersPromo) {
            Yii::$app->session->setFlash('error',
                'Вы уже активировали ' . $maxNumOfUsersPromo . ' дружеских промокодов.
            Это максимально возможное количество. 
            Но вы можете посылать свой промокод друзьям! Как только они его активируют, 
            вы тоже получите 200 рублей на счет.');
            return $this->redirect(Yii::$app->request->referrer);
        }
        
        if ($this->iAmPromocodeOwner($promoField) > 0) {
            Yii::$app->session->setFlash('error',
                'Вы не можете активировать свой собственный промокод.
            Но вы можете посылать свой промокод друзьям! 
            Как только они его активируют, вы тоже получите 200 рублей на счет.');
            return $this->redirect(Yii::$app->request->referrer);
        }
        
        if (!$promocode) {
            Yii::$app->session->setFlash('error', 'Введенный вами промокод не существует', false);
            return $this->redirect(Yii::$app->request->referrer);
        }
        
        if (Promocodes::activatePromocode($promocode)) {
            Yii::$app->session->setFlash('success', 'Промокод успешно активирован! Средства добавлены в ваш бумажник');
            return $this->redirect(Yii::$app->request->referrer);
        }
        
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    private function currNumOfPromo()
    {
        return Promocodes::find()
            ->where([
                'user_id' => Yii::$app->user->id,
                'status' => 1
            ])->count();
    }
    
    private function iAmPromocodeOwner($promocode)
    {
        return Promocodes::find()->where(['promocode' => $promocode, 'owner_user_id' => Yii::$app->user->id])->count();
    }
    
    private function isPromocodeActivated($promocode)
    {
        return Promocodes::find()->where([
            'promocode' => $promocode,
            'user_id' => Yii::$app->user->id,
            'status' => 1
        ])->count();
    }
}
