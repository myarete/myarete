<?php

namespace online\modules\promocodes;

class Promocodes extends \yii\base\Module
{
    public $controllerNamespace = 'online\modules\promocodes\controllers';

    public $label = 'Промокоды';

    public function init()
    {
        parent::init();
    }
}
