<?php

namespace online\modules\help;

class Help extends \yii\base\Module
{
    public $controllerNamespace = 'online\modules\help\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
