<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 03.08.15
 * Time: 21:04
 */

namespace online\modules\testing\forms;

use common\models\Theme;
use common\models\TestAnswer;
use common\models\Testing;
use common\models\TestingVsTestVariant;
use common\models\TestVariant;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class CreateTestingForm extends Model
{
    /**
     * @var array
     */
    public $quantity = [];

    /**
     * @var string
     */
    public $name = 'Тестирование';

    /**
     * @var Testing
     */
    private $_testing;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['quantity', 'checkQuantity'],

            ['name', 'required'],
        ];
    }

    /**
     *
     */
    public function checkQuantity()
    {
        foreach ($this->quantity as $id => $quantity)
        {
            if ((int)$quantity > 0)
                return;
        }
        $this->addError('quanity', 'Вы должны указать количество');
    }

    /**
     * @return bool
     */
    public function save($testing_id = null)
    {
        if (!$this->validate()) {
            return false;
        }

        $themes = Theme::find()->where(['in', 'id', array_keys($this->quantity)])->orderBy('lft')->indexBy('id')->all();
        $testingVsTestVariant = [];


        if($testing_id) {
            $this->_testing = Testing::find()->where('id = :id', [':id' => $testing_id])->one();
            $testingVsTestVariant = TestingVsTestVariant::find()
                                                ->select('test_variant_id')
                                                ->where('testing_id = :testing_id', [':testing_id' => $testing_id])
                                                ->all();
            $testingVsTestVariant = ArrayHelper::getColumn($testingVsTestVariant, 'test_variant_id');
        } else {
            $this->_testing = new Testing;
            $this->_testing->name = $this->name;
            $this->_testing->user_id = Yii::$app->user->id;
            $this->_testing->save(false);
        }

        $variants = [];

        foreach ($this->quantity as $id => $quantity)
        {
            if ($quantity <= 0)
                continue;

            $theme = $themes[$id];
            $variants += TestVariant::find()
                ->joinWith(['theme', 'test'])
                ->where('theme.lft >= :lft AND theme.rgt <= :rgt AND root = :root')
                ->andWhere(['not in', 'test_variant.id', $testingVsTestVariant])
                ->andWhere(['test.is_approved' => (int)true])
                ->params(['lft' => $theme->lft, 'rgt' => $theme->rgt, 'root' => $theme->root])
                ->limit($quantity)
                ->indexBy('id')
                ->orderBy('rand()')
                ->all();
        }

        foreach ($variants as $variant)
        {
            $countRight = TestAnswer::find()->where(['test_id' => $variant->test->id, 'is_right' => 1])->count();
            if(count($variant->test->answers) >= 2 && $countRight >= 1) {
                $model = new TestingVsTestVariant;
                $model->testing_id = $this->_testing->id;
                $model->test_variant_id = $variant->id;
                $model->save(false);
            }
        }

        return true;
    }

    /**
     * @return Testing
     */
    public function getTesting()
    {
        return $this->_testing;
    }
}
