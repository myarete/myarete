<?php

namespace online\modules\testing;

class Testing extends \yii\base\Module
{
    public $controllerNamespace = 'online\modules\testing\controllers';

    public function init()
    {
        parent::init();
    }
}
