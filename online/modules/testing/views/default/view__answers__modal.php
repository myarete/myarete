<?php

use common\models\TestComplexity;
use common\models\Theme;
use kartik\tree\TreeViewInput;
use admin\widgets\ckeditor\CKEditor;
use yii\widgets\ActiveForm;

?>

<div class="modal inmodal" id="add-answer-modal">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Добавить ответ</h4>
            </div>

            <?php $form = ActiveForm::begin(['id' => 'add-answer-form', 'action' => '/testing/test-answer/save']) ?>

                <div class="modal-body">

                    <!-- id -->
                    <?= $form->field($answerModel, 'id')->label(false)->hiddenInput() ?>

                    <!-- exercise_id -->
                    <?= $form->field($answerModel, 'test_id')->label(false)->hiddenInput() ?>
                    <div>
                        <?= $form->field($answerModel, 'is_right')->checkbox(['checked' => false]); ?>
                    </div>

                    <div>
                        <?= $form->field($answerModel, 'answer')->widget(CKEditor::className(), [
                            'name' => 'answer',
                            'options' => ['rows' => 6],
                            'enableFinder' => true,
                            'clientOptions' => [
                                'toolbar' => [
                                    ['Bold', 'Italic', 'Underline'],
                                    ['BulletedList', 'NumberedList'],
                                    ['Format', 'Image', 'Mathjax'],
                                    ['Source'],
                                ],
                                'height' => 100,
                                'allowedContent' => true,
                                'extraPlugins' => 'mathjax',
                                'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
                            ],
                        ]) ?>
                    </div>

                    <div>
                        <?= $form->field($answerModel, 'comment')->widget(CKEditor::className(), [
                            'name' => 'comment',
                            'options' => ['rows' => 6],
                            'enableFinder' => true,
                            'clientOptions' => [
                                'toolbar' => [
                                    ['Bold', 'Italic', 'Underline'],
                                    ['BulletedList', 'NumberedList'],
                                    ['Format', 'Image', 'Mathjax'],
                                    ['Source'],
                                ],
                                'height' => 100,
                                'allowedContent' => true,
                                'extraPlugins' => 'mathjax',
                                'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
                            ],
                        ]) ?>
                    </div>

                </div>
                <div class="modal-footer">
                    <a href class="btn btn-white" data-dismiss="modal">Закрыть</a>
                    <a href class="btn btn-primary submit-button">Сохранить</a>
                </div>

            <?php $form->end() ?>

        </div>
    </div>
</div>