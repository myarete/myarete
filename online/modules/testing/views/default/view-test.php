<?php

use yii\helpers\Html;
use common\components\other\StripTags;

if(!$model) return;

$favorite = null;

foreach($model->variants as $variant)
    if($variant->testFavorite)
        $favorite = $variant;
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
            <div class="side-block">
                <h2>Тест</h2>

                <hr>

                <div class="exercise-header">
                    <div class="row">
                        <div class="col-xs-9">
                            <strong>Тест №<?=$model->id?></strong>.
                            <?= $model->name ?>
                        </div>
                        <div class="col-xs-3 form-inline" style="text-align:right">
                            <?= Html::a(
                                'Избранное',
                                [($favorite ? '/testing/favorite/delete' : '/testing/favorite/add'),
                                    'id' => ($favorite ? $favorite->testFavorite->id : $variant->id)],
                                [
                                    'class' => 'btn btn-success btn-xs '.(($favorite) ? 'favorite' : 'favorite-off'),
                                    'title' => ($favorite ? 'Удалить из избранного' : 'Добавить в избранное')
                                ]
                            ) ?>
                        </div>
                    </div>
                </div>

                <div>
                    <?=$model->question?>
                </div>

                <?php if($model->type->id == 1): ?>
                    <div class="radio-answer-group">
                        <?php foreach ($model->answers as $answer) : ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label for="answer_<?=$answer->test_id ?>_<?=$answer->id ?>">
                                        <input id="answer_<?= $answer->test_id ?>_<?=$answer->id ?>" type="radio" ng-model="radioTest[<?= $answer->test_id ?>]" value="<?= $answer->id ?>">
                                        <?= StripTags::get($answer->answer) ?>
                                    </label>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif ?>
                <?php if($model->type->id == 2): ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?= Html::label('Ответ:', 'answer', ['class' => 'form-label']) ?>
                                <?= Html::textInput('answer', '', ['id' => 'answer', 'class' => 'form-control']) ?>
                            </div>
                        </div>
                    </div>
                <?php endif ?>

                <?php if($model->variants && $testings): ?>
                    <div>
                        <?= Html::a('Добавить в тестирование', "#testing-{$model->id}", ['data-toggle' => 'collapse']) ?>
                        <?= Html::beginTag('div', ['class' => 'collapse', 'id' => "testing-{$model->id}"])?>
                        <?= Html::beginForm(['add-to-testing']) ?>
                        <?= Html::hiddenInput('variant_id', $model->variants[rand(0, count($model->variants) - 1)]->id) ?>
                        <ul style="list-style:none">
                        <?php foreach($testings as $testing): ?>
                            <li>
                                <?= Html::checkbox('testings[]', false, ['value' => $testing->id]) ?> <?= Html::label($testing->name) ?>
                            </li>
                        <?php endforeach; ?>
                            <li><?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?></li>
                        </ul>
                        <?= Html::endForm() ?>
                        <?= Html::endTag('div') ?>
                    </div>
                <?php endif; ?>
            </div>
            <br><br>
        </div>
    </div>
</div>