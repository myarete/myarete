<?php

use common\models\Testing;
use yii\helpers\Html;

$this->title = 'Сохранённые тестирования';
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
            <div class="side-block">
                <h2>Сохранённые тестирования</h2>

                <hr>

                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => '{items}',
                    'options' => ['class' =>'testing-table'],
                    'columns' => [
                        [
                            'class' => \yii\grid\SerialColumn::className(),
                        ],
                        [
                            'attribute' => 'name',
                            'contentOptions' => ['class' => 'testing-name'],
                            'format' => 'raw',
                            'enableSorting' => false,
                            'value' => function($model, $key) {
                                $is_note = $model->note ? 'style="display:none"' : '';
                                $empty_note = $model->note ? '' : 'style="display:none"';
                                return Html::a($model->name, $model->getUrl()).' '.Html::a($model->note ? '<i class="glyphicon glyphicon-envelope"></i>' : '', '#', [
                                        'data' => [
                                            'toggle' => 'modal',
                                            'target' => '#show-note-'.$key
                                        ]
                                    ]).
                                    '<div class="modal fade" id="show-note-'.$key.'" data-backdrop="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">Просмотреть заметку</h4>
                                                </div>'.
                                                Html::beginForm('/testing/default/note').
                                                    Html::hiddenInput('testing_id', $model->id).'
                                                    <div class="modal-body">
                                                        <div id="view-note-'.$key.'" '.$empty_note.'>
                                                            '.$model->note.'
                                                        </div>
                                                        <div id="edit-note-'.$key.'" '.$is_note.'>
                                                            '.Html::textarea('note', $model->note, ['class' => 'form-control']).'
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" id="save-btn-'.$key.'" class="btn btn-primary" '.$is_note.'>Сохранить</button>
                                                        <button type="button" id="edit-btn-'.$key.'" class="btn btn-primary" '.$empty_note.'>Редактировать</button>
                                                        <button type="button" id="delete-btn-'.$key.'" class="btn btn-primary">Удалить</button>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                                    </div>'
                                                .Html::endForm().'
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        $("#edit-btn-'.$key.'").click(function() {
                                            $("#view-note-'.$key.'").hide();
                                            $("#edit-note-'.$key.'").show();
                                            $("#save-btn-'.$key.'").show();
                                            $(this).hide();
                                        });
                
                                        $("#delete-btn-'.$key.'").click(function() {
                                            $("#edit-note-'.$key.' textarea").val("");
                                            $(this).closest("form").submit();
                                        });
                                    </script>';
                            },
                        ],
                    [
                      'label' => 'Тестов',
                      'format' => 'raw',
                      'value' => function($model) {
                        return count($model->testingTests);
                      }
                    ],
                        [
                            'contentOptions' => ['class' => 'action-column'],
                            'template' => '{update} {delete}',
                            'class' => \yii\grid\ActionColumn::className(),
                        ]
                    ],
                ]) ?>

            </div>
            <br><br>
        </div>
    </div>
</div>