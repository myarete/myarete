<?php

use common\components\other\History;
use common\components\other\StripTags;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

History::put(Yii::$app->user->identity->email, $testing->name);

$this->title = $testing->name;

//$this->registerJsFile('/js/plugins/angular/angular.min.js');
$this->registerJsFile('/js/html2canvas.js');
$this->registerJsFile('/js/FileSaver.min.js');
$this->registerJsFile('/js/canvas-to-blob.min.js');

$js = <<<JS
$('.replace-btn').click(function() {
    $('#ReplaceModal').modal();
    $('#ReplaceModal form').attr('action', '/testing/testing-test/replace?id=' + $(this).data('id'))
});
Zepto('#sortable').dragswap({
    element: 'div.draggable', // the child element you are targeting
    overClass: 'over', // class when element goes over another element
    moveClass: 'moving', // class when element is moving
    dropClass: 'drop', // the class to add when the element is dropped
    dropAnimation: true, // do you want to detect animation end?
    exclude: '.disabled',  // excluded elements selector, here we can add array of excluded classes ['.exclude', '.exclude2']
    dropComplete: saveOrder
});
$('#screenshot').click(function() {
    html2canvas($('.side-block-user'), {
        onrendered: function(canvas) {
            canvas.toBlob(function(blob) {
                saveAs(blob, $testing->name);
            }, 'image/png');
        }
    });
});
$('#printscreen').click(function() {
    var content = $('#sortable').html();

    $.post('/testing/default/print-screen', {content: content}, function(resp) {
        if(resp != '') location.href = '/testing/default/print-screen?id=' + '$testing->id' + '&tmpfile=' + resp;
    });
});
$('#showSetup').click(function() {
    $('#testingSetup').modal();
});
$('#setupBtn').click(function() {
    if($('#shuffleTests').is(':checked')) {
        $.post('/testing/default/shuffle-tests', {id: '$testing->id'});
        setTimeout(function() {
            location.reload();
        }, 500);
    }
});

function saveOrder()
{
    var data = [];
    $.each($('#sortable .draggable'), function(i, e) {
        data.push($(e).attr('id'));
    });
    $.post('/testing/default/save-order', {
        testing_id: '$testing->id',
        data: data
    });
}

saveOrder();

$('.replace-test').click(function(){

    if (!yii.confirm('Вы уверены?'))
        return false;

    $.get(this.href).success(function(response){
        if (response === true)
        {
            location.reload();
        }
        else
        {
            bootbox.alert({
                title: 'Ошибка',
                message: response.error
            })
        }
    });

    return false;
});

$('#similar-testing').click(function() {
    yii.prompt('Название тестирования', function(name) {
        var id = '$testing->id';
        $.post('/testing/default/create-similar', {id: id, name: name}, function(response) {
            location.href = '/testing/default/run/' + response;
        });
    });
});

$('#edit-btn').click(function() {
    $('#view-note').hide();
    $('#edit-note').show();
    $('#save-btn').show();
    $(this).hide();
});

$('#delete-btn').click(function() {
    var id = $(this).closest('form').find('input[type=hidden]').val();
    $.post('/testing/default/delete-note', {id: id}, function(response) {
        location.reload();
    });
});

app.controller('TestCtrl', function(scope, http, sce) {
    scope.radioTest = {};
    scope.submitTest = function(){
        if(Object.keys(scope.radioTest).length != scope.countTests){
            alert('Необходимо ответить на все тесты!');
            return;
        }
    
        var radioTest = {};
        $.each($('.radio-answer-group .row input:checked'), function(i, e) {
            radioTest[$(e).attr('id')] = $(e).val();
            $(this).closest('div').next('div').find('.comment').show();
        });
        $.each($('.radio-answer-group .row input[type=text]'), function(i, e) {
            var comment = $(this).parent().parent().next('div').find('.comment'),
                wrong = true;
    
            $.each($(e).parent().find('input[data-right=1]'), function(j, el) {
                if($(e).val() == $(el).val()) {
                    comment.html($(el).data('comment'));
                    comment.removeClass('test-row-wrong');
                    comment.addClass('test-row-right');
                    comment.show();
                    wrong = false;
                    return false;
                }
            });
    
            if(wrong) {
                comment.html($(e).parent().find('input[data-right=0]').data('comment'));
                comment.removeClass('test-row-right');
                comment.addClass('test-row-wrong');
                comment.show();
            }
        });
    
        http.post('/testing/default/submit-test', {
            result: radioTest,
            testing_id: scope.testingID
        }).success(function(data, status){
            scope.result = data;
            MathJax.Hub.Queue(['Typeset', MathJax.Hub]);
            MathJax.Hub.Queue(['Typeset', MathJax.Hub]);
        });
    }

    scope.getContent = function(html){
        return sce.trustAsHtml(html);
    }
});
JS;
$this->registerJs($js);
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
            <div class="side-block">
                <h2><?= $testing->name ?></h2>
                
                <div class="row">
                    <div class="col-sm-8 training-buttons">
                        <?= Html::button('Управление тестированием', ['id' => 'showSetup', 'class' => 'btn btn-primary btn-xs']) ?>
                        &nbsp;
                        <?= Html::button('Создать аналогичное тестирование <i class="glyphicon glyphicon-question-sign"></i>', [
                            'id' => 'similar-testing',
                            'class' => 'btn btn-primary btn-xs',
                            'title' => 'В новой тестировании будут другие тесты, но на те же подтемы, что и в старой',
                            'data' => [
                                'toggle' => 'tooltip'
                            ]
                        ]) ?>&nbsp;
                        <?= Html::button('Сделать скриншот', ['id' => 'screenshot', 'class' => 'btn btn-primary btn-xs']) ?>
                        &nbsp;
                        <?= Html::button('Версия для печати', ['id' => 'printscreen', 'class' => 'btn btn-primary btn-xs']) ?>
                        <br><br>
                        <?= Html::button($testing->note ? 'Посмотреть заметку' : 'Создать заметку', [
                            'class' => 'btn btn-primary btn-xs',
                            'data' => [
                                'toggle' => 'modal',
                                'target' => '#create-note'
                            ]
                        ]) ?>
                    </div>
                    <div class="col-sm-4 text-right">
                        <?= Html::a('<i class="glyphicon glyphicon-question-sign"></i> <span class="hints">Как пользоваться тестированиями?</span>', '#', ['data' => ['toggle' => 'modal', 'target' => '#use-testing']]) ?>
                    </div>
                </div>
                
                <div id="sortable" class=" testing-default-run" ng-controller="TestCtrl"
                     ng-init="countTests = <?= count($testing->testingTests) ?>; testingID = <?= $testing->id ?>">
                    <?php foreach ($testing->testingTests as $i => $model): ?>
                        <div class="draggable" id="id_<?= $model->id ?>">
                            <div class="row">
                                <div class="col-sm-1 col-xs-12 col-sm-1-5">
                                    <span class="num"><?= $i + 1 ?></span>
                                </div>
                                <div class="col-sm-11 col-xs-12 col-sm-10-5">
                                    <div class="test-header">
                                        <?= Html::a('Показать информацию о тесте', '#test-info-' . $i, ['class' => 'btn btn-xs btn-default', 'data' => ['toggle' => 'collapse']]) ?>
                                        <div id="test-info-<?= $i ?>" class="collapse">
                                            <br>
                                            <div class="row">
                                                <div class="col-xs-9 col-sm-8">
                                                    <strong>Тест #<?= $model->variant->test->id ?></strong>.
                                                    <?= $model->variant->test->name ?>
                                                </div>
                                                
                                                <div class="col-xs-3 col-sm-4 form-inline text-right exercise-buttons">
                                                    <?= Html::a(
                                                        'Избранное',
                                                        [($model->variant->testFavorite ? '/testing/favorite/delete' : '/testing/favorite/add'),
                                                            'id' => ($model->variant->testFavorite ? $model->variant->testFavorite->id : $model->test_variant_id)],
                                                        [
                                                            'class' => 'btn btn-success btn-xs ' . ($model->variant->testFavorite ? 'favorite' : 'favorite-off'),
                                                            'title' => ($model->variant->testFavorite ? 'Удалить из избранного' : 'Добавить в избранное')
                                                        ]
                                                    ) ?>
                                                    
                                                    <?php
                                                    if (!isset($no_edit))
                                                        echo Html::a(
                                                            'Заменить',
                                                            ['/testing/testing-test/replace', 'id' => $model->id],
                                                            [
                                                                'class' => 'btn btn-warning btn-xs replace-btn',
                                                                'onclick' => 'return false',
                                                                'data' => [
                                                                    'id' => $model->id
                                                                ]
                                                            ]
                                                        );
                                                    ?>
                                                    
                                                    <?php
                                                    if (!isset($no_edit))
                                                        echo Html::a(
                                                            '<i class="fa fa-trash"></i>',
                                                            ['/testing/testing-test/delete', 'id' => $model->id],
                                                            [
                                                                'class' => 'btn btn-danger btn-xs delete-btn',
                                                                'data' => [
                                                                    'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                                                ]
                                                            ]
                                                        );
                                                    ?>
                                                    
                                                    <?= Html::a('<i class="glyphicon glyphicon-cog"></i>', "javascript:void(0)", ['id' => 'exerciseMobShow' . $i, 'class' => 'exercise-mob-buttons-show']) ?>
                                                    
                                                    <div class="modal fade exercise-mob-buttons"
                                                         id="exerciseMobButtons<?= $i ?>" data-backdrop="true">
                                                        <div class="modal-dialog modal-sm">
                                                            <div class="modal-content">
                                                                <div class="modal-header text-left">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal"><span
                                                                            aria-hidden="true">×</span><span
                                                                            class="sr-only">Закрыть</span></button>
                                                                    <h4 class="modal-title">Настройка тренинга</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <?= Html::a('Избранное',
                                                                        [(($model->variant->testFavorite) ? '/testing/favorite/delete' : '/testing/favorite/add'),
                                                                            'id' => ($model->variant->testFavorite ? $model->variant->testFavorite->id : $model->test_variant_id)],
                                                                        [
                                                                            'class' => 'btn btn-success btn-xs ' . (($model->variant->testFavorite) ? 'favorite' : 'favorite-off'),
                                                                            'title' => ($model->variant->testFavorite ? 'Удалить из избранного' : 'Добавить в избранное')
                                                                        ]
                                                                    ) ?>
                                                                    
                                                                    <?php
                                                                    if (!isset($no_edit))
                                                                        echo Html::a(
                                                                            'Заменить',
                                                                            ['/testing/testing-test/replace', 'id' => $model->id],
                                                                            [
                                                                                'class' => 'btn btn-warning btn-xs replace-btn',
                                                                                'onclick' => 'return false',
                                                                                'data' => [
                                                                                    'id' => $model->id
                                                                                ]
                                                                            ]
                                                                        );
                                                                    ?>
                                                                    
                                                                    <?php
                                                                    if (!isset($no_edit))
                                                                        echo Html::a(
                                                                            '<i class="fa fa-trash"></i>',
                                                                            ['/testing/testing-test/delete', 'id' => $model->id],
                                                                            [
                                                                                'class' => 'btn btn-danger btn-xs delete-btn',
                                                                                'data' => [
                                                                                    'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                                                                ]
                                                                            ]
                                                                        );
                                                                    ?>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-white"
                                                                            data-dismiss="modal">Отмена
                                                                    </button>
                                                                </div>
                                                                <?= Html::endForm() ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php $this->registerJs("
                                                        $('#exerciseMobShow$i').click(function () {
                                                            $('#exerciseMobButtons$i').modal();
                                                        });
                                                        ", yii\web\View::POS_LOAD);
                                                    ?>
                                                </div>
                                            </div>
                                            <br>
                                            <div>
                                                <strong>Тема:</strong> <?= $model->variant->theme->getFullName() ?>
                                            </div>
                                            <div>
                                                <strong>Сложность:</strong> <?= $model->variant->complexity ?>
                                            </div>
                                            
                                            <?php $rating = $model->variant->test->getRate()->where(['user_id' => $testing->user_id])->one(); ?>
                                            
                                            <?php if (!$rating): ?>
                                                <button id="rate-btn-<?= $model->variant->test->id ?>"
                                                        class="btn btn-info btn-xs"
                                                        onclick="
                                                            $('.rate-<?= $model->variant->test->id ?>').show();
                                                            $('#close-rate-<?= $model->variant->test->id ?>').show();
                                                            $(this).hide()">Оценить задачу
                                                </button>
                                            <?php endif; ?>
                                            
                                            <div
                                                class="rate rate-<?= $model->variant->test->id; ?>" <?= (($rating) ? '' : 'style="display:none;"') ?>>
                                                <strong>Оцененная сложность
                                                    задачи: </strong> <?= ($rating) ? $rating->rating . '/10' : '' ?>
                                                <?php for ($n = 1; $n <= 10; $n++): ?>
                                                    <?= Html::a('<i class="fa fa-star' . (($rating && $n <= $rating->rating) ? '' : '-o') . '"></i>', Url::to([
                                                        '/test/rate/add',
                                                        'test_id' => $model->variant->test->id,
                                                        'class' => $testing->user->profile->state_id,
                                                        'rating' => $n,
                                                        'testing_id' => $testing->id,
                                                    ])) ?>
                                                <?php endfor; ?>
                                                <button class="btn btn-danger btn-xs btn-close-rate"
                                                        id="close-rate-<?= $model->variant->test->id ?>"
                                                        onclick="
                                                            $('.rate-<?= $model->variant->test->id ?>').hide();
                                                            $('#rate-btn-<?= $model->variant->test->id ?>').show();
                                                            $(this).hide()">Скрыть
                                                </button>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    
                                    <div>
                                        <?= $model->variant->test->question ?>
                                    </div>
                                    
                                    <div class="radio-answer-group">
                                        <?php if ($model->variant->test->type->id == 1): ?>
                                            <?php foreach ($model->variant->test->answers as $answer) : ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="answer_<?= $answer->test_id ?>_<?= $answer->id ?>">
                                                            <input
                                                                id="answer_<?= $answer->test_id ?>_<?= $answer->id ?>"
                                                                type="radio"
                                                                ng-model="radioTest[<?= $answer->test_id ?>]"
                                                                value="<?= $answer->id ?>">
                                                            <?= StripTags::get($answer->answer) ?>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div
                                                            class="comment <?= $answer->is_right ? 'test-row-right' : 'test-row-wrong' ?>"><?= $answer->comment ?></div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php elseif ($model->variant->test->type->id == 2): ?>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <?= Html::label('Ответ:', 'answer_' . $model->variant->test->id . '_' . $model->variant->test->rightAnswer->id, ['class' => 'form-label']) ?>
                                                        <input type="text"
                                                               id="answer_<?= $model->variant->test->id ?>_<?= $model->variant->test->rightAnswer->id ?>"
                                                               ng-model="radioTest[<?= $model->variant->test->id ?>]"
                                                               class="form-control">
                                                        <?php foreach ($model->variant->test->answers as $answer): ?>
                                                            <input type="hidden"
                                                                   value="<?= trim(strip_tags($answer->answer)) ?>"
                                                                   data-right="<?= $answer->is_right ?>"
                                                                   data-comment="<?= $answer->comment ?>">
                                                        <?php endforeach ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="comment"></div>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                    <?php endforeach; ?>
                    
                    <button class="btn btn-success test-button" ng-click="submitTest()">Выполнить тест</button>
                
                </div>
                
                <div class="modal fade testing-setup" id="testingSetup" data-backdrop="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                                <h4 class="modal-title">Настройка тестирования</h4>
                            </div>
                            <div class="modal-body replace_item_modal_body">
                                <ul>
                                    <li><label><input type="checkbox" id="shuffleTests"> Перемешать тесты</label></li>
                                </ul>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="setupBtn" class="btn btn-white" data-dismiss="modal">Ок
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal fade" id="ReplaceModal" data-backdrop="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button>
                                <h4 class="modal-title">Заменить на:</h4>
                            </div>
                            <?= Html::beginForm() ?>
                            <div class="modal-body replace_item_modal_body">
                                <div>
                                    <label>
                                        <input type="radio" name="theme" value="section">
                                        Из того же раздела
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="radio" name="theme" value="theme">
                                        Из той же темы
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="radio" name="theme" value="subtheme" checked>
                                        Из той же подтемы
                                    </label>
                                </div>
                                <hr>
                                <div>
                                    <label>
                                        <input type="radio" name="type" value="same">
                                        Такого же типа
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="radio" name="type" value="variants">
                                        Тип "Варианты ответов"
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="radio" name="type" value="string">
                                        Тип "Ответ строкой"
                                    </label>
                                </div>
                                <hr>
                                <div>
                                    <label>
                                        <input type="radio" name="complexity" value="up">
                                        Посложнее
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="radio" name="complexity" value="down">
                                        Попроще
                                    </label>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Отмена</button>
                                <button type="submit" id="SubmitBtn" class="btn btn-primary">Заменить</button>
                            </div>
                            <?= Html::endForm() ?>
                        </div>
                    </div>
                </div>
                
                <div class="modal fade" id="use-testing" data-backdrop="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                                <h4 class="modal-title">Как пользоваться тестированиями?</h4>
                            </div>
                            <div class="modal-body">
                                <p><?= \common\models\Html::get('use_testing') ?></p>
                                <p><?= Html::a('Подробнее', ['/testing/default/page', 'view' => 'use_testing_detail']) ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal fade" id="create-note" data-backdrop="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                                <h4 class="modal-title"><?= $testing->note ? 'Редактировать заметку' : 'Создать заметку' ?></h4>
                            </div>
                            <?php $form = ActiveForm::begin(['action' => '/testing/default/note']) ?>
                            <?= $form->field($testing, 'id')->hiddenInput()->label(false) ?>
                            <div class="modal-body">
                                <div id="view-note" <?= $testing->note ? '' : 'style="display:none"' ?>>
                                    <?= $testing->note ?>
                                </div>
                                <div id="edit-note" <?= $testing->note ? 'style="display:none"' : '' ?>>
                                    <?= $form->field($testing, 'note')->widget(CKEditor::className(), [
                                        'options' => ['rows' => 10],
                                        'preset' => 'custom',
                                        'clientOptions' => [
                                            'toolbarGroups' => [
                                                ['name' => 'undo'],
                                                ['name' => 'styles'],
                                                ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                                                ['name' => 'align'],
                                                ['name' => 'colors'],
                                            ]
                                        ]
                                    ])->label(false) ?>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" id="save-btn"
                                        class="btn btn-primary" <?= $testing->note ? 'style="display:none"' : '' ?>>
                                    Сохранить
                                </button>
                                <button type="button" id="edit-btn"
                                        class="btn btn-primary" <?= $testing->note ? '' : 'style="display:none"' ?>>
                                    Редактировать
                                </button>
                                <button type="button" id="delete-btn" class="btn btn-primary">Удалить</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                            </div>
                            <?php ActiveForm::end() ?>
                        </div>
                    </div>
                </div>
            </div>
            <br><br>
        </div>
    </div>
</div>
