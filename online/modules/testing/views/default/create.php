<?php

use common\models\TestVariant;
use yii\helpers\Html;

$js = <<<JS
$('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Свернуть');
$('.tree li.parent_li > div span').on('click', function (e) {
    var children = $(this).parent().parent('li.parent_li').find(' > ul > li');
    if (children.is(":visible")) {
        children.hide('fast');
        $(this).attr('title', 'Развернуть').find(' > i').addClass('glyphicon-plus').removeClass('glyphicon-minus');
    } else {
        children.show('fast');
        $(this).attr('title', 'Свернуть').find(' > i').addClass('glyphicon-minus').removeClass('glyphicon-plus');
    }
    e.stopPropagation();
    $.each($('.tree li > div:visible'), function(i, e) {
        if((i % 2) == 0)
            $(e).css('background', '#fff');
        else
            $(e).css('background', '#f9f9f9');
    });
});

$.each($('.subtheme-list'), function(i, e) {
    if($(e).find('li').length == 0) {
        $(e).parent().remove();
    }
});
$.each($('.theme-list'), function(i, e) {
    if($(e).find('li').length == 0) {
        $(e).parent().remove();
    }
});
JS;
$this->registerJs($js);
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
            <div class="side-block">
                <h2><?= $theme->subject->name ?> - <?= $theme->class ?> класс</h2>
                
                <hr>
                <div class="row">
                    <div class="col-sm-12 text-right">
                        <?= Html::a('<i class="glyphicon glyphicon-question-sign"></i> <span class="hints">Как и зачем создавать тестирования?</span>', '#', ['data' => ['toggle' => 'modal', 'target' => '#create-testing-help']]) ?>
                    </div>
                </div>
                <hr>
                <form method="post" id="create-testing-form">
                    
                    <?php if (count($sections)): ?>
                        
                        <?= Html::errorSummary($model, ['class' => 'error-summary']) ?>
                        
                        <?= Html::activeHiddenInput($model, 'name', ['id' => 'testing-name']) ?>
                        
                        <div class="tree">
                            <ul class="section-list">
                                <?php foreach ($sections as $section): ?>
                                    <li>
                                        <div>
                                            <span class="indicator"><i class="glyphicon glyphicon-plus"></i></span><span
                                                class="section-name name"> <?= $section->name ?></span>
                                            <?= Html::activeInput(
                                                'number',
                                                $model,
                                                "quantity[$section->id]",
                                                ['class' => 'form-control', 'placeholder' => 'Кол-во']
                                            ) ?>
                                        </div>
                                        <ul class="theme-list">
                                            <?php foreach ($section->children(1)->all() as $theme): ?>
                                                <li>
                                                    <div class="tree-item">
                                                        <span class="indicator"><i class="glyphicon glyphicon-plus"></i></span><span
                                                            class="theme-name name"> <?= $theme->name ?></span>
                                                        <?= Html::activeInput(
                                                            'number',
                                                            $model,
                                                            "quantity[$theme->id]",
                                                            ['class' => 'form-control', 'placeholder' => 'Кол-во']
                                                        ) ?>
                                                    </div>
                                                    <ul class="subtheme-list">
                                                        <?php foreach ($theme->children(1)->all() as $subtheme): ?>
                                                            <?php if (TestVariant::findAll(['theme_id' => $subtheme->id])): ?>
                                                                <li>
                                                                    <div class="tree-item">
                                                                        <span
                                                                            class="name"> <?= $subtheme->name ?></span>
                                                                        <?= Html::activeInput(
                                                                            'number',
                                                                            $model,
                                                                            "quantity[$subtheme->id]",
                                                                            ['class' => 'form-control', 'placeholder' => 'Кол-во']
                                                                        ) ?>
                                                                    </div>
                                                                </li>
                                                            <?php endif ?>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-success pull-right">Создать</button>
                            </div>
                        </div>
                    <?php else: ?>
                        <p>Нет разделов</p>
                    <?php endif; ?>
                </form>
                
                <div class="modal fade" id="create-testing-help" data-backdrop="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                                <h4 class="modal-title">Как и зачем создавать тестирования?</h4>
                            </div>
                            <div class="modal-body">
                                <p><?= \common\models\Html::get('create_testing_help') ?></p>
                                <p><?= Html::a('Подробнее', ['/testing/default/page', 'view' => 'create_testing_help_detail']) ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br><br>
        </div>
    </div>
</div>
