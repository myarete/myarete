<?php
$this->title = $testing->name;

$js = <<<JS
    $('.btn').remove();
JS;
$this->registerJs($js);
?>
<div id="sortable" class=" testing-default-run">
    <h2><?= $testing->name ?></h2>
    <?php echo file_get_contents($tmpfile); ?>
</div>
<?php unlink($tmpfile) ?>
