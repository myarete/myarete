<?php
  use common\models\TestVariantFavoriteSearch;
  use yii\grid\GridView;
  use common\models\Test;
  use yii\helpers\Html;

?>

<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="side-block">
        <h2>Избранные тесты</h2>

        <hr>

        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
                [
                    'header' => '#',
                    'format' => 'raw',
                    'value'  => function (TestVariantFavoriteSearch $model)
                    {
                        return $model->testVariant->test->id;
                    },
                ],
                [
                    'header' => 'Название',
                    'format' => 'raw',
                    'value'  => function (TestVariantFavoriteSearch $model)
                {
                    return Html::a($model->testVariant->test->name, ['/testing/default/search-test', 'id' => $model->testVariant->test->id]);
                },
                ],
                [
                    'contentOptions' => ['class' => 'action-column'],
                    'template'       => '{delete}',
                    'class'          => \yii\grid\ActionColumn::className(),
                ],
            ],
        ]); ?>
    </div>
    <br><br>
</div>

