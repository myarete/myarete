<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 19.07.15
 * Time: 14:03
 */

namespace online\modules\testing\controllers;

use common\components\yii\base\BaseController;
use common\models\TestingVsTestVariant;
use common\models\TestVariant;
use Yii;
use yii\filters\AccessControl;

class TestingTestController extends BaseController
{
    public $layout = '/old';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = TestingVsTestVariant::findOne($id);

        if ($model === null || $model->testing->user_id !== Yii::$app->user->id)
            $this->pageNotFound();

        $model->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return array|bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReplace($id)
    {
        $oldModel = TestingVsTestVariant::findOne($id);

        if ($oldModel === null)
            $this->pageNotFound();

        if ($oldModel->testing->user_id !== Yii::$app->user->id)
            $this->forbidden();

        $allSubTheme = [];
        $themeIds = [];

		switch(Yii::$app->request->post('theme')) {
			case 'subtheme':
				$allSubTheme[] = $oldModel->variant->theme;
			break;
			case 'theme':
				$allSubTheme = $oldModel->variant->theme->parents(1)->one()->children(1)->all();
			break;
			case 'section':
				$allSection = $oldModel->variant->theme->parents(2)->one()->children(1)->all();
				foreach($allSection as $section) {
					$subThemes = $section->children(1)->all();
					foreach ($subThemes as $subTheme) {
						$allSubTheme[] = $subTheme;
					}
				}
			break;
		}

		foreach ($allSubTheme as $subTheme) {
            $themeIds[] = $subTheme->id;
        }

        $query = TestVariant::find()
            ->joinWith(['test'])
            ->where([
                'test.subject_id' => $oldModel->variant->test->subject_id,
                'test.is_approved' => (int)true,
            ])
            ->andWhere([
                'in',
                'theme_id',
                $themeIds
            ])
            ->andWhere([
                'not in',
                'test_variant.id',
                Yii::$app->db
                    ->createCommand('select test_variant_id from testing_vs_test_variant where testing_id = :testing_id')
                    ->bindValue(':testing_id', $oldModel->testing_id)
                    ->queryColumn()
            ]);

        if(Yii::$app->request->post('type') == 'variants')
 			$query = $query->andWhere(['test.type_id' => 1]);
        elseif(Yii::$app->request->post('type') == 'string')
 			$query = $query->andWhere(['test.type_id' => 2]);
        else
            $query = $query->andWhere(['test.type_id' => $oldModel->variant->test->type_id]);

        if(Yii::$app->request->post('complexity') && Yii::$app->request->post('complexity') == 'up')
			$query = $query->andWhere(['>', 'complexity', $oldModel->variant->complexity]);

		if(Yii::$app->request->post('complexity') && Yii::$app->request->post('complexity') == 'down')
			$query = $query->andWhere(['<', 'complexity', $oldModel->variant->complexity]);

		$newModel = $query
			->orderBy('RAND()')
			->one();

        if ($newModel && $oldModel->test_variant_id != $newModel->id) {
            $oldModel->test_variant_id = $newModel->id;
            $oldModel->save(false, ['test_variant_id']);
        } else {
            Yii::$app->session->setFlash('warning', 'Подходящего теста не найдено');
        }

        return $this->redirect(['/testing/default/run', 'id' => $oldModel->testing_id]);
    }
}
