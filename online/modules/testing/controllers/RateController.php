<?php

namespace online\modules\testing\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\TestRating;

class RateController extends \yii\web\Controller
{
    public $layout = '/old';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }


    /**
     * @param $exercise_id
     * @param $class
     * @param $rating
     * @return string
     */
    public function actionAdd($test_id, $class, $rating)
    {
        $model = TestRating::findOne([
            'user_id' => Yii::$app->user->id,
            'test_id' => $test_id,
        ]);

        if(!$model)
            $model = new TestRating();

        $model->user_id = Yii::$app->user->id;
        $model->test_id = $test_id;
        $model->class = $class;
        $model->rating = $rating;

        $model->save();

        return $this->redirect(Yii::$app->request->referrer);
    }
}
