<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 19.07.15
 * Time: 14:03
 */

namespace online\modules\testing\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\ViewAction;
use common\components\yii\base\BaseController;
use common\models\Theme;
use common\models\Subject;
use online\modules\testing\forms\CreateTestingForm;
use common\models\Test;
use common\models\TestAnswer;
use common\models\Testing;
use common\models\TestingSearch;
use common\models\TestingVsTestVariant;
use common\models\TestVariant;

class DefaultController extends BaseController
{
    public $layout = '/old';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'page' => [
                'class' => ViewAction::className(),
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionChooseClassSubject()
    {
        if (Yii::$app->user->can('teacher'))
            $query = Subject::find()
                ->innerJoinWith('exercise')
                ->where(['or', ['is_active' => 1], ['is_active' => 2]]);
        elseif (!Yii::$app->user->isGuest)
            $query = Subject::find()
                ->innerJoinWith('exercise')
                ->where(['is_active' => 2]);

        return $this->render('choose_class_subject', [
            'subjects' => $query->all(),
        ]);
    }

    /**
     * @param $class
     * @param $subject_id
     * @return string
     */
    public function actionCreate($class, $subject_id)
    {
        $theme = Theme::find()->where(['class' => $class, 'subject_id' => $subject_id])->one();
        if ($theme === null) {
            $this->pageNotFound();
        }

        $model = new CreateTestingForm();

        if ($model->load($_POST) && $model->save()) {
            $this->redirect($model->getTesting()->getUrl());
        }

        return $this->render('create', [
            'theme' => $theme,
            'sections' => $theme->children(1)->all(),
            'model' => $model,
        ]);
    }

    public function actionCreateSimilar()
    {
        Yii::$app->response->format = 'json';

        $id = Yii::$app->request->post('id');
        $name = Yii::$app->request->post('name');

        $testing = Testing::findOne($id);
        $new_testing = new Testing;
        $new_testing->attributes = $testing->attributes;
        $new_testing->name = $name;
        $new_testing->order_by = '';
        $new_testing->save();

        $subQuery = (new Query())->select('test_variant_id')->from('testing_vs_test_variant')->where(['testing_id' => $new_testing->id]);
        foreach ($testing->testingTests as $variant) {
            $new_variant = TestVariant::find()
                ->where(['theme_id' => $variant->variant->theme_id])
                ->andWhere(['not', ['id' => $subQuery]])
                ->orderBy('rand()')
                ->one();
            $new_testing_vs_test_variant = new TestingVsTestVariant;
            $new_testing_vs_test_variant->testing_id = $new_testing->id;
            $new_testing_vs_test_variant->test_variant_id = $new_variant->id;
            $new_testing_vs_test_variant->save(false);
        }

        return $new_testing->id;
    }

    /**
     * @param $class
     * @param $subject_id
     * @return string
     */
    public function actionAddtest($class, $subject_id, $testing_id)
    {
        $theme = Theme::find()->where(['class' => $class, 'subject_id' => $subject_id])->one();
        if ($theme === null) {
            $this->pageNotFound();
        }

        $model = new CreateTestingForm();
        if ($model->load($_POST) && $model->save($testing_id)) {
            $this->redirect($model->getTesting()->getUrl());
        }

        return $this->render('addtest', [
            'theme' => $theme,
            'sections' => $theme->children(1)->all(),
            'model' => $model,
            'name' => Testing::find()->where('id = :id', [':id' => $testing_id])->one()->name,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRun($id)
    {
        $testing = Testing::findOne($id);

        if ($testing === null)
            $this->pageNotFound();

        return $this->render('run', [
            'testing' => $testing,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = Testing::find()
            ->where([
                'id' => $id,
                'user_id' => Yii::$app->user->id,
            ])
            ->one();

        if ($model === null)
            $this->pageNotFound();

        $model->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return bool
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        Yii::$app->response->format = 'json';

        $model = Testing::find()
            ->where([
                'id' => $id,
                'user_id' => Yii::$app->user->id,
            ])
            ->one();

        if ($model === null)
            $this->pageNotFound();

        $model->name = Yii::$app->request->get('name');

        $model->update(true, ['name']);

        return $model->attributes;
    }

    /**
     * @return string
     */
    public function actionSaved()
    {
        $searchModel = new TestingSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('saved', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return array|bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReplaceTestVariant($id)
    {
        $oldModel = TestingVsTestVariant::findOne($id);
        if ($oldModel === null)
            $this->pageNotFound();

        $newModel = TestVariant::find()
            ->joinWith(['test'])
            ->where([
                'test.subject_id' => $oldModel->variant->test->subject_id,
                'class' => $oldModel->variant->class,
                'theme_id' => $oldModel->variant->theme_id,
                'complexity' => $oldModel->variant->complexity,
            ])
            ->andWhere('test_variant.id != :id', [':id' => $oldModel->test_variant_id])
            ->one();

        Yii::$app->response->format = 'json';

        if ($newModel === null)
            return ['error' => 'Похожей задачи не найдено'];

        $oldModel->test_variant_id = $newModel->id;
        return $oldModel->save(false, ['exercise_variant_id']);
    }

    public function actionSubmitTest()
    {
        $req = json_decode(Yii::$app->request->getRawBody(), true);

        if (!array_key_exists('testing_id', $req) || !array_key_exists('result', $req))
            return json_encode([
                'error' => 'Error data!'
            ]);

        $countTests = TestingVsTestVariant::find()->where(['testing_id' => $req['testing_id']])->count();
        if (count($req['result']) != $countTests) return json_encode(['error' => 'Необходимо ответить на все тесты!']);

        $result = [];
        $resCount = 0;
        foreach ($req['result'] as $test_id => $answer_id) {
            preg_match('/answer_(\d+)_/', $test_id, $match);
            $test_id = $match[1];
            $answer = TestAnswer::findOne(['id' => $answer_id]);
            $rightAnswer = TestAnswer::findOne(['test_id' => $test_id, 'is_right' => 1]);
            $test = Test::findOne(['id' => $test_id]);
            $result[] = [
                'test_id' => $answer->test_id,
                'name' => $test->name,
                'answer' => $answer->answer,
                'comment' => $answer->comment,
                'is_right' => $answer->is_right
            ];

            if ($answer->is_right) $resCount++;
        }

        return json_encode([
            'result_detail' => $result,
            'result' => $resCount . '/' . $countTests
        ]);
    }

    public function actionSubmitOneTest()
    {
        $req = json_decode(Yii::$app->request->getRawBody(), true);

        if (!array_key_exists('test_id', $req) || !array_key_exists('result', $req))
            return json_encode([
                'error' => 'Error data!'
            ]);

        $test = Test::findOne(['id' => $req['test_id']]);
        //$rightAnswer = TestAnswer::findOne(['test_id' => $req['test_id'], 'is_right' => 1]);
        $answer = TestAnswer::findOne(['id' => $req['result']]);

        $result = [
            'test_id' => $answer->test_id,
            'name' => $test->name,
            'answer' => $answer->answer,
            'comment' => $answer->comment,
            'is_right' => $answer->is_right
        ];

        return json_encode($result);
    }

    public function actionSearchTest($id)
    {
        $model = Test::findOne($id);

        if (!$model) Yii::$app->session->setFlash('warning', 'Такого теста не существует!');

        return $this->render('view-test', [
            'model' => $model,
            'testings' => Testing::findAll(['user_id' => Yii::$app->user->id])
        ]);
    }

    public function actionAddToTesting()
    {
        $variant_id = Yii::$app->request->post('variant_id');

        if (Yii::$app->request->post('testings')) {
            foreach (Yii::$app->request->post('testings') as $testing_id) {

                $model = TestingVsTestVariant::findAll(['test_variant_id' => $variant_id, 'testing_id' => $testing_id]);

                if (!$model) {
                    $model = new TestingVsTestVariant;
                    $model->test_variant_id = $variant_id;
                    $model->testing_id = $testing_id;
                    $model->save(false);
                    $testing = Testing::findOne($testing_id);
                    $testing->order_by = '';
                    $testing->save();
                }
            }
            Yii::$app->session->setFlash('success', 'Тест добавлен');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionSaveOrder()
    {
        $model = Testing::findOne(Yii::$app->request->post('testing_id'));
        $model->order_by = implode(',', Yii::$app->request->post('data'));
        $model->save();
    }

    public function actionPrintScreen($id = null, $tmpfile = null)
    {
        $this->layout = '/print';

        if ($content = Yii::$app->request->post('content')) {
            $tmpfile = tempnam('/tmp', 'log');
            file_put_contents($tmpfile, $content);
            return $tmpfile;
        }

        $testing = Testing::findOne($id);

        return $this->render('printscreen', [
            'testing' => $testing,
            'tmpfile' => $tmpfile
        ]);
    }

    public function actionShuffleTests()
    {
        $id = Yii::$app->request->post('id');
        $testing = Testing::findOne($id);
        $order = explode(',', $testing->order_by);
        $old_order = [];
        foreach ($order as $item)
            $old_order[] = $item;
        while ($order == $old_order)
            shuffle($order);
        $testing->order_by = implode(',', $order);
        $testing->save();
    }

    public function actionNote()
    {
        $id = Yii::$app->request->post('Testing')['id'];
        $note = Yii::$app->request->post('Testing')['note'];

        $testing = Testing::findOne($id);
        $testing->note = $note;
        $testing->update();

        return $this->redirect(['run', 'id' => $id]);
    }

    public function actionDeleteNote()
    {
        $id = Yii::$app->request->post('id');
        $testing = Testing::findOne($id);
        $testing->note = '';
        $testing->update();

        return $this->redirect(['run', 'id' => $id]);
    }
}
