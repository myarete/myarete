<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 19.07.15
 * Time: 14:03
 */

namespace online\modules\testing\controllers;

use common\components\yii\base\BaseController;
use common\models\TestVariant;
use common\models\TestVariantFavorite;
use common\models\TestVariantFavoriteSearch;
use Yii;
use yii\filters\AccessControl;

class FavoriteController extends BaseController
{
    public $layout = '/old';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TestVariantFavoriteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionAdd($id)
    {
        $model = new TestVariantFavorite();
        $model->test_variant_id = $id;
        $model->user_id = Yii::$app->user->id;
        $model->save();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {

        $model = TestVariantFavorite::findOne(['id' => $id]);

        $model->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => TestVariant::findOne($id),
        ]);
    }
}