<?php

namespace online\modules\html;

class Html extends \yii\base\Module
{
    public $controllerNamespace = 'online\modules\html\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
