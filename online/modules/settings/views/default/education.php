<?php
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

$this->registerCssFile('/css/plugins/imgareaselect/imgareaselect-default.css', ['depends' => ['online\assets\AppAsset']]);
$this->registerJsFile('/js/plugins/imgareaselect/jquery.imgareaselect.min.js', ['depends' => ['online\assets\AppAsset']]);

$this->title = 'Образование';
?>

<div class="crm__container">
    <div class="crm__menu__right">
        <ul class="crm__menu__right-list">
            <li class="crm__menu__right-item"><a class="crm__menu__right-toggle" href="/settings">Персональная информация</a></li>
            <li class="crm__menu__right-item"><a class="crm__menu__right-toggle" href="/settings/default/general">Общее</a></li>
            <li class="crm__menu__right-item active"><a class="crm__menu__right-toggle active" href="/settings/default/education">Образование</a></li>
            <li class="crm__menu__right-item"><a class="crm__menu__right-toggle" href="/settings/default/notifications">Уведомления</a></li>
            <li class="crm__menu__right-item"><a class="crm__menu__right-toggle" href="/settings/default/blacklist">Черный список</a></li>
        </ul>
    </div>
    <div class="crm__content">
        <?php $form = ActiveForm::begin(); ?>
        <div class="wihite__box shadow">
            <div class="page__header_title">Образование</div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>


<style>
    /* Right-menu */

    .crm__content {
        padding-right: 310px;
    }

    .crm__menu__right {
        position: absolute;
        width: 250px;
        background: #fff;
        top: 0;
        right: 0;
        margin-top: 30px;
        margin-right: 30px;
        border-left: 1px solid #ededed;
        z-index: 6;
    }

    .crm__menu__right-list {
        padding: 9px 20px 9px 0;
    }

    .crm__menu__right-item {
        display: table;
        width: 100%;
        height: 66px;
        border-left: 5px solid transparent;
        -webkit-transition: all .3s;
        -o-transition: all .3s;
        transition: all .3s;
    }

    .crm__menu__right-item:hover,
    .crm__menu__right-item.active {
        background: rgba(71, 133, 252, 0.1);
        border-left: 5px solid #4785fc;
        border-radius: 0 3px 3px 0;
    }

    .crm__menu__right-item:hover .crm__menu__right-toggle,
    .crm__menu__right-item.active .crm__menu__right-toggle{
        color: #333;
    }

    .crm__menu__right-toggle {
        display: table-cell;
        vertical-align: middle;
        color: #939aab;
        font-weight: 500;
        height: 66px;
        padding-left: 20px;
        font-size: 16px;
        -webkit-transition: all .3s;
        -o-transition: all .3s;
        transition: all .3s;
        cursor: pointer;
    }

    .crm__menu__right-toggle:hover {
        text-decoration: none;
    }

    /* --- */

    @media screen and (max-width: 768px) {
        .crm__container {
            padding: 10px;
        }

        .crm__content {
            padding: 10px 0 0 0;
        }

        .crm__menu__right {
            position: relative;
            width: 100%;
            margin: 0;
        }
    }
</style>


