<?php

namespace online\modules\settings\controllers;

use signup\modules\user\forms\SetPasswordForm;
use Yii;
use yii\helpers\Html;
use yii\web\Controller;

/**
 * Default controller for the `settings` module
 */
class DefaultController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'uploadPhoto' => [
                'class' => '\budyaga\cropper\actions\UploadAction',
                'url' => '/tmp',
                'path' => '@online/web/tmp',
            ]
        ];
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->model;
        $setPassForm = new SetPasswordForm;
        
        $setPassForm->user = $user;
        
        $areLoad = $user->profile->load(Yii::$app->request->post());
        $areSave = $user->profile->save();
        
        if (!empty(Yii::$app->request->post('SetPasswordForm')['password']) && !empty(Yii::$app->request->post('SetPasswordForm')['passwordConfirm'])) {
            $areLoad = $areLoad && $setPassForm->load(Yii::$app->request->post());
            $areSave = $areSave && $setPassForm->save();
        }
        
        if ($areLoad && $areSave) {
            return $this->redirect(['index']);
        }
        
        return $this->render('index', [
            'user' => $user,
            'setPassForm' => $setPassForm
        ]);
    }
    
    public function actionGeneral()
    {
        $user = Yii::$app->user->model;
        $setPassForm = new SetPasswordForm;
        
        $setPassForm->user = $user;
        
        $areLoad = $user->load(Yii::$app->request->post()) && $user->profile->load(Yii::$app->request->post());
        $areSave = $user->save() && $user->profile->save();
        
        if (!empty(Yii::$app->request->post('SetPasswordForm')['password']) && !empty(Yii::$app->request->post('SetPasswordForm')['passwordConfirm'])) {
            $areLoad = $areLoad && $setPassForm->load(Yii::$app->request->post());
            $areSave = $areSave && $setPassForm->save();
        }
        
        $isSuccess = $areLoad && $areSave;
        
        if ($isSuccess) {
            return $this->redirect(['index']);
        }
        
        return $this->render('general', [
            'user' => $user,
            'setPassForm' => $setPassForm
        ]);
    }
    
    public function actionEducation()
    {
        return $this->render('education');
    }
    
    public function actionNotifications()
    {
        return $this->render('notifications');
    }
    
    public function actionBlacklist()
    {
        return $this->render('blacklist');
    }
    
    public function actionIntermediatePage()
    {
        $user = Yii::$app->user->model;
        $setPassForm = new SetPasswordForm;
        
        $setPassForm->user = $user;
        
        $areLoad = $user->load(Yii::$app->request->post()) && $user->profile->load(Yii::$app->request->post());
        $areSave = $user->save() && $user->profile->save();
        
        if (!empty(Yii::$app->request->post('SetPasswordForm')['password']) && !empty(Yii::$app->request->post('SetPasswordForm')['passwordConfirm'])) {
            $areLoad = $areLoad && $setPassForm->load(Yii::$app->request->post());
            $areSave = $areSave && $setPassForm->save();
        }
        
        $isSuccess = $areLoad && $areSave;
        
        if ($isSuccess) {
            Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['noreplyEmail'])
                ->setTo($user->email)
                ->setSubject('Регистрация в портале Арете Онлайн.')
                ->setHtmlBody('<p>Здравствуйте! Вы зарегистрировались в образовательном портале  '
                    . Html::a('Арете Онлайн', 'https://myarete.com') . ', используя этот почтовый адрес.</p>'
                    . '<p>Добро пожаловать! У нас вы найдете множество образовательных материалов, а также сможете создать свои собственные подборки задач.</p>'
                    . '<p>Наш проект постоянно развивается. Если у вас есть какие-то пожелания, комментарии, критические замечания - пишите нам на адрес writeus@myarete.com. Хотите увидеть новые функции Арете Онлайн? Испытываете трудности с использованием портала? Нужны определенные материалы в ближайшее время? Напишите нам! Мы обязательно рассмотрим все такие сообщения.</p>'
                    . '<p>Если вы не регистрировались в образовательной системе Арете Онлайн, напишите нам на адрес writeus@myarete.com, и мы удалим Ваш аккаунт.</p>')
                ->send();
            return $this->redirect(['index']);
        }
        
        return $this->render('intermediate-page', [
            'user' => $user,
            'setPassForm' => $setPassForm
        ]);
    }
    
}
