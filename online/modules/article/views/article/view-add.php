<?php

use yii\helpers\Html;

//History::put(Yii::$app->user->identity->email, $article->title);

/* @var $this \yii\web\View */
/* @var $article \online\modules\article\models\Article */

$this->registerJsFile('/js/plugins/slick/slick.min.js', ['depends' => ['online\assets\AppAsset']]);
$this->registerJsFile('/js/page/article/article/VkShare.js', ['position' => \yii\web\View::POS_HEAD]);
$this->registerCssFile('/css/plugins/slick/slick.css', ['depends' => ['online\assets\AppAsset']]);
$this->registerCssFile('/css/plugins/slick/slick-theme.css', ['depends' => ['online\assets\AppAsset']]);
$this->registerCssFile('/css/fa/font-awesome.min.css', ['depends' => ['online\assets\AppAsset']]);

$this->title = $article->title;
$isAdmin = 0;
if (Yii::$app->user->can('admin')) {
    $isAdmin = 1;
}

$js = <<<JS

	$('.article_slider_view').slick({
		dots: true,
		draggable: false,
		swipe: false,
		swipeToSlide: false,
		accessibility: false
	});

    function getComments(orderBy)
    {
        orderBy = (orderBy === undefined) ? 'DESC' : orderBy;
        
        var d = new Date(),
        time_zone = d.getTimezoneOffset();

        $.post('/article/article/add-comment', {
            article_id: $article->id,
            text: $('[name=new_comment]').val(),
            time_zone: time_zone,
            order_by: orderBy
        }, function(data) {
            var html = '',
                count = 5,
                div = 0;
            for(var i = 0; i < data.length; i++) {
                var c = data[i];

                if(i != 0 && i % count == 0) {
                    html += '<a href="#comment-' + i + '" data-toggle="collapse">Показать следующие 5 комментариев</a>';
                    html += '<div id="comment-' + i + '" class="collapse">';
                    div++;
                }

                var avatar = c.avatar.substr(0, 3) + '/' + c.avatar.substr(3, 3) + '/' + c.avatar,
                    name = c.first_name + ' ' + c.last_name;

                name = name == ' ' ? c.email : name;
                email = c.view_contacts == 1 ? c.email : '';

                html += '<div class="comment-body">'
                html += '<div class="comment-img">'
                        + '<a href="#" data-toggle="modal" data-target="#user-info-' + i + '">'
                            + '<img width="50" height="50" src="/upload/profile/' + avatar + '" alt="">'
                        + '</a>'
                    + '</div>';
                html += '<div class="comment-content">';
                html += '<div class="head">' + name + '<span style="margin-left:15px;color:rgba(120, 127, 140, 0.6);font-weight:normal;">'
                    + $.timeago(c.create_dt) + '</span></div>';
                html += '<div class="text">' + c.text + '</div>';
                if($isAdmin == 1) {
                    html += '<div class="text-right"><a href="javascript:void(0)" class="btn btn-warning btn-xs btn-delete-comment"\
                        data-id="' + c.comment_id + '">Удалить</a></div>';
                }
                html += '</div></div>';
                html += '\
                    <div class="modal fade" id="user-info-' + i + '" data-backdrop="true">\
                        <div class="modal-dialog">\
                            <div class="modal-content">\
                                <div class="modal-header">\
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
                                    <h4 class="modal-title">О пользователе</h4>\
                                </div>\
                                <div class="modal-body">\
                                    <p>' + c.state + '</p>\
                                    <p>' + email + '</p>\
                                    <p><a href="/communication/default/view/' + c.user_id + '">Посмотреть страницу пользователя</a></p>\
                                </div>\
                                <div class="modal-footer">\
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>\
                                </div>\
                            </div>\
                        </div>\
                    </div>';
            }
            for(var i = 0; i < div; i++) {
                html += '</div>';
            }
            $('#comments').html(html);
            $('[href ^= "#comment-"]').click(function() {
                $(this).hide();
            });
            $('.btn-delete-comment').click(function() {
                if(confirm("Удалить комментарий?"))
                    $.post('/article/article/delete-comment', {
                        id: $(this).data('id'),
                        article_id: $article->id
                    });
            });
        });
        $('[name=new_comment]').val('');
    }
    $('#addComment').click(function() {
        getComments();
    });
    $('#new-comment').on("keypress", function(e) {
        if (e.keyCode == 13 || e.keyCode == 10) {
            if (e.ctrlKey) {
                getComments();
            }
        }
    });
    //sort comments
    $('.btn-sort-comments-asc').click(function() {getComments('ASC'); });
    $('.btn-sort-comments-desc').click(function() {getComments('DESC'); });
    getComments();
JS;
$this->registerJs($js);
?>

<div class="col-xs-12 col-sm-12 col-md-10 col-lg-8">
    <div class="side-block article-block">
        <h3>Комментарии</h3>
        <div class="text-center">
            <a href="javascript:void(0)" class="btn btn-primary btn-xs btn-sort-comments-desc">Новые вверху</a>
            <a href="javascript:void(0)" class="btn btn-primary btn-xs btn-sort-comments-asc">Новые внизу</a>
        </div>
        <div class="task-comments">
            <div id="comments"></div>
            <?php if (Yii::$app->user->model->is_confirmed == 1): ?>
                <div class="comment-body">
                    <div class="comment-img">
                        <?= Html::img(Yii::$app->user->model->profile->getImageSrc(), ['width' => 50, 'height' => 50]) ?>
                    </div>
                    <div class="comment-content">
                        <?= Html::textarea('new_comment', '', [
                            'id' => 'new-comment',
                            'class' => 'form-control',
                            'rows' => 2
                        ]) ?>
                    </div>
                </div>
                <div class="text-right">
                    <?= Html::a('Отправить', '#', ['id' => 'addComment', 'class' => 'btn btn-xs btn-primary']) ?>
                </div>
            <?php endif ?>
        </div>
        <br><br>
    </div>
