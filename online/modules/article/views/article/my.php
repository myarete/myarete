<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\UserVsCategory;
use common\models\UserVsArticle;
use yii\grid\GridView;

$path = '@web/upload/article/';
$this->title = 'Мои уроки';
?>

<div class="crm__container">
    <div class="crm__content">
        <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
            <?= \kartik\alert\AlertBlock::widget([
                'delay' => false
            ]) ?>
        <?php endif; ?>
        <div class="wihite__box shadow">
            <div class="page__header_title">Мои уроки</div>
            <div class="event__content">
                <?php if(!empty($articlesFromCategories) || !empty($articles)): ?>
                    <b>Сортировать:</b> <?= $sort->link('title') . ' | ' . $sort->link('id');?> <br><br>
                    <?php foreach($articlesFromCategories as $article) : ?>
                        <div class="table__row">
                            <div class="table__row_item">
                                <?= Html::a($article->title, ['/article/article/view', 'id' => $article->id]) ?>
                            </div>
                            <div class="table__row_item"><?= Html::a('Открыть', ['/article/article/view', 'id' => $article->id], ['class' => 'button button__green']); ?></div>
                        </div>
                    <?php endforeach; ?>
                    <?php foreach($articles as $article) : ?>
                        <div class="table__row">
                            <div class="table__row_item">
                                <?= Html::a($article->title, ['/article/article/view', 'id' => $article->id]) ?>
                            </div>
                            <div class="table__row_item"><?= Html::a('Открыть', ['/article/article/view', 'id' => $article->id], ['class' => 'button button__green']); ?></div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <p>У Вас пока нет ни одного урока</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
