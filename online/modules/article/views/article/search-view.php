<?php

use yii\helpers\Html;
use yii\widgets\ListView;

?>

<div class="search-view-index">

    <div class="row">
        <?= Html::beginForm('/article/article/search', 'post', ['name' => 'ArticleSearch', 'class' => '']) ?>
        <div class="col-sm-12">
            <div class="input-group">
                <?= Html::textInput('search', $search_string, [
                    'id' => 'searchInput',
                    'class' => 'form-control',
                    'placeholder' => 'Поиск в статьях'
                ]) ?>
                <span class="input-group-btn">
                    <?= Html::submitButton('Найти', ['class' => 'btn btn-primary', 'style' => 'width:100px']) ?>
                </span>
            </div>
        </div>
        <?= Html::endForm() ?>
    </div>

    <h2>Поиск по названиям</h2>
    <hr>
    <div class="row">
        <?= ListView::widget([
                'id' => 'onTitle',
                'dataProvider' => $dataOnTitle,
                'itemView' => function($model) {
                    return '<div class="col-sm-4">
                                <div class="thumbnail" style="text-align:center">'
                                    .Html::a($model->title, ['/article/article/view', 'id' => $model->id]).
                                '</div>
                            </div>';
                },
                'summary' => ''
            ]);
        ?>
    </div>
    <hr>

    <h2>Поиск по содержимому</h2>
    <hr>
    <div class="row">
        <?= ListView::widget([
                'id' => 'onDescription',
                'dataProvider' => $dataOnDescription,
                'itemView' => function($model) {
                    return '<div class="col-sm-4">
                                <div class="thumbnail" style="text-align:center">'
                                    .Html::a($model->title, ['/article/article/view', 'id' => $model->id]).
                                '</div>
                            </div>';
                },
                'summary' => ''
            ]);
        ?>
    </div>
    <hr>
</div>
