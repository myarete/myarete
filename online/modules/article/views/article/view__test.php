<div class="article-test">
    <div>
        <div class="test-header">
            <strong>Тест #<?=$test->id?>. <?= $test->name ?></strong>
        </div>

        <div>
            <?=$test->question?>
        </div>

        <div class="radio-answer-group">
            <?php foreach ($test->answers as $answer) : ?>
                <label for="answer_<?=$answer->test_id ?>_<?=$answer->id ?>_<?= $i ?>">
                    <input id="answer_<?= $answer->test_id ?>_<?=$answer->id ?>_<?= $i ?>" type="radio" ng-model="radioTest[<?= $i ?>]" value="<?= $answer->id ?>">
                    <?= $answer->answer ?></label>
            <?php endforeach; ?>
        </div>
    </div>

    <hr>

    <button class="btn btn-success test-button" ng-click="submitTest(<?=$answer->test_id ?>, <?= $i ?>)">Посмотреть результат</button>

    <div class="modal fade" id="testModal-<?= $i ?>-<?= $answer->test_id ?>" data-backdrop="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Результат</h4>
                </div>
                <div class="modal-body">
                    <div class="row" ng-class="{'test-row-right' : result.is_right == 1, 'test-row-wrong' : result.is_right == 0}">
                        <h4 ng-class="{'test-right' : result.is_right == 1, 'test-wrong' : result.is_right == 0}">Тест #[[ result.test_id ]] - <span ng-show="result.is_right">Правильно</span> <span ng-hide="result.is_right">Неправильно</span></h4>
                        <div class="test-comment" compile="result.comment"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
</div>
