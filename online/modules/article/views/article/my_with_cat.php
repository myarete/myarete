<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\UserVsCategory;
use common\models\UserVsArticle;
use yii\grid\GridView;

$path = '@web/upload/article/';
$this->title = 'Мои уроки';
?>

<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">

    <div class="article-block">
        <div class="row side-block">
            <div class="col-md-12">
                <h3>Мои категории</h3>
                <?php if(!empty($childCategory)) : ?>
                    <h2><?= $childCategory->name ?></h2>
                <?php endif; ?>
                <?php if(!empty($categories)) : ?>
                    <div class="article-block">
                        <div class="row">
                            <?php foreach($categories as $category):
                                if ($category->active!=1): continue; endif;?>
                                <?php if (UserVsCategory::findOne(['user_id' => Yii::$app->user->id, 'article_category_id' => $category->id]) || UserVsCategory::findOne(['user_id' => Yii::$app->user->id, 'pid' => $category->id]) || UserVsCategory::findOne(['user_id' => Yii::$app->user->id, 'root' => $category->id])):?>
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                        <div class="thumbnail">
                                            <?php if(!empty($category->getMainImage($path))): ?>
                                                <a href="<?= Url::to(['/article/article/my', 'categoryId' => $category->id]) ?>"><img src="<?= $category->getMainImage($path) ?>"></a>
                                            <?php endif; ?>
                                            <div class="caption">
                                                <a href="<?= Url::to(['/article/article/my', 'categoryId' => $category->id]) ?>"><h3><?= $category->name ?></h3> </a>
                                                <?php if(!empty($category->description_short)): ?>
                                                    <p class="desc-short"><?= $category->description_short ?></p>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                            <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($dataProvider->models) && !empty($categoryId)) : ?>
                    <div class="article-block">
                        <div class="row">
                            <?php foreach($dataProvider->models as $model) : ?>
                                <div class="col-sm-6 col-md-4 col-lg-3">
                                    <div class="thumbnail">
                                        <?php if(!empty($model->getMainImage($path))): ?>
                                            <img src="<?= $model->getMainImage($path) ?>">
                                        <?php endif; ?>
                                        <div class="caption">
                                            <div class="article-title"><?= Html::a($model->title, ['/article/article/view', 'id' => $model->id]) ?></div>
                                            <p><?= $category->description_short ?></p>

                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <div class="col-md-12">
                <h3>Список уроков</h3>
                <b>Сортировать:</b> <?= $sort->link('title') . ' | ' . $sort->link('id');?> <br><br>
                <?php foreach($articlesFromCategories as $article) : ?>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <div class="thumbnail">
                            <?php if(!empty($article->getMainImage($path))): ?>
                                <?= Html::a('<img src="'.$article->getMainImage($path).'">', ['/article/article/view', 'id' => $article->id]) ?>

                            <?php endif; ?>
                            <div class="caption">
                                <div class="article-title"><?= Html::a($article->title, ['/article/article/view', 'id' => $article->id]) ?></div>
                                <?php if(!empty($article->description_short)): ?>
                                    <p class="desc-short"><?= $article->description_short ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

                <?php foreach($articles as $article) : ?>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <div class="thumbnail">
                            <?php if(!empty($article->getMainImage($path))): ?>
                                <?= Html::a('<img src="'.$article->getMainImage($path).'">', ['/article/article/view', 'id' => $article->id]) ?>

                            <?php endif; ?>
                            <div class="caption">
                                <div class="article-title"><?= Html::a($article->title, ['/article/article/view', 'id' => $article->id]) ?></div>
                                <?php if(!empty($article->description_short)): ?>
                                    <p class="desc-short"><?= $article->description_short ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
        <br><br>
    </div>
</div>
