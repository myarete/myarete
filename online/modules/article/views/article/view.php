<?php

use common\components\other\History;
use common\models\UserVsArticle;
use common\models\UserVsCategory;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

History::put(Yii::$app->user->identity->email, $article->title);

$this->title = $article->title;
$js = "
    $('.table-style-3, .table-all-style-1').wrap('<div class=\"table-wrap\"></div>');
";
$this->registerJs($js);
?>

<div class="crm__container">
    <div class="crm__content">
        <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
            <?= \kartik\alert\AlertBlock::widget(['delay' => false]) ?>
        <?php endif; ?>
        <div class="wihite__box shadow">
            <div class="page__header_title"><?= $article->title; ?></div>
            <?php if ($add == 1): ?>
                <div class="lesson-full">
                    <div class="lesson-full__p" ng-app="testing" ng-controller="TestCtrl">
                        <?php
                        if ($article->price > 0 && !ArrayHelper::isIn($article->id, $userVsArticle) && ($article->is_action == 0 || ($article->is_action == 1 && $article->action_price > 0))) {
                            echo '<div class="lesson-full__title">Превью урока</div>';
                            $description = $article->description_short;
                        } elseif (!ArrayHelper::isIn($article->id, $userVsArticle)) {
                            echo '<div class="lesson-full__title">Превью урока</div>';
                            $description = $article->description_short;
                        } else {
                            $description = $article->description;
                        }
                        ?>
                        <div class="lesson-full__p"><?= $description ?></div>
                    </div>
                </div>
            <?php else: ?>
                <div class="lesson-full">
                    <div class="article-description" ng-app="testing" ng-controller="TestCtrl">
                        <?php
                        if ($article->price > 0 && !ArrayHelper::isIn($article->id, $userVsArticle) && ($article->is_action == 0 || ($article->is_action == 1 && $article->action_price > 0))) {
                            echo '<div class="lesson-full__title">Превью урока</div>';
                            $description = $article->description_short;
                        } elseif (!ArrayHelper::isIn($article->id, $userVsArticle)) {
                            echo '<div class="lesson-full__title">Превью урока</div>';
                            $description = $article->description_short;
                        } else {
                            $description = $article->description;
                        }
                        ?>
                        <div class="lesson-full__p"><?= $description ?></div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="lesson-full">
                <?php if ($article->price > 0 && !ArrayHelper::isIn($article->id, $userVsArticle) && ($article->is_action == 0 || ($article->is_action == 1 && $article->action_price > 0))): ?>
                    <div class="lesson-full__title">Для просмотра данного урока целиком, просто купите его</div>
                    <?= Html::a('Купить урок за ' . $article->price . ' руб.', [
                        '/article/article/buy-article',
                        'articleId' => $article->id,
                        'articlePrice' => $article->price
                    ], [
                        'data' => [
                            'confirm' => 'С вашего Арете Кошелька будет списано ' . $article->price . ' рублей. Вы уверены?',
                            'method' => 'post'
                        ],
                        'class' => 'button button__green'
                    ]); ?>
                <?php elseif (!ArrayHelper::isIn($article->id, $userVsArticle)): ?>
                    <div class="lesson-full__title"> Для просмотра данного урока целиком, просто добавьте его</div>
                    <?= Html::a('Добавить урок', [
                        '/article/article/add-article',
                        'articleId' => $article->id,
                        'articlePrice' => $article->price
                    ], [
                        'data' => [
                            'confirm' => 'Вы уверены?',
                            'method' => 'post'
                        ],
                        'class' => 'button button__green'
                    ]); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="tab-menu tab-menu--short tab-menu--single">
            <?= $this->render('view-add', ['article' => $article]) ?>
        </div>
    </div>
</div>
