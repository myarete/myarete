<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\article\models\ArticleSliderItem;
use yii\widgets\ActiveForm;

?>


<?php
if ($this->context->action->id === 'create')
    $this->title = 'Создать слайдер';
else $this->title = 'Обновить слайдер';
$this->params['breadcrumbs'][] = ['label' => 'Слайдеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
    function setData(e){
        var url = $(this).attr('href');
        var SliderItemModal = $('#SliderItemModal');
        var SliderItemModalBody = $('.slider_item_modal_body');

        $.get(url, function (data) {
			SliderItemModalBody.html(data);
			SliderItemModal.modal();
         });
        return false;
    }

    var body = $('body');
    body.on('click', '.SloderItemModalCreate', setData);
    body.on('click', '.SloderItemModalUpdate', setData);
JS;
$this->registerJs($js);
?>

<div class="ibox float-e-margins">
    
    <div class="ibox-title">
        <h5><?= $this->title ?></h5>
    </div>
    
    <div class="ibox-content">
        <?php $form = ActiveForm::begin(); ?>
        
        <?= $form->field($model, 'name') ?>
        
        <hr>
        
        <button type="submit" class="btn btn-primary">Сохранить</button>
        
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php if (!$model->isNewRecord) : ?>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Элементы слайдера</h5>
            <div class="ibox-tools">
                <?= Html::a('<i class="fa fa-plus"></i> Создать', ['create-item', 'id' => $model->id], ['class' => 'SloderItemModalCreate', 'title' => 'Создать']) ?>
            </div>
        </div>
        
        <div class="ibox-content">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'id' => 'slider-grid',
                'filterModel' => $searchModel,
                'layout' => '{items}',
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => [
                            'style' => 'width: 50px;'
                        ],
                    ],
                    [
                        'attribute' => 'title',
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'order',
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'description',
                        'format' => 'raw'
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width: 1px; white-space: nowrap;'],
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update-item', 'id' => $model->id], ['class' => 'SloderItemModalUpdate']);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete-item', 'id' => $model->id]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
    
    <div class="modal inmodal fade" id="SliderItemModal" tabindex="" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                            class="sr-only">Close</span></button>
                    <h4 class="modal-title">Элемент слайдера</h4>
                    <small class="font-bold">Добавьте/измените элемент слайдера</small>
                </div>
                <div class="modal-body slider_item_modal_body">
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Закрыть</button>
                    <button type="submit" form="formModal" class="btn btn-primary">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
