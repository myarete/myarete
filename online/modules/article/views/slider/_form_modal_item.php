<?php
/**
 * User: Evgeniy Karpeev [karpeevea@gmail.com]
 * Date: 23.01.2016
 * Time: 19:17
 */
use yii\bootstrap\ActiveForm;
use admin\widgets\ckeditor\CKEditor;
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formModal']]); ?>

    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'order') ?>

<?= $form->field($model, 'description')->widget(CKEditor::className(), [
    'options' => ['rows' => 100],
    'enableFinder' => true,
    'clientOptions' => [
        'toolbar' => [
            [
                'name' => 'clipboard',
                'items' => [
                    'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                ],
            ],
            [
                'name' => 'links',
                'items' => [
                    'Link', 'Unlink', 'Anchor'
                ],
            ],
            [
                'name' => 'insert',
                'items' => [
                    'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                ],
            ],
            [
                'name' => 'align',
                'items' => [
                    'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                ],
            ],
            [
                'name' => 'document',
                'items' => [
                    'Maximize', 'ShowBlocks', 'Source'
                ],
            ],
            '/',
            [
                'name' => 'basicstyles',
                'items' => [
                    'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                ],
            ],
            [
                'name' => 'color',
                'items' => [
                    'TextColor', 'BGColor'
                ],
            ],
            [
                'name' => 'paragraph',
                'items' => [
                    'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                ],
            ],
            [
                'name' => 'styles',
                'items' => [
                    'Styles', 'Format', 'Font', 'FontSize'
                ],
            ],
            '/',
            [
                'name' => 'insert',
                'items' => [
                    'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Accordion'
                ],
            ],
        ],
        'height' => 500,
        'allowedContent' => true,
        'contentsCss' => [
            '/css/fa/font-awesome.min.css',
            '/css/bootstrap.min.css',
            Yii::$app->params['scheme'].'://'.Yii::$app->params['host'].'/css/main.css',
            Yii::$app->params['scheme'].'://'.Yii::$app->params['subdomain.online'].'.'.Yii::$app->params['host'].'/css/article.css'
        ],
        'extraPlugins' => 'mathjax,exercise,test,sliders,accordion',
		'mathJaxLib' => 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
    ],
]) ?>

<?php ActiveForm::end(); ?>
