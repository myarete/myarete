<?php

use kartik\tree\TreeView;
use common\models\ArticleCategory;
use yii\helpers\Html;


$this->title = 'Классификация статей';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox">
    <div class="ibox-title">
        <i class="fa fa-tree"></i> Классификация статей
    </div>

    <div class="ibox-content">

        <?= TreeView::widget([
            'query' => ArticleCategory::find()->addOrderBy('root, lft'),
            'mainTemplate' => '<div class="row"><div class="col-sm-6">{wrapper}</div><div class="col-sm-6">{detail}</div></div>',
            'treeOptions' => ['style' => 'height:auto'],
            'headingOptions' => ['label' => 'Категории'],
            'fontAwesome' => true,
            'isAdmin' => false,
            'displayValue' => 1,
            //'softDelete' => false,
            'showIDAttribute' => false,
            'cacheSettings' => [
                'enableCache' => true,
            ],
            'nodeFormOptions' => ['enctype' => 'multipart/form-data'],
            'nodeAddlViews' => [
                \kartik\tree\Module::VIEW_PART_2 => '@admin/modules/article/views/category/_form_part_2',
            ],
        ]);
        ?>
    </div>
</div>
