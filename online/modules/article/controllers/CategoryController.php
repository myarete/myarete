<?php

namespace online\modules\article\controllers;

use common\components\yii\base\BaseController;
use common\models\ArticleCategory;
use common\models\ArticleVsCategory;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class CategoryController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'save', 'delete-from-article'],
                        'roles' => ['admin']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ArticleCategory();
        if ($model->load(Yii::$app->request->post()) && $model->save())
            return $this->redirect(['view', 'id' => $model->id]);
        return $this->render('create_update', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!$model->allowEdit())
            $this->forbidden();

        if ($model->load(Yii::$app->request->post()) && $model->save())
            return $this->redirect(['view', 'id' => $model->id]);

        return $this->render('create_update', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (!$model->allowEdit())
            $this->forbidden();

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return bool
     */
    public function actionDeleteFromArticle($article_id, $article_category_id)
    {
        Yii::$app->response->format = 'json';
        return (bool)ArticleVsCategory::deleteAll(['article_id' => $article_id, 'article_category_id' => $article_category_id]);
    }


    /**
     * @return array|bool
     */
    public function actionSave()
    {
        Yii::$app->response->format = 'json';

        $model = ArticleVsCategory::find()->where(['article_id' => $_POST['ArticleVsCategory']['article_id'], 'article_category_id' => $_POST['ArticleVsCategory']['article_category_id']])->one();

        if ($model === null)
            $model = new ArticleVsCategory();

        $model->load(Yii::$app->request->post());

        if ($model->save())
            return true;
        else
            return $model->errors;
    }

    /**
     * @param integer $id
     * @return ArticleCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArticleCategory::findOne($id)) !== null)
            return $model;

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
