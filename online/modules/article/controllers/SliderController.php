<?php

namespace online\modules\article\controllers;

use common\components\yii\base\BaseController;
use common\models\Article;
use common\models\ArticleSlider;
use common\models\ArticleSliderItem;
use common\models\ArticleSliderItemSearch;
use common\models\ArticleSliderSearch;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class SliderController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['delete', 'delete-item'],
                        'roles' => ['admin']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'create-item', 'update-item', 'get-sliders'],
                        'roles' => ['admin']
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSliderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $slider = $this->findModel($id);
        foreach($slider->articleSliderItems as $item){
            $item->setAttribute('description', $this->strReplace($item->description));
        }

        return $this->render('view', [
            'model' => $slider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ArticleSlider();
        if ($model->load(Yii::$app->request->post()) && $model->save())
            return $this->redirect(['update', 'id' => $model->id]);

        return $this->render('create_update', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!$model->allowEdit())
            $this->forbidden();

        $searchModel = new ArticleSliderItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        if ($model->load(Yii::$app->request->post()) && $model->save())
            return $this->redirect(['view', 'id' => $model->id]);

        return $this->render('create_update', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (!$model->allowEdit())
            $this->forbidden();

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return $this|string
     */
    public function actionCreateItem($id)
    {
        $model = new ArticleSliderItem();
        if ($model->load(Yii::$app->request->post()))
        {
            $model->article_slider_id = $id;
            if($model->save())
                return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->renderAjax('_form_modal_item', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return $this|string
     * @throws NotFoundHttpException
     */
    public function actionUpdateItem($id)
    {
        $model = $this->findItemModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save())
            return $this->redirect(Yii::$app->request->referrer);

        return $this->renderAjax('_form_modal_item', [
            'model' => $model
        ]);
    }

    public function actionDeleteItem($id)
    {
        $model = $this->findItemModel($id);

        $model->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return string
     */
    public function actionGetSliders()
    {
        $sliders = ArrayHelper::map(ArticleSlider::find()->select(['id', 'name'])->asArray()->all(), 'id', 'name');
        return json_encode($sliders);
    }


    /**
     * @param integer $id
     * @return ArticleSlider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArticleSlider::findOne($id)) !== null)
            return $model;

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findItemModel($id)
    {
        if (($model = ArticleSliderItem::findOne($id)) !== null)
            return $model;

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
