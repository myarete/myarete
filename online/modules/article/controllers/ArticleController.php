<?php

namespace online\modules\article\controllers;

use common\components\yii\base\BaseController;
use common\models\Article;
use common\models\ArticleCategory;
use common\models\ArticleComment;
use common\models\ArticleReady;
use common\models\ArticleSearch;
use common\models\ArticleVsCategory;
use common\models\Payment;
use common\models\Profile;
use common\models\UserVsArticle;
use common\models\UserVsCategory;
use Yii;
use yii\data\Sort;
use yii\filters\AccessControl;

class ArticleController extends BaseController
{
    //public $layout = '/old';
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    public function actionMy($categoryId = null, $articleId = null)
    {
        $articles = null;
        $category = null;
        if (empty($categoryId)) {
            $model = new ArticleCategory();
            $categoriesRoots = ArticleCategory::getRoots();
            $categories = $model->getUserVsCategory();
            $categories = $categories->all();
            $categories = ArticleCategory::find()->joinWith('userVsCategory')->where(['user_vs_category.user_id' => Yii::$app->user->id])->all();
        } else {
            $category = ArticleCategory::findOne($categoryId);
            $categories = $category->children(1)->all();
            $categoriesRoots = $categories;
        }

        $sort = new Sort([
            'attributes' => [
                'id' => [
                    'label' => 'Номер урока',
                ],
                'title' => [
                    'label' => 'Название',
                ],
            ],
        ]);

        $articlesFromCategories = Article::find()->joinWith('articleVsCategory')->orderBy($sort->orders)->all();
        $articles = Article::find()->joinWith('userVsArticle')->orderBy($sort->orders)->all();

        $approvedBy = [Article::SUCCESS_ALL];
        if (Yii::$app->user->can('teacher'))
            $approvedBy[] = Article::SUCCESS_TEACHER;

        $searchModel = new ArticleSearch([
            'is_approved' => $approvedBy,
            'pagination' => null,
            'categoryId' => $categoryId
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('my', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'articles' => $articles,
            'articlesFromCategories' => $articlesFromCategories,
            'childCategory' => $category,
            'categoryId' => $categoryId,
            'categories' => $categories,
            'category' => $category,
            'categoriesRoots' => $categoriesRoots,
            'sort' => $sort
        ]);
    }

    private function filterCategoriesBy_HideFrClients($categories)
    {
        foreach ($categories as $key => $category) {
            if ($category->hide_fr_clients == 1) {
                unset($categories[$key]);
            }
        }
        return $categories;
    }

    public function actionStore($categoryId = null)
    {
        $category = null;
        $categories_buy = ArticleCategory::find()
            ->joinWith('userVsCategory')
            ->where(['user_id' => Yii::$app->user->id])
            ->all();

        if (empty($categoryId)) {
            $categories = ArticleCategory::getRoots();
        } else {
            $category = ArticleCategory::findOne($categoryId);
            $categories = $category->children(1)->all();
        }

        //filter categories by 'hide_fr_clients' field for role 'client'
        if (Yii::$app->user->can('user'))
            $categories = $this->filterCategoriesBy_HideFrClients($categories);

        $approvedBy = [Article::SUCCESS_ALL];
        if (Yii::$app->user->can('teacher'))
            $approvedBy[] = Article::SUCCESS_TEACHER;

        $searchModel = new ArticleSearch([
            'is_approved' => $approvedBy,
            'pagination' => null,
            'categoryId' => $categoryId
        ]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        foreach ($dataProvider->models as $model) {
            if ($model->is_action == 1) {
                $actionDateStart = strtotime($model->action_date_start);
                $actionDateEnd = strtotime($model->action_date_end);

                if ($model->action_date_start != null && $model->action_date_end != null) {
                    if (time() < $actionDateStart) {
                        $model->is_action = 0;
                    } else if (time() > $actionDateEnd) {
                        $model->is_action = 0;
                        $model->action_price = 0;
                        $model->action_date_start = null;
                        $model->action_date_end = null;
                        $model->update();
                    }
                }
            }
        }

        return $this->render('store', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'childCategory' => $category,
            'categoryId' => $categoryId,
            'categories' => $categories,
            'category' => $category,
            'categories_buy' => $categories_buy
        ]);
    }

    public function actionAddArticle($articleId, $articlePrice)
    {
        $articleId = Yii::$app->request->get('articleId');
        $articlePrice = Yii::$app->request->get('articlePrice');
        $article = Article::find()->where(['id' => $articleId])->one();
        $articleVsCategory = ArticleVsCategory::find()->where(['article_id' => $articleId])->one();

        if ($articleId != '') {
            if ($articlePrice == 0) {
                if (!UserVsArticle::find()->where(['user_id' => Yii::$app->user->id, 'article_id' => $articleId])->one()) {
                    $model = new UserVsArticle();
                    $model->user_id = Yii::$app->user->id;
                    $model->article_id = $article->id;
                    $model->category_id = $articleVsCategory->article_category_id;
                    $model->save();
                    Yii::$app->session->setFlash('success', 'Вы добавили урок - <a href="/article/article/view/' . $article->id . '">' . $article->title . '</a><br>Вы сможете найти его в разделе <a href="/article/article/my">Мои уроки</a>');
                }
            } else {
                return $this->redirect(['store', 'articleId' => $articleId]);
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionBuyArticle($articleId, $articlePrice)
    {
        $articleId = Yii::$app->request->get('articleId');
        $articlePrice = Yii::$app->request->get('articlePrice');
        $article = Article::find()->where(['id' => $articleId])->one();
        $articleVsCategory = ArticleVsCategory::find()->where(['article_id' => $articleId])->one();

        $profile = Profile::find()->where(['user_id' => Yii::$app->user->id])->one();

        if ($articleId != '') {
            if ($articlePrice != 0) {
                if ($profile->cash > $articlePrice) {
                    if (!UserVsArticle::find()->where(['user_id' => Yii::$app->user->id, 'article_id' => $articleId])->one()) {
                        $profile->cash -= $articlePrice;
                        $profile->save();
                        $payment = new Payment();
                        $payment->user_id = Yii::$app->user->id;
                        $payment->type_id = 3;
                        $payment->refill_sum = $articlePrice;
                        $payment->is_paid = 1;
                        $payment->save();

                        $model = new UserVsArticle();
                        $model->user_id = Yii::$app->user->id;
                        $model->article_id = $article->id;
                        $model->category_id = $articleVsCategory->article_category_id;
                        $model->save();
                        Yii::$app->session->setFlash('success', 'Вы купили урок - <a href="/article/article/view/' . $article->id . '">' . $article->title . '</a> за ' . $articlePrice . ' рублей.<br>Вы сможете найти его в разделе <a href="/article/article/my">Мои уроки</a>');
                    }
                } else {
                    Yii::$app->session->setFlash('info', 'В вашем бумажнике не хватает средств для покупки данного урока - <a href="/payment/default/refill-cash">Пополнить бумажник</a>');
                }
            } else {
                return $this->redirect(['store', 'articleId' => $articleId]);
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionAddCategory($categoryId)
    {
        $categoryId = Yii::$app->request->get('categoryId');
        $category = ArticleCategory::find()->where(['id' => $categoryId])->one();
        $categoryRoot = ArticleCategory::find()->where(['id' => $category->root])->one();

        $categoriesChildren = $category->children(5)->all();

        if ($categoryId != '') {
            if ($category->price == 0 || $category->price == NULL) {

                if (!UserVsCategory::find()->where(['user_id' => Yii::$app->user->id, 'article_category_id' => $category->id])->one()) {
                    UserVsArticle::deleteAll(['user_id' => Yii::$app->user->id, 'category_id' => $category->id]);
                    $model = new UserVsCategory();
                    $model->user_id = Yii::$app->user->id;
                    $model->article_category_id = $categoryId;
                    $model->root = $category->root;
                    $model->pid = $category->pid;
                    $model->lvl = $category->lvl;
                    $model->save();

                    foreach ($categoriesChildren as $child) {
                        UserVsArticle::deleteAll(['category_id' => $category->id]);
                        //list($id, $name) = explode("_", $key);
                        $cat = new UserVsCategory();
                        $cat->user_id = Yii::$app->user->id;
                        $cat->article_category_id = $child->id;
                        $cat->root = $child->root;
                        $cat->pid = $child->pid;
                        $cat->lvl = $child->lvl;
                        $cat->save();
                    }

                    Yii::$app->session->setFlash('success', 'Категория была успешно добавленна в раздел - <a href="/article/article/my">Мои уроки</a>');

                }
            } else {
                return $this->redirect(['store', 'categoryId' => $categoryId]);
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionBuyCategory($categoryId)
    {
        $categoryId = Yii::$app->request->get('categoryId');
        $category = ArticleCategory::find()->where(['id' => $categoryId])->one();

        $categoriesChildren = $category->children(5)->all();

        $profile = Profile::find()->where(['user_id' => Yii::$app->user->id])->one();

        if ($categoryId != '') {
            if ($category->price != 0 || $category->price != NULL) {
                if ($profile->cash > $category->price) {
                    if (!UserVsCategory::find()->where(['user_id' => Yii::$app->user->id, 'article_category_id' => $category->id])->one()) {
                        UserVsArticle::deleteAll(['user_id' => Yii::$app->user->id, 'category_id' => $category->id]);
                        $profile->cash -= $category->price;
                        $profile->save();
                        $payment = new Payment();
                        $payment->user_id = Yii::$app->user->id;
                        $payment->type_id = 6;
                        $payment->refill_sum = $category->price;
                        $payment->is_paid = 1;
                        $payment->save();
                        $model = new UserVsCategory();
                        $model->user_id = Yii::$app->user->id;
                        $model->article_category_id = $categoryId;
                        $model->root = $category->root;
                        $model->pid = $category->pid;
                        $model->lvl = $category->lvl;
                        $model->save();

                        foreach ($categoriesChildren as $child) {
                            UserVsArticle::deleteAll(['user_id' => Yii::$app->user->id, 'category_id' => $category->id]);
                            //list($id, $name) = explode("_", $key);
                            $cat = new UserVsCategory();
                            $cat->user_id = Yii::$app->user->id;
                            $cat->article_category_id = $child->id;
                            $cat->root = $child->root;
                            $cat->pid = $child->pid;
                            $cat->lvl = $child->lvl;
                            $cat->save();
                        }

                        Yii::$app->session->setFlash('success', 'Категория была успешно куплена и добавленна в раздел - <a href="/article/article/my">Мои уроки</a><br>С баланса было списана сумма - ' . $category->price . ' ₽');

                    }
                } else {
                    Yii::$app->session->setFlash('info', 'В вашем бумажнике не хватает средств для покупки данной категории - <a href="/payment/default/refill-cash">Пополнить бумажник</a>');
                }
            } else {
                return $this->redirect(['store', 'categoryId' => $categoryId]);
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionView($id)
    {
        $article = Article::findOne(['id' => $id]);

        $article_comments = ArticleComment::findAll(['article_id' => $id, 'user_id' => Yii::$app->user->id]);
        $articleVsCategory = ArticleVsCategory::find()->where(['article_id' => $id])->one();
        $categoryId = $articleVsCategory->article_category_id;
        $add = 0;
        $article->description = $this->strReplace($article->description);
        $userVsArticle = UserVsArticle::find()
            ->select('article_id')
            ->where(['user_id' => Yii::$app->user->id])
            ->asArray()
            ->all();
        $userVsArticle = array_map(function ($value) {
            return $value['article_id'];
        }, $userVsArticle);
        ArticleReady::deleteAll(['user_id' => Yii::$app->user->id, 'article_id' => $id]);
        $ready = new ArticleReady;
        $ready->article_id = $id;
        $ready->user_id = Yii::$app->user->id;
        $ready->ready_at = date('U');
        $ready->save();
        $add = 1;

        return $this->render('view', [
            'article' => $article,
            'article_comments' => $article_comments,
            'categoryId' => $categoryId,
            'add' => $add,
            'userVsArticle' => $userVsArticle
        ]);

    }

    public function actionAddComment()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $article_id = Yii::$app->request->post('article_id');
        $text = Yii::$app->request->post('text');
        $time_zone = Yii::$app->request->post('time_zone') * 60;
        $article = Article::findOne($article_id);

        $orderBy = Yii::$app->request->post('order_by');
        $orderBy = isset($orderBy) ? $orderBy : 'DESC';

        if ($text != '') {
            $comment = new ArticleComment;
            $comment->user_id = Yii::$app->user->id;
            $comment->article_id = $article_id;
            $comment->text = $text;
            $comment->create_dt = date('Y-m-d H:i:s');
            $comment->save(false);
            Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['noreplyEmail'])
                ->setTo(Yii::$app->params['contactsEmail'])
                ->setSubject('Добавлен комментарий')
                ->setTextBody('Добавлен комментарий в статью ' . $article->title . "\n" . 'http://pa.myarete.com/article/default/comments')
                ->send();
        }

        $conn = Yii::$app->getDb();
        $command = $conn->createCommand('SELECT article_comment.id as comment_id,text,create_dt,user.id as user_id,user.email,user.is_confirmed,
                profile.image AS avatar,
                profile.first_name AS first_name,
                profile.last_name AS last_name,
                (SELECT name FROM state WHERE profile.state_id = state.id) AS state,
                profile.view_contacts AS view_contacts
                FROM article_comment
                LEFT JOIN user ON article_comment.user_id = user.id
                LEFT JOIN profile ON article_comment.user_id = profile.user_id
                WHERE article_id=' . $article_id . '
                AND article_comment.visible = 1  ORDER BY create_dt ' . $orderBy
        );
        $response = $command->queryAll();

        foreach ($response as $i => $r)
            $response[$i]['create_dt'] = date('Y-m-d H:i:s', strtotime($r['create_dt']) - $time_zone - 3 * 60 * 60);

        return $response;
    }

    public function actionDeleteComment()
    {
        $id = Yii::$app->request->post('id');

        ArticleComment::findOne($id)->delete();

        return $this->redirect(['view', 'id' => Yii::$app->request->post('article_id')]);
    }

    public function actionSearch()
    {
        $search_string = Yii::$app->request->post('search');

        $searchModel = new ArticleSearch();

        $dataOnTitle = $searchModel->search([
            'ArticleSearch' => [
                'title' => $search_string
            ]
        ]);
        $dataOnTitle->pagination = false;

        $searchModel = new ArticleSearch();

        $dataOnDescription = $searchModel->search([
            'ArticleSearch' => [
                'description' => $search_string
            ]
        ]);
        $dataOnDescription->pagination = false;

        return $this->render('search-view', [
            'search_string' => $search_string,
            'dataOnTitle' => $dataOnTitle,
            'dataOnDescription' => $dataOnDescription
        ]);
    }
}
