<?php

namespace online\modules\communication;

/**
 * communication module definition class
 */
class Communication extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'online\modules\communication\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
