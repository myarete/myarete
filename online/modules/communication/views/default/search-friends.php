<?php

use yii\helpers\Html;

$this->title = 'Поиск друзей';

$js = <<<JS
$('.send-request').click(function() {
    var id = $(this).data('id')
        btn = $(this);
    
    $.post('/communication/default/send-request', {id: id}, function(response) {
        if(response) {
            btn.replaceWith('Запрос отправлен');
        }
    });
});
JS;
$this->registerJs($js);
?>

<div class="crm__container">
    <div class="crm__menu__right">
        <ul class="crm__menu__right-list">
            <li class="crm__menu__right-item"><a class="crm__menu__right-toggle active"
                                                 href="/communication/default/friends">Мои друзья</a></li>
            <li class="crm__menu__right-item active"><a class="crm__menu__right-toggle"
                                                        href="/communication/default/search-friends">Поиск друзей</a>
            </li>
            <li class="crm__menu__right-item"><a class="crm__menu__right-toggle"
                                                 href="/communication/default/friend-requests">Запросы в друзья
                    (<?= $incoming_request ?>)</a></li>
        </ul>
    </div>
    <div class="crm__content">
        <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
            <?= \kartik\alert\AlertBlock::widget([
                'delay' => false
            ]) ?>
        <?php endif; ?>
        <div class="wihite__box shadow">
            <div class="page__header_title">Поиск друзей</div>
            <div class="page__header_search-row">
                <?= Html::beginForm('search-friends') ?>
                <div class="page__header_search">
                    <?= Html::textInput('UserSearch[search]', '', ['class' => 'page__header_input', 'placeholder' => 'Введите любое имя']) ?>
                    <i class="fa fa-search page__header_i"></i>
                </div>
                <?= Html::submitButton('Найти', ['class' => 'button button__green page__header_btn']) ?>
                <?= Html::endForm() ?>
            </div>
            <div class="event__content">
                <?php if (count($dataProvider->models)): ?>
                    <?= $this->render('_list3', [
                        'friends' => $dataProvider->models,
                    ]) ?>
                <?php else: ?>
                    <p>Не удалось никого найти</p>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>

<style>
    /* Right-menu */
    
    .crm__content {
        padding-right: 310px;
    }
    
    .crm__menu__right {
        position: absolute;
        width: 250px;
        background: #fff;
        top: 0;
        right: 0;
        margin-top: 30px;
        margin-right: 30px;
        border-left: 1px solid #ededed;
        z-index: 6;
    }
    
    .crm__menu__right-list {
        padding: 9px 20px 9px 0;
    }
    
    .crm__menu__right-item {
        display: table;
        width: 100%;
        height: 66px;
        border-left: 5px solid transparent;
        -webkit-transition: all .3s;
        -o-transition: all .3s;
        transition: all .3s;
    }
    
    .crm__menu__right-item:hover,
    .crm__menu__right-item.active {
        background: rgba(71, 133, 252, 0.1);
        border-left: 5px solid #4785fc;
        border-radius: 0 3px 3px 0;
    }
    
    .crm__menu__right-item:hover .crm__menu__right-toggle,
    .crm__menu__right-item.active .crm__menu__right-toggle {
        color: #333;
    }
    
    .crm__menu__right-toggle {
        display: table-cell;
        vertical-align: middle;
        color: #939aab;
        font-weight: 500;
        height: 66px;
        padding-left: 20px;
        font-size: 16px;
        -webkit-transition: all .3s;
        -o-transition: all .3s;
        transition: all .3s;
        cursor: pointer;
    }
    
    .crm__menu__right-toggle:hover {
        text-decoration: none;
    }
    
    /* --- */
    
    @media screen and (max-width: 768px) {
        .crm__container {
            padding: 10px;
        }
        
        .crm__content {
            padding: 10px 0 0 0;
        }
        
        .crm__menu__right {
            position: relative;
            width: 100%;
            margin: 0;
        }
    }
</style>

