<?php

use common\models\Training;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

//$this->registerJsFile('/js/plugins/jquery/jquery.cookie.js', ['depends' => ['online\assets\AppAsset']]);
$this->title = 'Обмен материалами';

$groups_array = [];
foreach ($groups as $group) {
    $groups_array[$group->id] = $group->name;
}

$training_array = ArrayHelper::map($trainings, 'id', 'name');
$training_array[0] = 'Название тренировки';
ksort($training_array);
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="store__ban" style="background-image: url(/img/ban-bg.jpg)">
            <div class="store__ban_title-wrap">
                <div class="store__ban_title">
                    <div class="store__ban_title-text">Попробуйте готовые тренировки <br>из нашего магазина</div>
                </div>
            </div>
        </div>
        <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
            <?= \kartik\alert\AlertBlock::widget([
                'delay' => false
            ]) ?>
        <?php endif; ?>
        <div class="wihite__box shadow">
            <div class="send__traning">
                <?= Html::beginForm('', 'post') ?>
                <div class="send__traning_top">
                    <div class="send__traning_left">
                        <a class="button button__blue button__question" data-toggle="modal" data-target="#questions_and_answers__modal">
                            Вопросы и ответы
                            <div class="fa fa-question"></div>
                        </a>
                    </div>
                    <div class="send__traning_right">
                        <button type="submit" class="button button__green--full">Отправить тренировку</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Выбрать группу</label>
                        <?php
                        echo Select2::widget([
                            'name' => 'group_ids',
                            'data' => $groups_array,
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder' => 'Наименование группы ...', 'multiple' => true, 'autocomplete' => 'off'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <label>Выбрать тренировку</label>
                        <?php
                        echo Select2::widget([
                            'name' => 'training_ids',
                            'data' => $training_array,
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder' => 'Название тренировки ...', 'multiple' => true, 'autocomplete' => 'off'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                </div>
                <?= Html::endForm() ?>
            </div>
            <?php if (count($materials)): ?>
                <?php foreach ($materials as $record): ?>
                    <div class="row">
                        <div class="col-md-4">
                            <?php
                            $training = Training::findOne($record->training_id);
                            echo Html::a($training->name, ['/communication/default/run-training', 'id' => $training->id]);
                            ?>
                        </div>
                        <div class="col-md-4">
                            <div class="table__row_label">Куда:</div>
                            <?= Html::a($record->group->name, ['/teacher/default/view-group', 'id' => $record->group->id]) ?>
                        </div>
                        <div class="col-md-offset-3 col-md-1">
                            <div class="button__group">
                                <?= Html::a('Открыть', ['/communication/default/run-training', 'id' => $training->id],
                                    ['class' => 'button button__green']) ?>
                                <?= Html::a('<i class="fa fa-trash-o"></i>', ['delete-training', 'id' => $record->id], [
                                    'data' => ['confirm' => 'Вы уверены?', 'method' => 'post'], 'class' => 'button button__red button__icon']) ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <p>У вас пока нет материалов</p>
            <?php endif; ?>
        </div>
    </div>
</div>
