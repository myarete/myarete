<?php

use yii\helpers\Html;
use common\models\Training;
use common\models\Testing;

$record = $materials[0];
if($record->from_user_id == Yii::$app->user->id) {
    if($record->toUser->profile->first_name || $record->toUser->profile->last_name)
        $user_name = $record->toUser->profile->first_name.' '.$record->toUser->profile->last_name;
    else
        $user_name = $record->toUser->email;
} else {
    if($record->fromUser->profile->first_name || $record->fromUser->profile->last_name)
        $user_name = $record->fromUser->profile->first_name.' '.$record->fromUser->profile->last_name;
    else
        $user_name = $record->fromUser->email;
}
?>

<div class="row">
    <div class="col-sm-4 col-xs-12">
        <?= Html::a('<-- Назад', ['send-material'], ['class' => 'btn btn-default pull-left']) ?>
    </div>
    <div class="col-sm-8 col-xs-12">
        <h4 class="pull-left"><?= $user_name ?></h4>
    </div>
</div>

<div class="row material-header">
    <div class="col-sm-3">
        Наименование
    </div>
    <div class="col-sm-2">
        Тип
    </div>
    <div class="col-sm-2">
        Дата/время
    </div>
    <div class="col-sm-2">
        От кого
    </div>
    <div class="col-sm-2">
        Кому
    </div>
    <div class="col-sm-1">
    </div>
</div>
<?php foreach($materials as $record): ?>
    <?php
        if($record->from_user_id == Yii::$app->user->id)
            $background = '#eee';
        else
            $background = '#aaff7f';
    ?>
    <div class="row" style="background-color:<?= $background ?>">
        <div class="col-sm-3">
            <?php
                if($record->className() == 'common\models\SendTraining') {
                    $training = Training::findOne($record->training);
                    echo Html::a($training->name, ['/communication/default/run-training', 'id' => $training->id, 'params' => $record->training_params]);
                } else {
                    $testing = Testing::findOne($record->testing);
                    echo Html::a($testing->name, ['/communication/default/run-testing', 'id' => $testing->id]);
                }
            ?>
        </div>
        <div class="col-sm-2">
            <p><?= $record->type ?></p>
        </div>
        <div class="col-sm-2">
            <p class="dt"><?= date('d.m.Y H:i:s', strtotime($record->dt)) ?></p>
        </div>
        <div class="col-sm-2">
            <?php
                if($record->fromUser->profile->first_name || $record->fromUser->profile->last_name)
                    echo Html::a($record->fromUser->profile->first_name.' '.$record->fromUser->profile->last_name,
                        ['view', 'id' => $record->from_user_id]);
                else
                    echo Html::a($record->fromUser->email, ['/profile/default/profile/', 'id' => $record->from_user_id]);
            ?>
        </div>
        <div class="col-sm-2">
            <?php
                if($record->toUser->profile->first_name || $record->toUser->profile->last_name)
                    echo Html::a($record->toUser->profile->first_name.' '.$record->toUser->profile->last_name,
                        ['view', 'id' => $record->to_user_id]);
                else
                    echo Html::a($record->toUser->email, ['/profile/default/profile/', 'id' => $record->to_user_id]);
            ?>
        </div>
        <div class="col-sm-1">
            <?php if($record->className() == 'common\models\SendTraining'): ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete-training', 'id' => $record->id], [
                    'data' => ['confirm' => 'Вы уверены?', 'method' => 'post']]) ?>
            <?php else: ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete-testing', 'id' => $record->id], [
                    'data' => ['confirm' => 'Вы уверены?', 'method' => 'post']]) ?>
            <?php endif ?>
        </div>
    </div>
<?php endforeach ?>

