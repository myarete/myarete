<?php

use common\models\Chat;
use yii\helpers\Html;

$where_array[0] = 'Кому отправить';

foreach ($friends as $friend) {
    if ($friend->from_user_id == Yii::$app->user->id) {
        if ($friend->toUser->profile->first_name || $friend->toUser->profile->last_name) {
            $where_array[$friend->toUser->id] = $friend->toUser->profile->first_name . ' ' . $friend->toUser->profile->last_name;
        } else {
            $where_array[$friend->toUser->id] = $friend->toUser->email;
        }
    } else {
        if ($friend->fromUser->profile->first_name || $friend->fromUser->profile->last_name) {
            $where_array[$friend->fromUser->id] = $friend->fromUser->profile->first_name . ' ' . $friend->fromUser->profile->last_name;
        } else {
            $where_array[$friend->fromUser->id] = $friend->fromUser->email;
        }
    }
}

$js = <<<JS
$('.send-btn').click(function() {
    var id = $(this).closest('.modal').data('id'),
        text = $(this).closest('.modal').find('textarea').val(),
        modal = $(this).closest('.modal');
    if(text.length > 0) {
        $.post('/communication/default/create-message', {id: id, text: text}, function(response) {
            var html = '<div class="msg-out"><div class="dt">[' + response.dt + '] ' + response.user_name + '</div>';
            html += '<div class="text">' + text + '</div></div><hr>';
            modal.find('.msg-content').prepend(html);
            modal.find('textarea').val('');
            if(modal.attr('id') == 'new-dialog') {
                sessionStorage.setItem('key', 'value');
                location.reload();
            }
        });
    }
});
$('#select-friend-btn').click(function() {
    var id = $('#select-friend select').val(),
        new_dialog = true;
    $.each($('[id^=chat-modal-]'), function(i, e) {
        if($(e).data('id') == id) {
            $(e).modal('show');
            new_dialog = false;
            return false;
        }
    });
    if(new_dialog) {
        $('#new-dialog').data('id', id);
        $('#new-dialog .modal-title').text($('#select-friend select option:selected').text());
        $('#new-dialog').modal('show');
    }
});
JS;
$this->registerJs($js);
?>
<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="side-block">
        <div class="row">
            <div class="col-sm-10 col-xs-12">
                <h2 class="pull-left">Сообщения</h2>
            </div>
            <div class="col-sm-2 col-xs-12">
                <?= Html::button('Новый диалог', ['class' => 'btn btn-primary send-to',
                    'data' => ['toggle' => 'modal', 'target' => '#select-friend']]) ?>
            </div>
        </div>
        <div id="dialog-content">
            <?php foreach ($messages as $i => $message): ?>
                <?php if ($message->user_id == Yii::$app->user->id): ?>
                    <?php if ($message->toUser->profile->first_name || $message->toUser->profile->last_name): ?>
                        <?php $user_name = $message->toUser->profile->first_name . ' ' . $message->toUser->profile->last_name; ?>
                    <?php else: ?>
                        <?php $user_name = $message->toUser->email; ?>
                    <?php endif ?>
                    <?php $id = $message->to_user_id; ?>
                <?php else: ?>
                    <?php if ($message->user->profile->first_name || $message->user->profile->last_name): ?>
                        <?php $user_name = $message->user->profile->first_name . ' ' . $message->user->profile->last_name; ?>
                    <?php else: ?>
                        <?php $user_name = $message->user->email; ?>
                    <?php endif ?>
                    <?php $id = $message->user_id; ?>
                <?php endif ?>
                <p>
                    <?= Html::a($user_name, '#', [
                        'data' => [
                            'target' => '#chat-modal-' . $i,
                            'toggle' => 'modal'
                        ]
                    ]) ?>
                </p>
                <div class="modal fade" id="chat-modal-<?= $i ?>" data-backdrop="true" data-id="<?= $id ?>">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title"><?= $user_name ?></h4>
                            </div>
                            <div class="modal-body">
                                <div class="msg-content">
                                    <?php foreach (Chat::chatContents($message->chat) as $content): ?>
                                        <?php if ($content->user_id == Yii::$app->user->id): ?>
                                            <?php if ($message->user->profile->first_name || $message->user->profile->last_name): ?>
                                                <?php $user_name = $message->user->profile->first_name . ' ' . $message->user->profile->last_name; ?>
                                            <?php else: ?>
                                                <?php $user_name = $message->user->email; ?>
                                            <?php endif ?>
                                            <div class="msg-out">
                                                <div class="dt">[<?= date('d.m.Y H:i:s', strtotime($content->dt)) ?>
                                                    ] <?= $user_name ?>:
                                                </div>
                                                <div class="text"><?= $content->text ?></div>
                                            </div>
                                        <?php else: ?>
                                            <?php if ($message->toUser->profile->first_name || $message->toUser->profile->last_name): ?>
                                                <?php $user_name = $message->toUser->profile->first_name . ' ' . $message->toUser->profile->last_name; ?>
                                            <?php else: ?>
                                                <?php $user_name = $message->toUser->email; ?>
                                            <?php endif ?>
                                            <div class="msg-in">
                                                <div class="dt">[<?= date('d.m.Y H:i:s', strtotime($content->dt)) ?>
                                                    ] <?= $user_name ?>:
                                                </div>
                                                <div class="text"><?= $content->text ?></div>
                                            </div>
                                        <?php endif ?>
                                        <hr>
                                    <?php endforeach ?>
                                </div>
                                <?= Html::textarea('text', '', ['class' => 'form-control']) ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary send-btn">Отправить</button>
                                <button type="button" class="btn btn-default cancel" data-dismiss="modal">Закрыть
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
        
        <div class="modal fade" id="select-friend" data-backdrop="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Открыть диалог с:</h4>
                    </div>
                    <div class="modal-body">
                        <?= Html::dropDownList('select_friend', '', $where_array, [
                            'class' => 'form-control',
                        ]) ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="select-friend-btn" class="btn btn-primary" data-dismiss="modal">Ok
                        </button>
                        <button type="button" class="btn btn-default cancel" data-dismiss="modal">Отмена</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="new-dialog" data-backdrop="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="msg-content">
                        </div>
                        <?= Html::textarea('text', '', ['class' => 'form-control']) ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary send-btn">Отправить</button>
                        <button type="button" class="btn btn-default cancel" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br>
</div>
