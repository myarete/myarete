<?php
use yii\widgets\DetailView;
use yii\grid\GridView;

$this->title = 'Информация о пользователе';
?>

<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="article-block">
        <div class="row side-block">
            <h2><?= $this->title ?></h2>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'profile.image',
                        'format' => 'raw',
                        'value' => function($model) {
                            return $model->profile->getImageTag([
                                        'defaultSrc' => '/img/default_user.png',
                                        'style' => 'max-width: 100px;',
                                    ]);
                        }
                    ],
                    [
                        'attribute' => 'email',
                        'format' => 'raw',
                        'visible' => $model->profile->view_contacts,
                        'value' => function($model) {
                            return preg_match('/\@/', $model->email) ? $model->email : $model->profile->email;
                        }
                    ],
                    'profile.first_name',
                    'profile.last_name',
                    'profile.birthdate',
                    'profile.state.name:text:Статус',
                ],
            ]) ?>
        </div>
    </div>
</div>

