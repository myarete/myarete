<?php

use common\components\other\History;
use yii\helpers\Html;
use yii\helpers\Url;

History::put(Yii::$app->user->identity->email, $training->name);

$this->title = $training->name;

$js = <<<JS
$('#showSetup').click(function() {
    $('#trainingSetup').modal();
});
$('.setup-btn').click(function() {
    if($('#showHint').is(':checked')) {
        $('a[href ^= #hint-]').removeClass('collapsed').attr('aria-expanded', true);
        $('div[id ^= hint-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
    } else {
        $('a[href ^= #hint-]').addClass('collapsed').attr('aria-expanded', false);
        $('div[id ^= hint-]').removeClass('in').attr('aria-expanded', false);
    }
    if($('#showDecision').is(':checked')) {
        $('a[href ^= #decision-]').removeClass('collapsed').attr('aria-expanded', true);
        $('div[id ^= decision-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
    } else {
        $('a[href ^= #decision-]').addClass('collapsed').attr('aria-expanded', false);
        $('div[id ^= decision-]').removeClass('in').attr('aria-expanded', false);
    }
    if($('#showAnswer').is(':checked')) {
        $('a[href ^= #answer-]').removeClass('collapsed').attr('aria-expanded', true);
        $('div[id ^= answer-]').addClass('in').attr('aria-expanded', true).removeAttr('style');
    } else {
        $('a[href ^= #answer-]').addClass('collapsed').attr('aria-expanded', false);
        $('div[id ^= answer-]').removeClass('in').attr('aria-expanded', false);
    }
    if($('#shuffleExercises').is(':checked')) {
        $.post('/training/default/shuffle-exercises', {id: '<?= $training->id ?>'});
        setTimeout(function() {
            location.reload();
        }, 500);
    }
    $('#trainingSetup').hide();
});
JS;
$this->registerJs($js);
?>

<div class="traning">
    <div class="traning__title"><?= $training->name ?></div>
    <div class="traning__top">
        <a class="button button__blue button__question" data-toggle="modal" data-target="#questions_and_answers__modal">Вопросы
            и ответы
            <div class="fa fa-question"></div>
        </a>
        <div class="button__group_flip">
            <button class="button button__round button__red" data-dismiss="modal">Отменить</button>
            <button class="button button__round button__green" id="save-params-btn" data-dismiss="modal"
                    data-id="<?= $training->id ?>">Сохранить
            </button>
        </div>
    </div>
    <div class="traning__list">
        <?php foreach ($training->trainingExercises as $i => $trainingExercise): ?>
            <!-- begin traning__list_item 1 -->
            <div class="traning__list_item" id="id_<?= $trainingExercise->id ?>">
                <div class="traning__list_count"><?= $i + 1 ?> </div>
                <div class="traning__list_action">
                    <a class="button button__round button__green traning__btn_more" href="#">
                        <span class="traning__btn_more--show">Подробнее </span>
                        <span class="traning__btn_more--hide">Скрыть </span>
                    </a>
                </div>
                <div class="traning__list_center" id="exercise-info-<?= $i ?>" class="collapse"
                     data-id="<?= $trainingExercise->variant->exercise->id ?>">
                    <div class="traning__list_row">
                        <div class="traning__task">
                            <p><label for="hide_exercise-<?= $i ?>"><input type="checkbox" id="hide_exercise-<?= $i ?>"
                                                                           data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                    Не отсылать задачу</label></p>
                            <p><?= $trainingExercise->variant->exercise->question ?></p>
                        </div>
                    </div>
                    <?php if (!empty($trainingExercise->variant->exercise->hint)): ?>
                        <div class="traning__list_row">
                            <a class="traning__link"
                               href="#<?= "hint-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                               data-traning-link="<?= "hint-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                               data-toggle="collapse">
                                <span class="traning__link--show">Показать подсказку</span>
                                <span class="traning__link--hide">Скрыть подсказку</span>
                            </a>
                            <div class="traning__link_slide collapse"
                                 id="<?= "hint-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                <p><?= $trainingExercise->variant->exercise->hint ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                    
                    <?php if (!empty($trainingExercise->variant->exercise->hint_second)): ?>
                        <div class="traning__list_row">
                            <a class="traning__link"
                               href="#<?= "hint_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                               data-traning-link="<?= "hint_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                               data-toggle="collapse">
                                <span class="traning__link--show">Показать вторую подсказку</span>
                                <span class="traning__link--hide">Скрыть вторую подсказку</span>
                            </a>
                            <div class="traning__link_slide collapse"
                                 id="<?= "hint_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                <p><?= $trainingExercise->variant->exercise->hint_second ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                    
                    <?php if (!empty($trainingExercise->variant->exercise->hint_third)): ?>
                        <div class="traning__list_row">
                            <a class="traning__link"
                               href="#<?= "hint_third-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                               data-traning-link="<?= "hint_third-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                               data-toggle="collapse">
                                <span class="traning__link--show">Показать третью подсказку</span>
                                <span class="traning__link--hide">Скрыть третью подсказку</span>
                            </a>
                            <div class="traning__link_slide collapse"
                                 id="<?= "hint_third-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                <p><?= $trainingExercise->variant->exercise->hint_third ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                    
                    <?php if (!empty($trainingExercise->variant->exercise->decision)): ?>
                        <div class="traning__list_row">
                            <a class="traning__link"
                               href="#<?= "decision-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                               data-traning-link="<?= "decision-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                               data-toggle="collapse">
                                <span class="traning__link--show">Показать решение</span>
                                <span class="traning__link--hide">Скрыть решение</span>
                            </a>
                            <div class="traning__link_slide collapse"
                                 id="<?= "decision-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                <p><?= $trainingExercise->variant->exercise->decision ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                    
                    <?php if (!empty($trainingExercise->variant->exercise->decision_detailed)): ?>
                        <div class="traning__list_row">
                            <a class="traning__link"
                               href="#<?= "decision_detailed-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                               data-traning-link="<?= "decision_detailed-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                               data-toggle="collapse">
                                <span class="traning__link--show">Показать подробное решение</span>
                                <span class="traning__link--hide">Скрыть подробное решение</span>
                            </a>
                            <div class="traning__link_slide collapse"
                                 id="<?= "decision_detailed-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                <p><?= $trainingExercise->variant->exercise->decision_detailed ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                    
                    <?php if (!empty($trainingExercise->variant->exercise->decision_second)): ?>
                        <div class="traning__list_row">
                            <a class="traning__link"
                               href="#<?= "decision_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                               data-traning-link="<?= "decision_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                               data-toggle="collapse">
                                <span class="traning__link--show">Показать второе решение</span>
                                <span class="traning__link--hide">Скрыть второе решение</span>
                            </a>
                            <div class="traning__link_slide collapse"
                                 id="<?= "decision_second-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                <p><?= $trainingExercise->variant->exercise->decision_second ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                    
                    <?php if (!empty($trainingExercise->variant->exercise->answer)): ?>
                        <div class="traning__list_row">
                            <a class="traning__link"
                               href="#<?= "answer-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                               data-traning-link="<?= "answer-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                               data-toggle="collapse">
                                <span class="traning__link--show">Показать ответ</span>
                                <span class="traning__link--hide">Скрыть ответ</span>
                            </a>
                            <div class="traning__link_slide collapse"
                                 id="<?= "answer-{$i}-{$trainingExercise->variant->exercise->id}" ?>"
                                 data-id="<?= $trainingExercise->variant->exercise->id ?>">
                                <p><?= $trainingExercise->variant->exercise->answer ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                
                </div>
                <div class="traning__sublist">
                    <div class="traning__sublist_row">
                        <div class="traning__sublist_name">Задача
                            №<?= $trainingExercise->variant->exercise->id ?> <?= $trainingExercise->variant->exercise->name ?></div>
                    </div>
                    <div class="traning__sublist_row">
                        <div class="traning__sublist_name">Тема:</div>
                        <div class="traning__sublist_text"><?= $trainingExercise->variant->theme->getFullName() ?></div>
                    </div>
                    <div class="traning__sublist_row">
                        <div class="traning__sublist_name">Сложность:</div>
                        <div class="traning__sublist_text"><?= $trainingExercise->variant->complexity ?></div>
                    </div>
                    <?php $rating = $trainingExercise->variant->exercise->getRate()->where(['user_id' => $training->user_id])->one(); ?>
                    <?php if (!$rating): ?>
                        <br>
                        <button id="rate-btn-<?= $trainingExercise->variant->exercise->id ?>"
                                class="button button__round button__green"
                                onclick="
                                    $('.rate-<?= $trainingExercise->variant->exercise->id ?>').show();
                                    $('#close-rate-<?= $trainingExercise->variant->exercise->id ?>').show();
                                    $(this).hide()">Оценить задачу
                        </button>
                    <?php endif; ?>
                    
                    <div
                        class="traning__sublist_row rate-<?= $trainingExercise->variant->exercise->id; ?>" <?= (($rating) ? '' : 'style="display:none;"') ?>>
                        <div class="traning__sublist_name">Оцененная сложность
                            задачи: <?= ($rating) ? $rating->rating . '/10' : '' ?> &nbsp
                        </div>
                        <div class="traning__sublist_text">
                            <div class="traning__rate">
                                <?php for ($n = 1; $n <= 10; $n++): ?>
                                    <?= Html::a('<i class="fa fa-star' . (($rating && $n <= $rating->rating) ? '' : '-o') . '"></i>', Url::to([
                                        '/exercise/rate/add',
                                        'exercise_id' => $trainingExercise->variant->exercise->id,
                                        'class' => $training->user->profile->state_id,
                                        'rating' => $n,
                                        'training_id' => $training->id,
                                    ])) ?>
                                <?php endfor; ?>
                            </div>
                        </div>
                        <br>
                        <button class="button button__round button__red btn-close-rate"
                                id="close-rate-<?= $trainingExercise->variant->exercise->id ?>"
                                onclick="
                                    $('.rate-<?= $trainingExercise->variant->exercise->id ?>').hide();
                                    $('#rate-btn-<?= $trainingExercise->variant->exercise->id ?>').show();
                                    $(this).hide()">Скрыть
                        </button>
                    </div>
                </div>
            </div>
            <!-- end traning__list_item 1 -->
        <?php endforeach; ?>
    </div>
</div>

<div class="modal fade training-setup" id="trainingSetup" data-backdrop="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close setup-btn"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span>
                </button>
                <h4 class="modal-title">Настройка тренинга</h4>
            </div>
            <div class="modal-body replace_item_modal_body">
                <ul>
                    <li><label><input type="checkbox" id="showHint"> Показать подсказки</label></li>
                    <li><label><input type="checkbox" id="showDecision"> Показать решения</label></li>
                    <li><label><input type="checkbox" id="showAnswer"> Показать ответы</label></li>
                    <li><label><input type="checkbox" id="shuffleExercises"> Перемешать задачи</label></li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white setup-btn">Ок</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="why-configure-in-training" data-backdrop="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                        class="sr-only">Закрыть</span></button>
                <h4 class="modal-title">Зачем настраивать параметры в тренировке?</h4>
            </div>
            <div class="modal-body">
                <p>
                    Отправляя тренировку другу, вы можете полностью настраивать
                    ее вид для него.
                    Все поля (подсказки, решения, ответы, информация о задаче),
                    которые вы откроете, будут доступны адресату. Он сможет их
                    открывать и закрывать по своему усмотрению.
                    Если вы не откроете какое-то поле, то адресат его просто не
                    увидит. Вы так же можете поставить галочку для «Не отсылать
                    задачу». Тогда адресат увидит тренировку без этой задачи.
                    Вы как отправитель всегда сможете увидеть полную версию
                    тренировки.
                    Настраивать параметры тренировок полезно, если вы хотите
                    давать домашние или классные задания, тренировки, тесты,
                    диагностики.
                </p>
                <?= Html::a('Подробнее', ['page', 'view' => 'why_configure_in_training_detail']) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
