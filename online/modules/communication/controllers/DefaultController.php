<?php

namespace online\modules\communication\controllers;

use common\models\Chat;
use common\models\FriendRequest;
use common\models\Group;
use common\models\SendTesting;
use common\models\SendTraining;
use common\models\Training;
use common\models\TrainingVsGroup;
use common\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ViewAction;

/**
 * Default controller for the `messages` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'send-material',
                            'send-training-to',
                            'send-testing-to',
                            'run-training',
                            'run-testing',
                            'params',
                            'delete-training',
                            'delete-testing',
                        ],
                        'roles' => ['teacher']
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'friend-requests',
                            'confirm',
                            'not-confirm',
                            'friends',
                            'search-friends',
                            'send-request',
                            'view',
                            'remove-friend',
                            'remove-friend-from-profile',
                            'confirm-friend-from-profile',
                            'get-avatar',
                            'page',
                            'create-message',
                            'messages',
                            'get-materials'
                        ],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'page' => [
                'class' => ViewAction::className(),
            ],
        ];
    }

    public function actionSendMaterial()
    {
        $groups = Group::findAll(['user_id' => Yii::$app->user->id]);
        $trainings = Training::findAll(['user_id' => Yii::$app->user->id]);
        $materials = TrainingVsGroup::find()
            ->where(['group_id' => Group::find()->select('id')->where(['user_id' => Yii::$app->user->id])])
            ->orderBy('create_dt DESC')->all();

        if ($post = Yii::$app->request->post()) {
            foreach ($post['group_ids'] as $group_id) {
                foreach ($post['training_ids'] as $training_id) {
                    if (!TrainingVsGroup::findAll(['group_id' => $group_id, 'training_id' => $training_id])) {
                        $training_vs_group = new TrainingVsGroup();
                        $training_vs_group->group_id = $group_id;
                        $training_vs_group->training_id = $training_id;
                        $training_vs_group->save();
                    }
                }
            }
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('send-material', [
            'groups' => $groups,
            'trainings' => $trainings,
            'materials' => $materials,
        ]);
    }

    public function actionView($id)
    {
        $model = User::findOne($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionGetAvatar()
    {
        Yii::$app->response->format = 'json';

        $model = User::findOne(Yii::$app->request->post('id'));

        return $model->profile->getImageTag([
            'defaultSrc' => '/img/default_user.png',
            'style' => 'max-width: 64px;',
        ]);
    }

    public function actionParams($id)
    {
        $this->layout = false;

        $training = Training::findOne($id);

        if (!$training->trainingExercises) {
            Yii::$app->session->setFlash('error', 'Не сохранено, для этой тренировки задачи не найдены.');
            $training->delete();
            return $this->redirect(['/main/default/index']);
        }

        if ($training === null)
            $this->pageNotFound();

        return $this->render('params', [
            'training' => $training,
        ]);
    }

    public function actionRunTraining($id, $params = null)
    {
        $training = Training::findOne($id);

        return $this->render('@online/modules/training/views/default/run_communication', [
            'training' => $training
        ]);
    }

    public function actionDeleteTraining($id)
    {
        TrainingVsGroup::findOne($id)->delete();

        return $this->redirect(['send-material']);
    }

    public function actionMessages()
    {
        if (Yii::$app->user->can('teacher')) {
            $learners = Learner::find()
                ->select('learner_id')
                ->where(['teacher_id' => Yii::$app->user->id, 'is_confirmed' => 1]);
            $friends = FriendRequest::find()
                ->where(['or', ['to_user_id' => $learners], ['from_user_id' => $learners]])
                ->andWhere(['confirmed' => 1])
                ->all();
        } elseif (Yii::$app->user->can('user')) {
            $teachers = Learner::find()
                ->select('teacher_id')
                ->where(['learner_id' => Yii::$app->user->id, 'is_confirmed' => 1]);
            $friends = FriendRequest::find()
                ->where(['or', ['to_user_id' => $teachers], ['from_user_id' => $teachers]])
                ->andWhere(['confirmed' => 1])
                ->all();
        } else {
            $friends = [];
        }

        $messages = Chat::find()
            ->where(['or', ['user_id' => Yii::$app->user->id], ['to_user_id' => Yii::$app->user->id]])
            ->groupBy('chat')
            ->all();

        return $this->render('messages', [
            'messages' => $messages,
            'friends' => $friends
        ]);
    }

    public function actionCreateMessage()
    {
        Yii::$app->response->format = 'json';

        $id = Yii::$app->request->post('id');
        $text = Yii::$app->request->post('text');
        $my_id = Yii::$app->user->id;

        $chat = $id > $my_id ? $my_id . '-' . $id : $id . '-' . $my_id;

        $model = new Chat;
        $model->user_id = $my_id;
        $model->to_user_id = $id;
        $model->chat = $chat;
        $model->text = $text;
        $model->dt = date('Y-m-d H:i:s');
        $model->save();

        if ($model->user->profile->first_name || $model->user->profile->last_name)
            $user_name = $model->user->profile->first_name . ' ' . $model->user->profile->last_name;
        else
            $user_name = $model->user->email;

        return [
            'dt' => date('d.m.Y H:i:s', strtotime($model->dt)),
            'user_name' => $user_name,
        ];
    }
}
