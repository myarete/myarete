<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model online\modules\profile\models\Profilepublication */
/* @var $form yii\widgets\ActiveForm */


Modal::begin([
    'options' => [
        'id' => 'publication'
    ],
    'size' => 'modal-lg',
    'header' => '<h2>Добавить публикацию</h2>',
]);
?>
<div class="side-block">
    <div class="profile-publication-form">

        <?php $form = ActiveForm::begin(
            /*[
                'action' => ['/profile/default/add-publication'],
                'method' => 'post',
                'options' => [
                    'class' => ''
                ],
                'id' => 'form-add-publication'
            ]*/); ?>


        <?= $form->field($publication, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($publication, 'publication')->textInput(['maxlength' => true]) ?>

        <?= $form->field($publication, 'publication_date')->widget(\kartik\widgets\DatePicker::className(), [
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd.mm.yyyy',
            ],
        ]);
        ?>

        <?= $form->field($publication, 'url')->textInput(['maxlength' => true]) ?>

        <?= $form->field($publication, 'description')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton($publication->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $publication->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?php
Modal::end();
?>