<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model online\modules\profile\models\ProfileSkill */
/* @var $form yii\widgets\ActiveForm */


Modal::begin([
    'options' => [
        'id' => 'languages'
    ],
    'size' => 'modal-lg',
    'header' => '<h2>Добавить язык</h2>',
]);
?>
<div class="side-block">
    <div class="profile-languages-form">

        <?php $form = ActiveForm::begin(
            /*[
                'action' => ['/profile/default/add-education'],
                'method' => 'post',
                'options' => [
                    'class' => ''
                ],
                'id' => 'form-add-education'
            ]*/); ?>


        <?= $form->field($languages, 'lang')->textInput(['maxlength' => true]) ?>

        <?= $form->field($languages, 'level')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton($languages->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $languages->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?php
Modal::end();
?>