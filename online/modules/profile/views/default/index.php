<?php

use kartik\alert\AlertBlock;
use yii\helpers\Html;

$this->title = "Профиль";
?>

<div class="crm__container">
    <div class="crm__content">
        <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
            <?= AlertBlock::widget([
                'delay' => false
            ]) ?>
        <?php endif; ?>
        <div class="profile__persons shadow">
            <div class="profile__persons-header">
                <div class="img-circle">
                    <?= Yii::$app->user->isGuest ? '' : Yii::$app->user->model->profile->getImageTag(['defaultSrc' => '/img/default_user.png']) ?>
                </div>
                <?= Html::a('Изменить фотографию', ['/settings'], ['class' => 'button button__blue']) ?>
                <h3 class="profile__persons-title"><?= Yii::$app->user->model->profile->first_name . ' ' . Yii::$app->user->model->profile->last_name ?></h3>
                <div class="profile__persons-proof"><?= $state->name ?></div>
            </div>

            <?php
            // Образование
            $count_education = count($dataProviderEducation->models);

            // Навыки
            $count_skill = count($dataProviderSkill->models);

            // Публикации
            $count_publication = count($dataProviderPublication->models);

            // Языки
            $count_languages = count($dataProviderLanguages->models);

            if ($count_education > 0 || $count_skill > 0 || $count_publication > 0 || $count_languages > 0): ?>
                <div class="profile__persons-info">
                    <?php
                    // Образование
                    echo $this->render('/education/_form', [
                        'education' => $education,
                    ]);
                    ?>

                    <?php if ($count_education > 0): ?>
                        <div class="profile__persons-info_names"><i class="fa fa-envelope" aria-hidden="true"></i>образование</div>
                        <ul class="profile__persons-info_list">
                            <?php foreach ($dataProviderEducation->models as $model): ?>
                                <li>
                                    <?php if ($model->from_date && $model->to_date): ?>
                                        <p class="period"><?= substr($model->from_date, 0, 4); ?>
                                            - <?= substr($model->to_date, 0, 4); ?></p>
                                    <?php endif; ?>
                                    <p><?= $model->school ?></p>
                                </li>
                            <?php endforeach; ?>
                        </ul>

                    <?php endif; ?>

                    <?php
                    // Навыки
                    echo $this->render('/skill/_form', [
                        'skill' => $skill,
                    ]);
                    ?>

                    <?php if ($count_skill > 0): ?>
                        <div class="profile__persons-info_names">
                            <i class="fa fa-envelope" aria-hidden="true"></i>навыки и умения
                        </div>
                        <ul class="profile__persons-info_list">
                            <?php foreach ($dataProviderSkill->models as $model): ?>
                                <li>
                                    <?php if ($model->from_date && $model->to_date): ?>
                                        <p class="period"><?= substr($model->from_date, 0, 4); ?>
                                            - <?= substr($model->to_date, 0, 4); ?></p>
                                    <?php endif; ?>
                                    <p><?= $model->skill ?></p>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>

                    <?php
                    // Публикации
                    echo $this->render('/publication/_form', [
                        'publication' => $publication,
                    ]);
                    ?>

                    <?php if ($count_skill > 0): ?>
                        <div class="profile__persons-info_names"><i class="fa fa-envelope" aria-hidden="true"></i>публикации
                        </div>
                        <ul class="profile__persons-info_list">
                            <?php foreach ($dataProviderPublication->models as $model): ?>
                                <li>
                                    <?php if ($model->from_date && $model->to_date): ?>
                                        <p class="period"><?= substr($model->from_date, 0, 4); ?>
                                            - <?= substr($model->to_date, 0, 4); ?></p>
                                    <?php endif; ?>
                                    <p><?= $model->publication ?></p>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>

                    <?php
                    // Языки
                    echo $this->render('/languages/_form', [
                        'languages' => $languages,
                    ]);
                    ?>

                    <?php if ($count_languages > 0): ?>
                        <div class="profile__persons-info_names"><i class="fa fa-envelope" aria-hidden="true"></i>языки
                        </div>
                        <ul class="profile__persons-info_list">
                            <?php foreach ($dataProviderLanguages->models as $model): ?>
                                <li>
                                    <?php if ($model->level): ?>
                                        <p class="period"><?= $model->level ?></p>
                                    <?php endif; ?>
                                    <p><?= $model->lang ?></p>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
        <style>
            .promocode__item .promocode__code {
                color: #000;
            }

            .promocode__item .promocode__gift {
                color: #000;
            }

            .promocode__item.active {
                border: 2px solid #04be5b;
            }

            .promocode__item.active:after {
                content: '✓';
                color: #04be5b;
                font-size: 26px;
                position: absolute;
                top: 50%;
                left: 14px;
                margin-top: -15px;
            }

            .promocode__item.active .promocode__left:after {
                content: '';
                position: absolute;
                display: block;
                top: 50%;
                left: -15px;
                margin-top: -13px;
                height: 20px;
                width: 20px;
                border-radius: 50%;
                border: 2px solid #04be5b;
                z-index: 4;
            }

            .promocode__item.active .promocode__left:before {
                content: '';
                position: absolute;
                display: block;
                top: 50%;
                left: -17px;
                margin-top: -14px;
                height: 26px;
                width: 15px;
                background-color: #fff;
                z-index: 5;
            }
        </style>
        <div class="profile__events shadow">
            <div class="wihite__box shadow">
                <div class="tab-menu">
                    <div class="tab-menu__label active" data-tab="tab2">Последние уроки <?= date('H:i') ?></div>
                    <div class="tab-menu__label" data-tab="tab3">Мои тренировки</div>
                    <?php
                    if (Yii::$app->user->can('user')): ?>
                        <div class="tab-menu__label" data-tab="tab4">Мои тестирования</div>
                    <?php endif; ?>
                    <div class="tab-menu__label" data-tab="tab1">Ежедневные задания</div>
                </div>
                <div class="tab-content" id="width-check">
                    <div class="tab-content__item event__content" id="tab1">
                        <?php if (!empty($dailyTasks)): ?>
                            <div class="promocode grid">
                                <?php foreach ($dailyTasks as $task): ?>
                                    <div class="grid__wrapper promocode__wrap">
                                        <div class="promocode__item <?= $task->status ? 'active' : '' ?>">
                                            <div class="promocode__left">
                                                <div class="promocode__code"><?= $task->task->reward ?> рублей</div>
                                                <div class="promocode__gift" style="font-size:13px">
                                                    <?= $task->task->title ?>
                                                </div>
                                            </div>
                                            <a class="promocode__right" href="#"><i class="fa fa-info"></i></a>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php else: ?>
                            <p style="text-align: center;">Нет активных ежедневных заданий</p>
                        <?php endif; ?>
                    </div>
                    <div class="tab-content__item event__content show" id="tab2">
                        <?php if (!empty($readyArticles)): ?>
                            <?php foreach ($readyArticles as $model) : ?>
                                <div class="table__row">
                                    <div class="table__row_item">
                                        <?= Html::a('<p class="text-lblue">' . $model->article->title . '</p>', ['/article/article/view', 'id' => $model->article->id]) ?>
                                    </div>
                                    <div class="table__row_item">
                                        <?= Html::a('Открыть', ['/article/article/view', 'id' => $model->article->id], ['class' => 'button button__green']) ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <p style="text-align: center;">Вы не изучили ни одного
                                урока<br><br><?= Html::a('В магазин', ['/article/article/store'], ['class' => 'button', 'style' => 'left: 38%;']) ?>
                            </p>
                        <?php endif; ?>
                    </div>
                    <div class="tab-content__item event__content" id="tab3">
                        <?php if (!empty($trainings)): ?>
                            <?php foreach ($trainings as $model) : ?>
                                <div class="table__row">
                                    <div class="table__row_item">
                                        <?= Html::a('<p class="text-lblue">' . $model->name . '</p>', ['/training/default/run', 'id' => $model->id]) ?>
                                    </div>
                                    <?php if (!empty($model->trainingExercises)): ?>
                                        <div class="table__row_item">
                                            <p><?= count($model->trainingExercises) ?> задач</p>
                                        </div>
                                        <div class="table__row_item">
                                            <p><?= $model->trainingExercises[0]->variant->theme->class ?> Класс</p>
                                        </div>
                                    <?php endif; ?>
                                    <div class="table__row_item">
                                        <?= Html::a('Открыть', ['/training/default/run', 'id' => $model->id], ['class' => 'button button__green']) ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <p style="text-align: center;">У вас нет ни одной
                                тренировки<br><br><?= Html::a('Создать тренировку', ['/training/default/choose-class-subject'], ['class' => 'button']) ?>
                            </p>
                        <?php endif; ?>
                    </div>
                    <?php if (Yii::$app->user->can('user')): ?>
                        <div class="tab-content__item event__content" id="tab4">
                            <?php if (!empty($testings)): ?>
                                <?php foreach ($testings as $model) : ?>
                                    <div class="table__row">
                                        <div class="table__row_item">
                                            <?= Html::a('<p class="text-lblue">' . $model->name . '</p>', ['/testing/default/run', 'id' => $model->id]) ?>
                                        </div>
                                        <?php if (!empty($model->trainingExercises)): ?>
                                            <div class="table__row_item">
                                                <p><?= count($model->trainingExercises) ?> задач</p>
                                            </div>
                                            <div class="table__row_item">
                                                <p><?= $model->trainingExercises[0]->variant->theme->class ?>
                                                    Класс</p>
                                            </div>
                                        <?php endif; ?>
                                        <div class="table__row_item">
                                            <?= Html::a('Открыть', ['/testing/default/run', 'id' => $model->id], ['class' => 'button button__green']) ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <p style="text-align: center;">У вас нет ни одного
                                    теста<br><br><?= Html::a('Создать тест', ['/testing/default/choose-class-subject'], ['class' => 'button']) ?>
                                </p>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($dailyTasksSuccess)): ?>
    <?php foreach ($dailyTasksSuccess as $index => $task): ?>
        <!-- begin Modal modal__promocode -->
        <div class="modal fade modal__promocode" id="modal__daily__task__<?= $index ?>">
            <div class="modal-dialog">
                <div class="modal-content"><a class="modal-close" href="#" data-dismiss="modal"></a>
                    <div class="modal__promocode_wrap">
                        <div class="modal-header">Ежедневное задание</div>
                        <div class="modal-body">
                            <div class="modal-body_p">Вы выполнили ежедневное задание: <b><?= $task->task->title ?></b>.<br>На
                                ваш счет начислено <?= $task->task->reward ?> рублей.<br><br><br>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="modal-footer-promocode"><?= $task->task->reward ?> рублей</div>
                            <a class="button button__green--full button__round" href="#" data-dismiss="modal">Да,
                                спасибо!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

<?php if ($first_session == 0): ?>
    <!-- begin Modal modal__promocode -->
    <div class="modal fade modal__promocode" id="modal__promocode">
        <div class="modal-dialog">
            <div class="modal-content"><a class="modal-close" href="#" data-dismiss="modal"></a>
                <div class="modal__promocode_wrap">
                    <div class="modal-header">Добро пожаловать в Арете Онлайн!</div>
                    <div class="modal-body">
                        <div class="modal-body_p">Мы зачислили на ваш Арете Кошелек 200 рублей, чтобы вам было удобнее
                            начать учиться! В дальнейшем вы сможете пополнять кошелек с помощью банковской карты или
                            приглашая друзей зарегистрироваться в портале.
                            <br>С уважением, команда Арете Онлайн.
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="modal-footer-promocode">200 рублей</div>
                        <a class="button button__green--full button__round" href="#" data-dismiss="modal">Начать
                            учиться!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal modal__no-purchase-->
<?php endif; ?>
<?php
$js = <<<JS
$('#modal__promocode').modal('show');
$('#modal__daily__task__0').modal('show');
$('#modal__daily__task__0 .modal-dialog .modal-content .modal-close').click(function () {
    setTimeout(function () {
        $('#modal__daily__task__1') . modal('show');
    }, 1000);
});
JS;
$js .= '
    if (' . (isset($timezone) ? 'false' : 'true') . ') {
        var visitortime = new Date();
        var visitortimezone = "Etc/GMT-" + -visitortime.getTimezoneOffset() / 60;
        $.ajax({
            type: "GET",
            url: "/profile/default/timezone",
            data: {time: visitortimezone},
            success: function () {
                location.reload();
            }
        });
    }
';
$this->registerJs($js);
?>
