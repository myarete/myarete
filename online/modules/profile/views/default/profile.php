<?php

use common\models\FriendRequest;
use yii\helpers\Html;

$this->title = "Профиль";

$js = <<<JS
$('body').on('click', '.send-request', function() {
    var id = $(this).data('id')
    btn = $(this);

    $.post('/communication/default/send-request', {id: id}, function(response) {
        if(response) {
            btn.replaceWith('<button class="button curve button__reverse button__red not-confirm" data-id="'+id+'"><span>Запрос отправлен</span><span>Отменить запрос</span></button>');
        }
    });
});

$('body').on('click', '.confirm-request', function() {
    var id = $(this).data('id')
    btn = $(this);

    $.post('/communication/default/confirm-friend-from-profile', {id: id}, function(response) {
        if(response) {
            $('.remove-friend').remove();
            btn.replaceWith('<button class="button curve button__reverse button__red remove-friend" data-id="'+id+'"><span>У Вас в друзьях</span><span>Удалить из друзей</span></button>');
        }
    });
});

$('body').on('click', '.not-confirm', function() {
    var id = $(this).data('id')
    btn = $(this);

    $.post('/communication/default/remove-friend-from-profile', {id: id}, function(response) {
        if(response) {
            btn.replaceWith('<button class="button curve button__green send-request" data-id="'+id+'">Добавить в друзья</button>');
        }
    });
});

$('body').on('click', '.remove-friend', function() {
    var id = $(this).data('id')
    btn = $(this);

    $.post('/communication/default/remove-friend-from-profile', {id: id}, function(response) {
        if(response) {
            btn.replaceWith('<button class="button curve button__green send-request" data-id="'+id+'">Добавить в друзья</button>');
        }
    });
});
JS;
$this->registerJs($js);
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="profile__persons shadow">
            <div class="profile__persons-header">
                <div
                    class="img-circle"><?= Yii::$app->user->isGuest ? '' : $user->profile->getImageTag(['defaultSrc' => '/img/default_user.png']) ?></div>
                <h3 class="profile__persons-title"><?= $user->profile->first_name . ' ' . $user->profile->last_name ?></h3>
                <div class="profile__persons-proof">Преподаватель</div>
                
                <?php
                if ($request = FriendRequest::find()->where(['to_user_id' => $user->id, 'from_user_id' => Yii::$app->user->id])->one()) {
                    if ($request->confirmed == 1)
                        echo Html::button('<span>У Вас в друзьях</span><span>Удалить из друзей</span>', [
                            'class' => 'button curve button__reverse button__red remove-friend',
                            'data' => ['id' => $user->id]
                        ]);
                    else if ($request->confirmed == 0)
                        echo Html::button('<span>Запрос отправлен</span><span>Отменить запрос</span>', [
                            'class' => 'button curve button__reverse button__red not-confirm',
                            'data' => ['id' => $user->id]
                        ]);
                } else if ($request = FriendRequest::find()->where(['to_user_id' => Yii::$app->user->id, 'from_user_id' => $user->id])->one()) {
                    if ($request->confirmed == 1) {
                        echo Html::button('Удалить из друзей', [
                            'class' => 'button curve button__red remove-friend',
                            'data' => ['id' => $user->id]
                        ]);
                    } else if ($request->confirmed == 0) {
                        echo '<div class="button__group button__group-2">';
                        echo '<div>';
                        echo Html::button('Добавить', [
                            'class' => 'button curve confirm-request',
                            'data' => ['id' => $user->id]
                        ]);
                        echo '</div><div>';
                        echo Html::button('Отклонить', [
                            'class' => 'button curve button__red remove-friend',
                            'data' => ['id' => $user->id]
                        ]);
                        echo '</div>';
                        echo '</div>';
                    }
                } else {
                    echo Html::button('Добавить в друзья', [
                        'class' => 'button curve button__green send-request',
                        'data' => ['id' => $user->id]
                    ]);
                }
                ?>
            </div>
            
            <?php
            // Образование
            $count_education = count($education);
            
            // Навыки
            $count_skill = count($skill);
            
            // Публикации
            $count_publication = count($publication);
            
            // Языки
            $count_languages = count($languages);
            
            if ($count_education > 0 || $count_skill > 0 || $count_publication > 0 || $count_languages > 0):
                ?>
                <div class="profile__persons-info">
                    
                    <?php if ($count_education > 0): ?>
                        <div class="profile__persons-info_names"><i class="fa fa-envelope" aria-hidden="true"></i>образование
                        </div>
                        <ul class="profile__persons-info_list">
                            <?php foreach ($education as $model): ?>
                                <li>
                                    <?php if ($model->from_date && $model->to_date): ?>
                                        <p class="period"><?= substr($model->from_date, 0, 4); ?>
                                            - <?= substr($model->to_date, 0, 4); ?></p>
                                    <?php endif; ?>
                                    <p><?= $model->school ?></p>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    
                    <?php endif; ?>
                    
                    <?php if ($count_skill > 0): ?>
                        <div class="profile__persons-info_names"><i class="fa fa-envelope" aria-hidden="true"></i>навыки
                            и умения
                        </div>
                        <ul class="profile__persons-info_list">
                            <?php foreach ($skill as $model): ?>
                                <li>
                                    <?php if ($model->from_date && $model->to_date): ?>
                                        <p class="period"><?= substr($model->from_date, 0, 4); ?>
                                            - <?= substr($model->to_date, 0, 4); ?></p>
                                    <?php endif; ?>
                                    <p><?= $model->skill ?></p>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    
                    <?php if ($count_skill > 0): ?>
                        <div class="profile__persons-info_names"><i class="fa fa-envelope" aria-hidden="true"></i>публикации
                        </div>
                        <ul class="profile__persons-info_list">
                            <?php foreach ($publication as $model): ?>
                                <li>
                                    <?php if ($model->from_date && $model->to_date): ?>
                                        <p class="period"><?= substr($model->from_date, 0, 4); ?>
                                            - <?= substr($model->to_date, 0, 4); ?></p>
                                    <?php endif; ?>
                                    <p><?= $model->publication ?></p>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    
                    <?php if ($count_languages > 0): ?>
                        <div class="profile__persons-info_names"><i class="fa fa-envelope" aria-hidden="true"></i>языки
                        </div>
                        <ul class="profile__persons-info_list">
                            <?php foreach ($languages as $model): ?>
                                <li>
                                    <?php if ($model->from_date && $model->to_date): ?>
                                        <p class="period"><?= substr($model->from_date, 0, 4); ?>
                                            - <?= substr($model->to_date, 0, 4); ?></p>
                                    <?php endif; ?>
                                    <p><?= $model->lang ?></p>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

