<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model online\modules\profile\models\ProfileSkill */
/* @var $form yii\widgets\ActiveForm */


Modal::begin([
    'options' => [
        'id' => 'skill'
    ],
    'size' => 'modal-lg',
    'header' => '<h2>Добавить навык или умение</h2>',
]);
?>
<div class="side-block">
    <div class="profile-skill-form">

        <?php $form = ActiveForm::begin(
            /*[
                'action' => ['/profile/default/add-education'],
                'method' => 'post',
                'options' => [
                    'class' => ''
                ],
                'id' => 'form-add-education'
            ]*/); ?>


        <?= $form->field($skill, 'skill')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton($skill->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $skill->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?php
Modal::end();
?>