<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model online\modules\profile\models\ProfileEducation */
/* @var $form yii\widgets\ActiveForm */


Modal::begin([
    'options' => [
        'id' => 'education'
    ],
    'size' => 'modal-lg',
    'header' => '<h2>Добавить образование</h2>',
]);
?>
<div class="side-block">
    <div class="profile-education-form">

        <?php $form = ActiveForm::begin(
            /*[
                'action' => ['/profile/default/add-education'],
                'method' => 'post',
                'options' => [
                    'class' => ''
                ],
                'id' => 'form-add-education'
            ]*/); ?>


        <?= $form->field($education, 'school')->textInput(['maxlength' => true]) ?>

        <?= $form->field($education, 'degree')->textInput(['maxlength' => true]) ?>

        <?= $form->field($education, 'field_of_study')->textInput(['maxlength' => true]) ?>

        <?= $form->field($education, 'grade')->textInput(['maxlength' => true]) ?>

        <?= $form->field($education, 'activities')->textarea(['rows' => 6]) ?>

        <?= $form->field($education, 'from_date')->widget(\kartik\widgets\DatePicker::className(), [
                'type' => DatePicker::TYPE_INPUT,
                'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd.mm.yyyy',
                ],
            ]);
        ?>

        <?= $form->field($education, 'to_date')->widget(\kartik\widgets\DatePicker::className(), [
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd.mm.yyyy',
            ],
        ]);
        ?>

        <?= $form->field($education, 'description')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton($education->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $education->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?php
Modal::end();
?>