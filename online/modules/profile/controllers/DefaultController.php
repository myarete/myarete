<?php

namespace online\modules\profile\controllers;

use common\models\ArticleReady;
use common\models\DailyTasks;
use common\models\ExerciseVariantFavorite;
use common\models\News;
use common\models\Payment;
use common\models\PremiumPlanList;
use common\models\Profile;
use common\models\ProfileEducation;
use common\models\ProfileEducationSearch;
use common\models\ProfileLanguages;
use common\models\ProfileLanguagesSearch;
use common\models\ProfilePublication;
use common\models\ProfilePublicationSearch;
use common\models\ProfileSkill;
use common\models\ProfileSkillSearch;
use common\models\State;
use common\models\Testing;
use common\models\TestVariantFavorite;
use common\models\Training;
use common\models\User;
use common\models\UserVsDailyTasks;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `profile` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionProfile($userId = null)
    {
        $userId = Yii::$app->request->get('id');

        if (empty($userId) || $userId == Yii::$app->user->id) {
            $this->redirect('/');
        } else {
            $user = User::find()->with('profile')->where(['id' => $userId])->one();

            $education = ProfileEducation::find()->where(['user_id' => $userId])->all();
            $skill = ProfileSkill::find()->where(['user_id' => $userId])->all();
            $publication = ProfilePublication::find()->where(['user_id' => $userId])->all();
            $languages = ProfileLanguages::find()->where(['user_id' => $userId])->all();

            return $this->render('profile', [
                'user' => $user,
                'education' => $education,
                'skill' => $skill,
                'publication' => $publication,
                'languages' => $languages,
            ]);
        }
    }

    public function actionIndex()
    {
        if (isset($_SESSION['time'])) {
            $timezone = $_SESSION['time'];
            unset($_SESSION['time']);
            date_default_timezone_set($timezone);
        }

        $user = User::find()->where(['id' => Yii::$app->user->id])->one();

        $first_session = 1;
        if ($user->profile->first_session == 0) {
            $profile = $user->profile;
            $profile->cash += 200;
            $profile->first_session = 1;
            $profile->update();
            $payment = new Payment();
            $payment->user_id = $user->id;
            $payment->type_id = 2;
            $payment->refill_sum = 200;
            $payment->save();
            $first_session = 0;
        }

        $dailyTasks = UserVsDailyTasks::find()->where(['user_id' => Yii::$app->user->id])->all();
        $profile = Profile::find()->where(['user_id' => Yii::$app->user->id])->one();
        $dailyTasksSuccess = [];

        if (!empty($dailyTasks)) {
            foreach ($dailyTasks as $task) {
                $taskCreatedTime = strtotime($task->created_at);
                $taskResetTime = strtotime(date('Y-m-d 01:00:00', $taskCreatedTime + 86400));
                $differentTime = $taskResetTime - time();
                if ($differentTime < 0) {
                    $task->status = 0;
                    $task->created_at = date('Y-m-d H:i:s');
                    $task->update();
                }
                if ($task->status == 0) {
                    // Следующий день
                    if (time() > strtotime('01:00') && time() < strtotime('13:00')) {
                        if ($task->task_id == 1) {
                            $dailyTasksSuccess[] = $task;
                            if ($task->status == 0) {
                                $task->status = 1;
                                $task->update();
                                $profile->cash += $task->task->reward;
                                $profile->update();
                                $payment = new Payment();
                                $payment->user_id = $user->id;
                                $payment->type_id = 4;
                                $payment->refill_sum = $task->task->reward;
                                $payment->save();
                            }
                        }
                        // Заканчивающийся день
                    } else if (time() > strtotime('13:00') && time() < strtotime('23:59')) {
                        if ($task->task_id == 2) {
                            $dailyTasksSuccess[] = $task;
                            if ($task->status == 0) {
                                $task->status = 1;
                                $task->update();
                                $profile->cash += $task->task->reward;
                                $profile->update();
                                $payment = new Payment();
                                $payment->user_id = $user->id;
                                $payment->type_id = 4;
                                $payment->refill_sum = $task->task->reward;
                                $payment->save();
                            }
                        }
                    }
                }
            }
            if ($dailyTasks[0]->status == 1 && $dailyTasks[1]->status == 1 && $dailyTasks[2]->status == 0) {
                $dailyTasks[2]->status = 1;
                $dailyTasks[2]->update();
                $dailyTasksSuccess[] = $dailyTasks[2];
                $profile->cash += $dailyTasks[2]->task->reward;
                $profile->update();
                $payment = new Payment();
                $payment->user_id = $user->id;
                $payment->type_id = 4;
                $payment->refill_sum = $dailyTasks[2]->task->reward;
                $payment->save();
            }
        } else {
            $dailyTasks = DailyTasks::find()->all();
            foreach ($dailyTasks as $task) {
                $model = new UserVsDailyTasks();
                $model->task_id = $task->id;
                $model->user_id = Yii::$app->user->id;
                $model->status = 0;
                $model->save();
            }
            $dailyTasks = UserVsDailyTasks::find()->where(['user_id' => Yii::$app->user->id])->all();
        }

        $premiumPlan = PremiumPlanList::find()->with('premiumPlan')->where(['user_id' => Yii::$app->user->id, 'status' => 'active'])->one();

        if ($premiumPlan) {
            if ($premiumPlan->premiumPlan->validity == 'month') {
                $premiumPlanDate = strtotime($premiumPlan->date) + 30 * 24 * 3600;
            } else if ($premiumPlan->premiumPlan->validity == 'year') {
                $premiumPlanDate = strtotime($premiumPlan->date) + 365 * 24 * 3600;
            } else if ($premiumPlan->premiumPlan->validity == 'month') {
                $current = strtotime(date('d.m.Y'));
                $june = strtotime("30.06.2018");
                $result = $june - $current;

                $premiumPlanDate = strtotime($premiumPlan->date) + $result;
            }

            $current = strtotime(date('d.m.Y'));

            if ($current > $premiumPlanDate) {
                $premiumPlan->status = 'suspended';

                if ($premiumPlan->update()) {
                    $user->role = User::ROLE_CLIENT;
                    $user->update();
                }
            }
        }

        $education = new ProfileEducation();
        $searchModelEducation = new ProfileEducationSearch();
        $dataProviderEducation = $searchModelEducation->search(Yii::$app->request->queryParams);

        $skill = new ProfileSkill();
        $searchModelSkill = new ProfileSkillSearch();
        $dataProviderSkill = $searchModelSkill->search(Yii::$app->request->queryParams);

        $publication = new ProfilePublication();
        $searchModelPublication = new ProfilePublicationSearch();
        $dataProviderPublication = $searchModelPublication->search(Yii::$app->request->queryParams);

        $languages = new ProfileLanguages();
        $searchModelLanguages = new ProfileLanguagesSearch();
        $dataProviderLanguages = $searchModelLanguages->search(Yii::$app->request->queryParams);

        $user = Yii::$app->user->model;

        $state = State::find()->where(['id' => Yii::$app->user->model->profile->state_id])->one();

        if ($education->load(Yii::$app->request->post()) && $education->save()) {
            return $this->redirect(['/']);
        }
        if ($skill->load(Yii::$app->request->post()) && $skill->save()) {
            return $this->redirect(['/']);
        }
        if ($publication->load(Yii::$app->request->post()) && $publication->save()) {
            return $this->redirect(['/']);
        }
        if ($languages->load(Yii::$app->request->post()) && $languages->save()) {
            return $this->redirect(['/']);
        } else {
            $retval = [
                'education' => $education,
                'searchModelEducation' => $searchModelEducation,
                'dataProviderEducation' => $dataProviderEducation,
                'skill' => $skill,
                'searchModelSkill' => $searchModelSkill,
                'dataProviderSkill' => $dataProviderSkill,
                'publication' => $publication,
                'searchModelPublication' => $searchModelPublication,
                'dataProviderPublication' => $dataProviderPublication,
                'languages' => $languages,
                'searchModelLanguages' => $searchModelLanguages,
                'dataProviderLanguages' => $dataProviderLanguages,
                'user' => $user,
                'news' => News::getAllNews(),
                'readyArticles' => ArticleReady::find()->where(['user_id' => Yii::$app->user->id])->orderBy('ready_at DESC')->limit(7)->all(),
                'trainings' => Training::find()->where(['user_id' => Yii::$app->user->id])->orderBy('rand()')->limit(6)->all(),
                'testings' => Testing::find()->where(['user_id' => Yii::$app->user->id])->orderBy('rand()')->limit(6)->all(),
                'exercise_favorites' => ExerciseVariantFavorite::find()->where(['user_id' => Yii::$app->user->id])->orderBy('rand()')->limit(3)->all(),
                'test_favorites' => TestVariantFavorite::find()->where(['user_id' => Yii::$app->user->id])->orderBy('rand()')->limit(3)->all(),
                'first_session' => $first_session,
                'dailyTasks' => $dailyTasks,
                'dailyTasksSuccess' => $dailyTasksSuccess,
                'state' => $state
            ];
            if (isset($timezone)) {
                $retval['timezone'] = $timezone;
            }
            return $this->render('index', $retval);
        }

        //return $this->render('index');
    }

    public function actionUpdate($id)
    {
        $education = $this->findModel($id);

        if ($education->load(Yii::$app->request->post()) && $education->save()) {
            return $this->redirect(['/']);
        }
    }

    public function actionEducationDelete($id)
    {
        if (ProfileEducation::findOne($id)->delete()) {
            return $this->redirect(['index']);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSkillDelete($id)
    {
        if (ProfileSkill::findOne($id)->delete()) {
            return $this->redirect(['index']);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPublicationDelete($id)
    {
        if (ProfilePublication::findOne($id)->delete()) {
            return $this->redirect(['index']);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionLanguagesDelete($id)
    {
        if (ProfileLanguages::findOne($id)->delete()) {
            return $this->redirect(['index']);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionTimezone()
    {
        $_SESSION['time'] = $_GET['time'];
        unset($_GET['time']);
    }
}
