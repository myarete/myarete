<?php

namespace online\modules\bonus\assets;

use yii\web\AssetBundle;

class BonusAsset extends AssetBundle
{
	public $sourcePath = '@online/modules/bonus/assets';
	public $css = [
		'css/base.css',
		'css/preset-1.css',
		'css/jquery.smallipop.min.css'
	];
	public $js = [
		'js/jquery.smallipop.min.js'
	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset'
	];
}