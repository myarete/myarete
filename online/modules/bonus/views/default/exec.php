<?php

use yii\helpers\Html;
use online\modules\bonus\assets\BonusAsset;

BonusAsset::register($this);
?>
    <div class="crm__container">
        <div class="crm__content">
            <?= Html::beginForm('', 'post') ?>
            <div class="page__header_title"><?= $model->name ?></div>
            <?php foreach ($model->polls as $poll): ?>
                <div class="col-title"><h1><?= $poll->name ?></h1></div>
                <section class="intro first"><?= $poll->description ?></section>
                <?php foreach ($poll->bonusQuestions as $i => $question): ?>
                    <section class="last">
                        <fieldset class="required">
                            <h2>
                                <div class="title-part">
                                    <?= strip_tags($question->question) ?>
                                    <div class="help-block"></div>
                                    <?php if ($question->is_required && $question->type != 3): ?>
                                        <div class="require smallipop<?= $i + 1 ?>" title="">
												<span class="smallipop-hint">
													<?= 'Требуется ответ' ?>
												</span>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </h2>
                            <div class="special-padding-row">
                                <?php if ($question->type == 0): ?>
                                    <p class="title"><?= strip_tags($question->description) ?></p>
                                    <label class="input-group input-group-textarea">
                                        <span class="input-group-addon"></span>
                                        <?= Html::textarea('answer_' . $question->id, '', ['class' => 'form-control paragraph counter validate', 'placeholder' => 'Введите текст']) ?>
                                    </label>
                                <?php elseif ($question->type == 1): ?>
                                    <div class="label-cont">
                                        <?php foreach ($question->bonusAnswers as $answer): ?>
                                            <label class="input-group input-group-radio row">
                                                <?= Html::radio('answer_' . $question->id, false, ['value' => $answer->id, 'class' => 'hidden-inputs']) ?>
                                                <span class="input-group-addon"></span>
                                                <span class="input-group-title"><?= strip_tags($answer->answer) ?></span>
                                            </label>
                                        <?php endforeach; ?>
                                    </div>
                                <?php elseif ($question->type == 2): ?>
                                    <div class="label-cont">
                                        <?php foreach ($question->bonusAnswers as $answer): ?>
                                            <label class="input-group input-group-checkbox row">
                                                <?= Html::checkbox('answer_' . $question->id . '[]', false, ['value' => $answer->id, 'class' => 'hidden-inputs']) ?>
                                                <span class="input-group-addon"></span>
                                                <span class="input-group-title"><?= strip_tags($answer->answer) ?></span>
                                            </label>
                                        <?php endforeach; ?>
                                    </div>
                                <?php elseif ($question->type == 3): ?>
                                    <p class="title"><?= strip_tags($question->description) ?></p>
                                    <?= \wbraganca\videojs\VideoJsWidget::widget([
                                        'options' => [
                                            'class' => 'video-js',
                                            'controls' => true,
                                            'preload' => 'auto',
                                        ],
                                        'tags' => [
                                            'source' => [
                                                ['src' => '/upload/bonus/' . $question->video, 'type' => 'video/mp4']
                                            ]
                                        ]
                                    ]); ?>
                                    <?= Html::hiddenInput('answer_' . $question->id) ?>
                                <?php endif; ?>
                            </div>
                        </fieldset>
                    </section>
                    <hr>
                <?php endforeach; ?>
            <?php endforeach; ?>
            <div class="text-right">
                <?= Html::submitButton('Отправить', ['class' => 'button button__green', 'id' => 'submit-button']) ?>
            </div>
        </div>
        <?= Html::endForm() ?>
    </div>
<?php
$js = <<< END
    $('.require').smallipop({
        preferredPosition: 'left',
        theme: 'black',
        popupOffset: 5,
        popupDistance: 10,
        popupAnimationSpeed: 100,
        invertAnimation: true
    });
    $('.input-group-textarea textarea').on('keyup', function () {
        if ($(this).val()) {
            $(this).closest('.input-group-textarea').addClass('input-group-textarea-active');
        } else {
            $(this).closest('.input-group-textarea').removeClass('input-group-textarea-active');
        }
    });
    $('#submit-button').on('click', function () {
        var ret = true;
        $.each($('fieldset.required'), function (i, e) {
            if ($(e).find('h2 .title-part .require').length) {
                if (!$(e).find('.special-padding-row input:checked').length && !$(e).find('.special-padding-row textarea').val()) {
                    $(e).find('.help-block').text('Необходимо ответить на вопрос.').css('color', 'red');
                    ret = false;
                }
            }
        });
        return ret;
    });
    $('.video-js').on('click', function () {
        $(this).next().val($(this)[0].currentTime);
    });
END;
$this->registerJs($js);