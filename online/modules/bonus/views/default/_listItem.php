<?php

use yii\helpers\Html;

?>

<a href="/bonus/default/exec?id=<?= $model->id ?>" class="list-link">
    <div class="flex">
        <div class="list-td">
            <div class="list-image-wrap">
                <?= Html::img(
                    $model->image ? '/upload/bonus/' . $model->image : '/img/no_image.gif',
                    ['class' => 'list-image']
                ) ?>
            </div>
        </div>
        <div class="list-td">
            <p><?= $model->name ?></p>
        </div>
    </div>
    <div class="flex">
        <div class="list-td">
            <?= $model->price . '&nbsp;руб.' ?>
        </div>
    </div>
</a>
<div class="flex">
    <div class="list-td">
        <?= $model->description ?>
    </div>
</div>

