<?php

use yii\widgets\ListView;

?>

<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="page__header_title">Бонусы</div>
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_listItem',
                'options' => [
                    'class' => 'event__content'
                ],
                'itemOptions' => [
                    'class' => 'table__row'
                ],
                'layout' => "{items}\n{pager}"
            ]); ?>
        </div>
    </div>
</div>
