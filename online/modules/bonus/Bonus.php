<?php

namespace online\modules\bonus;

/**
 * bonus module definition class
 */
class Bonus extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'online\modules\bonus\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
