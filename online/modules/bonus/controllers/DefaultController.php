<?php

namespace online\modules\bonus\controllers;

use common\models\Bonus;
use common\models\BonusQuestion;
use common\models\BonusStat;
use common\models\BonusStatValue;
use common\models\Payment;
use common\models\Profile;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Bonus::find(),
            'pagination' => [
                'pageSize' => 10
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionExec($id)
    {
        $model = $this->findModel($id);
        if ($resp = Yii::$app->request->post()) {
            $bonusStat = new BonusStat();
            $bonusStat->user_id = Yii::$app->user->id;
            $bonusStat->bonus_id = $id;
            $bonusStat->save();
            foreach ($resp as $key => $value) {
                if (preg_match('/answer_\d+/', $key)) {
                    $tmp = explode('_', $key);
                    $question_id = $tmp[1];
                    $question = BonusQuestion::findOne($question_id);
                    if (is_array($value)) {
                        foreach ($value as $val) {
                            $statValue = new BonusStatValue();
                            $statValue->poll_id = $question->poll_id;
                            $statValue->question_id = $question_id;
                            $statValue->answer_id = $val;
                            $statValue->stat_id = $bonusStat->id;
                            $statValue->save();
                        }
                    } else {
                        $statValue = new BonusStatValue();
                        $statValue->poll_id = $question->poll_id;
                        $statValue->question_id = $question_id;
                        $statValue->answer_id = 0;
                        $statValue->stat_id = $bonusStat->id;
                        if ($question->type == 0) {
                            $statValue->text = $value;
                        } elseif ($question->type == 3) {
                            $statValue->video_time = $value;
                        } else {
                            $statValue->answer_id = $value;
                        }
                        $statValue->save(false);
                    }
                }
            }
            $model->numbers_of_uses += 1;
            $model->update();
        }

        return $this->render('exec', ['model' => $model]);
    }

    protected function findModel($id)
    {
        if (($model = Bonus::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
