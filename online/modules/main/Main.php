<?php

namespace online\modules\main;

class Main extends \yii\base\Module
{
    public $controllerNamespace = 'online\modules\main\controllers';

    public $label = 'Пользователи';

    public function init()
    {
        parent::init();
    }
}
