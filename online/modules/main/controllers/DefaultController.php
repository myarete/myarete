<?php

namespace online\modules\main\controllers;

use common\components\yii\base\BaseController;
use common\models\Help;
use common\models\Article;
use common\models\ArticleReady;
use common\models\CallRequest;
use common\models\Exercise;
use common\models\ExerciseVariantFavorite;
use common\models\ContactForm;
use common\models\Messages;
use common\models\News;
use common\models\Test;
use common\models\Testing;
use common\models\TestVariantFavorite;
use common\models\Training;
use signup\modules\user\forms\SetPasswordForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\ViewAction;

class DefaultController extends BaseController
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'page' => [
                'class' => ViewAction::className(),
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['email-confirm'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionPageHelp()
    {
        return $this->render('pages/help');
    }

    /**
     * @return string
     */
    public function actionPageHelpSub()
    {
        $id = Yii::$app->request->get('id');
        $content = Help::get($id);

        return $this->render('pages/help_sub', ['content' => $content]);
    }

    /**
     * @return string
     */
    public function actionPagePrivacy()
    {
        return $this->render('pages/privacy_policy');
    }

    /**
     * @return string
     */
    public function actionPageRequisites()
    {
        return $this->render('pages/requisites');
    }

    /**
     * @return string
     */
    public function actionPageContacts()
    {
        return $this->render('pages/contacts');
    }

    public function actionContactUs()
    {
        $flashMessage = 'Спасибо за Ваше обращение. В ближайшее время мы свяжемся с Вами по одному из указанных контактов.';
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(
                Yii::$app->params['writeusEmail'], Yii::$app->params['noreplyEmail']
            )) {
            Yii::$app->session->setFlash('success', $flashMessage);
            return $this->refresh();
        } else {
            return $this->render('contact-us', ['model' => $model]);
        }
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->model;

        return $this->render('index', [
            'user' => $user,
            'news' => News::getAllNews(),
            'readyArticles' => ArticleReady::find()->where(['user_id' => Yii::$app->user->id])->orderBy('ready_at DESC')->limit(7)->all(),
            'trainings' => Training::find()->where(['user_id' => Yii::$app->user->id])->orderBy('rand()')->limit(6)->all(),
            'testings' => Testing::find()->where(['user_id' => Yii::$app->user->id])->orderBy('rand()')->limit(6)->all(),
            'exercise_favorites' => ExerciseVariantFavorite::find()->where(['user_id' => Yii::$app->user->id])->orderBy('rand()')->limit(3)->all(),
            'test_favorites' => TestVariantFavorite::find()->where(['user_id' => Yii::$app->user->id])->orderBy('rand()')->limit(3)->all(),
        ]);
    }

    public function actionGetAllNews()
    {
        return Json::encode(News::getAllNews());
    }

    /**
     * @return string
     */
    public function actionEdit()
    {
        $user = Yii::$app->user->model;
        $setPassForm = new SetPasswordForm;

        $setPassForm->user = $user;

        $areLoad = $user->load(Yii::$app->request->post()) && $user->profile->load(Yii::$app->request->post());
        $areSave = $user->save() && $user->profile->save();

        if (!empty(Yii::$app->request->post('SetPasswordForm')['password']) && !empty(Yii::$app->request->post('SetPasswordForm')['passwordConfirm'])) {
            $areLoad = $areLoad && $setPassForm->load(Yii::$app->request->post());
            $areSave = $areSave && $setPassForm->save();
        }

        $isSuccess = $areLoad && $areSave;

        if ($isSuccess) {
            return $this->redirect(['index']);
        }

        return $this->render('edit', [
            'user' => $user,
            'setPassForm' => $setPassForm
        ]);
    }

    public function actionSearch($search)
    {
        if (!$search)
            return $this->goBack();

        $articles = new ActiveDataProvider([
            'query' => Article::find()
                ->where(['like', 'title', $search])
                ->orWhere(['like', 'description', $search]),
            'pagination' => [
                'pageParam' => 'articlePage'
            ],
        ]);

        $exercises = new ActiveDataProvider([
            'query' => Exercise::find()
                ->where(['like', 'name', $search])
                ->orWhere(['like', 'question', $search])
                ->orWhere(['like', 'decision_detailed', $search])
                ->orWhere(['like', 'decision_second', $search])
                ->orWhere(['like', 'answer', $search])
                ->orWhere(['like', 'hint', $search])
                ->orWhere(['like', 'hint_second', $search])
                ->orWhere(['like', 'hint_third', $search]),
            'pagination' => [
                'pageParam' => 'exercisePage'
            ],
        ]);

        $tests = new ActiveDataProvider([
            'query' => Test::find()
                ->where(['like', 'name', $search])
                ->orWhere(['like', 'question', $search]),
            'pagination' => [
                'pageParam' => 'testPage'
            ],
        ]);

        return $this->render('search-result', [
            'articles' => $articles,
            'exercises' => $exercises,
            'tests' => $tests,
        ]);
    }

    public function actionAddMessage()
    {
        $message = new Messages;
        $call_request = new CallRequest;
        $message_success = false;
        $call_request_success = false;

        if ($message->load(Yii::$app->request->post()) && $message->save()) {
            $message_success = true;
            Yii::$app->session->setFlash('success', 'Ваше сообщение отправлено');
        }

        if ($call_request->load(Yii::$app->request->post()) && $call_request->save()) {
            $call_request_success = true;
            Yii::$app->session->setFlash('success', 'Ваш запрос успешно отправлен, ожидайте звонка');
        }

        if ($message_success || $call_request_success)
            return $this->redirect(['/']);

        return $this->render('message', [
            'message' => $message,
            'call_request' => $call_request
        ]);
    }

    public function actionErrorMessage()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['noreplyEmail'])
            ->setTo(Yii::$app->params['contactsEmail'])
            ->setSubject('Найдена ошибка')
            ->setTextBody('Текст: ' . Yii::$app->request->post('errorText'))
            ->send();
        return true;
    }

    public function actionEmailConfirm($code = null, $email = null)
    {
        $user = Yii::$app->user->model;

        if ($code && $email && $code == $user->confirm_code) {
            $user->profile->email_confirm = 1;
            $user->profile->update();

            return $this->redirect(['index']);
        }

        if (Yii::$app->request->post('email')) {
            $user->profile->email = Yii::$app->request->post('email');
            $user->profile->update();
        }

        Yii::$app->mailer->compose('send_confirm_profile_email_link', ['user' => $user])
            ->setTo($user->profile->email)
            ->setFrom([Yii::$app->params['noreplyEmail'] => Yii::$app->name])
            ->setSubject('Подтвердите регистрацию')
            ->send();

        Yii::$app->session->setFlash('success', 'Ссылка для подтверждения регистрации была отправлена на Ваш E-mail');

        return $this->redirect(['index']);
    }

    public function actionReplaceEmail()
    {
        $user = Yii::$app->user->model;

        $user->profile->email = '';
        $user->profile->email_confirm = 0;
        $user->profile->update();

        $this->redirect(['index']);
    }
}
