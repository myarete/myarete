<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 14.07.15
 * Time: 7:47
 */

namespace online\modules\main\controllers;

use common\models\SocialNetwork;
use common\components\yii\base\BaseController;
use Yii;
use yii\filters\VerbFilter;

class SocialNetworkController extends BaseController
{
    public $layout = '/old';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['delete'],
                ],
            ],
        ];
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionAll()
    {
        Yii::$app->response->format = 'json';
        return SocialNetwork::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->asArray()
            ->all();
    }

    /**
     * @param $id
     * @return false|int
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = 'json';

        $model = SocialNetwork::find()->where([
            'id' => $id,
            'user_id' => Yii::$app->user->id,
        ])->one();

        if ($model === null)
            $this->pageNotFound();

        return $model->delete();
    }

    /**
     * @param $url
     * @return bool
     */
    public function actionCreate($url)
    {
        Yii::$app->response->format = 'json';

        $model = new SocialNetwork;
        $model->url = $url;
        $model->user_id = Yii::$app->user->id;
        return (bool)$model->save();
    }

    /**
     * @param $id
     * @param $url
     * @return bool
     */
    public function actionUpdate($id, $url)
    {
        Yii::$app->response->format = 'json';

        $model = SocialNetwork::find()->where([
            'id' => $id,
            'user_id' => Yii::$app->user->id,
        ])->one();

        if ($model === null)
            $this->pageNotFound();

        $model->url = $url;

        return (bool)$model->update(true, ['url']);
    }
}