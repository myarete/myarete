<?php
/**
 * Created by PhpStorm.
 * User: Ilia Goloveshko (elementfani2@gmail.com)
 * Date: 14.07.15
 * Time: 7:47
 */

namespace online\modules\main\controllers;

use common\models\Education;
use common\components\yii\base\BaseController;
use Yii;
use yii\filters\VerbFilter;

class EducationController extends BaseController
{
    public $layout = '/old';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['delete'],
                ],
            ],
        ];
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionAll()
    {
        Yii::$app->response->format = 'json';
        return Education::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->asArray()
            ->all();
    }

    /**
     * @param $id
     * @return false|int
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = 'json';

        $model = Education::find()->where([
            'id' => $id,
            'user_id' => Yii::$app->user->id,
        ])->one();

        if ($model === null)
            $this->pageNotFound();

        return $model->delete();
    }

    /**
     * @param $name
     * @return bool
     */
    public function actionCreate($name)
    {
        Yii::$app->response->format = 'json';

        $model = new Education;
        $model->name = $name;
        $model->user_id = Yii::$app->user->id;
        return (bool)$model->save();
    }

    /**
     * @param $id
     * @param $name
     * @return bool
     */
    public function actionUpdate($id, $name)
    {
        Yii::$app->response->format = 'json';

        $model = Education::find()->where([
            'id' => $id,
            'user_id' => Yii::$app->user->id,
        ])->one();

        if ($model === null)
            $this->pageNotFound();

        $model->name = $name;

        return (bool)$model->update(true, ['name']);
    }
}