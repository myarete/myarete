<?php

use yii\helpers\Html;
use common\components\other\FirstWords;

$this->title = 'АРЕТЕ онлайн';
$email = preg_match('/\@/', $user->email) ? $user->email : $user->profile->email;

$js = <<<JS
    var email = '<?= $user->email ?>',
        email2 = '<?= $user->profile->email ?>';
    if (email.indexOf('@') == -1 && email2.indexOf('@') == -1) {
        $('#input-email').modal('show');
    }
JS;
$this->registerJs($js);
?>

<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <h4><?= Html::a('Информация обо мне', '#my-info', ['data-toggle' => 'collapse']) . ' '
                . Html::a('<i class="glyphicon glyphicon-pencil"></i>', '/main/default/edit') ?></h4>
            
            <?php if (!preg_match('/\@/', $user->email) && !$user->profile->email_confirm): ?>
            <div id="my-info" class="collapse in">
                <?php else: ?>
                <div id="my-info" class="collapse">
                    <?php endif ?>
                    <div class="form-group">
                        <h3><?= $user->profile->first_name ?> <?= $user->profile->last_name ?></h3>
                    </div>
                    <div class="form-group">
                        <p><?= Yii::$app->formatter->asDate(strtotime($user->profile->birthdate), 'dd MMMM yyyy'); ?></p>
                    </div>
                    <br>
                    <div class="form-group">
                        <p><?= $user->profile->state->name ?></p>
                    </div>
                    <div ng-cloak ng-controller="EducationController as educationCtrl">
                        <p class="form-group" ng-repeat="education in educations" ng-show="loaded">
                            <span>{{education.name}}</span>
                            <a href class="btn btn-warning btn-xs" ng-click="update(education)"><i
                                    class="fa fa-edit"></i></a>
                            <a href class="btn btn-danger btn-xs" ng-click="delete(education)"><i
                                    class="fa fa-trash"></i></a>
                        </p>
                        
                        <p ng-show="loaded && educations.length == 0">Нет образования</p>
                        
                        <a href class="btn btn-success btn-sm" ng-click="add()"><i class="fa fa-plus"></i> Добавить
                            образование</a>
                    </div>
                    <br>
                    <h4>
                        Контакты
                    </h4>
                    <hr>
                    <div class="form-group">
                        <p>E-mail: <?= $email ?></p>
                    </div>
                    <?php if (!preg_match('/\@/', $user->email) && !$user->profile->email_confirm): ?>
                        <div class="thumbnail" style="background:rgba(255, 0, 0, 0.38)">
                            <h2>Пожалуйста, подтвердите ваш почтовый адрес</h2>
                            <p>
                                На ваш почтовый
                                адрес <i><?= $email ?></i> было выслано письмо с ссылкой для
                                подтверждения. Пожалуйста, перейдите по этой ссылке. Если вы
                                не видите письма во «Входящих», возможно, оно попало в папку
                                «Спам».
                            </p>
                            <?= Html::a('Изменить адрес', ['replace-email'], ['class' => 'btn btn-primary btn-xs']) ?>
                            &nbsp;
                            <?= Html::a('Отправить повторно', ['email-confirm'], ['id' => 'screenshot', 'class' => 'btn btn-primary btn-xs']) ?>
                            &nbsp;
                            <?= Html::a('Сообщить о проблеме', ['add-message'], ['id' => 'screenshot', 'class' => 'btn btn-primary btn-xs']) ?>
                            &nbsp;
                        </div>
                    <?php endif ?>
                    <div ng-cloak ng-controller="SocialNetworkController as networkCtrl">
                        <p class="form-group" ng-repeat="network in networks" ng-show="loaded">
                            <span>{{network.url}}</span>
                            <a href class="btn btn-warning btn-xs" ng-click="update(network)"><i class="fa fa-edit"></i></a>
                            <a href class="btn btn-danger btn-xs" ng-click="delete(network)"><i class="fa fa-trash"></i></a>
                        </p>
                        
                        <p ng-show="loaded && networks.length == 0">Нет социальных сетей</p>
                        
                        <a href class="btn btn-success btn-sm" ng-click="add()"><i class="fa fa-plus"></i> Добавить соц.
                            сеть</a>
                    </div>
                </div>
                <br>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#articles" data-toggle="tab">Последние статьи</a></li>
                    <li><a href="#trainings" data-toggle="tab">Мои тренировки</a></li>
                    <li><a href="#testings" data-toggle="tab">Мои тестирования</a></li>
                    <li><a href="#exercises" data-toggle="tab">Избранные задачи</a></li>
                    <li><a href="#tests" data-toggle="tab">Избранные тесты</a></li>
                </ul>
                
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="articles">
                        <h4><?= Html::a('Последние статьи', ['/article/article/index']) ?></h4>
                        <hr>
                        <div class="article-block">
                            <div class="row">
                                <?php foreach ($readyArticles as $model) : ?>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="thumbnail">
                                            <?php if (!empty($model->article->mainImage)): ?>
                                                <img src="<?= $model->article->mainImage ?>">
                                            <?php endif; ?>
                                            <div class="caption">
                                                <div
                                                    class="article-title"><?= Html::a($model->article->title, ['/article/article/view', 'id' => $model->article->id]) ?></div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="trainings">
                        <h4><?= Html::a('Мои тренировки', ['/training/default/saved']) ?></h4>
                        <hr>
                        <div class="article-block">
                            <div class="row">
                                <?php foreach ($trainings as $model) : ?>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="thumbnail">
                                            <div class="caption">
                                                <div
                                                    class="article-title"><?= Html::a($model->name, ['/training/default/run', 'id' => $model->id]) ?></div>
                                            </div>
                                            <?php if (!empty($model->trainingExercises)): ?>
                                                <p>Количество задач <?= count($model->trainingExercises) ?></p>
                                                <p>Класс <?= $model->trainingExercises[0]->variant->theme->class ?></p>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="testings">
                        <h4><?= Html::a('Мои тестирования', ['/testing/default/saved']) ?></h4>
                        <hr>
                        <div class="article-block">
                            <div class="row">
                                <?php foreach ($testings as $model) : ?>
                                    <div class="col-sm-6 col-md-4">
                                        <div class="thumbnail">
                                            <div class="caption">
                                                <div
                                                    class="article-title"><?= Html::a($model->name, ['/testing/default/run', 'id' => $model->id]) ?></div>
                                            </div>
                                            <?php if (!empty($model->trainingExercises)): ?>
                                                <p>Количество задач <?= count($model->trainingExercises) ?></p>
                                                <p>Класс <?= $model->trainingExercises[0]->variant->theme->class ?></p>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="exercises">
                        <h4><?= Html::a('Избранные задачи', ['/exercise/favorite/index']) ?></h4>
                        <hr>
                        <div class="article-block">
                            <div class="row">
                                <?php foreach ($exercise_favorites as $model) : ?>
                                    <div class="col-sm-12 col-md-12">
                                        <div class="thumbnail">
                                            <div class="caption">
                                                <div
                                                    class="article-title"><?= Html::a($model->exerciseVariant->exercise->id . '. ' . $model->exerciseVariant->exercise->name,
                                                        ['/exercise/favorite/search-exercise', 'id' => $model->exerciseVariant->exercise->id]) ?>
                                                </div>
                                            </div>
                                            <p><?= FirstWords::get($model->exerciseVariant->exercise->question, 30) ?></p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tests">
                        <h4><?= Html::a('Избранные тесты', ['/testing/favorite/index']) ?></h4>
                        <hr>
                        <div class="article-block">
                            <div class="row">
                                <?php foreach ($test_favorites as $model) : ?>
                                    <div class="col-sm-12 col-md-12">
                                        <div class="thumbnail">
                                            <div class="caption">
                                                <div
                                                    class="article-title"><?= Html::a($model->testVariant->test->id . '. ' . $model->testVariant->test->name,
                                                        ['/testing/default/search-test', 'id' => $model->testVariant->test->id]) ?>
                                                </div>
                                            </div>
                                            <p><?= FirstWords::get($model->testVariant->test->question, 30) ?></p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="input-email" data-backdrop="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <?= Html::beginForm('/main/default/email-confirm') ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Введите e-mail адрес</h4>
                </div>
                <div class="modal-body">
                    <p>Пожалуйста, введите e-mail адрес, к которому будет привязана ваша учетная запись на
                        myarete.com.</p>
                    <p><?= Html::input('email', 'email', '', ['class' => 'form-control']) ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary">Ок</button>
                </div>
                <?= Html::endForm() ?>
            </div>
        </div>
    </div>
