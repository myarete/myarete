<?php
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

$this->registerCssFile('/css/plugins/imgareaselect/imgareaselect-default.css', ['depends' => ['online\assets\AppAsset']]);
$this->registerJsFile('/js/plugins/imgareaselect/jquery.imgareaselect.min.js', ['depends' => ['online\assets\AppAsset']]);
?>

<?php $form = ActiveForm::begin(); ?>

    <?= Html::hiddenInput('x1') ?>
    <?= Html::hiddenInput('y1') ?>
    <?= Html::hiddenInput('w') ?>
    <?= Html::hiddenInput('h') ?>
    <?= Html::hiddenInput('img_width') ?>
    <?= Html::hiddenInput('img_height') ?>

    <?= $form->field($user->profile, 'image')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginEvents' => [
            'fileimageloaded' => 'function() {

                function preview(img, selection) {
                    var scaleX = 100 / (selection.width || 1);
                    var scaleY = 100 / (selection.height || 1);

                    $("#preview-img-wrap > div > div").css({
                        width: Math.round(scaleX * $(".file-preview-image").width()) + "px",
                        height: Math.round(scaleY * $(".file-preview-image").height()) + "px",
                        marginLeft: "-" + Math.round(scaleX * selection.x1) + "px",
                        marginTop: "-" + Math.round(scaleY * selection.y1) + "px"
                    });

                    $("[name=x1]").val(selection.x1);
                    $("[name=y1]").val(selection.y1);
                    $("[name=w]").val(selection.width);
                    $("[name=h]").val(selection.height);
                }

                $("#preview-wrap").remove();

                var elem = "<div id=\"preview-wrap\" class=\"file-preview-thumbnails\">\
                                <div id=\"preview-img-wrap\">\
                                    <div>\
                                        <div>\
                                            <img src=\""+$(".file-preview-image").attr("src")+"\" style=\"position: relative\" />\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>";

                $(elem).insertAfter($(".file-preview-thumbnails"));

                $("#preview-img-wrap").css({
                    position: "relative",
                    margin: "8px",
                    border: "1px solid #ddd",
                    "box-shadow": "1px 1px 5px 0 #a2958a",
                    padding: "6px",
                    float: "left",
                    "text-align": "center"
                });

                $("#preview-img-wrap > div").css({
                    position: "relative",
                    overflow: "hidden",
                    width: "100px",
                    height: "100px"
                });

                var img_width = $(".file-preview-image").width(),
                    img_height = $(".file-preview-image").height();

                $("#preview-img-wrap > div > div").css({
                    width: img_width,
                    height: "auto"
                });

                var x1 = Math.round((img_width - 200) / 2),
                    y1 = Math.round((img_height - 200) / 2),
                    x2, y2;

                x1 = x1 < 0 ? 0 : x1;
                y1 = y1 < 0 ? 0 : y1;
                x2 = x1 + (img_width >= 200 ? 200 : img_width);
                y2 = y1 + (img_height >= 200 ? 200 : img_height);

                var ias = $(".file-preview-image").imgAreaSelect({
                    x1: x1,
                    y1: y1,
                    x2: x2,
                    y2: y2,
                    //maxWidth: 400,
                    //maxHeight: 400,
                    aspectRatio: "1:1",
                    onSelectChange: preview,
                    instance: true
                });

                var selection = ias.getSelection();

                $("[name=x1]").val(x1);
                $("[name=y1]").val(y1);
                $("[name=w]").val(selection.width);
                $("[name=h]").val(selection.height);
                $("[name=img_width]").val(img_width);
                $("[name=img_height]").val(img_height);

                $(".fileinput-remove-button, .btn-file").click(function() {
                    ias.update();
                    $("[class^=imgareaselect-border],.imgareaselect-outer,.imgareaselect-selection").remove();
                });
            }'
        ]
    ]); ?>

	<?= $form->field($user->profile, 'last_name')  ?>

	<?= $form->field($user->profile, 'first_name')  ?>

	<?= $form->field($user->profile, 'state_id')->dropDownList(\common\models\State::getAll())  ?>

	<?= $form->field($user, 'email')  ?>

	<?php
        $user->profile->birthdate = date('d.m.Y', strtotime($user->profile->birthdate));
        echo $form->field($user->profile, 'birthdate')->widget(\kartik\widgets\DatePicker::className(), [
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd.mm.yyyy',
            ],
        ]);
	?>

	<?= $form->field($user->profile, 'allow_search')->checkbox() ?>

	<?= $form->field($user->profile, 'view_contacts')->checkbox() ?>

	<?= $form->field($user->profile, 'about_me')->textarea() ?>

	<hr>

    <?= $form->field($setPassForm, 'oldPassword', [
            'inputOptions' => [
                'type' => 'password',
                'class' => 'form-control',
            ],
            //'enableClientValidation' => false,
        ])->label('Старый пароль');
    ?>

    <?= $form->field($setPassForm, 'password', [
            'inputOptions' => [
                'type' => 'password',
                'class' => 'form-control',
            ],
            'enableClientValidation' => false,
        ])->label('Новый пароль');
    ?>

    <?= $form->field($setPassForm, 'passwordConfirm', [
            'inputOptions' => [
                'type' => 'password',
                'class' => 'form-control',
            ],
            'enableClientValidation' => false,
        ]);
    ?>

    <hr>

	<button type="submit" class="btn btn-success">Сохранить</button>

<?php ActiveForm::end(); ?>

