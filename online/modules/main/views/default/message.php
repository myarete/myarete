<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
    <div class="side-block">
        <h2>Наши контакты</h2>

        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <?= \common\models\Html::get('online_contacts') ?>
            </div>
        </div>

        <h2>Отправить сообщение</h2>

        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <?php $form = ActiveForm::begin([
                    'id' => 'message-form'
                ]); ?>

                <?= $form->field($message, 'name')->input('text', ['placeholder' => 'Ваше имя'])->label(false) ?>

                <?= $form->field($message, 'email')->hiddenInput(['value' => Yii::$app->user->identity->email])->label(false) ?>

                <?= $form->field($message, 'message')->textarea(['placeholder' => 'Текст сообщения'])->label(false) ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end() ?>
            </div>
        </div>

        <hr>

        <h2>Заказать звонок</h2>

        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <?php
                $form2 = ActiveForm::begin([
                    'id' => 'call-request-form'
                ]);
                ?>

                <?= $form2->field($call_request, 'fio')->input('text', ['placeholder' => 'Ваше имя'])->label(false) ?>

                <?= $form2->field($call_request, 'phone')->input('text', ['placeholder' => 'Номер телефона'])->label(false) ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
    <br><br>
</div>