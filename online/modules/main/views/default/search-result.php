<?php

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = "Поиск";

?>

<div class="crm__container">
    <div class="crm__content">
        <div class="wihite__box shadow">
            <div class="page__header_title">Уроки</div>
            <div class="tab-content">
                <div class="tab-content__item event__content show" id="tab1">
                    <?php foreach($articles->models as $article): ?>
                        <div class="table__row">
                            <div class="table__row_item">
                                <?= Html::a($article->title, ['/article/article/view', 'id' => $article->id, 'class' => 'button button__green']) ?>
                            </div>
                            <div class="table__row_item"><?= Html::a('Открыть', ['/article/article/view', 'id' => $article->id], ['class' => 'button button__green']) ?></div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <br>
        <div class="wihite__box shadow">
            <div class="page__header_title">Задачи</div>
            <div class="tab-content">
                <div class="tab-content__item event__content show" id="tab1">
                    <?php foreach($exercises->models as $exercise): ?>
                        <div class="table__row">
                            <div class="table__row_item">
                                <?= Html::a($exercise->name, ['/exercise/favorite/search-exercise', 'id' => $exercise->id, 'class' => 'button button__green']) ?>
                            </div>
                            <div class="table__row_item"><?= Html::a('Открыть', ['/exercise/favorite/search-exercise', 'id' => $exercise->id], ['class' => 'button button__green']) ?></div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <br>
        <!--div class="wihite__box shadow">
            <div class="page__header_title">Тесты</div>
            <div class="tab-content">
                <div class="tab-content__item event__content show" id="tab1">
                    <!--?php foreach($tests->models as $test): ?>
                        <div class="table__row">
                            <div class="table__row_item">
                                <!--?= Html::a($test->name, ['/testing/default/search-test', 'id' => $test->id, 'class' => 'button button__green']) ?-->
                            <!--/div>
                            <div class="table__row_item"><!--?= Html::a('Открыть', ['/testing/default/search-test', 'id' => $test->id], ['class' => 'button button__green']) ?--><!--/div>
                        <!--/div>
                    <!--?php endforeach; ?-->
                <!--/div>
            </div>
        </div-->
    </div>
</div>

