<div class="crm__container">
    <div class="crm__content">
        <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
            <?= \kartik\alert\AlertBlock::widget([
                'delay' => false
            ]) ?>
        <?php endif; ?>
        <div class="wihite__box shadow">
            <?= \common\models\Html::get('requisites') ?>
        </div>
    </div>
</div>