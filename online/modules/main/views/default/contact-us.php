<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Свяжитесь с нами';
?>
<div class="crm__container">
    <div class="crm__content">
        <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
            <?= \kartik\alert\AlertBlock::widget([
                'delay' => false
            ]) ?>
        <?php endif; ?>
        <div class="wihite__box shadow">
        <div class="page__header_title"><?= Html::encode($this->title) ?></div>
        
        <div class="form__row" style="margin: 20px; padding: 20px;">
        Здесь вы можете задать любой интересующий вас вопрос, высказать предложения или
         претензии по проекту Арете Онлайн. Мы ответим вам по почте, которую вы укажете в обращении
        </div>
        <div class="form">
        <?php $form = ActiveForm::begin([
                'id' => 'contact-form', 
            ]); ?>
            
            <div class="form__row">
            <?= $form->field($model, 'name', [
                    'template' => "{label}\n<div class='col-md-6'>{input}</div>\n{hint}\n{error}",
                    'labelOptions' => [ 'class' => 'form__label' ]
                    ])->textInput(['maxlength' => true])?>
            </div>
            <div class="form__row">
            <?= $form->field($model, 'email', [
                    'template' => "{label}\n<div class='col-md-6'>{input}</div>\n{hint}\n{error}",
                    'labelOptions' => [ 'class' => 'form__label' ]
                    ])->textInput(['maxlength' => true])?>
            </div>
            <div class="form__row">
            <?= $form->field($model, 'phone', [
                    'template' => "{label}\n<div class='col-md-6'>{input}</div>\n{hint}\n{error}",
                    'labelOptions' => [ 'class' => 'form__label' ]
                    ])->textInput(['maxlength' => true])?>
            </div>
            <div class="form__row">
            <?= $form->field($model, 'body', [
                    'template' => "{label}\n<div class='col-md-6'>{input}</div>\n{hint}\n{error}",
                    'labelOptions' => [ 'class' => 'form__label' ]
                    ])->textArea(['rows' => 6])?>
            </div>
            <div class="form__row">
            <div class="text-center">
                <button type="submit" class="button">Отправить сообщение</button>
            </div>
            </div>
        <?php ActiveForm::end(); ?>
        </div>
        </div>
    </div>
</div>