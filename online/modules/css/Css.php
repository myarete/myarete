<?php

namespace online\modules\css;

class Css extends \yii\base\Module
{
    public $controllerNamespace = 'online\modules\css\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
