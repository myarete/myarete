<?php

namespace online\modules\css\controllers;

use online\modules\css\forms\CssForm;
use common\components\yii\base\BaseController;
use Yii;
use yii\filters\AccessControl;

class DefaultController extends BaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all css models.
     * @return mixed
     */
    public function actionIndex()
    {
		$model = new CssForm;

		return $this->render('index', [
			'names' => array_keys($model->getNames())
		]);
    }

    /**
     * @return string
     */
    public function actionView($id)
    {
        $model = new CssForm($id);

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            Yii::$app->session->setFlash('success', 'Изменения сохранены успешно');
            return $this->refresh();
        }

        return $this->render('view', [
            'model' => $model,
            'title' => ucfirst($id)
        ]);
    }
}
