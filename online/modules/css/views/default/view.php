<?php
use conquer\codemirror\CodemirrorAsset;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
?>


<?php
$this->title = 'CSS';
?>


<style>
    .CodeMirror {
        min-height: 400px;
    }
</style>

<?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
    <?= \kartik\widgets\AlertBlock::widget([
        'options' => ['class' => 'container'],
        'delay' => 9000
    ]) ?>
<?php endif; ?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5><?= $title ?>.css</h5>
            </div>

            <div class="ibox-content">

                <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'code')->widget(\conquer\codemirror\CodemirrorWidget::className(), [
                        'assets'=>[
                            CodemirrorAsset::MODE_CSS,
                        ],
                        'settings'=>[
                            'lineNumbers' => true,
                            'matchBrackets' => true,
                            'mode' => "text/css",
                            'indentUnit' => 4,
                        ],
                    ]) ?>

                    <hr/>

                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
