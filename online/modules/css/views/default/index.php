<?php
?>


<?php
$this->title = 'HTML';
?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <h5><?= $this->title ?></h5>
            </div>

            <div class="ibox-content">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th width="30px">#</th>
                            <th>Файл</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($names as $name): ?>
                        <tr>
                            <td>1</td>
                            <td><?= \yii\helpers\Html::a($name.'.css', ['/css/default/view', 'id' => $name]) ?></td>
                        </tr>
					<?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
