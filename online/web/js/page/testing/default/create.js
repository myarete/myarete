$(function(){
    /**
     * Update name
     */
    $('#create-testing-form').submit(function(e){

        e.preventDefault();

        var form = this,
            nameElement = $('#testing-name');

        bootbox.prompt({
            title: 'Название тестирования',
            value: nameElement.val(),
            callback: function (name) {
                name = $.trim(name);

                if (name === null)
                    return;

                nameElement.val(name);
                form.submit();
            }
        });

    });

});
