$(function(){

    /**
     * Update name
     */
    $('.action-column a:first-child').click(function(){

        var _this = this;
        var nameElement = $(this).parents('tr').find('.testing-name a');

        bootbox.prompt({
            title: 'Редактировать тестирование',
            value: nameElement.text(),
            callback: function (name) {
                name = $.trim(name);

                if (name === null)
                    return;

                $.get(_this.href, {
                    name: name
                }).success(function (response) {
                    nameElement.text(response.name);
                });
            }
        });

        return false;
    });

});