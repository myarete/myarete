$(function(){

    /**
     * Update name
     */
    $('#create-training-form').submit(function(e){

        e.preventDefault();

        var form = this,
            nameElement = $('#training-name');

        bootbox.prompt({
            title: 'Название тренировки',
            value: nameElement.val(),
            callback: function (name) {
                name = $.trim(name);

                if (name === null)
                    return;

                nameElement.val(name);
                form.submit();
            }
        });

    });

});
