/**
 *
 */
var app = angular.module('profileApp', []);


/**
 *
 */
angular.module('profileApp').filter('arrayToString', ['$sce', function($sce) {
    return function(errors) {
        if (angular.isArray(errors))
            return $sce.trustAsHtml(errors.join('<br>', errors));
        else
            return errors;
    };
}]);


/**
 *
 */
app.controller('ProfileController', ['$scope', '$http', function($scope, $http) {
    $scope.user = {};
    $scope.errors = {};
    $scope.editMode = false;
    $scope.states = [];

    // Получаем данные
    $http.get('get-my-info').success(function(response) {
        $scope.states = response.states;
        $scope.user = response.attributes;
    });

    // Сохраняем данные
    $scope.save = function(){
        $http.get('save-profile', {params:$scope.user}).success(function(response) {
            if (response.success)
                $scope.editMode = false;
            else
                $scope.errors = response.errors;
        });
    }
}]);


/**
 *
 */
app.controller('EducationController', ['$scope', '$http', function($scope, $http) {
    $scope.educations = [];

    $scope.loaded = false;

    // Обновляем список образований
    $scope.updateList = function(){
        $http.get('education/all').success(function(response){
            $scope.educations = response;
            $scope.loaded = true;
        });
    };
    $scope.updateList();

    // Удаляем образование
    $scope.delete = function(education) {
        if (!confirm('Вы уверены?')) return;
        $http.delete('education/delete', {params:{id:education.id}}).success(function(response) {
            $scope.updateList();
        });
    };

    // Обновляем образование
    $scope.update = function(education) {
        bootbox.prompt({
            title: 'Редактировать образование',
            value: education.name,
            callback: function (name) {
                if (name === null)
                    return;

                $http.get('education/update', {
                    params: {
                        id: education.id,
                        name: name
                    }
                }).success(function (response) {
                    if (response === true)
                        $scope.updateList();
                });
            }
        });
    };

    // Добавляем образование
    $scope.add = function() {
        bootbox.prompt("Добавить образование", function(name) {
            $http.get('education/create', {params:{name:name}}).success(function(response) {
                if (response === true)
                    $scope.updateList();
            });
        });
    };
}]);



/**
 *
 */
app.controller('SocialNetworkController', ['$scope', '$http', function($scope, $http) {
    $scope.networks = [];

    $scope.loaded = false;

    // Обновляем список социальных сетей
    $scope.updateList = function(){
        $http.get('social-network/all').success(function(response){
            $scope.networks = response;
            $scope.loaded = true;
        });
    };
    $scope.updateList();

    // Удаляем социальную сеть
    $scope.delete = function(network) {
        if (!confirm('Вы уверены?')) return;
        $http.delete('social-network/delete', {params:{id:network.id}}).success(function(response) {
            $scope.updateList();
        });
    };

    // Обновляем социальную сеть
    $scope.update = function(network) {
        bootbox.prompt({
            title: 'Редактировать социальную сеть',
            value: network.url,
            callback: function (url) {
                if (url === null)
                    return;

                $http.get('social-network/update', {
                    params: {
                        id: network.id,
                        url: url
                    }
                }).success(function (response) {
                    if (response === true)
                        $scope.updateList();
                });
            }
        });
    };

    // Добавляем социальную сеть
    $scope.add = function() {
        bootbox.prompt("Добавить социальную сеть", function(url) {
            $http.get('social-network/create', {params:{url:url}}).success(function(response) {
                if (response === true)
                    $scope.updateList();
            });
        });
    };
}]);

app.controller('NewsController', ['$scope', '$http', '$sce', function($scope, $http, $sce) {

    $scope.news = [];
    $scope.currentNews = {};
    $scope.more = function(index){
        $scope.currentNews = $scope.news[index];
        $scope.currentNews.description = $sce.trustAsHtml($scope.currentNews.description);
    };

    function getAllNews(){
        $http.get('/main/default/get-all-news').success(function(response) {
            $scope.news = response;
        });
    }

    getAllNews();
}]);

