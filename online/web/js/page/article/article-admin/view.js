$(function(){

    /**
     *
     */
    var form = $('#add-category-form'),
        modal = $('#add-category-modal'),
        categoryGrid = $('#categories-grid');


    /**
     * Add category
     */
    form.find('.submit-button').click(function(e){

        $.post(form.attr('action'), form.serialize()).success(function(response) {
            if (response === true)
            {
                modal.modal('hide');
                categoryGrid.reload();
            }
            else
            {
                window.populateErrors(form, response);
            }
        });

        return false;
    });



    /**
     * Delete category
     */
    $(document).on('click', '.js-delete-category', function(){
        if (!confirm('Вы уверены?'))
            return false;

        $.get(this.href).success(function(response) {
            if (response === true)
                categoryGrid.reload();
        });

        return false;
    });


    /**
     * Close modal
     */
    modal.on('hidden.bs.modal', function (e) {
        form[0].reset();
    });


    /**
     * Test
     */
    //form.modal();

});