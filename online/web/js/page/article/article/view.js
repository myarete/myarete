$(function () {

    var footnote = $('a[rel=footnote]');

    footnote.click(function (event) {
        event.preventDefault();
        //alert($($(this).attr('href')).children('cite').html())
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });

    $(footnote).popover({
        html: true,
        content: function () {
            return $($(this).attr('href')).children('cite').html();
        },
        placement: 'bottom',
        container: '.side-block'
    }).click(function (event) {
        event.preventDefault();
    });

    $('body').on('click', function (e) {
        $(footnote).each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

});

function TestCtrl($scope, $http, $sce) {
    $scope.radioTest = [];
    $scope.submitTest = function (test_id, ind) {
        if ($scope.radioTest[ind] === undefined || $scope.radioTest[ind] === null) {
            alert('Необходимо выбрать вариант ответа!');
            return;
        }
        $http.post('/testing/default/submit-one-test', {
            result: $scope.radioTest[ind],
            test_id: test_id
        }).success(function (data, status) {
            $scope.result = data;
            $('#testModal-' + ind + '-' + test_id).modal('show');
        });
    };

    $scope.getContent = function (html) {
        return $sce.trustAsHtml(html);
    }

}

//var app = angular.module('profileApp', []);

// app.config(function($interpolateProvider) {
//     $interpolateProvider.startSymbol('[[');
//     $interpolateProvider.endSymbol(']]');
// });
//
// app.controller('TestCtrl', ['$scope', '$http', '$sce', TestCtrl]);
//
// app.directive('compile', ['$compile', function ($compile) {
//     return function(scope, element, attrs) {
//         scope.$watch(
//             function(scope) {
//                 return scope.$eval(attrs.compile);
//             },
//             function(value) {
//                 element.html(value);
//                 $compile(element.contents())(scope);
//                 MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
//             }
//         );
//     };
// }]);

$(function () {
    var $height = $('.crm__header').height();
    $(".crm__header_right .dropdown-menu").css('top', $height);
});

