$(function(){

    $('.replace-test').click(function(){

        if (!confirm("Вы уверены?"))
            return false;

        $.get(this.href).success(function(response){
            if (response === true)
            {
                location.reload();
            }
            else
            {
                bootbox.alert({
                    title: 'Ошибка',
                    message: response.error
                })
            }
        });

        return false;
    });

});

app.controller('TestCtrl', function($scope, $http, $sce) {
    $scope.radioTest = {};
    $scope.submitTest = function(){
        if(Object.keys($scope.radioTest).length != $scope.countTests){
            alert('Необходимо ответить на все тесты!');
            return;
        }

        var radioTest = {};
        $.each($('.radio-answer-group .row input:checked'), function(i, e) {
            radioTest[$(e).attr('id')] = $(e).val();
            $(this).closest('div').next('div').find('.comment').show();
        });
        $.each($('.radio-answer-group .row input[type=text]'), function(i, e) {
            var comment = $(this).parent().parent().next('div').find('.comment'),
                wrong = true;

            $.each($(e).parent().find('input[data-right=1]'), function(j, el) {
                if($(e).val() == $(el).val()) {
                    comment.html($(el).data('comment'));
                    comment.removeClass('test-row-wrong');
                    comment.addClass('test-row-right');
                    comment.show();
                    wrong = false;
                    return false;
                }
            });

            if(wrong) {
                comment.html($(e).parent().find('input[data-right=0]').data('comment'));
                comment.removeClass('test-row-right');
                comment.addClass('test-row-wrong');
                comment.show();
            }
        });

        $http.post('/testing/default/submit-test', {
            result: radioTest,
            testing_id: $scope.testingID
        }).success(function(data, status){
            $scope.result = data;
            MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
            MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
        });
    }

    $scope.getContent = function(html){
        return $sce.trustAsHtml(html);
    }

});

