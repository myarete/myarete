yii.confirm = function(message, okCallback) {
    swal({
        title: message,
        type: 'warning',
        showCancelButton: true,
        allowOutsideClick: true,
        value: '1234'
    }).then(okCallback);
}

yii.prompt = function(message, okCallback, inputValue) {
    swal({
        title: message,
        input: 'text',
        showCancelButton: true,
        confirmButtonText: 'Ок',
        showLoaderOnConfirm: true,
        allowOutsideClick: true,
        inputValue: inputValue
    }).then(okCallback);
}

alert = function(message) {
    swal(message);
}

