<?php

use kartik\alert\AlertBlock;
use online\assets\AppAsset;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
    <?= AlertBlock::widget([
        'options' => ['class' => 'container'],
        'delay' => false
    ]) ?>
<?php endif; ?>

<?php $this->beginBody() ?>
<body>

<div class="wrapper">
    <!-- begin header-->
    <header class="crm__header">
        <!-- crm__header_left-->
        <div class="crm__header_left"><a href="/"><img class="crm__header_logo" src="/img/logo.png" alt=""/></a>
            <div class="menu_toggle fa fa-reorder"></div>
        </div>
        <!-- right-buttons-->
        <div class="crm__header_right">
            <?= ButtonDropdown::widget([
                    'label' => '',
                    'options' => [
                        'class' => 'fa fa-question-circle-o drop'
                    ],
                    'dropdown' => [
                        'items' => [
                            [
                                'label' => 'Помощь',
                                'url' => Url::to(['/main/default/page-help']),

                            ],
                            [
                                'label' => 'Политика конфиденциальности',
                                'url' => Url::to(['/main/default/page-privacy']),
                            ],
                            [
                                'label' => 'Наши реквизиты',
                                'url' => Url::to(['/main/default/page-requisites']),
                            ],
                            [
                                'label' => 'Наши контакты',
                                'url' => Url::to(['/main/default/page-contacts']),
                            ],
                            [
                                'label' => 'Свяжитесь с нами',
                                'url' => Url::to(['/main/default/contact-us']),
                            ]
                        ]
                    ]
                ]);
            ?>
            <?= ButtonDropdown::widget([
                    'label' => number_format(Yii::$app->user->model->profile->cash, 0, '', ' ') . ' ₽',
                    'options' => [
                        'class' => 'drop',
                        'style' => [
                            'color' => '#565656',
                            'font-weight' => 'bold',
                            'font-size' => '16px',
                            'width' => '85px',
                        ]
                    ],
                    'dropdown' => [
                        'items' => [
                            [
                                'label' => 'В бумажнике <b>' . number_format(Yii::$app->user->model->profile->cash, 0, '', ' ') . ' ₽</b>',
                                'url' => Url::to(['/']),
                                'encode' => false,
                            ],
                            [
                                'label' => 'Пополнить бумажник',
                                'url' => Url::to(['/payment/default/refill-cash']),
                            ],
                            [
                                'label' => 'История операций',
                                'url' => Url::to(['/payment/default/index']),
                            ],
                        ]
                    ]
                ]);
            ?>
            <?php $itemsMenu = [
                    [
                        'label' => '<i class="fa fa-user"></i> Мой профиль',
                        'url' => Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host'],
                        'encode' => false,
                    ],
                    [
                        'label' => '<i class="fa fa-gear"></i> Настройки',
                        'url' => Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host'] . '/settings',
                        'encode' => false,
                    ],
                    [
                        'label' => '<i class="fa fa-external-link"></i> Выйти',
                        'url' => Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.signup'] . '.' . Yii::$app->params['host'] . '/logout',
                        'encode' => false,
                    ],
                ];
                if (Yii::$app->user->can('admin')) {
                    $item = [
                        'label' => '<i class="fa fa-gear"></i> Панель управления',
                        'url' => Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.admin'] . '.' . Yii::$app->params['host'],
                        'encode' => false,
                    ];
                    array_unshift($itemsMenu, $item);
                }
                echo ButtonDropdown::widget([
                    'label' => '',
                    'options' => [
                        'class' => 'drop',
                        'style' => [
                            'background' => 'url(' . Yii::$app->user->model->profile->getImage(['defaultSrc' => '/img/default_user.png']) . ') center center no-repeat',
                            'background-size' => 'contain',
                        ]
                    ],
                    'dropdown' => [
                        'items' => $itemsMenu,
                    ]
                ]);
            ?>
        </div>

        <div class="crm__header_center"><a class="fa fa-search search__toggle visible-xs" href="#"></a>
            <?= Html::beginForm('/main/default/search', 'get', ['class' => 'search']) ?>
            <?= Html::textInput('search', '', ['placeholder' => 'Поиск']); ?><i class="fa fa-search"></i>
            <?= Html::endForm() ?>
        </div>

    </header>
    <!-- end header-->
    <!-- begin menu-->
    <div class="crm__menu">
        <?= online\widgets\sidebarmenu\Menu::widget() ?>
    </div>
    <!-- end menu-->
    <!-- begin content-->
    <?php
    Nav::widget([
        'items' => [
            [
                'label' => '',
                'items' => [
                    [
                        'label' => '',
                        'url' => ['#']
                    ],

                ],
            ],
        ],
    ]);
    ?>
    <!--    <div class="side-block"></div>-->

    <?= $content ?>

    <!-- end content-->
    <!-- begin Modal traning__modal -->
    <div class="modal fade traning__modal" id="traning__modal">
        <div class="modal-dialog">
            <div class="modal-content"><a class="modal-close" href="#" data-dismiss="modal"></a>

            </div>
        </div>
    </div>
    <!-- end modal traning__modal -->

    <!-- begin Modal Questions And Answers -->
    <div class="modal fade modal-sm modal__faq" id="questions_and_answers__modal">
        <div class="modal-dialog">
            <div class="modal-content"><a class="modal-close" href="#" data-dismiss="modal"></a>
                <div class="modal__promocode_wrap">
                    <div class="modal-header">Вопросы и ответы
                        <div class="modal-header__sub"> Не смогли найти ответ на вопрос? Позвоните нам 8 800 707-53-79
                        </div>
                    </div>
                    <div class="modal-body">
                        <?php $questionsAndAnswers = \common\models\QuestionsAndAnswers::find()->joinWith('pages')->where(['pages.url' => Yii::$app->controller->route])->all(); ?>
                        <?php foreach ($questionsAndAnswers as $questionAndAnswer): ?>
                            <div class="modal__faq_item">
                                <div class="modal__faq_left"></div>
                                <div class="modal__faq_right">
                                    <div class="modal__faq_question"><?= $questionAndAnswer->question ?></div>
                                    <div class="modal__faq_answer"><?= $questionAndAnswer->answer ?></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end Modal Questions And Answers -->
    <!-- begin Modal  -->
    <div class="modal modal-sm fade" id="search__task__modal">
        <div class="modal-dialog">
            <div class="modal-content"><a class="modal-close" href="#" data-dismiss="modal"></a>
                <div class="modal__promocode_wrap">
                    <div class="modal-header">Найти задачу по номеру
                        <div class="modal-header__sub"> Просто введите номер задачи и система найдет Вам ее.</div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form">
                        <div class="form__row">
                            <div class="form__label">Номер задачи</div>
                            <div class="form__control">
                                <input class="form__input" name="search_task_number" id="search_task_number" autofocus
                                       type="text"/>
                                <button id="search_task_button" class="button">Найти задачу</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal  -->
</div>
<?php
$js = <<<JS
    $(function () {

        $('body').on('click', '#search_task_button', function () {
            if ($('#search_task_number').val().length > 0) {
                var value = $('[name=search_task_number]').val();
                location.href = '/exercise/favorite/search-exercise/' + value;
            }
        });


        var enterFlag = false;
        $('body').on('click', '#search__task__modal__button', function () {
            enterFlag = true;
            $('#search_task_number').attr('autofocus');
        });

        $('html').keydown(function (e) {
            if (e.keyCode == 13) {
                if (enterFlag) {
                    $('#search_task_button').click();
                }
            }
        });
    });
JS;
$this->registerJs($js);
?>
<?php $this->registerJsFile('/js/page/article/article/view.js?01', ['depends' => ['online\assets\AppAsset']]); ?>
</body>
<?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>
