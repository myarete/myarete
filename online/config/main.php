<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'online',
    'basePath' => dirname(__DIR__),
//    'controllerNamespace' => 'online\controllers',
    'defaultRoute' => 'main/default',
    'modules' => [
        'article' => [
            'class' => 'online\modules\article\Article',
        ],
        'exercise' => [
            'class' => 'online\modules\exercise\Exercise',
        ],
        'testing' => [
            'class' => 'online\modules\testing\Testing',
        ],
        'training' => [
            'class' => 'online\modules\training\Training',
        ],
        'main' => [
            'class' => 'online\modules\main\Main',
        ],
        'communication' => [
            'class' => 'online\modules\communication\Communication',
        ],
        'treemanager' => [
            'class' => '\kartik\tree\Module',
        ],
        'teacher' => [
            'class' => 'online\modules\teacher\Teacher',
        ],
        'premium' => [
            'class' => 'online\modules\premium\Premium',
        ],
        'payment' => [
            'class' => 'online\modules\payment\Payment',
        ],
        'profile' => [
            'class' => 'online\modules\profile\Profile',
        ],
        'settings' => [
            'class' => 'online\modules\settings\Settings',
        ],
        'blog' => [
            'class' => 'online\modules\blog\Blog',
        ],
        'promocodes' => [
            'class' => 'online\modules\promocodes\Promocodes',
        ],
        'bonus' => [
            'class' => 'online\modules\bonus\Bonus',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-online',
            'cookieValidationKey' => $params['cookieValidationKey'],
            'enableCsrfValidation' => false,
        ],
        'errorHandler' => [
            'errorAction' => 'main/default/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => require(__DIR__ . '/routes.php'),
        ],

        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => '//' . Yii::getAlias('signup') . strstr($_SERVER['SERVER_NAME'], '.'),
            'identityCookie' => ['name' => '_identity', 'httpOnly' => true, 'domain' => $params['cookieDomain']],
        ],
        'session' => [
            'name' => 'advanced',
            'cookieParams' => [
                'httpOnly' => true,
                'domain' => $params['cookieDomain'],
            ]
        ],
    ],
    'params' => $params,
];
