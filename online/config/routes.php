<?php
return [
    '' => 'profile',
    'paid' => 'payment/default/paid',
    'education/<action:\w+>' => 'main/education/<action>',
    'social-network/<action:\w+>' => 'main/social-network/<action>',
    
    'help' => 'main/default/page-help',
    'help/<id:[-\w]+>' => 'main/default/page-help-sub',
    'privacy-policy' => 'main/default/page-privacy',
    'requisites' => 'main/default/page-requisites',
    'contacts' => 'main/default/page-contacts',
    
    '<controller:\w+>/<id:\d+>' => '<controller>/view',
    '<controller:\w+>/<action:[-\w]+>/<id:\d+>' => '<controller>/<action>',
    '<controller:\w+>/<action:[-\w]+>' => '<controller>/<action>',
    '<module:\w+>/<controller:\w+>/<action:[-\w]+>/<id:\d+>' => '<module>/<controller>/<action>',
    '<module:\w+>/<controller:[-\w]+>/<action:[-\w]+>/<id:\d+>/<new_id:\d+>' => '<module>/<controller>/<action>',
];
