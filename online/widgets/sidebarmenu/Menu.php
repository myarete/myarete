<?php

namespace online\widgets\sidebarmenu;

use common\models\Learner;
use Yii;

class Menu extends \yii\widgets\Menu
{
    public function init()
    {
        $this->options = ['class' => 'crm__menu-list'];
        $this->encodeLabels = false;
        $this->activateParents = true;
        $this->items = $this->getItems();
        $this->submenuTemplate = '<ul class="crm__menu-sub">{items}</ul>';

        parent::init();
    }

    /**
     * @return array
     */
    private function getItems()
    {
        return [
            [
                'options' => ['class' => 'crm__menu-item'],
                'label' => '<i class="fa fa-user-o"></i><span>Мой профиль</span>',
                'url' => ['/profile/default/index'],
                'template' => '<a href="{url}" class="crm__menu-toggle"><span class="crm__menu-item_name">{label}</span></a>',
            ],
            [
                'options' => ['class' => 'crm__menu-item crm__menu-item_accordeon'],
                'label' => '<i class="fa fa-copy"></i><span>Магазин</span>',
                'url' => ['#'],
                'template' => '<a href="#" class="crm__menu-toggle"><span class="crm__menu-item_name">{label}</span></a>',
                'items' => [
                    [
                        'options' => ['class' => 'crm__menu-sub_item'],
                        'label' => 'Купленные уроки',
                        'url' => ['/article/article/my'],
                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
                    ],
                    [
                        'options' => ['class' => 'crm__menu-sub_item'],
                        'label' => 'Магазин уроков',
                        'url' => '/article/article/store',
                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
                    ],
                    [
                        'options' => ['class' => 'crm__menu-sub_item'],
                        'label' => 'Магазин тренировок',
                        'url' => ['/training/default/store'],
                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
                    ],
                    [
                        'options' => ['class' => 'crm__menu-sub_item'],
                        'label' => 'Купленные тренировоки',
                        'url' => ['/training/default/my'],
                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
                    ],
                ],
            ],
            [
                'options' => ['class' => 'crm__menu-item  crm__menu-item_accordeon'],
                'label' => '<i class="fa fa-calculator"></i><span>Тренировки</span>',
                'url' => ['#'],
                'template' => '<a href="{url}" class="crm__menu-toggle"><span class="crm__menu-item_name">{label}</span></a>',
                'items' => [
                    [
                        'options' => ['class' => 'crm__menu-sub_item'],
                        'label' => 'Создать тренировку',
                        'url' => ['/training/default/choose-class-subject'],
                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
                    ],
                    [
                        'options' => ['class' => 'crm__menu-sub_item'],
                        'label' => 'Сохранённые тренировки',
                        'url' => ['/training/default/saved'],
                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
                    ],
                    [
                        'options' => [
                            'class' => 'crm__menu-sub_item',

                        ],
                        'label' => 'Найти задачу по номеру',
                        'url' => ['#'],
                        'template' => '<a href="{url}" class="crm__menu-sub_link" id="search__task__modal__button" data-toggle="modal" data-target="#search__task__modal">
                                            <span class="crm__menu-item_name">{label}</span>
                                        </a>',
                    ],
                    [
                        'options' => ['class' => 'crm__menu-sub_item'],
                        'label' => 'Избранные задачи',
                        'url' => ['/exercise/favorite/index'],
                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
                    ],
                    [
                        'options' => ['class' => 'crm__menu-sub_item'],
                        'label' => 'Обмен материалами',
                        'url' => ['/communication/default/send-material'],
                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
                        'visible' => Yii::$app->user->can('teacher'),
                    ],
                ],
            ],
//            [
//                'options' => ['class' => 'crm__menu-item  crm__menu-item_accordeon'],
//                'label' => '<i class="fa fa-pencil-square-o"></i><span>Тесты</span>',
//                'url' => '#',
//                'template' => '<a href="{url}" class="crm__menu-toggle"><span class="crm__menu-item_name">{label}</span></a>',
//                'visible' => Yii::$app->user->isFree || Yii::$app->user->isTeacher || Yii::$app->user->isDesigner || Yii::$app->user->isAdmin,
//                'items' => [
//                    [
//                        'options' => ['class' => 'crm__menu-sub_item'],
//                        'label' => 'Создать тестирование',
//                        'url' => ['/testing/default/choose-class-subject'],
//                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
//                        'items' => [
//                            [
//                                'url' => ['/testing/default/create'],
//                            ],
//                        ],
//                    ],
//                    [
//                        'options' => ['class' => 'crm__menu-sub_item'],
//                        'label' => 'Сохранённые тестирования',
//                        'url' => ['/testing/default/saved'],
//                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
//                        'items' => [
//                            [
//                                'url' => ['/testing/default/saved'],
//                            ],
//                        ],
//                    ],
//                    [
//                        'options' => ['class' => 'crm__menu-sub_item'],
//                        'label' => 'Избранные тесты',
//                        'url' => ['/testing/favorite/index'],
//                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
//                        'items' => [
//                            [
//                                'url' => ['/testing/favorite/index'],
//                            ],
//                        ],
//                    ],
//                ],
//            ],
            [
                'options' => ['class' => 'crm__menu-item  crm__menu-item_accordeon'],
                'label' => '<i class="fa fa-calculator"></i><span>Учебный процесс 2</span>',
                'url' => ['#'],
                'template' => '<a href="{url}" class="crm__menu-toggle"><span class="crm__menu-item_name">{label}</span></a>',
                'items' => [
                    [
                        'options' => ['class' => 'crm__menu-sub_item'],
                        'label' => 'Мои группы',
                        'url' => ['/teacher/default/groups'],
                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
                        'visible' => Yii::$app->user->can('teacher'),
                    ],
                    [
                        'options' => ['class' => 'crm__menu-sub_item'],
                        'label' => 'Мои ученики',
                        'url' => ['/teacher/default/clients'],
                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
                        'visible' => Yii::$app->user->can('teacher'),
                    ],
                    [
                        'options' => ['class' => 'crm__menu-sub_item'],
                        'label' => 'Мои учителя',
                        'url' => ['/teacher/default/teachers'],
                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
                        'visible' => Yii::$app->user->can('teacher'),
                    ],
                    [
                        'options' => ['class' => 'crm__menu-sub_item'],
                        'label' => 'Группы',
                        'url' => ['/teacher/default/teachers'],
                        'template' => '<a href="{url}" class="crm__menu-sub_link"><span class="crm__menu-item_name">{label}</span></a>',
                        'visible' => Yii::$app->user->can('user'),
                    ],
                ]
            ],
            [
                'options' => ['class' => 'crm__menu-item'],
                'label' => '<i class="fa fa-file-text-o"></i><span>Журнал</span>',
                'url' => ['/blog/post/index'],
                'template' => '<a href="{url}" class="crm__menu-toggle"><span class="crm__menu-item_name">{label}</span></a>',
                'visible' => Yii::$app->user->can('user'),
            ],
//            [
//                'options' => ['class' => 'crm__menu-item'],
//                'label' => '<i class="fa fa-angellist"></i><span>Друзья ' . $incoming_request . '</span>',
//                'url' => ['/communication/default/friends'],
//                'template' => '<a href="{url}" class="crm__menu-toggle"><span class="crm__menu-item_name">{label}</span></a>',
//                'visible' => Yii::$app->user->can('user'),
//            ],
            [
                'options' => ['class' => 'crm__menu-item'],
                'label' => '<i class="fa fa-ticket"></i><span>Промокоды</span>',
                'url' => ['/promocodes/default/promocodes'],
                'template' => '<a href="{url}" class="crm__menu-toggle"><span class="crm__menu-item_name">{label}</span></a>',
            ],
            [
                'options' => ['class' => 'crm__menu-item'],
                'label' => '<i class="fa fa-trophy"></i><span>Мой премиум</span>',
                'url' => ['/premium/default/index'],
                'template' => '<a href="{url}" class="crm__menu-toggle"><span class="crm__menu-item_name">{label}</span></a>',
                'visible' => Yii::$app->user->can('user'),
            ],
            [
                'options' => ['class' => 'crm__menu-item'],
                'label' => '<i class="fa fa-trophy"></i><span>Бонусы</span>',
                'url' => ['/bonus/default/index'],
                'template' => '<a href="{url}" class="crm__menu-toggle"><span class="crm__menu-item_name">{label}</span></a>',
                'visible' => Yii::$app->user->can('user')
            ],
        ];
    }
}

?>
