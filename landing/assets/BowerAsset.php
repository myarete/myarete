<?php
/**
 * User: Evgeniy Karpeev [karpeevea@gmail.com]
 * Date: 21.02.2016
 * Time: 0:33
 */

namespace landing\assets;

use yii\web\AssetBundle;
use yii\web\View;

class BowerAsset extends AssetBundle
{
    public $sourcePath = '@bower';
    public $css = [
    ];
    public $js = [
        'jquery.inputmask/dist/jquery.inputmask.bundle.min.js',
    ];
    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
    public $depends = [
        'landing\assets\AppAsset'
    ];
}
