$(function(){


    /**
     *
     */
    var main_default_page__affix_menu = $('.main-default-page .affix-menu');
    main_default_page__affix_menu.affix({
        offset: {
            top: function(el){
                return this.top = $(el).offset().top;
            }
        }
    });


    /**
     *
     */
    $('.main-default-page .affix-menu-container').height(main_default_page__affix_menu.outerHeight());


    /**
     *
     */
    $('.main-default-page .affix-menu a').click(function(){
        var href = $(this).attr('href');
        $('html,body').animate({scrollTop: $(href).offset().top}, 'slow', function(){
            location.hash = href;
        });
        return false;
    });


    /**
     *
     */
    $('.main-default-page .section-anchor').css('top', -main_default_page__affix_menu.outerHeight());


    /**
     *
     */
    $('.main-default-page .section-anchor').css('top', -main_default_page__affix_menu.outerHeight());

});