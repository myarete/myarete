<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/local/params.php')
);

return [
    'id' => 'frontend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'landing\controllers',
    'defaultRoute' => 'main/default',

    'modules' => [
        'main' => [
            'class' => 'landing\modules\main\Main',
        ],
    ],

    'components' => [
        'request' => [
            'cookieValidationKey' => 'k_mb1v75iog8pqwrvc-Eq4nOqqwe12EeEZsud7',
            'enableCsrfValidation' => false,
        ],
        'errorHandler' => [
            'errorAction' => 'main/default/error',
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => require(__DIR__ . '/routes.php'),
        ],

        'user' => [
            'loginUrl' => $params['scheme'].'://'.$params['host'].'/login',
            'identityCookie' => [
                'name' => '_identity',
                'domain' => '.'.$params['host'],
            ],
        ],

        'session' => [
            'name' => 'adv_session',
            'cookieParams' => [
                'domain' => '.'.$params['host'],
            ],
        ],
    ],
    'params' => $params,
];
