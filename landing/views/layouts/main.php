<?php

use yii\helpers\Html;
use landing\assets\AppAsset;
use landing\assets\BowerAsset;
?>


<?php
AppAsset::register($this);
BowerAsset::register($this);
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?=$this->context->module->id.'-'.$this->context->id.'-'.$this->context->action->id?>">

<?php $this->beginBody() ?>

    <?= $content ?>

    <footer class="footer">
        <div class="container">
            <div class="pull-left">
                <?= \yii\widgets\Menu::widget([
                    'options' => ['class' => 'footer-left-menu'],
                    'items' => [
                        ['label' => 'Политика конфиденциальности', 'url' => Yii::$app->params['scheme'].'://'.Yii::$app->params['host'].'/main/default/page?view=privacy_policy'],
                        ['label' => 'Об Арете', 'url' => Yii::$app->params['scheme'].'://'.Yii::$app->params['host'].'/main/default/page?view=page_about'],
                    ],
                ]) ?>
            </div>
            <div class="pull-right">
                <?= \yii\widgets\Menu::widget([
                    'options' => ['class' => 'footer-right-menu'],
                    'items' => [
                        ['label' => 'Вконтакте', 'url' => 'https://vk.com/myarete', 'options'=>['target'=>'_blank']],
                        ['label' => 'Facebook', 'url' => 'https://www.facebook.com/MyArete'],
                    ],
                    'linkTemplate' => '<a href="{url}" target="_blank">{label}</a>',
                ]) ?>
            </div>
        </div>
    </footer>

<?php $this->endBody() ?>
<!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter33042244 = new Ya.Metrika({ id:33042244, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/33042244" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
<!-- Google counter -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-68857197-1', 'auto');
    ga('send', 'pageview');

</script>
<!-- /Google counter -->
</body>
</html>
<?php $this->endPage() ?>
