<?php

/**
 * @var $content \landing\modules\main\models\Landing
 * @var $this \yii\web\View
 */

$this->title = $content->title;

$this->registerCssFile('/lp/css/'.$content->alias.'.css', ['depends' => 'landing\assets\AppAsset']);

echo $content->content;
