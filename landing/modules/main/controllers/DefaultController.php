<?php

namespace landing\modules\main\controllers;

use common\components\other\Notifier;
use common\components\yii\base\BaseController;
use common\models\CourseRequest;
use common\models\CourseRequestVsSubject;
use landing\modules\main\models\Landing;
use yii\web\ViewAction;
use Yii;

class DefaultController extends BaseController
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'page' => [
                'class' => ViewAction::className(),
            ],
        ];
    }

    /**
     * @param null $page
     * @return string
     */
    public function actionIndex($page = null)
    {

        $content = Landing::findOne(['alias' => empty($page) ? '/' : $page]);

        return $this->render('index', [
            'content' => $content
        ]);
    }

    /**
     * @return array
     */
    public function actionCreateRequestQuick()
    {
        Yii::$app->response->format = 'json';
        $model = new CourseRequest();
        $params = Yii::$app->request->post();
        if(empty($params['name']) || empty($params['phone']) || empty($params['email']) || empty($params['type']))
        {
            return [
                'success' => false,
                'errors' => 'Bad request!',
            ];
        }

        $model->type = $params['type'];
        $model->fio = $params['name'];
        $model->phone = $params['phone'];
        $model->email = $params['email'];
        $model->class = 0;
        if($model->save()){
            Notifier::createdCourseRequest($model);
            return ['success' => true];
        }

        return [
            'success' => false,
            'errors' => $model->errors,
        ];
    }

    /**
     * @return array
     */
    public function actionCreateRequest()
    {
        $model = new CourseRequest;
        Yii::$app->response->format = 'json';

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            foreach (Yii::$app->request->post('subjects', []) as $subjectId)
            {
                $courseRequestVsSubject = new CourseRequestVsSubject();
                $courseRequestVsSubject->subject_id = $subjectId;
                $courseRequestVsSubject->course_request_id = $model->id;
                $courseRequestVsSubject->save(false);
            }

            Notifier::createdCourseRequest($model);

            return ['success' => true];
        }

        return [
            'success' => false,
            'errors' => $model->errors,
        ];
    }
}
