<?php

namespace landing\modules\main;

class Main extends \yii\base\Module
{
    public $controllerNamespace = 'landing\modules\main\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
