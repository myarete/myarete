<?php

namespace landing\modules\main\models;

use Yii;

/**
 * This is the model class for table "landing".
 *
 * @property integer $id
 * @property string $content
 * @property string $alias
 * @property string $title
 */
class Landing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'landing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['alias'], 'required', 'message' => 'Не должно быть пустым'],
            [['alias', 'title'], 'string', 'max' => 255],
            [['alias'], 'unique', 'message' => 'Уже занят'],
            [['alias'], 'match', 'pattern' => '~^[_a-z\d]+$~', 'message' => 'Только латинские буквы и нижние подчеркивания (пример: price_block)']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Контент',
            'alias' => 'Алиас',
            'title' => 'Заголовок',
        ];
    }

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);
		$file = '../../landing/web/css/' . $this->alias . '.css';
		if(!file_exists($file))
			file_put_contents($file, '');
	}
}
