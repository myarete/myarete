<?php
return [
    'login/<service:google|vkontakte|yandex>' => 'user/default/login',
    'login' => 'user/default/login',
    'registration' => 'user/default/registration',
    'logout' => 'user/default/logout',
    'price' => 'main/default/price',
    'teachers' => 'main/default/teachers',
    'subjects' => 'main/default/subjects',
    'about' => 'main/default/about',
    'page-about' => 'main/default/page-about',
    'contacts' => 'user/default/contacts',
    'privacy-policy' => 'user/default/privacy-policy',

    '<view:contacts>' => 'main/default/page',
    '<view:privacy_policy>' => 'main/default/page',
    '<view:page_about>' => 'main/default/page',

    'gallery' => '/gallery/default/index',

    '<controller:\w+>/<id:\d+>' => '<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

    '<controller:\w+>/<id:\d+>' => '<controller>/view',
    '<controller:\w+>/<action:[-\w]+>/<id:\d+>' => '<controller>/<action>',
    '<controller:\w+>/<action:[-\w]+>' => '<controller>/<action>',
    '<module:\w+>/<controller:\w+>/<action:[-\w]+>/<id:\d+>' => '<module>/<controller>/<action>',
    '<module:\w+>/<controller:[-\w]+>/<action:[-\w]+>/<id:\d+>/<new_id:\d+>' => '<module>/<controller>/<action>',
];
