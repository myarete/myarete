<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'signup',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'signup\controllers',
    'defaultRoute' => 'user/default/login',
    'modules' => [
        'user' => [
            'class' => 'signup\modules\user\User',
        ],
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => $params['cookieValidationKey'],
            'enableCsrfValidation' => false,
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => require(__DIR__ . '/routes.php'),
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity', 'httpOnly' => true, 'domain' => $params['cookieDomain']],
        ],
        'session' => [
            'name' => 'advanced',
            'cookieParams' => [
                'httpOnly' => true,
                'domain' => $params['cookieDomain'],
            ]
        ],
        'eauth' => [
            'class' => 'nodge\eauth\EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'httpClient' => [
                // uncomment this to use streams in safe_mode
//                'useStreamsFallback' => true,
            ],
            'services' => [ // You can change the providers and their classes.
//                'google' => [
                    // register your app here: https://code.google.com/apis/console/
//                    'class' => 'common\components\user\oauth\GoogleOAuth2Service',
//                    'class' => 'nodge\eauth\services\GoogleOAuth2Service',
//                    'clientId' => '518844259153-tdt7usvehe6se5l9a6je4pfsa7dkurpe.apps.googleusercontent.com',
//                    'clientSecret' => 'sTB_5mMsO0phKDZGEdcBlQcw',
//                    'clientId' => '626802215002-lkfkqbm34nnoffp9lfs2hnuuqog1kdcs.apps.googleusercontent.com',
//                    'clientSecret' => 'iaMhxOrRObGZiKvBerayxIQy',
//                    'title' => 'Google',
//                ],
                'yandex' => [
                    // register your app here: https://oauth.yandex.ru/client/my
                    'class' => 'common\components\user\oauth\YandexOAuth2Service',
                    'clientId' => 'ef261de0c13a4eb9bf2112290d0fb4d9',
                    'clientSecret' => '730f2dce86ba4aff9727b25d4f462466',
                    'title' => 'Yandex',
                ],
                'vkontakte' => [
                    // register your app here: https://vk.com/editapp?act=create&site=1
                    'class' => 'common\components\user\oauth\VKontakteOAuth2Service',
                    'clientId' => '6052976',
                    'clientSecret' => 'jpdzC5I4PNG5O8BI7CU0',
                ],
                'mailru' => [
                    // register your app here: http://api.mail.ru/sites/my/add
                    'class' => 'common\components\user\oauth\MailruOAuth2Service',
                    'clientId' => '763949',
                    'clientSecret' => '22ea19901a2b546e52e53c0934e0757c',
                    'title' => 'Mail.ru',
                ],
            ],
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/eauth.log',
                    'categories' => ['nodge\eauth\*'],
                    'logVars' => [],
                ],
            ],
        ],
    ],
    'params' => $params,
];
