<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\State;

/* @var $this yii\web\View */
/* @var $model frontend\modules\user\models\User */
/* @var $form ActiveForm */

$this->title = 'Войти';

$js = <<<JS
    $('[name=policy-confirm]').on('change', function() {
        if($(this).is(':checked')) {
            $('[name=registration-button]').prop('disabled', false);
            $('[name=registration-button]').removeAttr('style');
            $('#policy-msg').addClass('collapse');
        } else {
            $('[name=registration-button]').prop('disabled', true);
            $('[name=registration-button]').css('background-color', '#aac4f7')
            $('#policy-msg').removeClass('collapse');
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
JS;
$this->registerJs($js);
?>

<div class="auth login">
    <div class="auth__wrap">
        <!-- Форма авторизации-->

        <?php $form = ActiveForm::begin([
            'action' => ['/user/default/login'],
            'options' => ['class' => 'form', 'id' => 'login_form']
        ]); ?>

        <p class="form__title">Добро пожаловать в Арете Онлайн!</p>
        <div class="input__block">
            <?= $form->field($login, 'email', ['template' => "{label}\n{input}\n{hint}\n{error}",
                'labelOptions' => ['class' => 'fa fa-user']])->textInput(['type' => 'email', 'placeholder' => 'Введите ваш e-mail адрес', 'id' => 'email'])->label('') ?>
        </div>


        <div class="input__block">
            <?= $form->field($login, 'password', ['template' => "{label}\n{input}\n{hint}\n{error}",
                'labelOptions' => ['class' => 'fa fa-lock']])
                ->passwordInput(['placeholder' => 'Введите ваш пароль', 'id' => 'password'])->label('') ?>
        </div>

        <div class="lose">
            <?= Html::a('Забыли пароль?', ['forgot-password'], ['class' => 'form__lose']) ?>
            <?= Html::submitButton('Войти', ['class' => 'button button__green--full']) ?>
        </div>

<!--        <p class="or">или</p>-->

        <?php //echo \nodge\eauth\Widget::widget(['action' => 'login']); ?>

        <div class="form__sub">Вы еще не стали пользователем Арете Онлайн? <br>
            <a href="#" data-form-switch="register_form">Зарегистрируйтесь</a>!
        </div>

        <div class="form__sub">
            <a href="<?= Yii::$app->params['scheme'] . '://' . Yii::$app->params['host'] . '/privacy-policy' ?>">Политика
                конфиденциальности</a>
        </div>

        <?php ActiveForm::end(); ?>
        <?php if (Yii::$app->getSession()->hasFlash('error')) {
            echo '<div class="alert alert-danger">' . Yii::$app->getSession()->getFlash('error') . '</div>';
        } ?>

        <?php $form = ActiveForm::begin([
            'action' => ['/user/default/registration'],
            'options' => ['class' => 'form hide', 'id' => 'register_form']
        ]); ?>

        <?php
        $status = array_slice(State::getAll(), 0, 14);
        array_unshift($status, "Выберите статус");
        ?>
        <div class="form__title">Добро пожаловать в Арете Онлайн!</div>

        <div class="input__block">
            <?= $form->field($registration, 'first_name', ['template' => "{label}\n{input}\n{hint}\n{error}",
                'labelOptions' => ['class' => 'fa fa-user']])
                ->textInput(['placeholder' => 'Введите ваше имя', 'id' => 'first_name'])
                ->label(false) ?>
        </div>
        <div class="input__block">
            <?= $form->field($registration, 'last_name', ['template' => "{label}\n{input}\n{hint}\n{error}",
                'labelOptions' => ['class' => 'fa fa-user']])
                ->textInput(['placeholder' => 'Введите вашу фамилию', 'id' => 'last_name'])
                ->label(false) ?>
        </div>
        <?= $form->field($registration, 'state_id', ['inputOptions' => ['value' => '10', 'class' => 'hidden']])->label(false) ?>
        <div class="input__block">
            <?= $form->field($registration, 'email', ['template' => "{label}\n{input}\n{hint}\n{error}",
                'labelOptions' => ['class' => 'fa fa-envelope-o']])
                ->textInput(['type' => 'email', 'placeholder' => 'Введите e-mail адрес', 'id' => 'email'])
                ->label(false) ?>
        </div>
        <div class="input__block">
            <?= $form->field($registration, 'password', ['template' => "{label}\n{input}\n{hint}\n{error}",
                'labelOptions' => ['class' => 'fa fa-lock']])
                ->passwordInput(['placeholder' => 'Введите ваш пароль', 'id' => 'password'])
                ->label(false) ?>
        </div>

        <div class="input__block">
            <?= $form->field($registration, 'promocode', ['template' => "{label}\n{input}\n{hint}\n{error}",
                'labelOptions' => ['class' => 'fa fa-ticket']])
                ->textInput(['placeholder' => 'Промокод', 'id' => 'promocode'])
                ->label(false) ?>
        </div>

        <?= Html::submitButton('Начать бесплатно', [
            'class' => 'button button__blue--full',
            'name' => 'registration-button',
        ]) ?>

        <div id="policy-msg" class="form__sub policy-msg collapse">
            Вы не можете создать аккаунт без согласия с правилами использования портала.
        </div>

        <div class="form__sub">
            <?= Html::label(Html::checkbox('RegistrationForm[policy_confirm]', true)
                . 'Я согласен с ' . Html::a('правилами использования', ['/privacy-policy'])
                . ' портала Арете-Онлайн', 'policy-confirm') ?>
        </div>

        <div class="form__sub">
            <?= Html::label(Html::checkbox('RegistrationForm[email_confirm]', true)
                . 'Я согласен получать рассылку на свой email. '
                . Html::tag('i', '', [
                    'data-toggle' => 'tooltip',
                    'class' => 'fa fa-question-circle-o',
                    'title' => 'Мы не станем перегружать вашу почту спамом. Мы высылаем только важные и
                    полезные сообщения, не больше одного-двух в неделю. К тому же, вы всегда сможете изменить
                    согласие на рассылку в настройках профиля.'
                ]), 'email-confirm')
            ?>
        </div>

<!--        <p style="text-align: center; margin-bottom: 25px; color: rgba(0, 0, 0, 0.4);">-->
<!--            Или начните в несколько кликов с помощью аккаунта в соцсетях.-->
<!--        </p>-->

        <?php //echo \nodge\eauth\Widget::widget(['action' => 'login']); ?>

        <div class="form__sub">
            Уже являетесь пользователем Арете Онлайн?
            <a href="#" data-form-switch="login_form">Войдите!</a>
        </div>

        <?php ActiveForm::end() ?>

        <!-- begin auth__slider -->
        <div class="auth__slider">
            <div class="auth__slider_item" style="background-image: url(img/auth1.jpg);">
                <div class="auth__slider_text">
                    <div class="auth__slider_title">Изучение теории</div>
                    <p>Уроки от Арете доступно объяснят любую тему из школьной программы. Многие уроки доступны
                        бесплатно!</p>
                </div>
            </div>
            <div class="auth__slider_item" style="background-image: url(img/auth2.jpg);">
                <div class="auth__slider_text">
                    <div class="auth__slider_title">Практический материал</div>
                    <p>Создавайте тренировки из тысяч задач в нашей базе, чтобы отрабатывать полученные навыки на
                        практике. Это бесплатно!</p>
                </div>
            </div>
            <div class="auth__slider_item" style="background-image: url(img/auth3.jpg);">
                <div class="auth__slider_text">
                    <div class="auth__slider_title">Подготовка к экзаменам</div>
                    <p>В магазине тренировок вы найдете реальные варианты экзаменов, контрольных и других работ. Вы
                        сможете увидеть подробные решения задач и научиться решать их самостоятельно!</p>
                </div>
            </div>
        </div>
        <!-- end auth__slider -->
    </div>
</div>
<!-- Promo    -->
<div class="promo" id="promo">
    <div class="promo__line">
        <div class="promo__img"><img src="img/nout-1.png" alt=""/></div>
        <div class="promo__descr">
            <div class="promo__title">Система образования подстроится под вас</div>
            <p>Вам больше не нужно тратить время на изучение того, что вы и так знаете. В Арете Онлайн вы найдете уроки
                по тем темам, которые вам действительно пригодятся.</p>
            <p>Вы сможете составить тренировки из тех задач, которые вам действительно полезны. Учите только то, что вам
                удобно. Так, как вам удобно.</p>
        </div>
    </div>
    <div class="promo__line promo__line--invert">
        <div class="promo__img"><img src="img/nout-2.png" alt=""/></div>
        <div class="promo__descr">
            <div class="promo__title">Изучайте теорию по урокам Арете Онлайн</div>
            <p>Каждый урок в Арете Онлайн описывает одну узкую тему. Вы сможете прочитать только те уроки, которые вам
                интересны, а на остальные не обращать внимания. Также в уроках вы найдете множество шпаргалок – и все
                они бесплатны!</p>
            <p>Не отвлекайтесь на бесполезную информацию! С помощью Арете Онлайн вы сможете выстроить свою
                собственную систему образования!
            </p>
        </div>
    </div>
    <div class="promo__line">
        <div class="promo__img"><img src="img/nout-3.png" alt=""/></div>
        <div class="promo__descr">
            <div class="promo__title">Создавайте тренировки специально для себя</div>
            <p>В Арете Онлайн вы можете создавать тренировки из задач и упражнений по нужным вам темам. Укажите, сколько
                задач по какой теме вы хотите, и система все сделает за вас! Она подберет нужное количество задач из
                нашей базы, и вы получите абсолютно индивидуальную подборку.</p>
            <p>Все тренировки сохранятся в вашем профиле, и вы всегда сможете к ним вернуться!</p>
        </div>
    </div>
    <div class="promo__line promo__line--invert">
        <div class="promo__img"><img src="img/nout-4.png" alt=""/></div>
        <div class="promo__descr">
            <div class="promo__title">Готовьтесь к экзаменам по тренировкам из нашего магазина</div>
            <p>Мы обрабатываем реальные варианты экзаменов, олимпиад, контрольных и других испытаний прошлых лет и
                делаем из них тренировки. Вы можете найти их в магазине Арете Онлайн.</p>
            <p>Этот вариант идеально подойдет тем, кто хочет подготовиться к конкретной олимпиаде или поступлению в ВУЗ.
                В тренировках из магазина Арете Онлайн вы сможете найти подсказки и подробные решения ко всем включенным
                задачам.</p>
        </div>
    </div>
    <div class="promo__line">
        <div class="promo__img"><img src="img/nout-5.png" alt=""/></div>
        <div class="promo__descr">
            <div class="promo__title">Мы дарим деньги на счет</div>
            <p>Вы можете пополнить свой Арете-Кошелек с помощью банковской карты. А можете получить их бесплатно,
                приглашая друзей и выполняя ежедневные задания от Арете Онлайн!</p>
            <p>Заходите в профиль, создавайте тренировки, оставляйте комментарии под уроками – и ваш Арете-Кошелек
                будет пополняться. На эти деньги вы сможете купить уроки и тренировки из магазина Арете.</p>
        </div>
    </div>
</div>
<!-- begin infobox -->
<div class="infobox">
    <div class="infobox__title">Информация</div>
    <!-- begin infobox__grid-->
    <div class="grid grid_3 infobox__grid">
        <div class="col-sm-3"></div>
        <div class="col-sm-3">

            <?= Html::a('<img src="img/info1.png" alt=""/>', ['contacts']) ?>
            <div class="infobox__name">Контакты</div>
            <div>
                <?= Html::a('Узнать больше', ['contacts'], ['class' => 'button button__green--full']) ?>
            </div>
        </div>
        <div class="col-sm-3">
            <?= Html::a('<img src="img/info3.png" alt=""/>', ['privacy-policy']) ?>

            <div class="infobox__name">Политика конфиденциальности</div>
            <div>
                <?= Html::a('Узнать больше', ['privacy-policy'], ['class' => 'button button__green--full']) ?>
            </div>
        </div>
    </div>
    <!-- end infobox__grid-->
</div>
