<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php
$this->title = 'Установить пароль';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="auth">

    <?php $form = ActiveForm::begin([
                'options' => ['class' => 'form']
            ]); ?>

    <p class="form__title">Новый пароль</p>

    <div class="input__block">
        <?= $form->field($model, 'password', ['template' => "{label}\n{input}\n{hint}\n{error}",
            'labelOptions' => [ 'class' => 'fa fa-lock' ]])
            ->passwordInput(['placeholder' => 'Введите ваш новый пароль', 'id' => 'password'])->label('') ?>
    </div>

    <?= Html::submitButton('Установить пароль', ['class' => 'button button__green--full', 'name' => 'registration-button']) ?>
    <?php
    if (Yii::$app->getSession()->hasFlash('error')) {
        echo '<div class="alert alert-danger">'.Yii::$app->getSession()->getFlash('error').'</div>';
    }
    ?>
    <?php ActiveForm::end(); ?>
</section>