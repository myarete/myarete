<?php

use yii\helpers\Html;

?>

<?= Html::beginForm() ?>
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            <div class="form-group">
                <?= Html::label('В вашем VK не зарегистрирован email, введите ваш номер телефона:', ['for' => 'id-email']) ?>
                <?= Html::textInput('email', '', ['id' => 'id-email', 'class' => 'form-control']) ?>
            </div>
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?= Html::endForm() ?>
