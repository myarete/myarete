<?php

use common\models\State;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<?php
$this->title = 'Регистрация';
?>

<div class="auth login">
    <div class="auth__wrap">
        <!-- Форма авторизации-->
        
        <?php $form = ActiveForm::begin([
            'action' => ['/user/default/login'],
            'options' => ['class' => 'form hide', 'id' => 'login_form']
        ]); ?>
        
        <p class="form__title">Добро пожаловать в Арете Онлайн!</p>
        <div class="input__block">
            <?= $form->field($login, 'email', ['template' => "{label}\n{input}\n{hint}\n{error}",
                'labelOptions' => ['class' => 'fa fa-user']])->textInput(['type' => 'email', 'placeholder' => 'Введите ваш e-mail адрес', 'id' => 'email'])->label('') ?>
        </div>
        
        <div class="input__block">
            <?= $form->field($login, 'password', ['template' => "{label}\n{input}\n{hint}\n{error}",
                'labelOptions' => ['class' => 'fa fa-lock']])
                ->passwordInput(['placeholder' => 'Введите ваш пароль', 'id' => 'password'])->label('') ?>
        </div>
        
        <div class="lose">
            <?= Html::a('Забыли пароль?', ['forgot-password'], ['class' => 'form__lose']) ?>
            <?= Html::submitButton('Войти', ['class' => 'button button__green--full']) ?>
        </div>
        
        <p class="or">или</p>
        
        <?= \nodge\eauth\Widget::widget(['action' => 'login']); ?>
        <div class="form__sub">Вы еще не стали пользователем Арете Онлайн? <br>
            <a href="#" data-form-switch="register_form">Зарегистрируйтесь</a>!
        </div>
        <div class="form__sub"><a
                href="<?= Yii::$app->params['scheme'] . '://' . Yii::$app->params['host'] . '/privacy-policy' ?>">Политика
                конфиденциальности</a></div>
        <?php ActiveForm::end(); ?>
        <?php
        if (Yii::$app->getSession()->hasFlash('error')) {
            echo '<div class="alert alert-danger">' . Yii::$app->getSession()->getFlash('error') . '</div>';
        }
        ?>
        
        <?php $form = ActiveForm::begin([
            'action' => ['/user/default/registration'],
            'options' => ['class' => 'form', 'id' => 'register_form']
        ]); ?>
        
        <?php
        $status = array_slice(State::getAll(), 0, 14);
        array_unshift($status, "Выберите статус");
        ?>
        
        <div class="form__title">Добро пожаловать в Арете Онлайн!</div>
        
        <div class="input__block">
            <?= $form->field($registration, 'first_name', ['template' => "{label}\n{input}\n{hint}\n{error}",
                'labelOptions' => ['class' => 'fa fa-user']])
                ->textInput(['placeholder' => 'Введите ваше имя', 'id' => 'first_name'])
                ->label('') ?>
        </div>
        <div class="input__block">
            <?= $form->field($registration, 'last_name', ['template' => "{label}\n{input}\n{hint}\n{error}",
                'labelOptions' => ['class' => 'fa fa-user']])
                ->textInput(['placeholder' => 'Введите вашу фамилию', 'id' => 'last_name'])
                ->label('') ?>
        </div>
        <?= $form->field($registration, 'state_id', ['inputOptions' => ['value' => '10', 'class' => 'hidden']])->label(false) ?>
        <div class="input__block">
            <?= $form->field($registration, 'email', ['template' => "{label}\n{input}\n{hint}\n{error}",
                'labelOptions' => ['class' => 'fa fa-envelope-o']])
                ->textInput(['type' => 'email', 'placeholder' => 'Введите e-mail адрес', 'id' => 'email'])
                ->label('') ?>
        </div>
        <div class="input__block">
            <?= $form->field($registration, 'password', ['template' => "{label}\n{input}\n{hint}\n{error}",
                'labelOptions' => ['class' => 'fa fa-lock']])
                ->passwordInput(['placeholder' => 'Введите ваш пароль', 'id' => 'password'])
                ->label('') ?>
        </div>
        
        <div class="input__block">
            <?= $form->field($registration, 'promocode', ['template' => "{label}\n{input}\n{hint}\n{error}",
                'labelOptions' => ['class' => 'fa fa-ticket']])
                ->textInput(['placeholder' => 'Промокод', 'id' => 'promocode'])
                ->label('') ?>
        </div>
        
        <?= Html::submitButton('Начать бесплатно', ['class' => 'button button__blue--full', 'name' => 'registration-button']) ?>
        <br><br>
<!--        <p style="text-align: center; margin-bottom: 25px; color: rgba(0, 0, 0, 0.4);">Или начните в несколько кликов с
            помощью аккаунта в соцсетях.</p>-->
        
        <?php // echo \nodge\eauth\Widget::widget(['action' => 'login']); ?>
        <div class="form__sub">Уже являетесь пользователем Арете Онлайн? <a href="#" data-form-switch="login_form">Войдите</a>!
        </div>
        <div class="form__sub"><a
                href="<?= Yii::$app->params['scheme'] . '://' . Yii::$app->params['host'] . '/privacy-policy' ?>">Политика
                конфиденциальности</a></div>
        <?php ActiveForm::end(); ?>
        
        
        <!-- begin auth__slider -->
        <div class="auth__slider">
            <div class="auth__slider_item" style="background-image: url(img/auth1.jpg);">
                <div class="auth__slider_text">
                    <div class="auth__slider_title">Изучение теории</div>
                    <p>Уроки от Арете доступно объяснят любую тему из школьной программы. Многие уроки доступны
                        бесплатно!</p>
                </div>
            </div>
            <div class="auth__slider_item" style="background-image: url(img/auth2.jpg);">
                <div class="auth__slider_text">
                    <div class="auth__slider_title">Практический материал</div>
                    <p>Создавайте тренировки из тысяч задач в нашей базе, чтобы отрабатывать полученные навыки на
                        практике. Это бесплатно!</p>
                </div>
            </div>
            <div class="auth__slider_item" style="background-image: url(img/auth3.jpg);">
                <div class="auth__slider_text">
                    <div class="auth__slider_title">Подготовка к экзаменам</div>
                    <p>В магазине тренировок вы найдете реальные варианты экзаменов, контрольных и других работ. Вы
                        сможете увидеть подробные решения задач и научиться решать их самостоятельно!</p>
                </div>
            </div>
        </div>
        <!-- end auth__slider -->
    </div>
</div>


<!-- Promo    -->
<div class="promo">
    <div class="promo__line">
        <div class="promo__img"><img src="img/nout-1.png" alt=""/></div>
        <div class="promo__descr">
            <div class="promo__title">Система образования подстроится под вас</div>
            Вам больше не придется читать множество ненужных текстов и решать скучные задачи. Учите то, что вам удобно.
            Так, как вам удобно.
        </div>
    </div>
    <div class="promo__line promo__line--invert">
        <div class="promo__img"><img src="img/nout-2.png" alt=""/></div>
        <div class="promo__descr">
            <div class="promo__title">Изучайте теорию по урокам Арете Онлайн</div>
            Каждый урок описывает очень узкую тему. Вы можете выбрать и прочитать интересные уроки, а на остальные не
            обращать внимания! В уроках вы также найдете множество шпаргалок – почти все бесплатно!
        </div>
    </div>
    <div class="promo__line">
        <div class="promo__img"><img src="img/nout-3.png" alt=""/></div>
        <div class="promo__descr">
            <div class="promo__title">Создавайте тренировки специально для себя</div>
            Укажите нужное вам количество задач из каждой темы, и Арете Онлайн сделает все за вас. Система подберет вам
            случайные задачи из нашей базы. Все тренировки сохранятся в вашем профиле, и вы всегда сможете к ним
            вернуться.
        </div>
    </div>
    <div class="promo__line promo__line--invert">
        <div class="promo__img"><img src="img/nout-4.png" alt=""/></div>
        <div class="promo__descr">
            <div class="promo__title">Готовьтесь к экзаменам по тренировкам из нашего магазина</div>
            Мы обрабатываем реальные варианты экзаменов, контрольных и других испытаний прошлых лет и делаем из них
            тренировки. А вы можете найти эти тренировки в магазине Арете Онлайн. Так вы увидите подробные решения всех
            задач экзамена и сможете лучше подготовиться!
        </div>
    </div>
    <div class="promo__line">
        <div class="promo__img"><img src="img/nout-5.png" alt=""/></div>
        <div class="promo__descr">
            <div class="promo__title">Мы дарим деньги на счет</div>
            Вы можете внести деньги на свой Арете-Кошелек через банковскую карту. А можете заработать их, приглашая
            друзей или выполняя ежедневные задания от Арете Онлайн. На эти деньги вы сможете купить уроки или тренировки
            из магазина Арете.
        </div>
    </div>
</div>
<!-- begin infobox -->
<div class="infobox">
    <div class="infobox__title">Информация</div>
    <!-- begin infobox__grid-->
    <div class="grid grid_3 infobox__grid">
        <div class="grid__wrapper"><img src="img/info1.png" alt=""/>
            <div class="infobox__name">Контакты</div>
            <div class="infobox__text">Все необходимые материалы и принадлежности мы выдаем бесплатно.</div>
            <div><a class="button button__green--full"
                    href="<?= Yii::$app->params['scheme'] . '://' . Yii::$app->params['host'] . '/contacts' ?>">Узнать
                    больше</a></div>
        </div>
        <div class="grid__wrapper"><img src="img/info2.png" alt=""/>
            <div class="infobox__name">Реквизиты</div>
            <div class="infobox__text">При записи на несколько курсов скидка. Больше учиться – выгодно!</div>
            <div><a class="button button__green--full"
                    href="<?= Yii::$app->params['scheme'] . '://' . Yii::$app->params['host'] . '/requisites' ?>">Узнать
                    больше</a></div>
        </div>
        <div class="grid__wrapper"><img src="img/info3.png" alt=""/>
            <div class="infobox__name">Политика конфиденциальности</div>
            <div class="infobox__text">Мы сохраняем индивидуальный подход к каждому ученику.</div>
            <div><a class="button button__green--full"
                    href="<?= Yii::$app->params['scheme'] . '://' . Yii::$app->params['host'] . '/privacy-policy' ?>">Узнать
                    больше</a></div>
        </div>
    </div>
    <!-- end infobox__grid-->
</div>
