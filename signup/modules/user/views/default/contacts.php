<?php

use common\models\Html;

?>
<div class="crm__content">
    <?php

    if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
        <?= \kartik\alert\AlertBlock::widget([
            'delay' => false
        ]) ?>
    <?php endif; ?>

    <div class="wihite__box shadow">
        <?= Html::get('contacts') ?>
    </div>
</div>