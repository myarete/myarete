<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php
$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="auth login">
    <div class="auth__wrap">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form']
    ]); ?>

    <p class="form__title">Забыли пароль?</p>
    <div class="input__block">
        <?= $form->field($model, 'email', ['template' => "{label}\n{input}\n{hint}\n{error}",
            'labelOptions' => [ 'class' => 'fa fa-book' ]])->textInput(['type' => 'email', 'placeholder' => 'Введите ваш e-mail адрес', 'id' => 'email'])->label('') ?>
    </div>

    <?= Html::submitButton('Восстановить', ['class' => 'button button__blue--full']) ?>


    <?php ActiveForm::end(); ?>
    <?php
    if (Yii::$app->getSession()->hasFlash('error')) {
        echo '<div class="alert alert-danger">'.Yii::$app->getSession()->getFlash('error').'</div>';
    }
    ?>
    </div>
</div>
