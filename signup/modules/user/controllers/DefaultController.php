<?php

namespace signup\modules\user\controllers;

use common\components\yii\base\BaseController;
use common\models\DailyTasks;
use common\models\UserVsDailyTasks;
use signup\modules\user\forms\ForgotPasswordForm;
use signup\modules\user\forms\SetPasswordForm;
use common\models\User;
use common\models\Profile;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use signup\modules\user\forms\LoginForm;
use signup\modules\user\forms\RegistrationForm;
use yii\filters\AccessControl;

class DefaultController extends BaseController
{
    //public $layout = '@online/views/layouts/main';
    
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['registration', 'login', 'confirm-email', 'forgot-password', 'restore-password', 'contacts', 'privacy-policy'],
                        //'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        //'roles' => ['@'],
                    ],
                    'eauth' => [
                        // required to disable csrf validation on OpenID requests
                        'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                        'only' => ['login'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        return true;///$this->redirect('/login');
    }
    
    /**
     * @return string
     */
    public function actionRegistration()
    {
        $login = new LoginForm();
        $registration = new RegistrationForm;
        if ($registration->load(Yii::$app->request->post()) && $registration->registration()) {
            Yii::$app->session->setFlash('success', 'Ссылка для подтверждения регистрации была отправлена на Ваш E-mail');
            Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['noreplyEmail'])
                ->setTo(Yii::$app->params['contactsEmail'])
                ->setSubject('Зарегистрирован пользователь')
                ->setTextBody('Зарегистрирован новый пользователь ' . $registration->email . "\nhttp://pa.myarete.com/user/default/index")
                ->send();
            
            return $this->redirect(Url::home());
        }
        
        return $this->render('registration', [
            'login' => $login,
            'registration' => $registration
        ]);
    }
    
    /**
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host']);
        }
        
        $serviceName = Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
//             $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setRedirectUrl('https://signup.myarete.com/login/');
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('login'));
            
            try {
                if ($eauth->authenticate()) {
                    $user = User::findByEAuth($eauth);
                    $email = $eauth->getAttribute('email') ? $eauth->getAttribute('email') : Yii::$app->request->post('email');
                    
                    if (!$user && !$email && $eauth->getServiceName() == 'vkontakte') {
                        return $this->render('input-email', ['service' => $eauth->getServiceName()]);
                    }
                    
                    if (!$user) {
                        if ($user = User::findByEmail($email)) {
                            $user->service = $eauth->getServiceName();
                            $user->social_id = $eauth->getId();
                            $user->save();
                        } else {
                            $user = new User;
                            $user->email = $email;
                            $user->auth_key = Yii::$app->security->generateRandomString();
                            $user->password = Yii::$app->security->generatePasswordHash(Yii::$app->security->generateRandomString());
                            $user->role = 'client';
                            $user->confirm_code = Yii::$app->security->generateRandomString();
                            $user->is_confirmed = 1;
                            $user->service = $eauth->getServiceName();
                            $user->social_id = $eauth->getId();
                            $user->save();
                            $profile = new Profile;
                            $profile->user_id = $user->id;
                            $profile->state_id = 16;
                            $profile->first_name = $eauth->getAttribute('given_name');
                            $profile->last_name = $eauth->getAttribute('family_name');
                            $profile->image = $eauth->getAttribute('picture');
                            $profile->save();
                            
                            Yii::$app->getSession()->set('user-' . $user->id, $user);
                            Yii::$app->getUser()->login($user);
                            $eauth->redirect(Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host'] . '/settings/default/intermediate-page');
                        }
                    }
                    
                    Yii::$app->getSession()->set('user-' . $user->id, $user);
                    
                    Yii::$app->getUser()->login($user);
                    
                    // special redirect with closing popup window
                    $eauth->redirect(Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host']);
                } else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            } catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: ' . $e->getMessage());
                
                // close popup window and redirect to cancelUrl
                //$eauth->cancel();
                $eauth->redirect(Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host']);
            }
        }
        
        $login = new LoginForm();
        $registration = new RegistrationForm;
        
        if ($login->load(Yii::$app->request->post()) && $login->login()) {
            $dailyTasks = UserVsDailyTasks::find()->where(['user_id' => Yii::$app->user->id])->all();
            if (empty($dailyTasks)) {
                $dailyTasks = DailyTasks::find()->all();
                
                foreach ($dailyTasks as $task) {
                    $model = new UserVsDailyTasks();
                    $model->task_id = $task->id;
                    $model->user_id = Yii::$app->user->id;
                    $model->status = 0;
                    $model->save();
                }
            }
            
            return $this->redirect(Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host']);
        }
        
        return $this->render('login', [
            'login' => $login,
            'registration' => $registration
        ]);
    }
    
    /**
     * @param $code
     * @param $email
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionConfirmEmail($code, $email)
    {
        $user = User::find()->where([
            'email' => $email,
            'confirm_code' => $code,
        ])->one();
        
        if ($user === null)
            $this->pageNotFound();
        
        $user->is_confirmed = 1;
        
        if ($user->save()) {
            Yii::$app->user->login($user, User::LOGGED_DURATION);
            Yii::$app->session->setFlash('success', 'Вы успешно вошли в систему');
            
            $dailyTasks = UserVsDailyTasks::find()->where(['user_id' => Yii::$app->user->id])->all();
            if (empty($dailyTasks)) {
                $dailyTasks = DailyTasks::find()->all();
                
                foreach ($dailyTasks as $task) {
                    $model = new UserVsDailyTasks();
                    $model->task_id = $task->id;
                    $model->user_id = Yii::$app->user->id;
                    $model->status = 0;
                    $model->save();
                }
            }
            Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['noreplyEmail'])
                ->setTo($user->email)
                ->setSubject('Регистрация в портале Арете Онлайн.')
                ->setHtmlBody('<p>Здравствуйте! Вы зарегистрировались в образовательном портале  '
                    . Html::a('Арете Онлайн', 'https://myarete.com') . ', используя этот почтовый адрес.</p>'
                    . '<p>Добро пожаловать! У нас вы найдете множество образовательных материалов, а также сможете создать свои собственные подборки задач.</p>'
                    . '<p>Наш проект постоянно развивается. Если у вас есть какие-то пожелания, комментарии, критические замечания - пишите нам на адрес writeus@myarete.com. Хотите увидеть новые функции Арете Онлайн? Испытываете трудности с использованием портала? Нужны определенные материалы в ближайшее время? Напишите нам! Мы обязательно рассмотрим все такие сообщения.</p>'
                    . '<p>Если вы не регистрировались в образовательной системе Арете Онлайн, напишите нам на адрес writeus@myarete.com, и мы удалим Ваш аккаунт.</p>')
                ->send();
            
            return $this->redirect(Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host']);
        }
    }
    
    /**
     * @return string
     */
    public function actionForgotPassword()
    {
        $model = new ForgotPasswordForm;
        
        if ($model->load(Yii::$app->request->post()) && $model->process()) {
            Yii::$app->session->setFlash('success', 'Инструкция для восстановления пароля была отправлена на Ваш E-mail');
            return $this->redirect(Url::home());
        }
        
        return $this->render('forgot_password', ['model' => $model]);
    }
    
    /**
     * @param $code
     * @param $email
     * @return string
     */
    public function actionRestorePassword($code, $email)
    {
        $user = User::find()->where([
            'email' => $email,
            'confirm_code' => $code,
        ])->one();
        
        if ($user === null)
            $this->pageNotFound();
        
        $model = new SetPasswordForm;
        $model->user = $user;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Ваш пароль был успешно изменен');
            Yii::$app->user->login($user, User::LOGGED_DURATION);
            return $this->redirect(Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host']);
        }
        
        return $this->render('set_password', ['model' => $model]);
    }
    
    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(Url::home());
        //return $this->redirect(Yii::$app->params['scheme'] . '://' . Yii::$app->params['host'] . '/home');
    }
    
    public function actionPrivacyPolicy()
    {
        return $this->render('privacy_policy');
    }
    
    /**
     * @return string
     */
    public function actionContacts()
    {
        return $this->render('contacts');
    }
}

