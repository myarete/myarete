<?php

namespace signup\modules\user;

class User extends \yii\base\Module
{
    public $controllerNamespace = 'signup\modules\user\controllers';

    public $label = 'Пользователи';

    public function init()
    {
        parent::init();
    }
}
