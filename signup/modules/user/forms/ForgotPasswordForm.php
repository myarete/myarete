<?php
namespace signup\modules\user\forms;

use common\components\other\Notifier;
use common\models\User;
use yii\base\Model;
use Yii;

class ForgotPasswordForm extends Model
{
    /**
     * @var string
     */
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            [
                'email',
                'exist',
                'targetClass' => User::className(),
                'targetAttribute' => 'email',
                'message' => 'Пользователь с таким E-mail не существует'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
        ];
    }

    /**
     * @return bool
     */
    public function process()
    {
        if (!$this->validate())
            return false;

        $user = User::find()->where(['email' => $this->email])->one();
        $user->generateConfirmCode();
        $user->update(false, ['confirm_code']);

        Notifier::sendRestorePasswordLink($user);

        return true;
    }
}
