<?php

namespace signup\modules\user\forms;

use common\components\other\Notifier;
use common\models\Profile;
use common\models\Promocodes;
use common\models\State;
use common\models\User;
use Yii;
use yii\base\Exception;
use yii\base\Model;

class RegistrationForm extends Model
{
    /**
     * @var string
     */
    public $first_name;
    public $last_name;
    public $email;
    public $password;
    public $promocode;
    public $policy_confirm;
    public $email_confirm;

    /**
     * @var int
     */
    public $state_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'promocode'], 'filter', 'filter' => 'trim'],
            [['first_name', 'last_name', 'email', 'password'], 'required'],
            ['email', 'email'],
            [
                'email',
                'unique',
                'targetClass' => 'common\models\User',
                'message' => 'This email address has already been taken.',
            ],
            ['state_id', 'required'],
            ['state_id', 'exist', 'targetClass' => State::className(), 'targetAttribute' => 'id'],
            ['promocode', 'exist', 'targetClass' => Promocodes::className(), 'targetAttribute' => 'promocode'],
            ['password', 'string', 'min' => 6],
            [['email_confirm', 'policy_confirm'], 'integer']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'password' => 'Пароль',
            'email' => 'E-mail',
            'state_id' => 'Статус',
            'promocode' => 'Промокод',
        ];
    }

    /**
     * @return bool
     */
    public function registration()
    {
        if (!$this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {
            // Save user
            $user = new User();
            $user->username = $this->email;
            $user->email = $this->email;
            $user->role = 'client';
            $user->setPassword($this->password);
            $user->generateConfirmCode();
            $user->generateAuthKey();

            if (!$user->save(false)) {
                throw new Exception('User save error');
            }

            $auth = Yii::$app->authManager;
            $role = $auth->getRole('user');
            $auth->assign($role, $user->id);

            $newPromocode = new Promocodes();
            $newPromocode->promocode = $newPromocode->random_string(10);
            $newPromocode->value = 200;
            $newPromocode->owner_user_id = $user->id;
            $newPromocode->user_id = $user->id;
            $newPromocode->register = 1;
            $newPromocode->save();

            // Save profile
            $profile = new Profile;
            $profile->user_id = $user->id;
            $profile->first_name = $this->first_name;
            $profile->last_name = $this->last_name;
            $profile->state_id = $this->state_id;
            $profile->policy_confirm = $this->policy_confirm ? $this->policy_confirm : 0;
            $profile->email_confirm = $this->email_confirm ? $this->email_confirm : 0;
            //$profile->promocode = $newPromocode->random_string(10);

            if (!empty($this->promocode)) {
                if (Promocodes::findOne(['promocode' => $this->promocode, 'register' => 1])) {
                    $promocode = Promocodes::findOne(['promocode' => $this->promocode, 'register' => 1]);
                    $profile->cash = $promocode->value;
                    $model = Profile::findOne(['user_id' => $promocode->owner_user_id]);
                    $model->cash += $promocode->value;
                    $model->update();
                } elseif (Promocodes::findOne(['promocode' => $this->promocode, 'gift' => 1])) {
                    $promocode = Promocodes::findOne(['promocode' => $this->promocode]);
                    $profile->cash = $promocode->value;
                }
            }

            if (!$profile->save(false))
                throw new Exception('User save error');

            Notifier::sendConfirmEmailLink($user);

            $transaction->commit();
            return $user;
        } catch (Exception $e) {
            //$transaction->rollBack();
            echo $e;
        }

        return false;
    }
}
