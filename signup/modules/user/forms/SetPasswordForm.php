<?php

namespace signup\modules\user\forms;

use common\models\User;
use yii\base\Model;
use Yii;

class SetPasswordForm extends Model
{
    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    public $oldPassword;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $passwordConfirm;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => User::PASSWORD_MIN_LENGTH],

            //['passwordConfirm', 'compare', 'compareAttribute' => 'password'],
            ['oldPassword', 'findPass'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            //'passwordConfirm' => 'Повторите пароль',
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate())
            return false;

        $this->user->setPassword($this->password);
        return $this->user->update(false, ['password']);
    }

    public function findPass($attribute, $params)
    {
        if(! $this->user->validatePassword($this->oldPassword))
            $this->addError($attribute, 'Старый пароль неверен');
    }
}
