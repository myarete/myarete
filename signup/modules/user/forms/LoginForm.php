<?php

namespace signup\modules\user\forms;

use common\models\User;
use Yii;
use yii\base\Model;

class LoginForm extends Model
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    public $rememberMe;

    /**
     * @var \common\models\User|bool|null
     */
    private $_user = false;


    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['rememberMe', 'integer'],
            ['password', 'required'],
            ['password', 'validatePassword'],
        ];
    }


    /**
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors())
        {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Некорректный E-mail или пароль.');
            }
        }
    }

    /**
     * @return bool
     */
    public function login()
    {
        if ($this->validate()){
            return Yii::$app->user->login($this->getUser(), User::LOGGED_DURATION);
        } else{
            return false;
        }
    }


    /**
     * @return \common\models\User
     */
    public function getUser()
    {
        if ($this->_user === false)
            $this->_user = User::find()->where(['email' => $this->email])->one();

        return $this->_user;
    }


    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня'
        ];
    }
}
