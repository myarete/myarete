$(function(){

    ymaps.ready(function(){

        $('.affiliate-map').each(function(){
            var _this = this;

            $.get('http://geocode-maps.yandex.ru/1.x/', {
                    format : 'json',
                    geocode : $(this).data('address')
                })
                .success(function(data) {
                    if (data.response.GeoObjectCollection.featureMember.length == 0)
                    {
                        alert('Ничего не найдено');
                    }
                    else
                    {
                        var coords = data.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos.split(' ');
                        lt = parseFloat(coords[1]);
                        lg = parseFloat(coords[0]);

                        // Map init
                        var map = new ymaps.Map(_this.id, {
                            center      : [lt, lg],
                            zoom        : 12,
                            behaviors   : ['default', 'scrollZoom']
                        });
                        map.controls.add('smallZoomControl', {top: 5, left: 5});

                        // Add placemark
                        var placemark = new ymaps.Placemark([lt, lg], {}, {})
                        map.geoObjects.add(placemark);
                    }
                });

        });

    });

});