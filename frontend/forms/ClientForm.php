<?php

namespace frontend\forms;

use common\models\Faq;
use Yii;
use yii\base\Model;

class ClientForm extends Model
{
    /**
     * @var string
     */
    public $question;

    /**
     * @var string
     */
    public $fio;

    /**
     * @var string
     */
    public $email;

    public $verifyCode;
    
    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['question', 'required', 'message' => 'Обязательно к заполнению'],

            ['fio', 'required', 'message' => 'Обязательно к заполнению'],
            ['fio', 'string', 'max' => 255],

            ['email', 'required', 'message' => 'Обязательно к заполнению'],
            ['email', 'email', 'message' => 'Некорректный E-mail'],
            ['email', 'string', 'max' => 255],
        ];
    }


    /**
     * @return array
     */
    public function attributeLabels()
    {
        return (new Faq)->attributeLabels();
    }


    /**
     * @return bool
     */
    public function send()
    {
        $message = '';
        foreach ($this->attributes as $attribute => $value)
            $message .= $this->getAttributeLabel($attribute).' : '.$value."\n";

        return Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['contactsEmail'])
            ->setFrom(Yii::$app->params['noreplyEmail'])
            ->setSubject('Новый вопрос')
            ->setTextBody($message)
            ->send();
    }
}
