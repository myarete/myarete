<?php

namespace frontend\modules\blog\controllers;

use common\models\Category;
use common\models\Tags;
use common\models\CommentForm;
use Yii;
use common\models\Post;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;

class PostController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $post = new Post();
        $category = new Category();
        $tags = Tags::find()->limit(10)->all();

        $posts = $post->getPublishedPosts();



        return $this->render('index', [
            'posts' => $posts,
            'categories' => $category->getCategories(),
            'tags' => $tags
        ]);
    }

    /**
     * @param string $id
     * @return string
     */
    public function actionView($id)
    {
        $post = new Post();
        return $this->render('view', [
            'model' => $post->getPost($id),
            'commentForm' => new CommentForm(Url::to(['comment/add', 'id' => $id])),
        ]);
    }
}
