<?php

use common\models\Comment;
use common\models\TagPost;
use yii\helpers\Html;

/* @var $model common\models\Post */
/* @var \frontend\models\CommentForm $commentForm \;
/* @var TagPost $post */

$this->title = "Новости"
?>


<section class="section home__what" >
    <p class="title"><?= $model->title ?></p>
    <p class="text"><?= $model->content ?></p>
    <br>
    <p class="text"><?= Html::a($model->category->title, ['category/view', 'id' => $model->category->id]) ?> <?= date("d-m-Y", strtotime($model->publish_date)) ?> в <?= date("H:i", strtotime($model->publish_date)) ?></p>
</section>



