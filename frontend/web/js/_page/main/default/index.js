$(function(){


    /**
     *
     */
    var main_default_index__affix_menu = $('.main-default-index .affix-menu');
    main_default_index__affix_menu.affix({
        offset: {
            top: function(el){
                return this.top = $(el).offset().top;
            }
        }
    });


    /**
     *
     */
    $('.main-default-index .affix-menu-container').height(main_default_index__affix_menu.outerHeight());


    /**
     *
     */
    $('.main-default-index .affix-menu a').click(function(){
        var href = $(this).attr('href');
        $('html,body').animate({scrollTop: $(href).offset().top}, 'slow', function(){
            location.hash = href;
        });
        return false;
    });


    /**
     *
     */
    $('.main-default-index .section-anchor').css('top', -main_default_index__affix_menu.outerHeight());


    /**
     *
     */
    $('.main-default-index .section-anchor').css('top', -main_default_index__affix_menu.outerHeight());


    /**
     *
     */
    $('.js-subject-item').click(function(){
            var modal = $('#course-request-subject-modal');
        modal.find('input[name="subjects[]"]').filter('[value='+$(this).data('subjectId')+']').trigger('click');
        modal.find('.description').html( $(this).find('.description').html() );
        modal.find('img').attr( 'src', $(this).find('img')[0].src );
        modal.modal();
    });

    /**
     *
     */
    $('.course-request-subject-btn').click(function(){
        $('.course-request-subject-form').show();
        $(this).hide();
    });

    /**
     *
     */
    $('#course-request-subject-modal').on('hidden.bs.modal', function(){
        $(this).find('form')[0].reset();
        //$(this).find('.description').html('');
        $(this).find('.course-request-subject-form').hide();
        $('.course-request-subject-btn').show();
    });
});