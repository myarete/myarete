<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

$this->title = 'Главная'
?>

<!--	модальное окно	-->
<div class="modal fade bs-example-modal-lg" id="ask-me" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <p class="title">Напишите нам</p>
            <?= frontend\widgets\user_form\FaqForm::widget() ?>
        </div>
    </div>
</div>

<div class="home__background"></div>
<a id="top">&nbsp</a>
<section class="section home__main" style="margin-bottom:-50px !important;">
    <p class="title">Образование,<br> достойное каждого</p>
    <p class="text">Мы готовим школьников к любым экзаменам, сертификатам и олимпиадам.<br> А еще просто помогаем им учиться и развиваться.</p>
    <!--  форма обратной связи	-->
    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'feedback__form shadow'],
        'action' => ['/course/default/create-request']
    ]); ?>

    <div class="input__block">
        <?= $form->field($courseRequest, 'fio')
            ->textInput(['type' => 'text', 'placeholder' => 'Введите ваше имя', 'id' => 'name'])->label('') ?>

    </div>
    <div class="input__block">
        <?= $form->field($courseRequest, 'phone')
            ->textInput(['type' => 'text', 'placeholder' => 'Введите ваш телефон', 'id' => 'phone'])->label('') ?>
    </div>
    <div class="input__block">
        <?= $form->field($courseRequest, 'subjects')->widget(Select2::classname(),[

                'data' => ArrayHelper::map($subjects,'id','name'),
                'options' => [
                    'id' => 'book',
                    'placeholder' => 'Выберите предметы ...',
                    'multiple' => true
                ],
            ])->label('');
        ?>

    </div>
        <?= Html::submitButton('Узнать больше', ['class' => 'button curve white']) ?>

        <a href="#">Политика конфиденциальности</a>
    <?php ActiveForm::end(); ?>

    <div id="scroll-arrow"><a href="#scroll"><div class="home__scroll"></div></a></div>

</section>
<br>
<br>
<a id="scroll">&nbsp</a>

<section class="section pricing home__pricing">
    <p class="title">Ученикам</p>
    <div class="content">
        <?php foreach ($prices as $price): ?>
            <div class="block">
                <a href="" data-toggle="modal" data-target="#<?= $price->alias ?>"><div class="photo property-<?= $price->alias ?>"></div></a>
                <a href="" data-toggle="modal" data-target="#<?= $price->alias ?>"><p class="class"><?= $price->name ?></p></a>
                <div action="" class="form">
                    <?php foreach ($price->pricesVsCategory as $pricesVsCategory): ?>
                        <p class="text"><?= $pricesVsCategory->category->title ?></p>
                        <p class="coast"><?= (int) $pricesVsCategory->category->price ?> <span>Рублей/ <?= $pricesVsCategory->category->period ?></span></p>
                        <div class="gray-line"></div>
                    <?php endforeach; ?>
                    <?php foreach ($price->pricesVsSubjects as $pricesVsSubjects): ?>
                        <?php if ($pricesVsSubjects->price_id == $price->id): ?>
                            <a href="#" data-toggle="modal" data-target="#<?= $price->alias ?>-<?= $pricesVsSubjects->subjects->alias ?>"><?= $pricesVsSubjects->subjects->name ?></a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <br>
                    <a class="button curve" data-toggle="modal" data-target="#<?= $price->alias ?>">Узнать больше</a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>

<div id="about"></div>
<br>
<section class="section home__what">
    <p class="title">Что такое <span>Арете</span>?</p>
    <p class="text"><?= \common\models\Html::get('what_is_arete') ?></p>
    <a href="/page_about" class="button curve">Узнать больше</a>
</section>

<div id="faq"></div>

<section class="section home__questions" >
    <!--	<div class="top-wave"></div>-->
    <div class="content">
        <p class="title">Часто задаваемые вопросы</p>
        <div class="home__accordion">
            <?php if (count($faqs)): ?>
                <?php foreach ($faqs as $index => $faq): ?>
                    <div>
                        <input id="ac-<?= $index?>" name="accordion" type="checkbox"/>
                        <label for="ac-<?= $index?>"><?= $faq->question ?></label>
                        <article>
                            <p><?= $faq->answer ?></p>
                        </article>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <p class="footer">Не нашли ответ на свой вопрос? <a data-toggle="modal" data-target="#ask-me">Напишите нам</a></p>
    </div>
    <!--	<div class="bottom-wave"></div>	-->
</section>

<div id="reviews"></div>
<br>
<section class="section home__reviews" >
    <p class="title">Отзывы</p>
    <article class="reviews">
        <div class="posts">
            <?php if (count($feedbacks)): ?>
                <?php foreach ($feedbacks as $index => $feedback): ?>
                    <div>
                        <p class="text">« <?= $feedback->content ?> »</p>
                        <div class="user" id="user-<?=$index?>">
                            <?= $feedback->getImageTag(['defaultHtml' => '<i class="fa fa-user"></i>']) ?>
                            <p class="name"><?= $feedback->fio ?></p>
                            <p class="rank">Студент</p>
                        </div>
                    </div>
                <?php endforeach;?>
            <?php endif; ?>
        </div>
    </article>
</section>

<script>
    $(document).ready(function(){
        $('.posts').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            autoplay: true,
            autoplaySpeed: 5000,
            mobileFirst: true,
        });

        $("#scroll-arrow, #scroll-about, #scroll-faq, #scroll-reviews").on("click","a", function (event) {
            event.preventDefault();

            var id  = $(this).attr('href'),
                top = $(id).offset().top;

            $('body,html').animate({scrollTop: top}, 1500);
        });
    });
</script>

<?= $this->render('pages/modal__price', [
        'prices' => $prices,
        'subjects' => $subjects
    ]
); ?>

<?= $this->render('pages/modal__subject', [
        'prices' => $prices,
        'subjects' => $subjects
    ]
); ?>

