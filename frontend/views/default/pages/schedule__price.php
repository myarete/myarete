<?php 

if (!empty($schedule)): ?>
    <div class="timetable">
        <ul>
            <li class="title">Понедельник</li>
            <?php foreach ($schedule as $item): ?>
                <?php if ($item->day == 'Понедельник'): ?>
                    <li>
                        <p><?= $item->subject->name ?></p>
                        <p><?= $item->class_name ?></p>
                        <p><?= substr($item->time, 0, -3) ?></p>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
        <ul>
            <li class="title">Вторник</li>
            <?php foreach ($schedule as $item): ?>
                <?php if ($item->day == 'Вторник'): ?>
                    <li>
                        <p><?= $item->subject->name ?></p>
                        <p><?= $item->class_name ?></p>
                        <p><?= substr($item->time, 0, -3) ?></p>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
        <ul>
            <li class="title">Среда</li>
            <?php foreach ($schedule as $item): ?>
                <?php if ($item->day == 'Среда'): ?>
                    <li>
                        <p><?= $item->subject->name ?></p>
                        <p><?= $item->class_name ?></p>
                        <p><?= substr($item->time, 0, -3) ?></p>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
        <ul>
            <li class="title">Четверг</li>
            <?php foreach ($schedule as $item): ?>
                <?php if ($item->day == 'Четверг'): ?>
                    <li>
                        <p><?= $item->subject->name ?></p>
                        <p><?= $item->class_name ?></p>
                        <p><?= substr($item->time, 0, -3) ?></p>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
        <ul>
            <li class="title">Пятница</li>
            <?php foreach ($schedule as $item): ?>
                <?php if ($item->day == 'Пятница'): ?>
                    <li>
                        <p><?= $item->subject->name ?></p>
                        <p><?= $item->class_name ?></p>
                        <p><?= substr($item->time, 0, -3) ?></p>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
        <ul>
            <li class="title">Суббота</li>
            <?php foreach ($schedule as $item): ?>
                <?php if ($item->day == 'Суббота'): ?>
                    <li>
                        <p><?= $item->subject->name ?></p>
                        <p><?= $item->class_name ?></p>
                        <p><?= substr($item->time, 0, -3) ?></p>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
