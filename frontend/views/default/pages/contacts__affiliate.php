<div class="row">
    <div class="col-sm-6">
        <?= $model->description ?>
    </div>
    <div class="col-sm-6">
        <?= \yii\helpers\Html::tag('div', '', [
            'class' => 'affiliate-map',
            'data-address' => $model->address,
            'id' => 'affiliate-map-'.$model->id,
        ]) ?>

        <p><?= $model->address ?></p>
    </div>
</div>


<div class="row">
    <div class="col-sm-12">
        <?php foreach ($model->teachers as $teacher): ?>
            <div class="row teacher-container vertical-align">
                <div class="col-xs-5 col-sm-4 col-md-3">
                    <?= $teacher->getImageTag(['class' => 'teacher-image']) ?>
                </div>
                <div class="col-xs-7 col-sm-8 col-md-9">
                    <h3><?= $teacher->fio ?></h3>
                    <p><?= $teacher->contacts ?></p>
                </div>
            </div>
        <?php endforeach; ?>

    </div>
</div>