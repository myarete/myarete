<?php

use common\models\SubjectOnHome;

$this->title = 'Учителя'

?>

<section class="section peoples">
    <p class="title">Преподаватели</p>
    <div class="content teachers">
        <div class="block">
            <div class="photo teacher__photo1" ></div>
            <p class="name teacher__name1" >Станислав Черкасов</p>
            <p class="text teacher__text1" >Выпускник Экономического факультета МГУ</p>
            <p class="text teacher__text1" >Математика, Физика, Химия</p>
        </div>
        <div class="block">
            <div class="photo teacher__photo2" ></div>
            <p class="name teacher__name2" >Дарья Завьялова</p>
            <p class="text teacher__text2" >Выпускница МГЮА им. Кутафина, диплом переводчика</p>
            <p class="text teacher__text1" >Английский, Обществознание</p>
        </div>
        <div class="block">
            <div class="photo teacher__photo3" ></div>
            <p class="name teacher__name3" >Арина Хализова</p>
            <p class="text teacher__text3" >Филологический факультет МГУ</p>
            <p class="text teacher__text1" >Русский язык, Литература</p>
        </div>
        <div class="block">
            <div class="photo teacher__photo4" ></div>
            <p class="name teacher__name4" >Марию Капинос</p>
            <p class="text teacher__text4" >Факультет психологии образования МГППУ</p>
            <p class="text teacher__text1" >Биология, Химия</p>
        </div>
        <div class="block">
            <div class="photo teacher__photo5" ></div>
            <p class="name teacher__name5" >Петр Хотеев</p>
            <p class="text teacher__text5" >Выпускник НИУ ВШЭ факультета медиакоммуникаций</p>
            <p class="text teacher__text1" >История, Обществознание</p>
        </div>
    </div>
</section>

<?php $this->registerJs("
        $('.teachers').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
            autoplay: true,
            autoplaySpeed: 4000,
            mobileFirst: true,


        });
    ", $this::POS_END);?>
