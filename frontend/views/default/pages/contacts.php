<?php
$this->title = 'Контакты'
?>
<section class="section map">
    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A284468707733c3d8cfd5d6aec93105ba5278ea7c8790de2a4f904cd27700c163&amp;width=100%25&amp;height=450&amp;lang=ru_RU&amp;scroll=true"></script>
    <p class="addres">Ленинский проспект, дом 90</p>
    <p><span>Как к нам пройти</span><br>Станция метро Проспект Вернадского, последний вагон из центра. Идти по проспекту Вернадского в сторону улицы Кравченко (около 40-50 метров). Затем свернуть на эту улицу и пройти до Ленинского проспекта. Дом 90 находится на углу Ленинского проспекта и улицы Кравченко. Вход со двора, в середине дома, черная металлическая дверь с крыльцом, под козырьком.</p>
</section>

<section class="section peoples">
    <p class="title">Единый номер поддержки<br>8 800 707-53-79</p>
    <div class="content">
        <div class="block" id="contact-1">
            <div class="photo"></div>
            <p class="name">Станислав Черкасов</p>
            <p class="text">Выпускник Экономического факультета МГУ</p>
            <p class="contact phone">+7 (916) 368-58-22</p>
            <p class="contact mail">stanislav-ch@myarete.com</p>
        </div>
        <div class="block" id="contact-2">
            <div class="photo"></div>
            <p class="name">Дарья Завьялова</p>
            <p class="text">Выпускница МГЮА им. Кутафина, диплом переводчика</p>
            <p class="contact phone">+7 (916) 805-92-32</p>
            <p class="contact mail">daria-z@myarete.com</p>
        </div>
        <div class="block" id="contact-3">
            <div class="photo"></div>
            <p class="name">Арина Хализова</p>
            <p class="text">Филологический факультет МГУ</p>
            <p class="contact phone">+7 (968) 835-29-45</p>
        </div>
    </div>

    <br>
    <br>
    <br>
    <div class="content">
        <div class="block" id="contact-4">
            <div class="photo"></div>
            <p class="name">Мария Капинос</p>
            <p class="text">Факультет психологии образования МГППУ</p>
            <p class="contact phone">+7 (926) 033-34-26</p>
        </div>
        <div class="block" id="contact-5">
            <div class="photo"></div>
            <p class="name">Петр Хотеев</p>
            <p class="text">Выпускник НИУ ВШЭ факультета медиакоммуникаций</p>
            <p class="contact phone">+7 (916) 771-21-43</p>
        </div>
    </div>
</section>

<br>
<br>
<br>
<br>

<?php $this->registerJs("
        $('.teachers').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
            autoplay: true,
            autoplaySpeed: 4000,
            mobileFirst: true,


        });
    ", $this::POS_END);?>