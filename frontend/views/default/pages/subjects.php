<?php

use common\models\SubjectOnHome;

$this->title = 'Предметы'

?>

<section class="section subjects">
    <p class="title">Предметы</p>
    <div class="content">
        <?php foreach ($subjects as $subject): ?>
            <a href="#" data-toggle="modal" data-target="#<?= $subject->alias ?>">
                <div class="subject">
                    <img src="<?= Yii::getAlias("@web/upload/subjectonhome/") ?><?= $subject->image ?>" />
                    <p class="name"><?= $subject->name ?></p>
                    <button class="button curve">Узнать больше</button>
                </div>
            </a>
        <?php endforeach; ?>
    </div>
</section>


<?= $this->render('modal__subject', [
        'subjects' => $subjects,
        'prices' => $prices
    ]
); ?>
