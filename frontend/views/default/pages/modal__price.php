<?php

use common\models\Html;
use common\models\SubjectsSchedule;

?>

<?php foreach ($prices as $price): ?>
    <?php foreach ($price->pricesVsSubjects as $pricesVsSubjects): ?>
        <div class="modal fade bs-example-modal-lg" id="<?= $price->alias ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-table">
                <div class="modal-content">
                    <p class="title"><?= Html::getModel('price_page__price_1')->title ?></p>

                    <?php $schedule = SubjectsSchedule::getSchedule($price->id) ?>
                    <?= $this->render('schedule__price', [
                        'schedule' => $schedule
                    ]); ?>
                    <p>Не нашли подходящего времени в расписании? Возможно, мы откроем новую группу специально для вас!</p><br>

                    <a class="button curve" data-dismiss="modal" id="registerCourse">Записаться на курс</a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endforeach; ?>

<script>
    $(function () {
        $('body').on('click', '#registerCourse', function(){
            $('input#name').focus();
            $('html, body').animate({ scrollTop: $('input#name').offset().top }, 500);
        })
    });
</script>
