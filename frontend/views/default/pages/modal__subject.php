<?php

use common\models\SubjectsSchedule;
use common\models\SubjectOnHome;

?>

<?php foreach ($prices as $price): ?>
    <?php foreach ($price->pricesVsSubjects as $pricesVsSubjects): ?>
        <div class="modal fade bs-example-modal-lg" id="<?= $price->alias ?>-<?= $pricesVsSubjects->subjects->alias ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-table">
                <div class="modal-content">
                    <p class="title"><?= $pricesVsSubjects->subjects->name ?></p>
                    <?= $pricesVsSubjects->subjects->description ?>
                    <?php $schedule = SubjectsSchedule::getScheduleForPrice($pricesVsSubjects->subjects->id, $price->id) ?>
                    <?= $this->render('schedule__price', [
                        'schedule' => $schedule
                    ]); ?>

                    <p>Не нашли подходящего времени в расписании? Возможно, мы откроем новую группу специально для вас!</p><br>

                    <a class="button curve" data-dismiss="modal" id="registerCourse">Записаться на курс</a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endforeach; ?>



<?php foreach ($subjects as $subject): ?>
<div class="modal fade bs-example-modal-lg" id="<?= $subject->alias?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-table">
        <div class="modal-content">
            <p class="title"><?= $subject->name ?></p>
            <?= $subject->description ?>
            <?php $schedule = SubjectsSchedule::getScheduleForSubject($subject->id) ?>
            <?= $this->render('schedule__price', [
                'schedule' => $schedule
            ]); ?>

            <p>Не нашли подходящего времени в расписании? Возможно, мы откроем новую группу специально для вас!</p><br>

            <a class="button curve" data-dismiss="modal" id="registerCourse">Записаться на курс</a>
        </div>
    </div>
</div>
<?php endforeach; ?>


<script>
    $(function () {
        $('body').on('click', '#registerCourse', function(){
            $('.footer input#name').focus();
            $('html, body').animate({ scrollTop: $('.footer input#name').offset().top }, 500);
        })
    });
</script>