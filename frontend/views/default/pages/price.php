<?php
use common\models\Html;

$this->title = 'Цены'
?>

<section class="section pricing home__pricing">
    <p class="title">Ученикам</p>
    <div class="content">
        <?php foreach ($prices as $price): ?>
            <div class="block">
                <a href="" data-toggle="modal" data-target="#<?= $price->alias ?>"><div class="photo property-<?= $price->alias ?>"></div></a>
                <a href="" data-toggle="modal" data-target="#<?= $price->alias ?>"><p class="class"><?= $price->name ?></p></a>
                <div action="" class="form">
                    <?php foreach ($price->pricesVsCategory as $pricesVsCategory): ?>
                        <p class="text"><?= $pricesVsCategory->category->title ?></p>
                        <p class="coast"><?= (int) $pricesVsCategory->category->price ?> <span>Рублей/ <?= $pricesVsCategory->category->period ?></span></p>
                        <div class="gray-line"></div>
                    <?php endforeach; ?>
                    <?php foreach ($price->pricesVsSubjects as $pricesVsSubjects): ?>
                        <?php if ($pricesVsSubjects->price_id == $price->id): ?>
                            <a href="#" data-toggle="modal" data-target="#<?= $price->alias ?>-<?= $pricesVsSubjects->subjects->alias ?>"><?= $pricesVsSubjects->subjects->name ?></a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <br>
                    <a class="button curve" data-toggle="modal" data-target="#<?= $price->alias ?>">Узнать больше</a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>

<?= $this->render('modal__price', [
        'prices' => $prices,
        'subjects' => $subjects
    ]
); ?>
<?= $this->render('modal__subject', [
        'prices' => $prices,
        'subjects' => $subjects
    ]
); ?>
