<?php

use frontend\assets\AppAsset;
use common\models\CourseRequest;
use common\models\SubjectOnHome;
use kartik\alert\AlertBlock;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>


<?php
AppAsset::register($this);
$courseRequest = new CourseRequest();
$subjects = SubjectOnHome::find()->where(['is_active' => 1])->orderBy('name')->all();
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=980">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<script type="text/javascript">
    $(document).ready(function () {
        $(window).scroll(function () {
            if (document.body.scrollTop >= 600) {
                $(".header-home a").css({
                    'color': '#333'
                });
            }
            ;
            if (document.body.scrollTop < 600) {
                $(".header-home a").css({
                    'color': '#fff'
                });
            }
            ;
        });
    });
</script>

<body class="<?= $this->context->module->id . '-' . $this->context->id . '-' . $this->context->action->id ?>">

<?php $this->beginBody() ?>
<div class="container-fluid">
    <div class="row">
        <header>
            <div class="header">
                <a href="/">
                    <div class="logo"></div>
                </a>
                <?= Html::a('Предметы', Url::toRoute(['/default/subjects'])) ?>
                <?= Html::a('Цены', Url::toRoute(['/default/price'])) ?>
                <!--                --><? //= Html::a('Учителя', Url::toRoute(['/teachers'])) ?>
                <?= Html::a('Новости', Url::toRoute(['/blog/post'])) ?>
                <?php if ($this->context->action->id == 'index'): ?>
                    <div id="scroll-about" style="margin-left: 25px; padding: 20px 0;"><?= Html::a('О нас', '#about') ?></div>
                    <div id="scroll-faq" style="margin-left: 25px; padding: 20px 0;"><?= Html::a('FAQ', '#faq') ?></div>
                    <div id="scroll-reviews" style="margin-left: 25px; padding: 20px 0;"><?= Html::a('Отзывы', '#reviews') ?></div>
                <?php else: ?>
                    <div style="margin-left: 25px; padding: 20px 0;"><?= Html::a('О нас', '/#about') ?></div>
                    <div style="margin-left: 25px; padding: 20px 0;"><?= Html::a('FAQ', '/#faq') ?></div>
                    <div style="margin-left: 25px; padding: 20px 0;"><?= Html::a('Отзывы', '/#reviews') ?></div>
                <?php endif; ?>
                <?= Html::a('Контакты', Url::toRoute(['/default/contacts'])) ?>
            </div>
            <div class="dropdown header">
                <?php if (Yii::$app->user->isGuest): ?>
                    <?= Html::a('Arete Online', Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.signup']
                        . '.' . Yii::$app->params['host'], ['class' => 'button curve white']) ?>
                <?php else: ?>
                    <?= Html::a('Arete Online', Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online']
                        . '.' . Yii::$app->params['host'], ['class' => 'button curve white']) ?>
                <?php endif; ?>
            </div>
        </header>

        <header class="mob">
            <a href="/">
                <div class="logo"></div>
            </a>
            <input type="checkbox" class="" id="burger">
            <label for="burger"></label>
            <div class="header__mob">
                <?= Html::a('Предметы', Url::toRoute(['/default/subjects'])) ?>
                <?= Html::a('Цены', Url::toRoute(['/default/price'])) ?>
                <?= Html::a('Учителя', Url::toRoute(['/teachers'])) ?>
                <?= Html::a('Новости', Url::toRoute(['/blog/post'])) ?>
                <?php if ($this->context->action->id == 'index'): ?>
                    <div id="scroll-about" style="margin-left: 25px; padding: 20px 0;"><?= Html::a('О нас', '#about') ?></div>
                    <div id="scroll-faq" style="margin-left: 25px; padding: 20px 0;"><?= Html::a('FAQ', '#faq') ?></div>
                    <div id="scroll-reviews" style="margin-left: 25px; padding: 20px 0;"><?= Html::a('Отзывы', '#reviews') ?></div>
                <?php else: ?>
                    <div style="margin-left: 25px; padding: 20px 0;"><?= Html::a('О нас', '/#about') ?></div>
                    <div style="margin-left: 25px; padding: 20px 0;"><?= Html::a('FAQ', '/#faq') ?></div>
                    <div style="margin-left: 25px; padding: 20px 0;"><?= Html::a('Отзывы', '/#reviews') ?></div>
                <?php endif; ?>
                <?= Html::a('Контакты', Url::toRoute(['/default/contacts'])) ?>
                <?php if (Yii::$app->user->isGuest) {
                    echo Html::a('Войти', Url::toRoute(['/login']));
                    echo Html::a('Зарегистрироваться', Url::toRoute(['/registration']));
                } else {
                    echo Html::a('Профиль', Url::toRoute([Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.online'] . '.' . Yii::$app->params['host']]));
                } ?>
            </div>
        </header>

        <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
            <?= AlertBlock::widget([
                'options' => ['class' => 'container'],
                'delay' => false
            ]) ?>
        <?php endif; ?>

        <?= $content ?>

        <br>
        <br>
        <br>
        <footer>
            <!--  форма обратной связи	-->
            <?php $form = ActiveForm::begin([
                'options' => ['class' => 'feedback__form shadow footer'],
                'action' => ['/course/default/create-request']
            ]); ?>

            <div class="input__block">
                <?= $form->field($courseRequest, 'fio')
                    ->textInput(['type' => 'text', 'placeholder' => 'Введите ваше имя', 'id' => 'name'])->label('') ?>

            </div>
            <div class="input__block">
                <?= $form->field($courseRequest, 'phone')
                    ->textInput(['type' => 'text', 'placeholder' => 'Введите ваш телефон', 'id' => 'phone'])->label('') ?>
            </div>
            <div class="input__block">
                <?= $form->field($courseRequest, 'subjects')->widget(Select2::classname(), [

                    'data' => ArrayHelper::map($subjects, 'id', 'name'),
                    'options' => [
                        'id' => 'book2',
                        'placeholder' => 'Выберите предметы ...',
                        'multiple' => true
                    ],
                ])->label('');
                ?>

            </div>

            <?= Html::submitButton('Узнать больше', ['class' => 'button curve white']) ?>

            <a href="#">Политика конфиденциальности</a>
            <?php ActiveForm::end(); ?>
            <div class="arc"></div>
        </footer>
    </div>
</div>
<?= \frontend\widgets\call_request_widget\CallRequestWidget::widget() ?>

<?php $this->endBody() ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript"> (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter33042244 = new Ya.Metrika({id: 33042244, clickmap: true, trackLinks: true, accurateTrackBounce: true});
            } catch (e) {
            }
        });
        var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
            n.parentNode.insertBefore(s, n);
        };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/33042244" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript><!-- /Yandex.Metrika counter -->
<!-- Google counter -->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-68857197-1', 'auto');
    ga('send', 'pageview');

</script>
<!-- /Google counter -->
</body>
</html>
<?php $this->endPage() ?>
