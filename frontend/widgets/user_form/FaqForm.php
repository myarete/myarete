<?php

namespace frontend\widgets\user_form;

use frontend\forms\ClientForm;

class FaqForm extends \yii\base\Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('index', [
            'model' => new ClientForm,
        ]);
    }
}
