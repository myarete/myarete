<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
?>

<?php $form = ActiveForm::begin([
    'id' => 'faq-form-widget',
    'action' => ['/faq/default/create'],
]); ?>

    <?= $form->field($model, 'question')->label('Задать вопрос')->textarea(['rows' => 6, 'placeholder' => $model->getAttributeLabel('question'), 'class' => 'input']) ?>


    <?= $form->field($model, 'fio')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('fio'), 'class' => 'input']) ?>


    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('email'), 'class' => 'input']) ?>

    <?= Html::submitButton('Отправить вопрос', ['class' => 'button curve white']) ?>


<?php ActiveForm::end(); ?>



<script>
    $(function(){

        /**
         *
         */
        $('#faq-form-widget').on('beforeSubmit', function(){
            var form = this;
            $.post($(this).attr('action'), $(this).serializeArray()).success(function(response){
                console.log(response);
                if ('success' in response && response.success)
                {
                    form.reset();
                    alert('Ваш вопрос успешно отправлен');
                }
                else
                {
                    alert('При отправке вопроса произошла ошибка');
                }
            });

            return false;
        });

    });
</script>
