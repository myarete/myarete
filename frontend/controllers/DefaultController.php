<?php

namespace frontend\controllers;

use common\components\helpers\YandexHelper;
use common\components\yii\base\BaseController;
use common\models\CourseRequest;
use common\models\Faq;
use common\models\Feedback;
use common\models\Payment;
use common\models\PricesOnHome;
use common\models\SubjectOnHome;
use Yii;
use yii\web\ViewAction;

class DefaultController extends BaseController
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'page' => [
                'class' => ViewAction::className(),
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $courseRequest = new CourseRequest();

        return $this->render('index', [
            'subjects' => SubjectOnHome::find()->where(['is_active' => 1])->orderBy('id')->all(),
            'prices' => PricesOnHome::find()->with('pricesVsCategory')->with('pricesVsSubjects')->all(),
            'faqs' => Faq::find()->all(),
//            'teachers' => Teacher::find()->with(['subjects' => function($query){ $query->with('subject'); }])->all(),
            'feedbacks' => Feedback::find()->where(['is_published' => 1])->all(),
            'courseRequest' => $courseRequest,
        ]);
    }

    /**
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('pages/about');
    }

    /**
     * @return string
     */
    public function actionSubjects()
    {
        return $this->render('pages/subjects', [
            'subjects' => SubjectOnHome::find()->where(['is_active' => 1])->orderBy('id')->all(),
            'prices' => PricesOnHome::find()->with('pricesVsCategory')->with('pricesVsSubjects')->all(),
        ]);
    }

    /**
     * @return string
     */
    public function actionTeachers()
    {
        return $this->render('pages/teachers');
    }

    /**
     * @return string
     */
    public function actionPrice()
    {
        return $this->render('pages/price', [
            'subjects' => SubjectOnHome::find()->where(['is_active' => 1])->orderBy('id')->all(),
            'prices' => PricesOnHome::find()->with('pricesVsCategory')->with('pricesVsSubjects')->all(),
        ]);
    }

    /**
     * @return string
     */
    public function actionPageAbout()
    {
        return $this->render('pages/page_about');
    }

    /**
     * @param $address
     * @return \stdClass
     */
    public function actionGeocode($address)
    {
        \Yii::$app->response->format = 'json';
        return YandexHelper::getCoordinatesByAddress($address);
    }

    public function actionPaidSuccess()
    {
        $payment = Payment::find()->where(['user_id' => Yii::$app->user->id, 'is_paid' => 0])->one();
        $payment->is_paid = 1;


        $profile = Profile::find()->where(['user_id' => Yii::$app->user->id])->one();
        $profile->cash += $payment->refill_sum;

        $profile->save();

        $payment->save();
        return 'Спасибо, ваш платеж был успешно выполнен!';
    }

    public function actionContacts()
    {
        return $this->render('pages/contacts');
    }
}
