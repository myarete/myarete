<?php

use kartik\alert\AlertBlock;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use signup\assets\AppAsset;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

?>


<?php
AppAsset::register($this);
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=980">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?= $this->context->module->id . '-' . $this->context->id . '-' . $this->context->action->id ?>">

<header>
    <div class="container">
        <div class="row">
            
            <div class="header">
                <a href="<?= Yii::$app->params['scheme'] . '://' . Yii::$app->params['host'] ?>">
                    <div class="logo"></div>
                </a>
                <?= Html::a('Войти/Регистрация', Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.signup'] . '.' . Yii::$app->params['host'] . '/login') ?>
                <?= Html::a('Журнал', Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.blog'] . '.' . Yii::$app->params['host'] . '/blog/post/index', ['class' => 'active']) ?>
                <?= Html::a('Почему Арете Онлайн', Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.signup'] . '.' . Yii::$app->params['host'] . '/#promo') ?>
                <?= Html::a('Политика конфиденциальности', Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.signup'] . '.' . Yii::$app->params['host'] . '/privacy-policy') ?>
                <?= Html::a('Контакты', Yii::$app->params['scheme'] . '://' . Yii::$app->params['subdomain.signup'] . '.' . Yii::$app->params['host'] . '/contacts') ?>
            </div>
        </div>
    </div>
</header>


<?php $this->beginBody() ?>
<div class="wrapper">
    <!-- begin content-->
    <!--    <div class="crm__content">-->
    <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
        <?= AlertBlock::widget([
            'options' => ['class' => 'container'],
            'delay' => false
        ]) ?>
    <?php endif; ?>
    <?= $content ?>
    <!--    </div>-->
    <!-- end content -->
</div>

<?php $this->endBody() ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript"> (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter33042244 = new Ya.Metrika({
                    id: 33042244,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true
                });
            } catch (e) {
            }
        });
        var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
            n.parentNode.insertBefore(s, n);
        };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/33042244" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript><!-- /Yandex.Metrika counter -->
<!-- Google counter -->
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-68857197-1', 'auto');
    ga('send', 'pageview');

</script>
<!-- /Google counter -->
</body>
</html>
<?php $this->endPage() ?>
