<?php

namespace blog\modules\blog\controllers;

use common\models\Category;
use common\models\Tags;
use Yii;
use yii\web\Controller;

class CategoryController extends Controller
{

    public function actionView($id)
    {
        $categoryModel = new Category();
        $category = $categoryModel->getCategory($id);
        $tags = Tags::find()->limit(10)->all();

        $posts = $category->getPosts();

        return $this->render('index', [
            'category' => $category,
            'posts' => $posts,
            'categories' => $categoryModel->getCategories(),
            'tags' => $tags
        ]);
    }
}

