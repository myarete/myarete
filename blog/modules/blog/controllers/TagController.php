<?php

namespace blog\modules\blog\controllers;

use common\models\Category;
use common\models\Tags;
use Yii;
use yii\web\Controller;

class TagController extends Controller
{

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $tagModel = new Tags();
        $tag = $tagModel->getTag($id);
        $categoryModel = new Category();

        $tags = Tags::find()->limit(10)->all();
        
        $posts = $tag->getPublishedPosts();

        return $this->render('view', [
            'tag' => $tag,
            'tags' => $tags,
            'posts' => $posts,
            'categories' => $categoryModel->getCategories()
        ]);
    }
}
