<?php

/** @var $tag common\models\Tags */
/** @var $posts \yii\data\ActiveDataProvider */
/** @var $categories yii\data\ActiveDataProvider */
/** @var $postTag \common\models\TagPost */

use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = $tag->title;
?>


<!-- begin content-->
<div class="crm__container">
    <!-- begin aside-->
    <div class="aside">
        <div class="tags">
            <div class="tags__title">Популярное сейчас</div>
            <div class="tags__list">
                <?php
                foreach ($tags as $tag) {
                    echo Html::a($tag->title, ['/blog/tag/view', 'id' => $tag->id], ['class' => 'tags__item']);
                }
                ?>
            </div>
        </div>
        <div class="social__widget_wrap" id="vk_groups"></div>
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?150"></script>
        <script>VK.Widgets.Group("vk_groups", {mode: 5, width: '270'}, 20003922);</script>
    </div>
    <!-- end aside-->
    <div class="crm__content">
        <div class="tab-menu tab-menu--short tab-menu--single">
            <?php
            foreach ($categories->models as $tagItem) {
                echo $this->render('/category/shortViewCategory', [
                    'model' => $tagItem
                ]);
            }
            ?>
        </div>
        <div class="blog__list">
            <?php
            if(count($posts->models) > 0) {
                foreach ($posts->models as $postTag) {
                    echo $this->render('/post/shortView', [
                        'model' => $postTag->getPost()->one()
                    ]);
                }
            } else {
                echo "<h3>Нет постов по данным тегам</h3>";
            }
            ?>
            <div>
                <?= LinkPager::widget([
                    'pagination' => $posts->getPagination()
                ]) ?>
            </div>
        </div>
    </div>
</div>


