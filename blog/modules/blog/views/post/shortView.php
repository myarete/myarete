<?php

use common\models\TagPost;
use yii\helpers\Html;

/* @var $model common\models\Post */
/* @var TagPost $postTag */
?>

<div class="blog__list_item">
    <div class="blog__list_top">
        <div class="blog__list_category"><?=$model->category->title?> </div>
        <div class="blog__list_date"><?= date("d-m-Y", strtotime($model->publish_date)) ?> в <?= date("H:i", strtotime($model->publish_date)) ?></div>
    </div>
    <a href="/blog/post/view/<?=$model->id?>">
        <div class="blog__list_title"><?= $model->title ?></div>
        <div class="blog__list_short"><?= $model->anons ?></div>
    </a>
</div>

