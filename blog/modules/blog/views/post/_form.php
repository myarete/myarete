<?php

use common\models\Post;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use admin\widgets\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Post */
/* @var $form yii\widgets\ActiveForm */
/* @var $authors yii\db\ActiveRecord[] */
/* @var $category yii\db\ActiveRecord[] */
/* @var $tags yii\db\ActiveRecord[] */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(
        ArrayHelper::map($category, 'id', 'title')
    ) ?>

    <?= $form->field($model, 'author_id')->dropDownList(
        ArrayHelper::map($authors, 'id', 'email')
    ) ?>

    <?= $form->field($model, 'anons')->widget(CKEditor::className(), [
        'options' => ['rows' => 100],
        'enableFinder' => true,
        'clientOptions' => [
            'toolbar' => [
                [
                    'name' => 'clipboard',
                    'items' => [
                        'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                    ],
                ],
                [
                    'name' => 'links',
                    'items' => [
                        'Link', 'Unlink', 'Anchor'
                    ],
                ],
                [
                    'name' => 'insert',
                    'items' => [
                        'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                    ],
                ],
                [
                    'name' => 'align',
                    'items' => [
                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                    ],
                ],
                [
                    'name' => 'document',
                    'items' => [
                        'Maximize', 'ShowBlocks', 'Source'
                    ],
                ],
                '/',
                [
                    'name' => 'basicstyles',
                    'items' => [
                        'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                    ],
                ],
                [
                    'name' => 'color',
                    'items' => [
                        'TextColor', 'BGColor'
                    ],
                ],
                [
                    'name' => 'paragraph',
                    'items' => [
                        'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                    ],
                ],
                [
                    'name' => 'styles',
                    'items' => [
                        'Styles', 'Format', 'Font', 'FontSize'
                    ],
                ],
                '/',
                [
                    'name' => 'insert',
                    'items' => [
                        'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                    ],
                ],
            ],
            'height' => 500,
            'allowedContent' => true,
            'contentsCss' => [
                '/css/fa/font-awesome.min.css',
                '/css/bootstrap.min.css',
                Yii::$app->params['scheme'].'://'.Yii::$app->params['host'].'/css/main.css',
                Yii::$app->params['scheme'].'://'.Yii::$app->params['subdomain.signup'].'.'.Yii::$app->params['host'].'/css/article.css'
            ],
            'extraPlugins' => 'mathjax,exercise,test,sliders,accordion,vkshare',
            'mathJaxLib' => 'http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML'
        ],
    ]) ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => ['rows' => 100],
        'enableFinder' => true,
        'clientOptions' => [
            'toolbar' => [
                [
                    'name' => 'clipboard',
                    'items' => [
                        'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'
                    ],
                ],
                [
                    'name' => 'links',
                    'items' => [
                        'Link', 'Unlink', 'Anchor'
                    ],
                ],
                [
                    'name' => 'insert',
                    'items' => [
                        'Image', 'Table', 'HorizontalRule', 'SpecialChar'
                    ],
                ],
                [
                    'name' => 'align',
                    'items' => [
                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'
                    ],
                ],
                [
                    'name' => 'document',
                    'items' => [
                        'Maximize', 'ShowBlocks', 'Source'
                    ],
                ],
                '/',
                [
                    'name' => 'basicstyles',
                    'items' => [
                        'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'
                    ],
                ],
                [
                    'name' => 'color',
                    'items' => [
                        'TextColor', 'BGColor'
                    ],
                ],
                [
                    'name' => 'paragraph',
                    'items' => [
                        'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'
                    ],
                ],
                [
                    'name' => 'styles',
                    'items' => [
                        'Styles', 'Format', 'Font', 'FontSize'
                    ],
                ],
                '/',
                [
                    'name' => 'insert',
                    'items' => [
                        'Mathjax', 'Youtube', 'Footnotes', 'Exercise', 'Test', 'Sliders', 'Accordion', 'Vkshare'
                    ],
                ],
            ],
            'height' => 500,
            'allowedContent' => true,
            'contentsCss' => [
                '/css/fa/font-awesome.min.css',
                '/css/bootstrap.min.css',
                Yii::$app->params['scheme'].'://'.Yii::$app->params['host'].'/css/main.css',
                Yii::$app->params['scheme'].'://'.Yii::$app->params['subdomain.signup'].'.'.Yii::$app->params['host'].'/css/article.css'
            ],
            'extraPlugins' => 'mathjax,exercise,test,sliders,accordion,vkshare',
            'mathJaxLib' => 'http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML'
        ],
    ]) ?>

    <?= $form->field($model, 'publish_status')->dropDownList(
        [Post::STATUS_DRAFT => Yii::t('backend', 'Draft'), Post::STATUS_PUBLISH => Yii::t('backend', 'Published')]
    ) ?>

    <?= $form->field($model, 'tags')->checkboxList(
        ArrayHelper::map($tags, 'id', 'title')
    ) ?>

    <?= $form->field($model, 'publish_date')->widget(\kartik\widgets\DatePicker::className(), [
        'type' => DatePicker::TYPE_INPUT,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
