<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $posts yii\data\ActiveDataProvider */
/* @var $categories yii\data\ActiveDataProvider */
/* @var $post common\models\Post */

$this->title = "Новости"
?>

<!-- begin content-->
<div class="crm__container">
    <?php if (isset($_SESSION['__flash']) && count($_SESSION['__flash'])): ?>
        <?= \kartik\alert\AlertBlock::widget([
            'delay' => false
        ]) ?>
    <?php endif; ?>
    <div class="crm__content">
        <div class="tab-menu tab-menu--short tab-menu--single">
            <?php
            foreach ($categories->models as $category) {
                echo $this->render('/category/shortViewCategory', [
                    'model' => $category
                ]);
            }
            ?>
        </div>
        <div class="blog__list">
            <?php
            foreach ($posts->models as $post) {
                echo $this->render('shortView', [
                    'model' => $post
                ]);
            }
            ?>
            <div>
                <?= LinkPager::widget([
                    'pagination' => $posts->getPagination()
                ]) ?>
            </div>
        </div>
    </div>
    <!-- begin aside-->
    <div class="aside">
        <div class="tags">
            <div class="tags__title">Популярное сейчас</div>
            <div class="tags__list">
                <?php
                foreach ($tags as $tag) {
                    echo Html::a($tag->title, ['/blog/tag/view', 'id' => $tag->id], ['class' => 'tags__item']);
                }
                ?>
            </div>
        </div>
        <div class="social__widget_wrap" id="vk_groups"></div>
        <script type="text/javascript" src="//vk.com/js/api/openapi.js?150"></script>
        <script>VK.Widgets.Group("vk_groups", {mode: 5, width: '270'}, 20003922);</script>
    </div>
    <!-- end aside-->
</div>