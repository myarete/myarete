<?php
return [
    'blog' => 'blog/post/index',
    '<module:\w+>/<controller:\w+>/<action:[-\w]+>/<id:\d+>' => '<module>/<controller>/<action>',
    '<module:\w+>/<controller:[-\w]+>/<action:[-\w]+>/<id:\d+>/<new_id:\d+>' => '<module>/<controller>/<action>',
];
