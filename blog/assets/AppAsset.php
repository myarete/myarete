<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace signup\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/css/style.css',
//        '/css/auth.css',
//        'css/plugins/sweetalert/sweetalert2.min.css',
    ];
    public $js = [
        'https://use.fontawesome.com/d2e3dc5d54.js',
//        '/js/scroll.js',
//        '/js/slick.min.js',
//        '/js/function.js',
    ];
//    public $jsOptions = [
//        'position' => View::POS_HEAD,
//    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\jui\JuiAsset'
    ];
}
